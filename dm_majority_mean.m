function guessed = dm_majority_mean(pp, trialidx, rejection)

    if nargin == 2
        rejection =  0.5;
    end
    
    rej_index = false(size(pp, 1), 1);
    
    % Rejection
    rej_up = rejection;
    rej_down = 1-rejection;
    rej_index(pp(:, 1) <= rej_up & pp(:, 1) >= rej_down) = true;
    
    % Trial
    trialId = unique(trialidx);
    ntrials = length(trialId);
    guessed = zeros(size(pp, 1), 1);
    
    for trId = 1:ntrials
        avgpp = mean(pp(trialidx == trialId(trId) & rej_index == false, 1)); 
        
        cclass_guessed = 2;
        if avgpp > 0.5
            cclass_guessed = 1;
        end
        
        guessed(trialidx == trialId(trId)) = cclass_guessed;
            
        
    end
    

end