function [files, numfiles] = cvsa_utilities_getdata(datapath, dirpattern, filepattern, extension)
% [files, numfiles] = cvsa_utilities_getdata(datapath, dirpattern, filepattern, extension)
%
% The function get cvsa datafiles from structured directory. It looks for
% directories in DATAPATH that match DIRPATTERN. Then, for each directory
% founded, it looks for all files with EXTENSION that match FILEPATTERN.
% It return a cell array with all files found and the number of files
% found.
%
% Example:
%
% Data folders is structured as follows:
%
% + data/
% |+ 20120704_b4/
%  |- b4.20120704.124619.offline.va.va_brbl.bdf
%  |- b4.20120704.125236.offline.va.va_brbl.bdf
%  |- b4.20120704.134239.online.va.va_brbl.bdf
%  |- b4.20120704.151751.eyeclosed.bdf
% |+ 20120705_b4/
%  |- eyetracker/
%  |- b4.20120705.121605.online.va.va_brbl.bdf
%  |- b4.20120705.133533.online.vabg.va_brbl.bdf
%  |- b4_va_20120705.xml
%
% datafile = cvsa_utilities_getdata(datapath, 'b4', '.va.', '.bdf');
% datafile = 
%
%     '/mnt/data/Research/cvsa/online//20120704_b4//b4.20120704.124619.offline.va.va_brbl.bdf'
%     '/mnt/data/Research/cvsa/online//20120704_b4//b4.20120704.125236.offline.va.va_brbl.bdf'
%     '/mnt/data/Research/cvsa/online//20120704_b4//b4.20120704.134239.online.va.va_brbl.bdf'
%     '/mnt/data/Research/cvsa/online//20120705_b4//b4.20120705.121605.online.va.va_brbl.bdf'
%
% Four files are found that match the input arguments.
%
% SEE ALSO: util_getdir, util_getfile


    folders = util_getdir(datapath, dirpattern);
    nfolders = length(folders);
    
    files = {};
  
    for dId = 1:nfolders
        cfiles = util_getfile(folders{dId}, extension, filepattern);
        files = cat(1, files, cfiles);
    end
    
    numfiles = length(files);

end