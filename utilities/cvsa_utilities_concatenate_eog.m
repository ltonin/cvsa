function [E, analysis] = cvsa_utilities_concatenate_eog(datafiles, sampleadjust)
% [E, analysis] = cvsa_utilities_concatenate_eog(datafiles)
%
% The function concatenate already preprocessed with cvsa_processing_*
% functions. Input argument is a cell array with paths of the datafiles.
% The output F represents the concatenated data along the first dimension.
% In the analysis structure are stored the label information (Run label, Modality
% label, Day label, Subject label), the concatenated events and the
% processing settings. The function check if the processing settings are
% the same for all files.
%
% SEE ALSO: cvsa_processing_eog

    if nargin == 1
        sampleadjust = 0;
    end

    NumFiles = length(datafiles);
    
    % Getting size info to allocate memory and speedup the concatenation
    datasize   = get_data_size(datafiles);
    datasize(1, :) = datasize(1, :) - sampleadjust;
    
    NumSamples = sum(datasize(1, :));
    NumChans   = unique(datasize(2, :));
    
    E     = zeros(NumSamples, NumChans);
    Rk    = zeros(NumSamples, 1);
    Dk    = zeros(NumSamples, 1);
    Mk    = zeros(NumSamples, 1);
    Sk  = [];
    event.TYP = [];
    event.POS = [];
    event.DUR = [];

    cdaylbl  = '';
    cday     = 0;
    csublbl  = '';
    csub     = 0;
    pproc    = '';
    pspatial = '';
    

    fileseek = 1;
    for fId = 1:NumFiles
        cfilename = datafiles{fId};
        info = util_getfile_info(cfilename);
        util_bdisp(['[io] - Loading file ' num2str(fId) '/' num2str(NumFiles)]);
        disp(['       File: ' cfilename]);

        % Get current position 
        cstart   = fileseek;
        cstop    = cstart + datasize(1, fId) - 1;
        
        % Get modality from filename
        switch lower(info.modality)
            case 'offline'
                cmod = 0;
            case 'online'
                cmod = 1;
            otherwise
                error('chk:mod', ['[' mfilename '] Unknown modality']);
        end

        % Get day from filename
        if strcmpi(info.date, cdaylbl) == false
            cday = cday + 1;
            cdaylbl = info.date;
        end
        
        % Get subject from filename
        if strcmpi(info.subject, csublbl) == false
            csub = csub + 1;
            csublbl = info.subject;
        end

        % Loading processed data
        cdata = load(cfilename);

        % Concatenate events structure
        cevent    = cdata.analysis.event;
        event.TYP = cat(1, event.TYP, cevent.TYP);
        event.DUR = cat(1, event.DUR, cevent.DUR);
        event.POS = cat(1, event.POS, cevent.POS + fileseek - 1);


        % Concatenate data
        E(cstart:cstop, :) = cdata.eog(1:end-sampleadjust, :);
        cnsamples = length(cstart:cstop);

        % Concatenate markers vector
        Rk(cstart:cstop) = fId*ones(cnsamples, 1);
        Mk(cstart:cstop) = cmod*ones(cnsamples, 1);
        Dk(cstart:cstop) = cday*ones(cnsamples, 1);
        Sk(cstart:cstop) = csub*ones(cnsamples, 1);
       
        
        % Update the fileseek position
        fileseek = cstop + 1;
    
    end
    
    analysis.fs         = cdata.analysis.processing.fs;
    analysis.event      = event;
    analysis.label.Rk   = Rk;
    analysis.label.Mk   = Mk;
    analysis.label.Dk   = Dk;
    analysis.label.Sk   = Sk;
end

function dsizes = get_data_size(filepaths)

    nfiles = length(filepaths);
    ndimensions = 2;                            % samples x chans
    
    dsizes = zeros(ndimensions, nfiles);
    
    for fId = 1:nfiles
        cfilepath = filepaths{fId};
        cinfo = whos('-file', cfilepath, 'eog');
        dsizes(:, fId) = cinfo.size;    
    end

end