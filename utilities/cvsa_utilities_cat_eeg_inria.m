function [F, analysis] = cvsa_utilities_cat_eeg_inria(files)

    NumFiles = length(files);
    
    % Getting size info to allocate memory and speedup the concatenation
    datasize   = get_data_size(files);
    NumSamples = sum(datasize(1, :));
    NumFreqs   = unique(datasize(2, :));
    NumChans   = unique(datasize(3, :));

    F     = zeros(NumSamples, NumFreqs, NumChans);
    Rk    = zeros(NumSamples, 1);
    Dk    = zeros(NumSamples, 1);
  
    event.TYP = [];
    event.POS = [];
    event.DUR = [];
    event.ART = [];
    
    cdaylbl  = '';
    cday     = 0;
    pproc    = '';
    pspatial = '';
    

    fileseek = 1;
    for fId = 1:NumFiles
        cfilename = files{fId};
        %info = util_getfile_info(cfilename);
        util_bdisp(['[io] - Loading file ' num2str(fId) '/' num2str(NumFiles)]);
        disp(['       File: ' cfilename]);  
    
        % Get current position 
        cstart   = fileseek;
        cstop    = cstart + datasize(1, fId) - 1;
        
        % Loading processed data
        cdata = load(cfilename);

        % Concatenate events structure
        cevent    = cdata.analysis.event;
        event.TYP = cat(1, event.TYP, cevent.TYP);
        event.DUR = cat(1, event.DUR, cevent.DUR);
        event.POS = cat(1, event.POS, cevent.POS + fileseek - 1);
        event.ART = cat(1, event.ART, cevent.ART + fileseek - 1);


        % Concatenate data
        F(cstart:cstop, :, :) = cdata.psd;
        
        % Concatenate markers vector
        Rk(cstart:cstop) = fId*ones(size(cdata.psd, 1), 1);
        Dk(cstart:cstop) = cday*ones(size(cdata.psd, 1), 1);
       
        
        % Compare processing settings
        if isempty(pproc) == true
            pproc = cdata.analysis.psd;
        else
            if isequal(pproc, cdata.analysis.psd) == false
                error('chk:ana', ['[' mfilename '] Concatenation of processed data with different processing settings']);
            end
        end
        
        % Compare spatial filter settings
        if isempty(pspatial) == true
            pspatial = cdata.analysis.spatial;
        else
            if isequal(pspatial, cdata.analysis.spatial) == false
                error('chk:ana', ['[' mfilename '] Concatenation of processed data with different spatial filter settings']);
            end
        end
        
        % Update the fileseek position
        fileseek = cstop + 1;
    end
    
    analysis.event      = event;
    analysis.label.Rk   = Rk;
    analysis.processing = pproc;
    analysis.spatial    = pspatial;
    analysis.artifact   = cdata.analysis.artifact;
end

function dsizes = get_data_size(filepaths)

    nfiles = length(filepaths);
    ndimensions = 3;                            % samples x freqs x chans
    
    dsizes = zeros(ndimensions, nfiles);
    
    for fId = 1:nfiles
        cfilepath = filepaths{fId};
        cinfo = whos('-file', cfilepath, 'psd');
        dsizes(:, fId) = cinfo.size;    
    end

end