% This script fix the status channels for bdf files recording with subjects
% b4, c2, b2, f1 and g7. There was a problem of event overlapping that has 
% been solved afterward. 
%
% #######################################################################
% ####### Study carefully the bdf files and code before using it ########
% #######################################################################

clc; clearvars;

subject     = 'g7';
pattern     = '.va.';

datapath    = '/mnt/data/Research/cvsa/online/';

wrghit_evts = [24 40];
wrgend_evts = [144 160];
ovlhit_evt  = 8;
ovlend_evt  = 128;
tmpevent  = 1001;
gapevent  = 10;

% Get datafiles
[Files, NumFiles] = cvsa_utilities_getdata(datapath, subject, pattern, '.bdf');

for fId = 5:NumFiles
    cfilename = Files{fId};
    info = util_getfile_info(cfilename);
    util_bdisp(['[io] - Loading file ' num2str(fId) '/' num2str(NumFiles)]);
    disp(['       File: ' cfilename]);
    
    [~, header] = io_read_bdf(cfilename);
    status      = io_read_bdf(cfilename, {'Status'});
    
    trig = bitand(hex2dec('FF'), status);
    
    disp('Before fix:');
    unique(trig)
    % For hit/miss event during the run
    for eId = 1:length(wrghit_evts)
        cevent = wrghit_evts(eId);
        cpos   = find(trig == cevent);
        trig(cpos) = tmpevent;
        
        x = find(diff(trig) == (tmpevent - (cevent - ovlhit_evt)));
        for sId = 1:length(x)
            trig(x(sId)+1:x(sId)+gapevent) = 0;
        end
        
        trig(trig == tmpevent) = ovlhit_evt;
    end
    
    % For hit/miss event at the end of the run
    for eId = 1:length(wrgend_evts)
        cevent = wrgend_evts(eId);
        cpos   = find(trig == cevent);
        if isempty(cpos) == false
            trig(cpos(1):int32(median(cpos)-5)) = cevent-ovlend_evt;
            trig(int32(median(cpos)-5):int32(median(cpos)+5)) = 0;
            trig(int32(median(cpos)+5):int32(cpos(end))) = ovlend_evt;
        end
    end
    
    disp('After fix:');
    unique(trig)
    event = io_event_bdf(trig);
    event
    
    % Re-saving bdf file (only status channel)
    util_bdisp('[io] - Re-saving the file with fixed status channel');
    writebdfdata(header, trig', 0, getbdfchannels(header, {'Status'}));
    
end