diffMOT =

    7.5000    0.8333
   -5.8333   -0.8333
    1.6667   -2.5000
   -0.8333   -3.3333
    0.8333    0.8333
   20.0000    5.8333
   12.5000    0.8333
   22.5000   -0.0000
   -1.6667    8.3333
   11.6667   -4.1667
    7.5000   15.8333
   13.3333   -8.3333
    6.6667   -3.3333
   13.3333    6.6667
   10.0000   -2.5000
   -8.3333   -2.5000

>> IAPSlopeOLD'

ans =

       NaN    0.4855
       NaN    0.2114
   -0.0038   -0.4181
    0.2108   -0.2990
   -0.0309   -0.0871
    0.5519    0.4342
    0.0880    0.1460
    0.5362       NaN
   -0.0409    0.1729
   -0.2304    0.1377
    0.2794    0.5348
    0.0341    0.1012
    0.4847    0.4465
       NaN    0.6614
       NaN    0.3389
    0.1819   -0.0307



c_SMpara =

    0.3968


p_SMpara =

    0.0405


c_SMNpara =

    0.3502


p_SMNpara =

    0.0733
