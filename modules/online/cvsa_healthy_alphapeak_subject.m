clc;
clearvars;

subject     = 'a7';
pattern     = '.va.';

features       = 'psd';
spatialfilter  = 'car';
experiment     = 'online';

Classes    = [1 2];
ClassesLb  = {'LeftCue', 'RightCue'};
NumClasses = length(Classes);

%% Initialization

% Timings
timings.trial.period    = 3;            % seconds
timings.offset.period   = -1;           % seconds

% Channels and RoIs
[~, channels.list]        = proc_get_montage('eeg.biosemi.64');
% channels.roi.left.labels  = {'P7', 'P5', 'PO7'};         % As in [Thut et al., 2006]
% channels.roi.right.labels = {'P8', 'P6', 'PO8'};         % As in [Thut et al., 2006]
channels.roi.left.labels  = {'P5', 'P3', 'P1', 'PO7', 'PO3', 'O1'};         % Custom
channels.roi.right.labels = {'P6', 'P4', 'P2', 'PO8', 'PO4', 'O2'};         % Custom
channels.roi.left.id      = proc_get_channel(channels.roi.left.labels,  channels.list);
channels.roi.right.id     = proc_get_channel(channels.roi.right.labels, channels.list);
channels.roi.labels       = {'LeftRoI', 'RightRoI'};

% Datafiles

baseroot      = ['/mnt/data/Git/Codes/cvsa/analysis/' experiment '/'];
datapath      = [baseroot '/' features '/' spatialfilter '/'];
[Files, NumFiles] = util_getfile(datapath, '.mat', [subject '*' pattern]);
[~, saveroot] = util_mkdir(baseroot, 'alphapeak/');


%% Analysis - Import data

% Concatanate datafiles
util_bdisp(['[io] - Concatenating ' num2str(NumFiles) ' datafiles']);
[F, analysis] = cvsa_utilities_concatenate_data(Files);

% Extract information
NumTotSamples = size(F, 1);
NumFreqs      = size(F, 2);
NumChans      = size(F, 3);
SampleRate    = analysis.processing.fs;
FrameSize     = analysis.processing.fsize;
FreqGrid      = analysis.processing.f;

%% Analysis - Masking data

util_bdisp('[proc] - Creating trial mask vector');
events = analysis.event;
timings.trial.size  = floor(timings.trial.period*SampleRate/FrameSize);
timings.offset.size = floor(timings.offset.period*SampleRate/FrameSize);
timings.trial.mask  = false(NumTotSamples, 1);
timings.trial.pos   = [];
for cId = 1:NumClasses
    [cmask, cpos, cdur] = proc_get_event(events, Classes(cId), NumTotSamples, timings.trial.size, timings.offset.size);
    timings.trial.pos   = sort([timings.trial.pos cpos']);
end

util_bdisp('[proc] - Creating trial based class labels');
index = false(length(events.TYP), 1);
for cId = 1:NumClasses
    index = events.TYP == Classes(cId) | index;
end
Sk = events.TYP(index);

%% Extracting trials
util_bdisp('[proc] - Extracting trials');
NumTrials  = length(timings.trial.pos);
NumSamples = timings.trial.size + abs(timings.offset.size);
S = zeros(NumSamples, NumFreqs, NumChans, NumTrials);
Mk = zeros(NumTrials, 1);
Rk = zeros(NumTrials, 1);
Dk = zeros(NumTrials, 1);
for trId = 1:NumTrials
    cstart = timings.trial.pos(trId);
    cstop  = cstart + NumSamples - 1;
    S(:, :, :, trId) = F(cstart:cstop, :, :);
    Mk(trId) = unique(analysis.label.Mk(cstart:cstop));
    Rk(trId) = unique(analysis.label.Rk(cstart:cstop));
    Dk(trId) = unique(analysis.label.Dk(cstart:cstop));
end
Modalities    = unique(Mk);
NumModalities = length(Modalities);
Runs          = unique(Rk);
NumRuns       = length(Runs);
Days          = unique(Dk);
NumDays       = length(Days);

%% Analysis - Artifact index
ArtifactFree = true;


%% Extracting spectrum
util_bdisp('[proc] - Extracting spectrum');
ToI = 1:abs(timings.offset.size);           % Time of interest 
SP  = zeros(NumFreqs, 2, NumTrials);
SPn = zeros(NumFreqs, 2, NumTrials);
for trId = 1:NumTrials
    cvalues_l = squeeze(mean(mean(S(ToI, :, channels.roi.left.id, trId), 1), 3));
    cvalues_r = squeeze(mean(mean(S(ToI, :, channels.roi.right.id, trId), 1), 3));
    
    SP(:, 1, trId) = cvalues_l;
    SP(:, 2, trId) = cvalues_r;

    SPn(:, 1, trId) = cvalues_l./max(cvalues_l);
    SPn(:, 2, trId) = cvalues_r./max(cvalues_r);
end

%% Find the alpha peak based on healthy RoI
util_bdisp('[proc] - Computing alpha peak');
FreqPeakRange = 8:12;


[pks.value.left.calibration, pks.locs.left.calibration] = findpeaks(mean(SPn(:, 1, ArtifactFree == true & Mk == 0), 3), 'NPeaks',1);
[pks.value.left.online, pks.locs.left.online] = findpeaks(mean(SPn(:, 1, ArtifactFree == true & Mk == 1), 3), 'NPeaks',1);

[pks.value.right.calibration, pks.locs.right.calibration] = findpeaks(mean(SPn(:, 2, ArtifactFree == true & Mk == 0), 3), 'NPeaks', 1);
[pks.value.right.online, pks.locs.right.online] = findpeaks(mean(SPn(:, 2, ArtifactFree == true & Mk == 1), 3), 'NPeaks', 1);

% If alpha peak cannot be found in affected roi then use the peak location
% in healthy roi (calibration for calibration, online for online)
if(isempty(pks.locs.right.calibration) || isempty(intersect(FreqPeakRange, FreqGrid(pks.locs.right.calibration))))
    pks.locs.right.calibration  = pks.locs.left.calibration;
    pks.value.right.calibration = pks.value.left.calibration;
end

if(isempty(pks.locs.right.online) || isempty(intersect(FreqPeakRange, FreqGrid(pks.locs.right.online))))
    pks.locs.right.online  = pks.locs.left.online;
    pks.value.right.online = pks.value.left.online;
end

pks.band.left.calibration  =  pks.locs.left.calibration-1:pks.locs.left.calibration+1;
pks.band.left.online       =  pks.locs.left.online-1:pks.locs.left.online+1;
pks.band.right.calibration =  pks.locs.right.calibration-1:pks.locs.right.calibration+1;
pks.band.right.online      =  pks.locs.right.online-1:pks.locs.right.online+1;


%% Compute the alpha power around the peak for each condition
AlphaPower = [];
AlphaPower(:, 1) = [squeeze(mean(SPn(pks.band.left.calibration, 1, ArtifactFree == true & Mk == 0), 1)); ...               %% Alpha power for calibration trials
                    squeeze(mean(SPn(pks.band.left.online,      1, ArtifactFree == true & Mk == 1), 1)); ...               %% Alpha power for online trials
                    squeeze(mean(SPn(pks.band.left.online,      1, ArtifactFree == true & Rk > (Runs(end) - 4)), 1))];     %% Alpha power for last four runs

AlphaPower(:, 2) = [squeeze(mean(SPn(pks.band.right.calibration, 2, ArtifactFree == true & Mk == 0), 1)); ...              %% Alpha power for calibration trials
                    squeeze(mean(SPn(pks.band.right.online,      2, ArtifactFree == true & Mk == 1), 1)); ...              %% Alpha power for online trials
                    squeeze(mean(SPn(pks.band.right.online,      2, ArtifactFree == true & Rk > (Runs(end) - 4)), 1))];    %% Alpha power for last four runs

Groups = [  ones(sum(ArtifactFree == true & Mk == 0), 1); ...
            2*ones(sum(ArtifactFree == true & Mk == 1), 1);...
            3*ones(sum(ArtifactFree == true & Rk > (Runs(end) - 4)), 1)];

        
%% Statitistics
% Between calibration and online trials
p.left.calibonline = anova1(AlphaPower(Groups < 3, 1), Groups(Groups < 3), 'off');

% Between calibration and last day trials
p.left.caliblastday = anova1(AlphaPower(Groups ~= 2, 1), Groups(Groups ~= 2), 'off');

% Between calibration and online trials
p.right.calibonline = anova1(AlphaPower(Groups < 3, 2), Groups(Groups < 3), 'off');

% Between calibration and last day trials
p.right.caliblastday = anova1(AlphaPower(Groups ~= 2, 2), Groups(Groups ~= 2), 'off');


fprintf('[stat] - Anova between calibration and online trials [left]:      p=%1.5f\n', p.left.calibonline);
fprintf('[stat] - Anova between calibration and last days trials [left]:   p=%1.5f\n', p.left.caliblastday);
fprintf('[stat] - Anova between calibration and online trials [right]:     p=%1.5f\n', p.right.calibonline);
fprintf('[stat] - Anova between calibration and last days trials [right]:  p=%1.5f\n', p.right.caliblastday);       
%% Saving analysis        

targetfile = [saveroot '/' subject '_alphapeak.mat'];
util_bdisp(['[out] - Saving analysis in: ' targetfile]);

alphapeak.spectrum.raw   = SP;
alphapeak.spectrum.norm  = SPn;
alphapeak.peaks          = pks;
alphapeak.peaks.range    = FreqPeakRange;
alphapeak.channels       = channels;
alphapeak.timings        = timings;
alphapeak.labels.Mk      = Mk;
alphapeak.labels.Rk      = Rk;
alphapeak.labels.Dk      = Dk;
alphapeak.labels.Ck      = Sk;

save(targetfile, 'alphapeak');       

%% Plotting
Condition1 = Mk == 0; % & Dk == Days(1);
Condition2 = Mk == 1; % & Dk == Days(end);

SP2Plot = SPn;
fig = figure;
fig_set_position(fig, 'All');

% Top-Left subplot
subplot(2, 2, 1);
hold on;
ax1 = plot(FreqGrid, mean(SP2Plot(:, 1, ArtifactFree == true & Condition1), 3), 'LineWidth', 2);
ax2 = plot(FreqGrid, mean(SP2Plot(:, 1, ArtifactFree == true & Condition2), 3), ':', 'LineWidth', 2);
plot(FreqGrid(pks.locs.left.calibration), pks.value.left.calibration+0.01, 'v', 'MarkerEdgeColor', get(ax1, 'Color'), 'MarkerFaceColor', get(ax1, 'Color'));
plot(FreqGrid(pks.locs.left.online), pks.value.left.online+0.01, 'v', 'MarkerEdgeColor', get(ax2, 'Color'), 'MarkerFaceColor', get(ax2, 'Color'));
hold off;
grid on;
xlim([FreqGrid(1) FreqGrid(end)]);
ylim([0 1]);
xlabel('[Hz]');
ylabel('NormPsd');
title('Spectrum - left RoI');
legend('Calibration', 'Online');

% Top-Right subplot
subplot(2, 2, 2);
hold on;
ax1 = plot(FreqGrid, mean(SP2Plot(:, 2, ArtifactFree == true & Condition1), 3), 'LineWidth', 2);
ax2 = plot(FreqGrid, mean(SP2Plot(:, 2, ArtifactFree == true & Condition2), 3), ':', 'LineWidth', 2);
plot(FreqGrid(pks.locs.right.calibration), pks.value.right.calibration+0.01, 'v', 'MarkerEdgeColor', get(ax1, 'Color'), 'MarkerFaceColor', get(ax1, 'Color'));
plot(FreqGrid(pks.locs.right.online), pks.value.right.online+0.01, 'v', 'MarkerEdgeColor', get(ax2, 'Color'), 'MarkerFaceColor', get(ax2, 'Color'));
hold off;
grid on;
xlim([FreqGrid(1) FreqGrid(end)]);
ylim([0 1]);
xlabel('[Hz]');
ylabel('NormPsd');
title('Spectrum - right RoI');
legend('Calibration', 'Online');

% Bottom-Rigth subplots
subplot(2, 2, 3);
boxplot(AlphaPower(:, 1), Groups, 'factorseparator', 1);
ylim([0 1]);
title('Left RoI');
set(gca, 'XTickLabel', {'Calibration', 'Online', 'LastDay'});
grid on;
ylabel('NormPsd');

subplot(2, 2, 4);
boxplot(AlphaPower(:, 2), Groups, 'factorseparator', 1);
ylim([0 1]);
title('Right RoI');
set(gca, 'XTickLabel', {'Calibration', 'Online', 'LastDay'});
grid on;
ylabel('NormPsd');

suptitle(['Subject ' subject]);

    
 
