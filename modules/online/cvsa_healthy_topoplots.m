clearvars; clc;

subject     = 'b4';
pattern     = '.va.';

features      = 'psd';
spatialfilter = 'laplacian';

% Configuration
evtfix.value         = 8;
evtfix.period        = 2; % seconds
trial.period         = 3; % seconds
proc.selectedfreqs   = 8:14; % Hz
class.values         = [1 2];
class.names          = {'Left', 'Right'};
plot.interval.number = 6;

load('../va/chanlocs64.mat');

% Get datafiles
datapath    = ['/mnt/data/Git/Codes/cvsa/analysis/online/' features '/' spatialfilter '/'];
[Files, NumFiles] = util_getfile(datapath, '.mat', [subject '*' pattern]);

% Concatenate datafile
util_bdisp(['[io] - Concatenating ' num2str(NumFiles) ' datafiles']);
[F, analysis] = cvsa_utilities_concatenate_data(Files);

nsamples = size(F, 1);
nfreqs   = size(F, 2);
nchans   = size(F, 3);

proc.samplerate = analysis.processing.fs;
proc.framesize  = analysis.processing.fsize;
proc.freqs      = analysis.processing.f;

% Create trial mask vector
util_bdisp('[proc] - Creating trial mask vector');
evtfix.offset   = floor(evtfix.period*proc.samplerate/proc.framesize);
trial.duration  = floor(trial.period*proc.samplerate/proc.framesize);

events = analysis.event;
[trial.mask, trial.position] = proc_event_get(events, evtfix.value, nsamples, trial.duration, evtfix.offset);

% Creating trial based class labels
util_bdisp('[proc] - Creating trial based class labels');
nclasses = length(class.values);
index = false(length(events.TYP), 1);
for cId = 1:nclasses
    index = events.TYP == class.values(cId) | index;
end
class.labels = events.TYP(index);

% Computing combination indexes between classes
util_bdisp('[proc] - Computing combination indexes between classes');
class.combination  = nchoosek(class.values, 2);
class.ncombination = size(class.combination, 1);

% Extracting trials
util_bdisp('[proc] - Extracting trials');
ntrials = length(trial.position);
P = zeros(trial.duration, nfreqs, nchans, ntrials);
for trId = 1:ntrials
    cstart = trial.position(trId);
    cstop  = cstart + trial.duration - 1;
    P(:, :, :, trId) = F(cstart:cstop, :, :);
end

% Averaging across selected frequency
util_bdisp('[proc] - Extracting selected frequencies and average');
[~, freqId] = intersect(proc.freqs, proc.selectedfreqs);
S = squeeze(mean(P(:, freqId, :, :), 2));

% Difference between classes
util_bdisp('[proc] - Computing differences between class averages');
dS = cell(class.ncombination, 1);
dCombNames = cell(class.ncombination, 1);
for cmbId = 1:class.ncombination
    cclassA = class.combination(cmbId, 1);
    cclassB = class.combination(cmbId, 2);
    dS{cmbId} = nanmean(S(:, :, class.labels == cclassA), 3) - nanmean(S(:, :, class.labels == cclassB), 3);
    dCombNames{cmbId} = [class.names{cclassA} ' - ' class.names{cclassB}];
end

% Computing average intervals for plotting
plot.interval.size  = floor(trial.duration/plot.interval.number);
plot.interval.start = 1:plot.interval.size:trial.duration;
plot.interval.stop  = plot.interval.start + plot.interval.size - 1;
plot.interval.stop(plot.interval.stop > trial.duration) = trial.duration;
for iId = 1:plot.interval.number
        cstart = plot.interval.start(iId);
        cstop  = plot.interval.stop(iId);
        plot.interval.names{iId} = [num2str(cstart*proc.framesize/proc.samplerate, 2) '-' num2str(cstop*proc.framesize/proc.samplerate, 2) ' s'];
end


dI = cell(class.ncombination, 1);
for cmbId = 1:class.ncombination
    cdata = zeros(plot.interval.number, nchans);
    for iId = 1:plot.interval.number
        cstart = plot.interval.start(iId);
        cstop  = plot.interval.stop(iId);
        cdata(iId, :) = squeeze(nanmean(dS{cmbId}(cstart:cstop, :), 1));
    end
    dI{cmbId} = cdata;
end
    
% Topoplots for each class combination
fig = fig_set_position(gcf, 'Top');
util_bdisp('[proc] - Plotting topograpic map for each class combination difference');
for cmbId = 1:class.ncombination
    plot_subtopoplot(dI{cmbId}, chanlocs, 'title', [subject ' - ' dCombNames{cmbId}], 'maplimits', [-0.1 0.1], 'xlabels', plot.interval.names, 'headrad', 'rim');
end


