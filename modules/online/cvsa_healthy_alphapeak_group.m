clearvars; clc;

sublist        = {'b4', 'c2', 'b2', 'f1', 'g7', 'h6', 'e7', 'a7'};
nsubjects      = length(sublist);
features       = 'psd';
spatialfilter  = 'car';
selectedband   = 'alpha';

baseroot     = '/mnt/data/Git/Codes/cvsa/';
[~, analysisroot] = util_mkdir(baseroot, 'analysis/');
[~, saveroot]     = util_mkdir(analysisroot, 'online/alphapeak/');

FreqGrid = 4:48;

SP = [];
SPn = [];
peaks = cell(nsubjects, 1);
Mk = [];
Rk = [];
Dk = [];
Ck = [];
Sk = [];
for sId = 1:nsubjects
    csubject = sublist{sId};
    util_bdisp(['[in] - Loading alpha peak analysis for subject ' csubject]);
    cfilename = [saveroot '/' csubject '_alphapeak.mat'];
    cdata = load(cfilename);
    
    SP = cat(3, SP, cdata.alphapeak.spectrum.raw);
    SPn = cat(3, SPn, cdata.alphapeak.spectrum.norm);
    Mk = cat(1, Mk, cdata.alphapeak.labels.Mk);
    Rk = cat(1, Rk, cdata.alphapeak.labels.Rk);
    Dk = cat(1, Dk, cdata.alphapeak.labels.Dk);
    Ck = cat(1, Ck, cdata.alphapeak.labels.Ck);
    Sk = cat(1, Sk, sId*ones(length(cdata.alphapeak.labels.Ck), 1));
    channels = cdata.alphapeak.channels;
    peaks{sId} = cdata.alphapeak.peaks;
    
end

Runs    = unique(Rk);
NumRuns = length(Runs);

%% Plotting
Condition1 = Mk == 0;
Condition2 = Mk == 1; 

SP2Plot = SPn;
fig = figure;
fig_set_position(fig, 'Top');

% Top-Left subplot
subplot(1, 2, 1);
hold on;
ax1 = plot(FreqGrid, mean(SP2Plot(:, 1, Condition1), 3), 'LineWidth', 2);
ax2 = plot(FreqGrid, mean(SP2Plot(:, 1, Condition2), 3), ':', 'LineWidth', 2);
% plot(FreqGrid(pks.locs.left.calibration), pks.value.left.calibration+0.01, 'v', 'MarkerEdgeColor', get(ax1, 'Color'), 'MarkerFaceColor', get(ax1, 'Color'));
% plot(FreqGrid(pks.locs.left.online), pks.value.left.online+0.01, 'v', 'MarkerEdgeColor', get(ax2, 'Color'), 'MarkerFaceColor', get(ax2, 'Color'));
hold off;
grid on;
xlim([FreqGrid(1) FreqGrid(end)]);
ylim([0 1]);
xlabel('[Hz]');
ylabel('NormPsd');
title('Spectrum - left RoI');
legend('Calibration', 'Online');

% Top-Right subplot
subplot(1, 2, 2);
hold on;
ax1 = plot(FreqGrid, mean(SP2Plot(:, 2, Condition1), 3), 'LineWidth', 2);
ax2 = plot(FreqGrid, mean(SP2Plot(:, 2, Condition2), 3), ':', 'LineWidth', 2);
% plot(FreqGrid(pks.locs.right.calibration), pks.value.right.calibration+0.01, 'v', 'MarkerEdgeColor', get(ax1, 'Color'), 'MarkerFaceColor', get(ax1, 'Color'));
% plot(FreqGrid(pks.locs.right.online), pks.value.right.online+0.01, 'v', 'MarkerEdgeColor', get(ax2, 'Color'), 'MarkerFaceColor', get(ax2, 'Color'));
hold off;
grid on;
xlim([FreqGrid(1) FreqGrid(end)]);
ylim([0 1]);
xlabel('[Hz]');
ylabel('NormPsd');
title('Spectrum - right RoI');
legend('Calibration', 'Online');


suptitle('Healthy subjects');

    
 
