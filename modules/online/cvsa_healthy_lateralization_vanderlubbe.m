clearvars; clc;

subject     = 'b4';
pattern     = '.va.';

features      = 'psd';
spatialfilter = 'none';
experiment    = 'online';

% Configuration
fix.value                   = 8;
fix.period                  = 3;       % seconds
trial.period                = 4;       % seconds
offset.period               = 0.5;       % seconds
proc.freq.selected.range{1} = 4:6;     % Hz
proc.freq.selected.label{1} = 'theta';
proc.freq.selected.range{2} = 8:14;    % Hz
proc.freq.selected.label{2} = 'alpha';
proc.freq.selected.range{3} = 16:30;   % Hz
proc.freq.selected.label{3} = 'beta';
proc.freq.selected.range{4} = 32:48;   % Hz
proc.freq.selected.label{4} = 'gamma';
class.values                = [1 2 3];
class.names                 = {'Left', 'Right', 'Middle'};
class.nclasses              = length(class.values);

[~, chanlist] = proc_get_montage('eeg.biosemi.64');

latpairs = { 'FP1', 'FP2';  'AF7', 'AF8';   'AF3', 'AF4';   'F7',  'F8'; ...
              'F5',  'F6';   'F3',  'F4';    'F1',  'F2';  'FT7', 'FT8'; ...
             'FC5', 'FC6';  'FC3', 'FC4';   'FC1', 'FC2';   'T7',  'T8'; ...
              'C5',  'C6';   'C3',  'C4';    'C1',  'C2';  'TP7', 'TP8'; ...
             'CP5', 'CP6';  'CP3', 'CP4';   'CP1', 'CP2';   'P9', 'P10'; ...
              'P7',  'P8';   'P5',  'P6';    'P3',  'P4';   'P1',  'P2'; ...
             'PO7', 'PO8';  'PO3', 'PO4';    'O1',  'O2'};

groupnames  = {'FP', 'AF', 'F', 'FT', 'FC', 'T', 'C', 'TP', 'CP', 'P', 'PO', 'O'};

% Get datafiles
datapath    = ['/mnt/data/Git/Codes/cvsa/analysis/' experiment '/' features '/' spatialfilter '/'];
[Files, NumFiles] = util_getfile(datapath, '.mat', [subject '*' pattern]);

% Concatenate datafile
util_bdisp(['[io] - Concatenating ' num2str(NumFiles) ' datafiles']);
[F, analysis] = cvsa_utilities_concatenate_data(Files);

NumSamples = size(F, 1);
NumFreqs   = size(F, 2);
NumChans   = size(F, 3);
NumPairs   = length(latpairs);

proc.samplerate = analysis.processing.fs;
proc.framesize  = analysis.processing.fsize;
proc.freq.grid  = analysis.processing.f;
for fId = 1:length(proc.freq.selected.range)
    [~, proc.freq.selected.id{fId}] = intersect(proc.freq.grid, proc.freq.selected.range{fId});
end

% Create trial mask vector
util_bdisp('[proc] - Creating trial mask vector');
fix.duration    = floor(fix.period*proc.samplerate/proc.framesize);
trial.duration  = floor(trial.period*proc.samplerate/proc.framesize);
offset.duration = floor(offset.period*proc.samplerate/proc.framesize);

events = analysis.event;
[fix.mask, fix.pos] = proc_get_event(events, fix.value, NumSamples, fix.duration);

trial.mask = false(NumSamples, 1);
trial.pos  = [];
for cId = 1:class.nclasses
    [cmask, cpos] = proc_get_event(events, class.values(cId), NumSamples, trial.duration);
    trial.mask    = trial.mask | cmask;
    trial.pos     = sort([trial.pos cpos']);
end

% Creating trial based class labels
util_bdisp('[proc] - Creating trial based class labels');

index = false(length(events.TYP), 1);
for cId = 1:class.nclasses
    index = events.TYP == class.values(cId) | index;
end
class.labels = events.TYP(index);

% Compute lateralization index for each pair ([Thut et al., 2006])
util_bdisp('[proc] - Computing lateralization index for simmetric electrode pairs');
S = F;
LPS  = zeros(NumSamples, NumFreqs, NumPairs);
Lg = zeros(NumPairs, 1);
Ld = zeros(NumPairs, 1);
Ln = cell(NumPairs, 1);
for pId = 1:NumPairs
    cpair  = latpairs(pId, :);
    cindex = proc_get_channel(cpair, chanlist);
    LPS(:, :, pId) = (S(:, :, cindex(1)) - S(:, :, cindex(2)))./(S(:, :, cindex(1)) + S(:, :, cindex(2)));
    
    Lg(pId) = util_cellfind({cpair{1}(regexp(cpair{1}, '\D'))}, groupnames);
    Ld(pId) = ceil(str2double(cpair{1}(regexp(cpair{1}, '\d')))/2);
    Ln{pId} = [cpair{1}(regexp(cpair{1}, '\D')) cpair{1}(regexp(cpair{1}, '\d')) '-' cpair{2}(regexp(cpair{2}, '\d'))];
end

% Extracting trials
util_bdisp('[proc] - Extracting trials');
NumTrials = length(trial.pos);
NumTrialSamples = trial.duration - offset.duration;
LPSt = zeros(NumTrialSamples, NumFreqs, NumPairs, NumTrials);
Mk_t = zeros(NumTrials, 1);
Rk_t = zeros(NumTrials, 1);
for trId = 1:NumTrials
    cstart = trial.pos(trId) + offset.duration;
    cstop  = trial.pos(trId) + trial.duration - 1;
    LPSt(:, :, :, trId) = LPS(cstart:cstop, :, :);
    Mk_t(trId) = analysis.label.Mk(cstart);
    Rk_t(trId) = analysis.label.Rk(cstart);
end


% Computing ERL-LPS (average along classes [Van der Lubbe et al., 2013])
util_bdisp('[proc] - Computing lateralization power spectra average across');
ERL_LPS = zeros(NumTrialSamples, NumFreqs, NumPairs);
for cId = 1:class.nclasses
    ERL_LPS = ERL_LPS + mean(LPSt(:, :, :, class.labels == class.values(cId)), 4);
end
ERL_LPS = ERL_LPS./length(class.values);

% Computing average per class across modality (calibration and online)
Modalities    = unique(analysis.label.Mk);
NumModalities = length(Modalities);
LPSm = zeros(NumFreqs, NumPairs, NumModalities, class.nclasses);
for mId = 1:length(Modalities)
    for cId = 1:class.nclasses
        LPSm(:, :, mId, cId) = squeeze(mean(mean(LPSt(:, :, :, Mk_t == Modalities(mId) & class.labels == class.values(cId)), 4), 1));
    end
end

% Computing average per class across runs
Runs    = unique(Rk_t);
NumRuns = length(Runs);
Mk_r    = zeros(NumRuns,1);
LPSr = zeros(NumFreqs, NumPairs, NumRuns, class.nclasses);
for rId = 1:NumRuns
    for cId = 1:class.nclasses
        LPSr(:, :, rId, cId) = squeeze(mean(mean(LPSt(:, :, :, Rk_t == Runs(rId) & class.labels == class.values(cId)), 4), 1));%), 4), 1)); %
    end
    Mk_r(rId) = unique(Mk_t(Rk_t == rId));
end



%% Plotting modality comparison on alpha
fig2 = figure;
fig_set_position(fig2, 'All');
groupfreqId = util_cellfind({'alpha'}, proc.freq.selected.label);
for cId = 1:class.nclasses
    subplot(class.nclasses, 1, cId);
    bar(squeeze(mean(LPSm(proc.freq.selected.id{groupfreqId}, :, :, class.values(cId)), 1)));
    grid on;
    set(gca, 'XTick', 1:length(Ln)); set(gca, 'XTickLabel', Ln);
    ylim([-1 1]);
    legend('Calibration', 'Online');
    title(['Subject ' subject ' - Class ' class.names{cId} ' - Alpha band']);
    xlabel('Channel pairs');
    ylabel('LPS');
end

%% Plotting run evolution
% GroupPairs{1} = {'O1-2'};
% GroupPairs{2} = {'PO7-8', 'PO3-4'};
% GroupPairs{3} = {'P7-8',  'P5-6', 'P3-4',  'P1-2'};
% %GroupPairs{3} = {'C5-6',  'C3-4',  'C1-2', 'CP5-6', 'CP3-4', 'CP1-2'};
% GroupPairs{4} = {'F7-8',  'F5-6',  'F3-4', 'F1-2',  'FC5-6', 'FC3-4', 'FC1-2'};

GroupPairs{1} = {'O1-2'};
GroupPairs{2} = {'PO3-4'};
GroupPairs{3} = {'PO7-8'};
GroupPairs{4} = {'P3-4'};


% GroupPairNames{1} = 'Occipital';
% GroupPairNames{2} = 'Parietal-Occipital';
% GroupPairNames{3} = 'Parietal';
% GroupPairNames{4} = 'Frontal';
GroupPairNames{1} = 'O1-2';
GroupPairNames{2} = 'PO3-4';
GroupPairNames{3} = 'PO7-8';
GroupPairNames{4} = 'P3-4';
GroupPairStyles   = {'bo-', 'ro-', 'go-', 'yo-'};

fig3 = figure;
fig_set_position(fig3, 'All');
for fId = 1:length(proc.freq.selected.range)
    subplot(2, 2, fId);
    cfreqId = proc.freq.selected.id{fId};
    hold on; 
    for gId = 1:length(GroupPairs)
        %plot(squeeze(mean(mean(mean(LPSr(cfreqId, util_cellfind(GroupPairs{gId}, Ln), :, 1), 4), 2), 1)), GroupPairStyles{gId});   
        plot(squeeze(mean(mean(LPSr(cfreqId, util_cellfind(GroupPairs{gId}, Ln), :, 1), 2), 1)), GroupPairStyles{gId});   
    end
    hold off;
    ylim([-1 1]);
    xlim([1 NumRuns]);
    plot_vline(find(diff(Mk_r)) + 0.5, 'k'); 
    plot_hline(0, 'k--'); 
    grid on; 
    title([proc.freq.selected.label{fId}]);
    legend(GroupPairNames, 'location', 'south', 'orientation', 'horizontal');
    xlabel('Runs');
    ylabel('LPS');
end
suptitle([subject ' - Lateralization index']);
