function [index, percentage] = cvsa_sport_artifacts_detection(s, samplerate, order, band, NSTD, MAXVALUE)

    % Filtering
    sf = filt_bp(s, order, band, samplerate);
    
    % Computing standard deviation for each channel
    stdch = std(sf, [], 1);
    
    % Thresholding and computing index
    index = sum((abs(sf) > NSTD*max(stdch)) & abs(sf) > MAXVALUE, 2) > 0;

    % Computing percentage
    percentage = 100*sum(index)./length(index);


end