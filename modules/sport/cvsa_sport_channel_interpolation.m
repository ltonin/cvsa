function si = cvsa_sport_channel_interpolation(s, chId, neightbours)
    si = s;
    if isempty(neightbours) == false
        si(:, chId) = mean(s(:, neightbours), 2);
    end
end
