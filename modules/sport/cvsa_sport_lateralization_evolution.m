clc; clearvars; 


sublist = {'S1c', 'S2c', 'S3c', 'S4c', 'S5c', 'S6c', 'S7c', 'S8c', 'S9c', 'S10', 'S11', 'S12', 'S14', 'S15', 'S16', 'S17'};
nsubjects = length(sublist);

features       = 'psd';
spatialfilter  = 'laplacian';
experiment     = 'sport';
%datapath       = ['/mnt/data/Git/Codes/cvsa/analysis/' experiment '/' features '/' spatialfilter '/'];
datapath       = ['D:/sauvegardePCPostdocRennes/post-doc_Camille/git/Codes/cvsa/analysis/' experiment '/' features '/' spatialfilter '/'];

tROIPeriod = 0.5;       % [seconds]
WinPeriod  = 0.0625;    % [s] from PSD processing

Dr = [];
LI = [];
Lk = [];
Xk = [];
Ck = [];
Rm = [];
Sk = [];
Rk = [];
Tk = [];
for sId = 1:nsubjects
    subject = sublist{sId};

    filename = [datapath subject '_li_alphapeak_' spatialfilter '.mat'];
    util_bdisp(['[io] - Loading LI for subject ' subject ': ' filename]);
    cdata = load(filename);
    
    % Trial based labels
    Lk = cat(1, Lk, cdata.li.labels.trials.Lk);
    Xk = cat(1, Xk, cdata.li.labels.trials.Xk);
    Ck = cat(1, Ck, cdata.li.labels.trials.Ck);
    Rk = cat(1, Rk, cdata.li.labels.trials.Rk);
    Rm = cat(1, Rm, cdata.li.labels.trials.Rm);
    Sk = cat(1, Sk, sId*ones(length(cdata.li.labels.trials.Lk), 1));
    
    if(isequal(cdata.li.labels.trials.Rm, cdata.li.alphapeak.labels.Rm) == false)
        disp([subject ' - different Rm']);
    end
    
    % Sample based trial label (including all trial length)
    Tk = cat(1, Tk, cdata.li.labels.overall.Tk > 0);
    
    % Sample based LI index
    LI = cat(1, LI, cdata.li.overall);
end

%% For each trial, computing the position of the last period
ntrials = length(Ck);
tROI.pos  = (find(diff(Tk) == -1) - 1) - (tROIPeriod/WinPeriod) + 1;
tROI.dur = (tROIPeriod/WinPeriod)*ones(ntrials, 1);

%% Computing classes
Classes = unique(Ck);
nclasses = length(Classes);

SideK = Ck;
SideK(SideK == 769 | SideK == 774) = 1;
SideK(SideK == 770 | SideK == 780) = 2;
Sides = unique(SideK);
nsides = length(Sides);

%% Computing Duration Labels
Durations  = unique(Lk);
ndurations = length(Durations);

%% Average LI in the last trial period
mLI = zeros(ntrials, 1);
for trId = 1:ntrials
    cstart = tROI.pos(trId);
    cstop  = cstart + tROI.dur(trId) - 1;
    
    % Compute the average value at the end of the trial
    mLI(trId) = mean(LI(cstart:cstop)); 
end

%% Saving LI
liend.values = mLI;
liend.labels.Lk = Lk;
liend.labels.Sk = Sk;
liend.labels.Rm = Rm;
liend.labels.Ck = Ck;
liend.labels.Rk = Rk;

targetfile = [datapath '/all_liend_alphapeak_' spatialfilter '.mat'];
util_bdisp(['[out] - Saving analysis in: ' targetfile]);
save(targetfile, 'liend');

% %% Computing the correlation of the evolution of LI for each class (and subject)
corrEvoV = zeros(nclasses, nsubjects, ndurations);
corrEvoP = zeros(nclasses, nsubjects, ndurations);
for lId = 1:ndurations
    for sId = 1:nsubjects
        for cId = 1:nclasses
            %cindex  = Lk == Durations(lId) & Sk == sId & Rm == false & Ck == Classes(cId);
            cindex  = Rm == false;

            if(sum(cindex) == 0)
                continue;
            end
            
            cy = abs(mLI(cindex));
            cx = (1:length(cy))';

            [cc, cp] = corr(cx, cy, 'rows', 'pairwise');

            corrEvoV(cId, sId, lId) = cc;
            corrEvoP(cId, sId, lId) = cp;

        end
    end
end

%% Computing the correlation of the evolution of LI for each side (and subject)
% corrEvoV = zeros(nsides, nsubjects, ndurations);
% corrEvoP = zeros(nsides, nsubjects, ndurations);
% 
% for lId = 1:ndurations
%     for sId = 1:nsubjects
%         for cId = 1:nsides
%             cindex  = Lk == Durations(lId) & Sk == sId & Rm == false & SideK == Sides(cId);
% 
% 
%             cy = mLI(cindex);
%             cx = (1:length(cy))';
% 
%             [cc, cp] = corr(cx, cy, 'rows', 'pairwise');
% 
%             corrEvoV(cId, sId, lId) = cc;
%             corrEvoP(cId, sId, lId) = cp;
% 
%         end
%     end
% end



