% clc; clearvars; 
% 
% subject     = 'S9c';

features       = 'psd';
spatialfilter  = 'car';
experiment     = 'sport';



% Events
fixation = 768;
cues_c   = [769 770 780 774];
cues_x   = [28 29 30 31];
target   = [778 779];
beep     = 33282;
respons  = [897 898]; %CAM

% Datafiles
datapath    = ['/mnt/data/Git/Codes/cvsa/analysis/' experiment '/' features '/' spatialfilter '/'];
% datapath    = ['D:/sauvegardePCPostdocRennes/post-doc_Camille/git/Codes/cvsa/analysis/' experiment '/' features '/' spatialfilter '/'];
[Files, NumFiles] = util_getfile(datapath, '.mat', ['covert-attention*' subject]);

% Concatenate data
[O, analysis] = cvsa_utilities_cat_eeg_inria(Files);

% Extracting RoI
channelsLb = {'PO7', 'PO3', 'O1', 'Oz', 'O2', 'PO4', 'PO8', 'POz', 'P8', 'P6', 'P4', 'P2', 'Pz', 'P1', 'P3', 'P5', 'P7'};
[~, labels] = proc_get_montage('eeg.inria.32.cvsa');
channelsId  = proc_get_channel(channelsLb, labels);

% Extracting frequencies
SelFreqs = 6:16;
[~, SelFreqId] = intersect(analysis.processing.f, SelFreqs);

F = O(:, SelFreqId, channelsId);

nsamples     = size(F, 1);
nfrequencies = size(F, 2);
nchannels    = size(F, 3);
events       = analysis.event;

%% Classifier parameters
% eegc3 style settings
eegc3.modules.smr.gau.somunits 	= [1 1]; % QDA-style
eegc3.modules.smr.gau.sharedcov = 'f'; % No difference anyway
eegc3.modules.smr.gau.epochs 	= 4;
eegc3.modules.smr.gau.mimean	= 0.01;
eegc3.modules.smr.gau.micov		= 0.001;
eegc3.modules.smr.gau.th        = 0.55;
eegc3.modules.smr.gau.terminate	= true;

%% Parameters
WinShift = 0.0625;
CueDur   = 0.2;

LeftUp      = 769;
LeftDown    = 774;
RightDown   = 770;
RightUp     = 780;

%% Extract events
[~, evtfix] = proc_get_event2(fixation, nsamples, events.POS, events.TYP, 1);
[~, evtcue] = proc_get_event2([cues_c cues_x], nsamples, events.POS, events.TYP, 1);
[~, evttrg] = proc_get_event2(target, nsamples, events.POS, events.TYP, 1);
[~, evtbep] = proc_get_event2(beep, nsamples, events.POS, events.TYP, 1);
[~, evtcor] = proc_get_event2(respons, nsamples, events.POS, events.TYP, 1); %CAM

trial.pos = evtfix.POS;
trial.dur = evttrg.POS - trial.pos -1;
ntrials = length(trial.pos);

cvsa.pos  = evtcue.POS + floor(CueDur*WinShift);
cvsa.dur  = evttrg.POS - cvsa.pos -1;

% Convert + and x cue to the same values
cuetyp   = evtcue.TYP;
for cId = 1:length(cues_x)
    cuetyp(cuetyp == cues_x(cId)) = cues_c(cId);
end
evtcue.TYP = cuetyp;
classes = unique(cuetyp);
nclasses = length(classes);

%% Extracting label vectors
rRk = analysis.label.Rk;

%% Extracting PSD features for
U = log(F);

P  = [];
Ck = [];
Rk = [];
Lk = [];
Tk = [];
tRk = zeros(ntrials, 1);
for trId = 1:ntrials
    cstart = cvsa.pos(trId);
    cstop  = cstart + cvsa.dur(trId) -1;
    
    cdata = U(cstart:cstop, :, :);
    
    % Trial lenght
    cduration = cstop - cstart + 1;
    ctime = cduration*WinShift;
    if(ctime < 1)
       cLk = 1;
    elseif(ctime > 1.5)
       cLk = 3;
    else
       cLk = 2;
    end
    
    P  = cat(1, P, cdata);
    Ck = cat(1, Ck, evtcue.TYP(trId)*ones(size(cdata, 1), 1));
    Rk = cat(1, Rk, rRk(cstart)*ones(size(cdata, 1), 1));
    Lk = cat(1, Lk, cLk*ones(size(cdata, 1), 1));
    Tk = cat(1, Tk, trId*ones(size(cdata, 1), 1));
    
    tRk(trId) =  rRk(cstart);
end

Dk = ones(length(Rk), 1);
Dk(Rk >= 5) = 2;

% Removal
tRmIdx = cvsa_sport_trial_removal(subject, tRk);
Ak = [];
Bk = [];
RmIdx = [];
for trId = 1:ntrials
    cstart = cvsa.pos(trId);
    cstop  = cstart + cvsa.dur(trId) -1;
    
    clength = length(cstart:cstop);
    
    cRmIdx = tRmIdx(trId);
    RmIdx = cat(1, RmIdx, cRmIdx*ones(clength, 1));
    

    cAk = isempty(find(analysis.event.ART >= cstart & analysis.event.ART <= cstop, 1)) == false;
    Ak = cat(1, Ak, cAk*ones(clength, 1));
    cidx = diff(evtbep.POS(evtbep.POS >=  cstart & evtbep.POS <= cstop));
    cBk = sum(cidx == 0 | cidx == 1) > 0;
    Bk = cat(1, Bk, cBk*ones(clength, 1));
end

Rm = Ak | Bk | RmIdx;
disp(['Percentage removed trials: ' num2str(100*sum(Rm)./length(Rm)) '%']);


%% Create cross-validation indices per Run in each session
CrossInd = unique(perms([1 1 1 0]), 'rows');
ncrossind = size(CrossInd, 1);
TrainIndices = false(length(Rk), ncrossind);
TestIndices  = false(length(Rk), ncrossind);

% Session 1
for crId = 1:ncrossind
    cindices_train = find(CrossInd(crId, :));
    cindices_test  = find(CrossInd(crId, :) == 0);
    
    cctrainid = false(length(Rk), 1);
    for i = 1:length(cindices_train)
        cctrainid = cctrainid | Rk == cindices_train(i);
    end
    TrainIndices(:, crId) = cctrainid;
    
    cctestid = false(length(Rk), 1);
    for i = 1:length(cindices_test)
        cctestid = cctestid | Rk == cindices_test(i);
    end
    TestIndices(:, crId) = cctestid;
end

% Session 2
for crId = 1:ncrossind
    cindices_train = find(CrossInd(crId, :)) + 4;
    cindices_test  = find(CrossInd(crId, :) == 0) +4;

    cctrainid = false(length(Rk), 1);
    for i = 1:length(cindices_train)
        cctrainid = cctrainid | Rk == cindices_train(i);
    end
    TrainIndices(:, crId+4) = cctrainid;
    
    cctestid = false(length(Rk), 1);
    for i = 1:length(cindices_test)
        cctestid = cctestid | Rk == cindices_test(i);
    end
    TestIndices(:, crId+4) = cctestid;
end

%% Classification
NumSelFeatures = 2;

Durations = unique(Lk);
ndurations = length(Durations);

acc = nan(size(TrainIndices, 2), ndurations);
for crId = 1:size(TrainIndices, 2)
    cclassid = Ck == LeftDown | Ck == RightDown;
    ctrainid = TrainIndices(:, crId);
    ctestid  = TestIndices(:, crId);
    
    cindex_train = ctrainid & cclassid & Rm == false;
    
    if(sum(cindex_train) == 0)
        continue;
    end
    
    % Compute discriminancy over the selected tasks
    util_bdisp('[proc] - Computing fisher score');
    cfs = proc_fisher2(P(cindex_train, :, :), Ck(cindex_train));
    
    % Rank fs and select features
    util_bdisp(['[proc] - Select the best ' num2str(NumSelFeatures) ' features']);
    [~, FeaturesRankIdx] =  sort(cfs, 'descend');
    FeatureIdx = FeaturesRankIdx(1:NumSelFeatures);
    
    D  = proc_reshape_ts_bc(P(cindex_train, :, :));
    Dk = Ck(cindex_train);
    Dk(Dk==770) = 1;
    Dk(Dk==774) = 2;
    
    % Train classifier
    util_bdisp('[proc] - Train classifier');
    [gau, performace] = eegc3_train_gau(eegc3, D(:, FeatureIdx), Dk);
    
    
    for lId = 1:ndurations
        cindex_test  = ctestid & cclassid & Rm == false & Lk == Durations(lId);
        X  = proc_reshape_ts_bc(P(cindex_test, :, :));
        Xk = Ck(cindex_test);
        cTk = Tk(cindex_test);

        % Evaluating classifier
        util_bdisp('[proc] - Evaluate classifier');
        cpp = zeros(size(X, 1), 2);
        for tId = 1:size(X, 1)
            [~, cpp(tId, :)] = gauClassifier(gau.M, gau.C, X(tId, FeatureIdx));
        end

%         Gk = zeros(size(X, 1), 1);
%         Gk(cpp(:, 1) >= 0.5) = RightDown;
%         Gk(cpp(:, 1) <  0.5) = LeftDown;
        
        Gk = dm_majority_mean(cpp, cTk, 0.55);
        Gk(Gk == 1) = RightDown;
        Gk(Gk == 2) = LeftDown;
        acc(crId, lId) = sum(Gk == Xk)./(length(Xk));

    end
end

%% Saving
targetfile = [datapath '/' subject '_psd_classification_' num2str(NumSelFeatures) '_' spatialfilter '.mat'];
util_bdisp(['[out] - Saving analysis in: ' targetfile]);
save(targetfile, 'acc');


%% Plotting

figure;
subplot(1, 3, 1);
bar(acc(1:4, :));
grid on;
xlabel('Run');
ylabel('%');
title('First session - Average per run');
ylim([0 1])
plot_hline(0.5);

subplot(1, 3, 2);
bar(acc(5:8, :));
grid on;
xlabel('Run');
ylabel('%');
title('Second session - Average per run');
ylim([0 1])
plot_hline(0.5);

subplot(1, 3, 3);
bar([nanmean(acc(1:4, :)); nanmean(acc(5:8, :))]);
grid on;
xlabel('Session');
ylabel('%');
title('Average per session');
ylim([0 1])
plot_hline(0.5);

suptitle(['Subject ' subject ' - LI classification']);
