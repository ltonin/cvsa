clc; clearvars; 

subject     = 'S17';

features       = 'psd';
spatialfilter  = 'laplacian';
experiment     = 'sport';

% RoIs
roi_l = {'P7', 'P5', 'P3', 'P1', 'PO7', 'PO3', 'O1'};
roi_r = {'P8', 'P6', 'P4', 'P2', 'PO8', 'PO4', 'O2'};
[~, labels] = proc_get_montage('eeg.inria.32.cvsa');
roi_id_l = proc_get_channel(roi_l, labels);
roi_id_r = proc_get_channel(roi_r, labels);

% Frequencies
SelFreqs = 8:14;

BeforeTargetPeriod = 0.5; %[s]
AfterTargetPeriod  = 0.5;

% Events
fixation = 768;
cues_c   = [769 770 780 774];
cues_x   = [28 29 30 31];
target   = [778 779];
beep     = 33282;

cues_lb  = {'LeftUp', 'RightDown', 'RightUp', 'LeftDown'};

% Datafiles
%datapath    = ['/mnt/data/Git/Codes/cvsa/analysis/' experiment '/' features '/' spatialfilter '/'];
datapath    = ['D:/sauvegardePCPostdocRennes/post-doc_Camille/git/Codes/cvsa/analysis/' experiment '/' features '/' spatialfilter '/'];
[Files, NumFiles] = util_getfile(datapath, '.mat', ['covert-attention*' subject]);

% Concatenate data
[F, analysis] = cvsa_utilities_cat_eeg_inria(Files);

nsamples     = size(F, 1);
nfrequencies = size(F, 2);
nchannels    = size(F, 3);
events = analysis.event;

[~, SelFreqId ] = intersect(analysis.processing.f, SelFreqs);

%% Extract events
[~, evtfix] = proc_get_event2(fixation, nsamples, events.POS, events.TYP, 1);
[~, evtcue] = proc_get_event2([cues_c cues_x], nsamples, events.POS, events.TYP, 1);
[~, evttrg] = proc_get_event2(target, nsamples, events.POS, events.TYP, 1);
[~, evtbep] = proc_get_event2(beep, nsamples, events.POS, events.TYP, 1);

startpos = evtfix.POS;
stoppos  = evttrg.POS;
cuepos   = evtcue.POS;
cuetyp   = evtcue.TYP;

% Convert + and x cue to the same values
for cId = 1:length(cues_x)
    cuetyp(cuetyp == cues_x(cId)) = cues_c(cId);
end
classes = unique(cuetyp);
nclasses = length(classes);

if(isequal(length(startpos), length(stoppos)) == false)
    error('chk:evt', 'Different number of fixation and target events');
end

if(isequal(length(startpos), length(cuepos)) == false)
    error('chk:evt', 'Different number of fixation and cue events');
end

ntrials = length(startpos);
Tk = zeros(nsamples, 1);
Rk = zeros(ntrials, 1);
Ak = false(ntrials, 1);
Bk = false(ntrials, 1);
for trId = 1:ntrials
    ctarget = stoppos(trId);
    cstop  = ctarget + AfterTargetPeriod/analysis.processing.wshift;
    cstart = ctarget - BeforeTargetPeriod/analysis.processing.wshift;

    Tk(cstart:cstop) = cuetyp(trId);
    Rk(trId) = unique(analysis.label.Rk(cstart:cstop));
    Ak(trId) = isempty(find(analysis.event.ART >= cstart & analysis.event.ART <= ctarget, 1)) == false;
    cidx = diff(evtbep.POS(evtbep.POS >=  cstart & evtbep.POS <= ctarget));
    Bk(trId) = sum(cidx == 0 | cidx == 1) > 0;
end

RmIdx = cvsa_sport_trial_removal(subject, Rk);

RemovedId = Ak | Bk | RmIdx;
disp(['Percentage removed trials: ' num2str(100*sum(RemovedId)./length(RemovedId)) '%']);
% RemovedId = false(ntrials, 1);
%% Computing lateralization
U = F;


power_l = squeeze(mean(mean(U(:, SelFreqId, roi_id_l), 2), 3));
power_r = squeeze(mean(mean(U(:, SelFreqId, roi_id_r), 2), 3));
LI = (power_r - power_l)./mean([power_r power_l], 2);


%% Epoch extraction
MaxTrialDuration = (BeforeTargetPeriod+AfterTargetPeriod)/analysis.processing.wshift;
LITrials = nan(MaxTrialDuration, ntrials);
for trId = 1:ntrials
    ctarget = stoppos(trId);
    cstop  = ctarget + AfterTargetPeriod/analysis.processing.wshift;
    cstart = ctarget - BeforeTargetPeriod/analysis.processing.wshift;
    cdur   = length(cstart:cstop);
    LITrials(1:cdur, trId) = LI(cstart:cstop);
end

%% Saving
liend.overall = LI;
liend.trials  = LITrials;
liend.labels.trials.Ck = cuetyp;
liend.labels.trials.Rm = RemovedId;
liend.labels.trials.Rk = Rk;
liend.labels.overall.Tk = Tk;
liend.classes.id = classes;
liend.classes.lb = cues_lb;
liend.subject = subject;
liend.events.pos.start = startpos;
liend.events.pos.stop  = stoppos;

targetfile = [datapath '/' subject '_liend_' spatialfilter '.mat'];
util_bdisp(['[out] - Saving analysis in: ' targetfile]);
save(targetfile, 'liend');

%% Plotting
fig1 = figure;
t = -BeforeTargetPeriod:analysis.processing.wshift:AfterTargetPeriod;
subplot(4, 1, [1 3]);
hold on;
for cId = 1:nclasses
    plot(t, nanmean(LITrials(:, cuetyp == classes(cId) & RemovedId == false), 2));
    set(gca, 'XTickLabel', []);
end
hold off;
grid on;
%ylim([-0.5 0.2]);

plot_vline(0, '-k');

ylabel('[uV]');
legend(cues_lb);
title([subject ' - LI index - end trial']);


subplot(4, 1, 4);
plot(t, 100 - 100*sum(isnan(LITrials(:, RemovedId == false)), 2)/size(LITrials(:, RemovedId == false), 2));
plot_vline(0, '-k');
grid on;
xlabel('Time [ms]');
ylabel('% Trials');




