% clc; clearvars; 
% 
subject     = 'S9c';

features       = 'psd';
spatialfilter  = 'laplacian';
experiment     = 'sport';



% Events
fixation = 768;
cues_c   = [769 770 780 774];
cues_x   = [28 29 30 31];
target   = [778 779];
beep     = 33282;
respons  = [897 898]; %CAM

% Datafiles
datapath    = ['/mnt/data/Git/Codes/cvsa/analysis/' experiment '/' features '/' spatialfilter '/'];
% datapath    = ['D:/sauvegardePCPostdocRennes/post-doc_Camille/git/Codes/cvsa/analysis/' experiment '/' features '/' spatialfilter '/'];
[Files, NumFiles] = util_getfile(datapath, '.mat', ['covert-attention*' subject]);

% Concatenate data
[O, analysis] = cvsa_utilities_cat_eeg_inria(Files);

% Extracting RoI
channelsLb = {'PO7', 'PO3', 'O1', 'Oz', 'O2', 'PO4', 'PO8', 'POz', 'P8', 'P6', 'P4', 'P2', 'Pz', 'P1', 'P3', 'P5', 'P7'};
[~, labels] = proc_get_montage('eeg.inria.32.cvsa');
channelsId  = proc_get_channel(channelsLb, labels);

% Extracting frequencies
SelFreqs = 6:16;
[~, SelFreqId] = intersect(analysis.processing.f, SelFreqs);

F = O(:, SelFreqId, channelsId);

nsamples     = size(F, 1);
nfrequencies = size(F, 2);
nchannels    = size(F, 3);
events       = analysis.event;

%% Classifier parameters
% eegc3 style settings
eegc3.modules.smr.gau.somunits 	= [1 1]; % QDA-style
eegc3.modules.smr.gau.sharedcov = 'f'; % No difference anyway
eegc3.modules.smr.gau.epochs 	= 4;
eegc3.modules.smr.gau.mimean	= 0.01;
eegc3.modules.smr.gau.micov		= 0.001;
eegc3.modules.smr.gau.th        = 0.55;
eegc3.modules.smr.gau.terminate	= true;

%% Parameters
WinShift = 0.0625;
CueDur   = 0.2;

LeftUp      = 769;
LeftDown    = 774;
RightDown   = 770;
RightUp     = 780;

%% Extract events
[~, evtfix] = proc_get_event2(fixation, nsamples, events.POS, events.TYP, 1);
[~, evtcue] = proc_get_event2([cues_c cues_x], nsamples, events.POS, events.TYP, 1);
[~, evttrg] = proc_get_event2(target, nsamples, events.POS, events.TYP, 1);
[~, evtbep] = proc_get_event2(beep, nsamples, events.POS, events.TYP, 1);
[~, evtcor] = proc_get_event2(respons, nsamples, events.POS, events.TYP, 1); %CAM

trial.pos = evtfix.POS;
trial.dur = evttrg.POS - trial.pos -1;
ntrials = length(trial.pos);

cvsa.pos  = evtcue.POS + floor(CueDur*WinShift);
cvsa.dur  = evttrg.POS - cvsa.pos -1;

% Convert + and x cue to the same values
cuetyp   = evtcue.TYP;
for cId = 1:length(cues_x)
    cuetyp(cuetyp == cues_x(cId)) = cues_c(cId);
end
evtcue.TYP = cuetyp;
classes = unique(cuetyp);
nclasses = length(classes);

%% Extracting label vectors
rRk = analysis.label.Rk;

%% Extracting PSD features for
U = log(F);

P  = [];
Ck = [];
Rk = [];
Lk = [];
Dk = [];
Tk = [];
tRk = zeros(ntrials, 1);

for trId = 1:ntrials
    cstart = cvsa.pos(trId);
    cstop  = cstart + cvsa.dur(trId) -1;
    
    cdata = U(cstart:cstop, :, :);
    
    % Trial lenght
    cduration = cstop - cstart + 1;
    ctime = cduration*WinShift;
    if(ctime < 1)
       cLk = 1;
    elseif(ctime > 1.5)
       cLk = 3;
    else
       cLk = 2;
    end
    
    P  = cat(1, P, cdata);
    Ck = cat(1, Ck, evtcue.TYP(trId)*ones(size(cdata, 1), 1));
    Rk = cat(1, Rk, rRk(cstart)*ones(size(cdata, 1), 1));
    Lk = cat(1, Lk, cLk*ones(size(cdata, 1), 1));
    
    tRk(trId) =  rRk(cstart);
    cDk = 1;
    if(rRk(cstart) > 5)
        cDk = 2;
    end
    Dk = cat(1, Dk, cDk*ones(size(cdata, 1), 1));
    
    Tk = cat(1, Tk, trId*ones(size(cdata, 1), 1));
end

Dk = ones(length(Rk), 1);
Dk(Rk >= 5) = 2;

% Removal
tRmIdx = cvsa_sport_trial_removal(subject, tRk);
Ak = [];
Bk = [];
RmIdx = [];
for trId = 1:ntrials
    cstart = cvsa.pos(trId);
    cstop  = cstart + cvsa.dur(trId) -1;
    
    clength = length(cstart:cstop);
    
    cRmIdx = tRmIdx(trId);
    RmIdx = cat(1, RmIdx, cRmIdx*ones(clength, 1));
    

    cAk = isempty(find(analysis.event.ART >= cstart & analysis.event.ART <= cstop, 1)) == false;
    Ak = cat(1, Ak, cAk*ones(clength, 1));
    cidx = diff(evtbep.POS(evtbep.POS >=  cstart & evtbep.POS <= cstop));
    cBk = sum(cidx == 0 | cidx == 1) > 0;
    Bk = cat(1, Bk, cBk*ones(clength, 1));
end

Rm = Ak | Bk | RmIdx;
disp(['Percentage removed trials: ' num2str(100*sum(Rm)./length(Rm)) '%']);

% %% Create indices for cross validation
% days = unique(Dk);
% ndays = length(days);
% 
% lengths = unique(Lk);
% nlengths = length(lengths);
% 
% nfolds        = 20;
% percentTrain  = 70;
% percentTest   = 20;
% percentValid  = 10;
% nTrialsTrain  = floor(percentTrain*ntrials/100);
% nTrialsTest   = floor(percentTest*ntrials/100);
% nTrialsValid  = floor(percentValid*ntrials/100);
% valTrain      = 1;
% valTest       = 2;
% valValid      = 3;
% 
% CrossIndexes = zeros(size(P, 1), nfolds, nlengths, ndays);
% for dId = 1:ndays
%     for lId = 1:nlengths
%         
%         cIdx = Lk == lId & Dk == dId & Rm == false;
%         %cnsamples = sum(cIdx);
%         cTrials = unique(Tk(cIdx));
%         cntrials  = length(cTrials);
%        
%         cnTrain = floor(percentTrain*cntrials/100);
%         cnTest  = floor(percentTest*cntrials/100);
%         cnValid = floor(percentValid*cntrials/100);
%         csubindex = [valTrain*ones(cnTrain, 1); valTest*ones(cnTest, 1); valValid*ones(cnValid, 1)];
%         
%         if(length(csubindex) < cntrials)
%             csubindex = [csubindex; valTrain*ones(cntrials-length(csubindex), 1)];
%         end
%         
% %         cnTrain = floor(percentTrain*cnsamples/100);
% %         cnTest  = floor(percentTest*cnsamples/100);
% %         cnValid = floor(percentValid*cnsamples/100);
% %         csubindex = [valTrain*ones(cnTrain, 1); valTest*ones(cnTest, 1); valValid*ones(cnValid, 1)];
% %         
% %         if(length(csubindex) < cnsamples)
% %             csubindex = [csubindex; valTrain*ones(cnsamples-length(csubindex), 1)];
% %         end
%         
%         for foId = 1:nfolds
%             cindex_trials = csubindex(randperm(length(csubindex)));
%             cindex = zeros(size(P, 1), 1);
%             for tIdx = 1:length(cindex_trials)
%                 val = cindex_trials(tIdx);
%                 numTrial = cTrials(tIdx);
%                 cindex(find(Tk==numTrial)) = val*ones(length(find(Tk==numTrial)),1);
%             end
%             %cIdx(find(cIdx)) = cindex
%             %cindex = zeros(size(P, 1), 1);
%             %cindex(cIdx) = cindex_trials(cTrials == Tk(cIdx));
%             CrossIndexes(:, foId, lId, dId) = cindex;
%         end
%     end
% end
%     
%% Classification
Left = 1;
Right = 2;
mCk = zeros(length(Ck), 1);
mCk(Ck == 769 | Ck == 774) = Left;
mCk(Ck == 770 | Ck == 780) = Right;
% mCk(Ck == 774) = Left;
% mCk(Ck == 780) = Right;

nfeatures = 2:6;

days = unique(Dk);
ndays = length(days);

lengths = unique(Lk);
nlengths = length(lengths);

final_acc = zeros(nlengths, ndays);

for dId = 1:ndays
    for lId = 1:nlengths
        disp(['Session ' num2str(dId) ' - Length ' num2str(lId)]);
        %accu_train = zeros(length(nfeatures), nfolds);
        %accu_test = zeros(length(nfeatures), nfolds);
        
        cTrainSet = Lk == lId & Dk == dId & Rk ~= dId*4 & Rm == false;
        cTestSet =  Lk == lId & Dk == dId & Rk == dId*4 & Rm == false;
        
        if(sum(cTrainSet) == 0)
            warning('chk:lenght', 'No data in TrainSet');
            continue;
        end
        
        % Fisher score
        cfs = proc_fisher2(P(cTrainSet, :, :), mCk(cTrainSet));

        crP  = proc_reshape_ts_bc(P);
        crPk = mCk;


        [~, FeaturesRankIdx] =  sort(cfs, 'descend');
        
        accu_train = zeros(length(nfeatures), 1);
        accu_test = zeros(length(nfeatures), 1);

        for ftId = 1:length(nfeatures)
            cnfeatures = nfeatures(ftId);

            FeatureIdx = FeaturesRankIdx(1:cnfeatures);
            [cguess, ~, cpostprob] = classify(crP(:, FeatureIdx), crP(cTrainSet, FeatureIdx), crPk(cTrainSet), 'quadratic'); 

            crejection = 0.55;
            cguess_dm_train = zeros(size(cguess, 1), 1);
            cguess_dm_train(cTrainSet) = dm_majority_mean(cpostprob(cTrainSet, :), Tk(cTrainSet), crejection);
            
            cguess_dm_test = zeros(size(cguess, 1), 1);
            cguess_dm_test(cTestSet) = dm_majority_mean(cpostprob(cTestSet, :), Tk(cTestSet), crejection);
            
           
            accu_train(ftId) = sum(cguess_dm_train(cTrainSet) == crPk(cTrainSet))./sum(cTrainSet);
            accu_test(ftId)  = sum(cguess_dm_test(cTestSet) == crPk(cTestSet))./sum(cTestSet);

        end
        
        % instead of just taking the best, we look for all the max
        % and then we keep the one with the highest mean for its
        % neighbours (NB) - improves the stability of number of
        % features (a bit...)

        [max_acc, max_acc_id] = max(accu_train);
        listId_max_acc = find(accu_train==max_acc); %cam
        list_max_acc_averagedNB = zeros(length(listId_max_acc),1); %cam

        if length(listId_max_acc) > 1 %cam
            for i = 1:length(listId_max_acc) 
                NB_low = listId_max_acc(i)-1;
                NB_high = listId_max_acc(i)+1;
                if listId_max_acc(i) <= 2;
                    NB_low = listId_max_acc(i);
                elseif listId_max_acc(i) == length(accu_train)
                    NB_high = listId_max_acc(i);
                end
                list_max_acc_averagedNB(i) = mean(accu_train(NB_low:NB_high));
            end
            [best_max_acc, best_max_acc_id] = max(list_max_acc_averagedNB);
            max_acc_id = listId_max_acc(best_max_acc_id);
            max_acc = accu_train(max_acc_id);
            max_acc_test = accu_test(max_acc_id);
        end
        
        [~, max_acc_id_train] = max(accu_train);
        final_acc(lId, dId) = accu_test(max_acc_id_train);
        disp(['Selected ' num2str(nfeatures(max_acc_id_train)) ' number of features']);
        
%         for foId = 1:nfolds
%            cTrainSet = CrossIndexes(:, foId, lId, dId) == valTrain; 
%            cTestSet  = CrossIndexes(:, foId, lId, dId) == valTest;
%            cValidSet = CrossIndexes(:, foId, lId, dId) == valValid;
%            
%            if(sum(cTrainSet) == 0)
%                 continue;
%            end
%             % Fisher score
%             cfs = proc_fisher2(P(cTrainSet, :, :), mCk(cTrainSet));
% 
%             crP  = proc_reshape_ts_bc(P);
%             crPk = mCk;
%             
%             
%             [~, FeaturesRankIdx] =  sort(cfs, 'descend');
%             
%             accu_train = zeros(length(nfeatures), 1);
%             accu_test = zeros(length(nfeatures), 1);
%         
%             for ftId = 1:length(nfeatures)
%                 cnfeatures = nfeatures(ftId);
% 
%                 FeatureIdx = FeaturesRankIdx(1:cnfeatures);
% %                 [gau, performace] = eegc3_train_gau(eegc3, crP(cTrainSet, FeatureIdx), crPk(cTrainSet));
% %                 
% %                 cpp = zeros(size(crP, 1), 2);
% %                 for sId = 1:size(crP, 1)
% %                     [~, cpp(sId, :)] = gauClassifier(gau.M, gau.C, crP(sId, FeatureIdx));
% %                 end
% %                 
% %                 cguess = zeros(size(crP, 1), 1);
% %                 cguess(cpp >= 0.5) = 1;
% %                 cguess(cpp < 0.5) = 2;
%                 cguess = classify(crP(:, FeatureIdx), crP(cTrainSet, FeatureIdx), crPk(cTrainSet)); 
%                 
%                 accu_train(ftId) = sum(cguess(cTrainSet) == crPk(cTrainSet))./sum(cTrainSet);
%                 accu_test(ftId)  = sum(cguess(cTestSet) == crPk(cTestSet))./sum(cTestSet);
% 
%             end
%             
%             % instead of just taking the best, we look for all the max
%             % and then we keep the one with the highest mean for its
%             % neighbours (NB) - improves the stability of number of
%             % features (a bit...)
%             
%             [max_acc, max_acc_id] = max(accu_test);
%             listId_max_acc = find(accu_test==max_acc); %cam
%             list_max_acc_averagedNB = zeros(length(listId_max_acc),1); %cam
%             
%             if length(listId_max_acc) > 1 %cam
%                 for i = 1:length(listId_max_acc) 
%                     NB_low = listId_max_acc(i)-1;
%                     NB_high = listId_max_acc(i)+1;
%                     if listId_max_acc(i) <= 2;
%                         NB_low = listId_max_acc(i);
%                     elseif listId_max_acc(i) == length(accu_test)
%                         NB_high = listId_max_acc(i);
%                     end
%                     list_max_acc_averagedNB(i) = mean(accu_test(NB_low:NB_high));
%                 end
%                 [best_max_acc, best_max_acc_id] = max(list_max_acc_averagedNB);
%                 max_acc_id = listId_max_acc(best_max_acc_id);
%                 max_acc = accu_test(max_acc_id);
%             end
%             
%             FeatureIdx = FeaturesRankIdx(1:nfeatures(max_acc_id));
%             disp(['nb of features: ' num2str(nfeatures(max_acc_id))]);
%             [cguess_val, ~, cpostp_val] = classify(crP(:, FeatureIdx), crP(cTrainSet, FeatureIdx), crPk(cTrainSet)); 
%             
%             cguess_val_dm = zeros(size(cguess_val, 1), 1);
%             cguess_val_dm(cValidSet) = dm_cumsum(cpostp_val(cValidSet, :), Tk(cValidSet), 0.55);
%             
%             accu_valid(lId, dId, foId) = sum(cguess_val_dm(cValidSet) == crPk(cValidSet))./sum(cValidSet);
%             
%         end
%         
%         mean_accu_valid(lId, dId) = mean(accu_valid(lId,dId),3);
%         
%         %[max_acc, max_acc_id] = max(mean(accu_test, 2));
%         %disp(['    Maximum accuracy on test set: ' num2str(max_acc) ' for ' num2str(nfeatures(max_acc_id)) ' features']);
%         
%         %accu_train(ftId, foId) = sum(cguess(cTrainSet) == crPk(cTrainSet))./sum(cTrainSet);
%         %accu_test(ftId, foId)  = sum(cguess(cTestSet) == crPk(cTestSet))./sum(cTestSet);
                
 
        
    end
end



% %% Create cross-validation indices per Run in each session
% CrossInd = unique(perms([1 1 1 0]), 'rows');
% ncrossind = size(CrossInd, 1);
% TrainIndices = false(length(Rk), ncrossind);
% TestIndices  = false(length(Rk), ncrossind);
% 
% % Session 1
% for crId = 1:ncrossind
%     cindices_train = find(CrossInd(crId, :));
%     cindices_test  = find(CrossInd(crId, :) == 0);
%     
%     cctrainid = false(length(Rk), 1);
%     for i = 1:length(cindices_train)
%         cctrainid = cctrainid | Rk == cindices_train(i);
%     end
%     TrainIndices(:, crId) = cctrainid;
%     
%     cctestid = false(length(Rk), 1);
%     for i = 1:length(cindices_test)
%         cctestid = cctestid | Rk == cindices_test(i);
%     end
%     TestIndices(:, crId) = cctestid;
% end
% 
% % Session 2
% for crId = 1:ncrossind
%     cindices_train = find(CrossInd(crId, :)) + 4;
%     cindices_test  = find(CrossInd(crId, :) == 0) +4;
% 
%     cctrainid = false(length(Rk), 1);
%     for i = 1:length(cindices_train)
%         cctrainid = cctrainid | Rk == cindices_train(i);
%     end
%     TrainIndices(:, crId+4) = cctrainid;
%     
%     cctestid = false(length(Rk), 1);
%     for i = 1:length(cindices_test)
%         cctestid = cctestid | Rk == cindices_test(i);
%     end
%     TestIndices(:, crId+4) = cctestid;
% end
% 
% %% Classification
% NumSelFeatures = 2;
% 
% Durations = unique(Lk);
% ndurations = length(Durations);
% 
% acc = nan(size(TrainIndices, 2), ndurations);
% for crId = 1:size(TrainIndices, 2)
%     cclassid = Ck == LeftDown | Ck == RightDown;
%     ctrainid = TrainIndices(:, crId);
%     ctestid  = TestIndices(:, crId);
%     
%     cindex_train = ctrainid & cclassid & Rm == false;
%     
%     if(sum(cindex_train) == 0)
%         continue;
%     end
%     
%     % Compute discriminancy over the selected tasks
%     util_bdisp('[proc] - Computing fisher score');
%     cfs = proc_fisher2(P(cindex_train, :, :), Ck(cindex_train));
%     
%     % Rank fs and select features
%     util_bdisp(['[proc] - Select the best ' num2str(NumSelFeatures) ' features']);
%     [~, FeaturesRankIdx] =  sort(cfs, 'descend');
%     FeatureIdx = FeaturesRankIdx(1:NumSelFeatures);
%     
%     D  = proc_reshape_ts_bc(P(cindex_train, :, :));
%     Dk = Ck(cindex_train);
%     
%     % Train classifier
%     util_bdisp('[proc] - Train classifier');
%     [gau, performace] = eegc3_train_gau(eegc3, D(:, FeatureIdx), Dk);
%     
%     
%     for lId = 1:ndurations
%         cindex_test  = ctestid & cclassid & Rm == false & Lk == Durations(lId);
%         X  = proc_reshape_ts_bc(P(cindex_test, :, :));
%         Xk = Ck(cindex_test);
% 
%         % Evaluating classifier
%         util_bdisp('[proc] - Evaluate classifier');
%         cpp = zeros(size(X, 1), 2);
%         for tId = 1:size(X, 1)
%             [~, cpp(tId, :)] = gauClassifier(gau.M, gau.C, X(tId, FeatureIdx));
%         end
% 
%         Gk = zeros(size(X, 1), 1);
%         Gk(cpp(:, 1) >= 0.5) = RightDown;
%         Gk(cpp(:, 1) <  0.5) = LeftDown;
% 
%         
%         acc(crId, lId) = sum(Gk == Xk)./(length(Xk));
% 
%     end
% end
% 
% %% Saving
% targetfile = [datapath '/' subject '_psd_classification_' num2str(NumSelFeatures) '_' spatialfilter '.mat'];
% util_bdisp(['[out] - Saving analysis in: ' targetfile]);
% save(targetfile, 'acc');
% 
% 
% %% Plotting
% 
% figure;
% subplot(1, 3, 1);
% bar(acc(1:4, :));
% grid on;
% xlabel('Run');
% ylabel('%');
% title('First session - Average per run');
% ylim([0 1])
% plot_hline(0.5);
% 
% subplot(1, 3, 2);
% bar(acc(5:8, :));
% grid on;
% xlabel('Run');
% ylabel('%');
% title('Second session - Average per run');
% ylim([0 1])
% plot_hline(0.5);
% 
% subplot(1, 3, 3);
% bar([nanmean(acc(1:4, :)); nanmean(acc(5:8, :))]);
% grid on;
% xlabel('Session');
% ylabel('%');
% title('Average per session');
% ylim([0 1])
% plot_hline(0.5);
% 
% suptitle(['Subject ' subject ' - LI classification']);
