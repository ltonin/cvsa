clc; clearvars; 

subject     = 'S17';

features       = 'psd';
spatialfilter  = 'car';
experiment     = 'sport';

% RoIs
roi = {'PO7', 'PO3', 'O1', 'Oz', 'O2', 'P04', 'PO8', 'POz'};

[~, labels] = proc_get_montage('eeg.inria.32.cvsa');
roi_id = proc_get_channel(roi, labels);

%% Events
fixation = 768;
cues_c   = [769 770 780 774];
cues_x   = [28 29 30 31];
target   = [778 779];
beep     = 33282;
respons  = [897 898]; %CAM

cues_lb  = {'LeftUp', 'RightDown', 'RightUp', 'LeftDown'};

%% Datafiles
%datapath    = ['/mnt/data/Git/Codes/cvsa/analysis/' experiment '/' features '/' spatialfilter '/'];
datapath    = ['D:/sauvegardePCPostdocRennes/post-doc_Camille/git/Codes/cvsa/analysis/' experiment '/' features '/' spatialfilter '/'];
[Files, NumFiles] = util_getfile(datapath, '.mat', ['covert-attention*' subject]);

%% Concatenate data
[F, analysis] = cvsa_utilities_cat_eeg_inria(Files);

nsamples     = size(F, 1);
nfrequencies = size(F, 2);
nchannels    = size(F, 3);
events       = analysis.event;

%% Extract fixation intervals
[~, evtfix] = proc_get_event2(fixation, nsamples, events.POS, events.TYP, 1);
[~, evtcue] = proc_get_event2([cues_c cues_x], nsamples, events.POS, events.TYP, 1);
[~, evtbep] = proc_get_event2(beep, nsamples, events.POS, events.TYP, 1);
[~, evtcor] = proc_get_event2(respons, nsamples, events.POS, events.TYP, 1); %CAM

ntrials = length(evtfix.POS);
correct  = evtcor.TYP; 
Rk = zeros(ntrials, 1);
Ak = false(ntrials, 1);
Bk = false(ntrials, 1);
Xk = false(ntrials, 1);

for trId = 1:ntrials
   cstart = evtfix.POS(trId);
   cstop  = evtcue.POS(trId) - 1;
   
   Fk(cstart:cstop) = 1;
   
   % Correctness of the response
   if correct(trId) == 897
       Xk(trId) = true;
   end
   
   % Runs
   Rk(trId) = unique(analysis.label.Rk(cstart:cstop));
   
   % Artifact Rejection
   Ak(trId) = isempty(find(events.ART >= cstart & events.ART <= cstop, 1)) == false;
   cidx = diff(evtbep.POS(evtbep.POS >=  cstart & evtbep.POS <= cstop));
   Bk(trId) = sum(cidx == 0 | cidx == 1) > 0;
end

% Fault recording removal
RmIdx = cvsa_sport_trial_removal(subject, Rk);

% All removed
RemovedId = Ak | Bk | RmIdx;
disp(['Percentage removed trials: ' num2str(100*sum(RemovedId)./length(RemovedId)) '%']);

%% Load individual alpha peak and extract frequency range
load([datapath subject '_alphapeak_' spatialfilter]);
iaf = nanmedian(alphapeak.trial(RemovedId == false));
iab = iaf-1:iaf+1;
[~, iabId ] = intersect(analysis.processing.f, iab);


%% Extract alpha power

IAP = zeros(ntrials, 1);

Fl = log(F);
U = mean(Fl(:, :, roi_id), 3);

for trId = 1:ntrials
    
    cstart = evtfix.POS(trId);
    cstop  = evtcue.POS(trId) - 1;
    
    cdata = mean(U(cstart:cstop, :), 1);
    
    ciap = mean(cdata(iabId)./max(cdata));
    
    IAP(trId) = ciap;
end

nIAP = IAP + 1;

nIAP(Rk >= 1 & Rk <= 4) = nIAP(Rk >= 1 & Rk <= 4)./max(nIAP(Rk >= 1 & Rk <= 4));
nIAP(Rk >= 5 & Rk <= 8) = nIAP(Rk >= 5 & Rk <= 8)./max(nIAP(Rk >= 5 & Rk <= 8));

% Correlation
[c, p] = corr(nIAP(RemovedId == false), (1:sum(RemovedId == false))', 'rows', 'pairwise');

disp(['Subject ' subject ' - Correlation over trials: ' num2str(c, 3) ' (p<' num2str(p, 3) ')']);

figure
pIAP = nan(ntrials, 1);
pIAP(RemovedId == false) = nIAP(RemovedId == false);
plot(1:ntrials, pIAP, '.'); 
title(subject);
lsline

iap.values   = IAP;
iap.slope    = c;
iap.label.Rm = RemovedId;
iap.label.Rk = Rk;
iap.label.Xk = Xk;
iap.iaf      = iaf;
iap.iab      = iab;

targetfile = [datapath '/' subject '_iap_' spatialfilter '.mat'];
util_bdisp(['[out] - Saving analysis in: ' targetfile]);
save(targetfile, 'iap')
