% clc; clearvars; 
% 
subject     = 'S9c';

features       = 'psd';
spatialfilter  = 'laplacian';
experiment     = 'sport';

% RoIs
channelLb = {'P5', 'P3', 'P1', 'PO7', 'PO3', 'O1', 'Oz', 'O2', 'PO4', 'PO8', 'POz', 'Pz', 'P2', 'P4', 'P6'};

[~, labels] = proc_get_montage('eeg.inria.32.cvsa');
channelId = proc_get_channel(channelLb, labels);

% Frequency band
SelBand = 6:2:16;

% Events
cues_c  = [769 770 780 774];
cues_x  = [28 29 30 31];
target  = [778 779];
beep = 33282;

cues_lb  = {'LeftUp', 'RightDown', 'RightUp', 'LeftDown'};
WinShift = 0.0625;
CueDur = 0.2;

% Datafiles
datapath    = ['/mnt/data/Git/Codes/cvsa/analysis/' experiment '/' features '/' spatialfilter '/'];
% datapath    = ['D:/sauvegardePCPostdocRennes/post-doc_Camille/git/Codes/cvsa/analysis/' experiment '/' features '/' spatialfilter '/'];
[Files, NumFiles] = util_getfile(datapath, '.mat', ['covert-attention*' subject]);

% Concatenate data
[F, analysis]   = cvsa_utilities_cat_eeg_inria(Files);
[~, SelFreqId ] = intersect(analysis.processing.f, SelBand);

F = F(:, SelFreqId, channelId);

nsamples     = size(F, 1);
nfrequencies = size(F, 2);
nchannels    = size(F, 3);
events = analysis.event;

%% Extract events
[~, evtcue] = proc_get_event2([cues_c cues_x], nsamples, events.POS, events.TYP, 1);
[~, evttrg] = proc_get_event2(target, nsamples, events.POS, events.TYP, 1);
[~, evtbep] = proc_get_event2(beep, nsamples, events.POS, events.TYP, 1);

% Convert + and x cue to the same values
cuetyp   = evtcue.TYP;
for cId = 1:length(cues_x)
    cuetyp(cuetyp == cues_x(cId)) = cues_c(cId);
end
classes = unique(cuetyp);
nclasses = length(classes);


ntrials = length(evtcue.POS);
Tk = zeros(nsamples, 1);
Ak = false(ntrials, 1);
Bk = false(ntrials, 1);
tRk = zeros(ntrials, 1);
for trId = 1:ntrials
    cstart = evtcue.POS(trId) + floor(CueDur/WinShift);
    cstop  = evttrg.POS(trId) - 1;
    Tk(cstart:cstop) = cuetyp(trId);
    % Runs
    tRk(trId) = unique(analysis.label.Rk(cstart:cstop));

    % Rejection
    Ak(trId) = isempty(find(analysis.event.ART >= cstart & analysis.event.ART <= cstop, 1)) == false;
    cidx = diff(evtbep.POS(evtbep.POS >=  cstart & evtbep.POS <= cstop));
    Bk(trId) = sum(cidx == 0 | cidx == 1) > 0;
end

RmIdx = cvsa_sport_trial_removal(subject, tRk);

RemovedId = Ak | Bk | RmIdx;
disp(['Percentage removed trials: ' num2str(100*sum(RemovedId)./length(RemovedId)) '%']);
Rm = false(nsamples, 1);
for trId = 1:ntrials
    cstart = evtcue.POS(trId) + floor(CueDur/WinShift);
    cstop  = evttrg.POS(trId) - 1;
    Rm(cstart:cstop) = RemovedId(trId);
end

%% Combinations
Combinations = nchoosek(cues_c, 2);
ncombinations = size(Combinations, 1);

%% Reshape features

F = log(F);

Rk = analysis.label.Rk;
Runs = unique(Rk);
nruns = length(Runs);
FisherScore = nan(nchannels*nfrequencies, nruns, ncombinations);
for cmId = 1:ncombinations
    for rId = 1:nruns
        cindex = Rk == rId & (Tk == Combinations(cmId, 1) | Tk == Combinations(cmId, 2)) & Rm == false;
        if(sum(cindex) == 0)
            continue;
        end
        cF = F(cindex, :, :);
        cTk = Tk(cindex);
        cfs = proc_fisher2(cF, cTk);
        FisherScore(:, rId, cmId) = cfs;
    end
end

%% Saving
discriminancy.values        = FisherScore;
discriminancy.combinations  = Combinations;

targetfile = [datapath '/' subject '_discriminancy_' spatialfilter '.mat'];
util_bdisp(['[out] - Saving analysis in: ' targetfile]);
save(targetfile, 'discriminancy')

%% Plotting
CombinationId = 5;
clim = [0 0.7];
for rId = 1:nruns
    subplot(nruns/4, 4, rId); 
    imagesc(squeeze(reshape(FisherScore(:, rId, CombinationId), nfrequencies, nchannels))', clim); 
    set(gca, 'YTick', 1:nchannels);
    set(gca, 'YTickLabel', channelLb);
    set(gca, 'XTick', 1:nfrequencies);
    set(gca, 'XTickLabel', num2str(SelBand'));
    title(['Run ' num2str(rId)]);
end

