clc; clearvars;

subject     = 'S2c';
%subject     = 'S17';

inchannels  = 1:34;                                 % Total number of channels
exchannels  = [17 34];                              % Excluded channels (trigger channels)
idchannels  = setdiff(inchannels, exchannels);      % Id eeg channels

Order = 4;
Band = [2 30];
Th = 5;
MaxValue = 80;
experiment  = 'sport';
%datapath    = ['/mnt/data/Research/cvsa/' experiment '/'];
datapath    = ['C:/post-doc_Camille/data_covertAttention/'];

% Get datafiles
[Files, NumFiles] = util_getfile([datapath subject '/EEGSignals/'], '.gdf', subject);

for fId = 1:NumFiles
    cfilename = Files{fId};
    util_bdisp(['[io] - Loading file ' num2str(fId) '/' num2str(NumFiles)]);
    disp(['       File: ' cfilename]);
   
    % Import file
    [s, h] = sload(cfilename, idchannels);  
    
    % Filtering
    sf = filt_bp(s, Order, Band, h.SampleRate);
    
    
    mchans = mean(sf, 1);
    schans = std(sf, [], 1);
    
    artIdx = sum((abs(sf) > Th*max(schans)) & abs(sf) > MaxValue, 2) > 0;
    
    hold on; 
    plot(sf); 
    plot(100*artIdx, 'LineWidth', 2, 'Color', 'k'); 
    hold off;
    
    
    keyboard
    
    
    
end