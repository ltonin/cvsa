clc; clearvars; 


sublist = {'S1c', 'S2c', 'S3c', 'S4c', 'S5c', 'S6c', 'S7c', 'S8c', 'S9c', 'S10', 'S11', 'S12', 'S14', 'S15', 'S16', 'S17'};
nsubjects = length(sublist);

features       = 'psd';
spatialfilter  = 'laplacian';
experiment     = 'sport';
%datapath       = ['/mnt/data/Git/Codes/cvsa/analysis/' experiment '/' features '/' spatialfilter '/'];
datapath       = ['D:/sauvegardePCPostdocRennes/post-doc_Camille/git/Codes/cvsa/analysis/' experiment '/' features '/' spatialfilter '/'];

figdir = './figures/sport/';
IAP = [];
Sk  = [];
Rm  = [];
Rk  = [];

%% Loading iap
for sId = 1:nsubjects
    subject = sublist{sId};

    filename = [datapath subject '_iap_' spatialfilter '.mat'];
    util_bdisp(['[io] - Loading IAP slope for subject ' subject ': ' filename]);
    cdata = load(filename);
    
    IAP = cat(1, IAP, cdata.iap.values);
    cnvalues = length(cdata.iap.values);
    
    Sk = cat(1, Sk, sId*ones(cnvalues, 1));

    Rm = cat(1, Rm, cdata.iap.label.Rm);
    Rk = cat(1, Rk, cdata.iap.label.Rk);
    
end

iap.values = IAP;
iap.labels.Sk = Sk;
iap.labels.Rm = Rm;
iap.labels.Rk = Rk;

%% Loading LI
cdata = load([datapath 'all_liend_alphapeak_laplacian.mat']);
li = cdata.liend;

%% Fuse the labels
Rm = li.labels.Rm | iap.labels.Rm;
Sk = li.labels.Sk;
Rk = li.labels.Rk;
Ck = li.labels.Ck;
Lk = li.labels.Lk;

%% Session labels
Dk(Rk <= 4) = 1;
Dk(Rk >= 5) = 2;
Dk = Dk';
Days = unique(Dk);
ndays = length(Days);

%% Class labels
Classes = unique(Ck);
nclasses = length(Classes);

%% Compute slope
IAPSlope = nan(nsubjects, ndays);
LISlope  = nan(nsubjects, ndays);

for sId = 1:nsubjects
    for dId = 1:ndays
        cindex = Sk == sId & Dk == Days(dId) & Rm == false;
        
        if(sum(cindex) == 0)
            continue;
        end
        
        ciaps = iap.values(cindex);
        idtrials = (1:length(ciaps))';
        
        cciap = corr(idtrials, ciaps, 'rows', 'pairwise');
        
        clis = abs(li.values(cindex));
        ccli = corr(idtrials, clis, 'rows', 'pairwise');
        
        IAPSlope(sId, dId) = cciap;
        LISlope(sId, dId) = ccli;
    end
end
    

rIAPSlope = cat(1, IAPSlope(:, 1), IAPSlope(:, 2));
rLISlope  = cat(1, LISlope(:, 1), LISlope(:, 2));





%% Compute correlation iap and li for each subject, session, class

corrV = nan(nclasses, ndays, nsubjects);
corrP = nan(nclasses, ndays, nsubjects);

for sId = 1:nsubjects
    for dId = 1:ndays
       for cId = 1:nclasses
           cindex = Sk == sId & Ck == Classes(cId) & Rm == false;
           
           if(sum(cindex) == 0)
               continue;
           end
           
           cx = li.values(cindex);
           cy = iap.values(cindex);
           
           [cc, cp] = corr(cx, cy, 'rows', 'pairwise');
           corrV(cId, dId, sId) = cc;
           corrP(cId, dId, sId) = cp;
       end
    end
end

%% Anovan
vLI  = li.values;
viap = iap.values;

%anovan(vLI(Rm == false), {Lk(Rm == false), Ck(Rm == false), Sk(Rm == false), viap(Rm == false)}, 'random', 3, 'continuous', 4, 'varnames', {'duration', 'classes', 'subject', 'alpha'}, 'model', 'full');
[P, T, STATS] = anovan(abs(vLI(Rm == false)), {Ck(Rm == false), Sk(Rm == false), viap(Rm == false)}, 'random', 2, 'continuous', 3, 'varnames', {'classes', 'subject', 'alpha'}, 'model', 'full');
