clc; clearvars; 


sublist = {'S1c', 'S2c', 'S3c', 'S4c', 'S5c', 'S6c', 'S7c', 'S8c', 'S9c', 'S10', 'S11', 'S12', 'S13', 'S14', 'S15', 'S16', 'S17'};
nsubjects = length(sublist);

features       = 'psd';
spatialfilter  = 'laplacian';
experiment     = 'sport';
% datapath       = ['D:/sauvegardePCPostdocRennes/post-doc_Camille/git/Codes/cvsa/analysis/' experiment '/' features '/' spatialfilter '/'];
datapath       = ['/mnt/data/Git/Codes/cvsa/analysis/' experiment '/' features '/' spatialfilter '/'];

Dr = [];
LI = [];
Lk = [];
Xk = [];
Ck = [];
Rm = [];
Sk = [];
Rk = [];
for sId = 1:nsubjects
    subject = sublist{sId};

    filename = [datapath subject '_li_' spatialfilter '.mat'];
    util_bdisp(['[io] - Loading LI for subject ' subject ': ' filename]);
    cdata = load(filename);
    
    Lk = cat(1, Lk, cdata.li.labels.trials.Lk);
    Xk = cat(1, Xk, cdata.li.labels.trials.Xk);
    Ck = cat(1, Ck, cdata.li.labels.trials.Ck);
    Rk = cat(1, Rk, cdata.li.labels.trials.Rk);
    Rm = cat(1, Rm, cdata.li.labels.trials.Rm);
    Sk = cat(1, Sk, sId*ones(length(cdata.li.labels.trials.Lk), 1));
    
    LI = cat(2, LI, cdata.li.trials);
    Dr = cat(1, Dr, cdata.li.duration);
end


ntrials = size(LI, 2);
ML = zeros(ntrials, 1);
for trId = 1:ntrials
    cstop  = Dr(trId);
    cstart = cstop - floor(0.5*16) + 1;
    ML(trId) = mean(LI(cstart:cstop, trId));
end

TML = cell(3, 1);
TTk = cell(3, 1);
TCk = cell(3, 1);
TRm = cell(3, 1);
for lId = 1:3
    cML = zeros(floor(0.5*16*lId), ntrials);
    cCk = [];
    cRm = [];
    for trId = 1:ntrials
        cstop  = Dr(trId);
        cstart = cstop - floor(0.5*lId*16) + 1;
        cML(:, trId) = LI(cstart:cstop, trId);
        cCk = cat(1, cCk, Ck(trId)*ones(length(cstart:cstop), 1));
        cRm = cat(1, cRm, Rm(trId)*ones(length(cstart:cstop), 1));
    end
    TML{lId} = reshape(cML, [numel(cML) 1]);
    TTk{lId} = repmat(1:length(cstart:cstop), [1 ntrials]);
    TCk{lId} = cCk; 
    TRm{lId} = cRm;
end

util_bdisp('2-way ANCOVA for each duration');
stats = cell(3, 1);
comp  = cell(3, 1);
means = cell(3, 1);
for lId = 1:3
    cMl = TML{lId};
    cTk = TTk{lId};
    cCk = TCk{lId};
    cRm = TRm{lId};
    cindex = cRm == false;
    [~, ~, stats{lId}] = anovan(cML(cindex), {cTk(cindex), cCk(cindex)}, 'continuous', 1, 'varnames', {'Time', 'Class'}, 'model', 'full');
    figure;
    [comp{lId}, means{lId}] = multcompare(stats{lId});
end

%% Stats

util_bdisp('2-way repeated measure ANOVA correctness/duration');
ranov_duration = rm_anova2(ML(Rm == false), Sk(Rm == false), Xk(Rm == false), Lk(Rm == false), {'correctness', 'duration'});
disp(ranov_duration);

util_bdisp('2-way repeated measure ANOVA correctness/classLeft');
ranov_classLeft = rm_anova2(ML(Rm == false & (Ck == 769 | Ck == 774)), Sk(Rm == false & (Ck == 769 | Ck == 774)), Xk(Rm == false & (Ck == 769 | Ck == 774)), Ck(Rm == false & (Ck == 769 | Ck == 774)), {'correctness', 'classLeft'});
disp(ranov_classLeft);

util_bdisp('2-way repeated measure ANOVA correctness/classRight');
ranov_classRight = rm_anova2(ML(Rm == false & (Ck == 770 | Ck == 780)), Sk(Rm == false & (Ck == 770 | Ck == 780)), Xk(Rm == false & (Ck == 770 | Ck == 780)), Ck(Rm == false & (Ck == 770 | Ck == 780)), {'correctness', 'classRight'});
disp(ranov_classRight);

%% Correlation
runs = unique(Rk);
nruns = length(runs);
classes = [769 770 774 780];
aML = zeros(8, 4, nsubjects);
aP = zeros(8, 4, nsubjects);
for sId = 1:nsubjects
    runs = unique(Rk(Sk == 1));
    nruns = length(runs);
    for rId = 1:nruns
        for cId = 1:4
            cindex = Rk == runs(rId) & Sk == sId & Ck == classes(cId);
            
            aML(rId, cId, sId) = mean(ML(cindex));
            aP(rId, cId, sId) = 100*sum(Xk(cindex))./sum(cindex);
        end
    end 
end



% 
% LengthId = unique(Lk);
% classes  = unique(Ck);
% nclasses = length(classes);
% Results = [0 1];
% 
% for resId = 1:2
%     for cId = 1:nclasses
%         ML(Rm == false & Xk == Results(resId) & Ck == classes(cId));
%     end
% end
% 
% 
% LengthId = unique(Lk);
% classes  = unique(Ck);
% aXk = zeros(length(LengthId), length(classes));
% for lId = 1:length(LengthId)
%     for cId = 1:length(classes)
%         cindex = Lk == LengthId(lId) & Ck == classes(cId) & Rm == false;
%         aXk(lId, cId) = 100*sum(Xk(cindex))./sum(cindex);
%     end
% end
% 
% aSk = zeros(nsubjects, length(LengthId));
% for lId = 1:length(LengthId)
%     for sId = 1:nsubjects
%         cindex = Lk == LengthId(lId) & Sk == sId & Rm == false;
%         aSk(sId, lId) = 100*sum(Xk(cindex))./sum(cindex);
%     end
% end