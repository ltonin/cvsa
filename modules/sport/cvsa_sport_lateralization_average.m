clc; clearvars; 

sublist = {'S1c', 'S2c', 'S3c', 'S4c', 'S5c', 'S6c', 'S7c', 'S8c', 'S9c', 'S10', 'S11', 'S12', 'S14', 'S15', 'S16', 'S17'};
nsubjects = length(sublist);
features       = 'psd';
spatialfilter  = 'car';
experiment     = 'sport';
%datapath    = ['/mnt/data/Git/Codes/cvsa/analysis/' experiment '/' features '/' spatialfilter '/'];
datapath    = ['D:/sauvegardePCPostdocRennes/post-doc_Camille/git/Codes/cvsa/analysis/' experiment '/' features '/' spatialfilter '/'];
figdir = './';

LengthLb = {'Short', 'Medium', 'Long'};
ResponLb = {'Wrong', 'Correct'};
LengthId = [1 2 3];

LI = [];
cliend_s = [];
cliend_m = [];
cliend_l = [];
Ck = [];
Rk = [];
Lk = [];
Rm = [];
Xk = [];
startp = [];
stopp  = [];
for sId = 1:nsubjects
    subject = sublist{sId};
    filename = [datapath subject '_li_' spatialfilter '.mat'];
    util_bdisp(['[io] - Loading LI for subject ' subject ': ' filename]);
    cdata = load(filename);

    LI = cat(2, LI, cdata.li.trials);

    cliend_s = cat(2, cliend_s, cdata.li.end{1});
    cliend_m = cat(2, cliend_m, cdata.li.end{2});
    cliend_l = cat(2, cliend_l, cdata.li.end{3});
 
    Ck = cat(1, Ck, cdata.li.labels.trials.Ck);
    Rk = cat(1, Rk, cdata.li.labels.trials.Rk);
    Lk = cat(1, Lk, cdata.li.labels.trials.Lk);
    Rm = cat(1, Rm, cdata.li.labels.trials.Rm);
    Xk = cat(1, Xk, cdata.li.labels.trials.Xk);
    startp = cat(1, startp, cdata.li.events.pos.start);
    stopp  = cat(1, stopp, cdata.li.events.pos.stop);
    
end
LIEnd{1} = cliend_s;
LIEnd{2} = cliend_m;
LIEnd{3} = cliend_l;
classes = unique(Ck);
nclasses = length(classes);
cues_lb  = {'LeftUp', 'RightDown', 'LeftDown', 'RightUp'};


%% Plotting

fig1 = figure;
fig_set_position(fig1, 'All');
t = 0:0.0625:(size(LI, 1)-1)*0.0625;
subplot(4, 1, [1 3]);
hold on;
for cId = 1:nclasses
    plot(t, nanmean(LI(:, Ck == classes(cId) & Rm == false), 2));
    set(gca, 'XTickLabel', []);
end
hold off;
grid on;
%ylim([-0.5 0.2]);

plot_vline(0, '-k', 'fix');
plot_vline(1, '-k', 'cue-on');
plot_vline(1.2, '-k', 'cue-off');
plot_vline(1.7, '--k', 'min');

ylabel('[uV]');
legend(cues_lb, 'location', 'best');
title(['Grand average - LI index (' spatialfilter ')']);

subplot(4, 1, 4);
plot(t, 100 - 100*sum(isnan(LI(:, Rm == false)), 2)/size(LI(:, Rm == false), 2));
plot_vline(0, '-k');
grid on;
xlabel('Time [ms]');
ylabel('% Trials');

cfilename = [figdir '/cvsa_sport_li_average_trial_' spatialfilter '.pdf'];
fig_export(fig1, cfilename, '-pdf');

% fig1 = figure;
% t = ((0:MaxTrialDuration - 1)*32/512) - 1;
% subplot(6, 1, [1 4]);
% hold on;
% for cId = 1:nclasses
%     plot(t, nanmean(P(:, Ck == classes(cId) & Rm == false), 2));
%     set(gca, 'XTickLabel', []);
% end
% hold off;
% grid on;
% xlim([t(1) t(end)]);
% 
% % switch(spatialfilter)
% %     case 'car'
% %         ylim([0.25 0.4]);
% %     case 'laplacian'
% %         ylim([0.1 0.25]);
% % end
% plot_vline(0, '-k');
% 
% ylabel('[uV]');
% legend(cues_lb, 'Location', 'Best');
% title(['Grand average - LI index (' spatialfilter ')']);
% 
% subplot(6, 1, 6);
% plot(t, 100 - 100*sum(isnan(P(:, Rm == false)), 2)/size(P(:, Rm == false), 2));
% plot_vline(0, '-k');
% xlim([t(1) t(end)]);
% grid on;
% xlabel('Time [ms]');
% ylabel('% Trials');

fig2 = figure;
fig_set_position(fig2, 'All');
h = zeros(3, 1);
for lId = 1:length(LengthLb)
    subplot(1, 3, lId);
    t = -(0.5*lId) + 0.0625:0.0625:0;
    cindex = Lk == lId;
    cdata = LIEnd{lId};
    hold on;
    for cId = 1:nclasses
        plot(t, nanmean(cdata(:, Ck(cindex) == classes(cId) & Rm(cindex) == false), 2));
    end
    h(lId) = gca;
    hold off;
    legend(cues_lb, 'location', 'best');
    xlabel('Time [s]');
    ylabel('[uV]');
    title(LengthLb{lId});
    grid on;
end
plot_set_limits(h, 'both', 'minmax');

cfilename = [figdir '/cvsa_sport_li_average_duration_' spatialfilter '.pdf'];
fig_export(fig2, cfilename, '-pdf');

fig3 = figure;
fig_set_position(fig3, 'Top');
tnumbers = zeros(length(LengthLb), nclasses, 2);
Responses = [0 1];
NumCols = 3;
NumRows = 2;
h2 = zeros(NumRows, NumCols);
for xId = 1:2
    for lId = 1:length(LengthLb)
        subplot(NumRows, NumCols, lId + NumCols*(xId -1));
        t = -(0.5*lId) + 0.0625:0.0625:0;
        cindex = Lk == lId;
        cdata = LIEnd{lId};
        hold on;
        for cId = 1:nclasses
            plot(t, nanmean(cdata(:, Ck(cindex) == classes(cId) & Rm(cindex) == false & Xk(cindex) == Responses(xId)), 2));
            tnumbers(lId, cId, xId) = sum(Ck(cindex) == classes(cId) & Rm(cindex) == false & Xk(cindex) == Responses(xId));
        end
        h2(xId, lId) = gca;
        hold off;
        legend(cues_lb, 'location', 'best');
        xlabel('Time [s]');
        ylabel('[uV]');
        title([ResponLb{xId} '-' LengthLb{lId}]);
        grid on;
    end
end
plot_set_limits(h2(:), 'both', 'minmax')
