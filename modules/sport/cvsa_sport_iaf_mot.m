clc; clearvars; 


sublist = {'S1c', 'S2c', 'S3c', 'S4c', 'S5c', 'S6c', 'S7c', 'S8c', 'S9c', 'S10', 'S11', 'S12', 'S14', 'S15', 'S16', 'S17'};
nsubjects = length(sublist);

features       = 'psd';
spatialfilter  = 'car';
experiment     = 'sport';
%datapath       = ['/mnt/data/Git/Codes/cvsa/analysis/' experiment '/' features '/' spatialfilter '/'];
datapath    = ['D:/sauvegardePCPostdocRennes/post-doc_Camille/git/Codes/cvsa/analysis/' experiment '/' features '/' spatialfilter '/'];
figdir = './figures/sport/';
IAP = [];
Sk  = [];
Rm  = [];
Rk  = [];
Xk  = [];

for sId = 1:nsubjects
    subject = sublist{sId};

    filename = [datapath subject '_iap_' spatialfilter '.mat'];
    util_bdisp(['[io] - Loading IAP slope for subject ' subject ': ' filename]);
    cdata = load(filename);
    
    IAP = cat(1, IAP, cdata.iap.values);
    cnvalues = length(cdata.iap.values);
    
    Sk = cat(1, Sk, sId*ones(cnvalues, 1));
    Rm = cat(1, Rm, cdata.iap.label.Rm);
    Rk = cat(1, Rk, cdata.iap.label.Rk);
    Xk = cat(1, Xk, cdata.iap.label.Xk);
end

Dk(Rk <= 4) = 1;
Dk(Rk >= 5) = 2;
Dk = Dk';
Days = unique(Dk);
ndays = length(Days);

Runs = unique(Rk);
nruns = length(Runs);

%% Normalize the IAP over runs and Subjects
nIAP = zeros(size(IAP));
for sId = 1:nsubjects
    for dId = 1:ndays
        cindex = Sk == sId  & Dk == Days(dId) & Rm == false;
        idx = find(cindex);
        ciap = IAP(cindex);
        nIAP(idx) = (ciap - min(ciap))./(max(ciap)-min(ciap));
    end
end

%% Errors - CVSA Performance (sorry what I called "errors" in in fact perf = 1 - error - to be changed
matErrors = zeros(nsubjects, length(unique(Rk)));
for id = 1:nsubjects
    for r = 1 : length(unique(Rk))
        matErrors(id, r) = sum(Xk(Rm == false & Sk == id & Rk == r)) ./ length(Xk(Rm == false & Sk == id & Rk == r)) * 100 ;
    end
end

%% Extracting slopes (correlation) from IAP & from number of errors
IAPSlope = nan(ndays, nsubjects);
IAPSlopeOLD = nan(ndays, nsubjects);
IAPSlopeNEW = nan(ndays, nsubjects);
errorSlope = nan(ndays, nsubjects);
errorSlopeNEW = nan(ndays, nsubjects);

for sId = 1:nsubjects
   
    for dId = 1:ndays
        cindex = Sk == sId & Dk == Days(dId) & Rm == false;
        
        if sum(cindex == 0) == length(cindex)
            continue
        end
        ciap = IAP(cindex);
        ctrialId = 1:length(ciap);
        IAPSlopeOLD(dId, sId) = corr(ciap, ctrialId', 'rows', 'pairwise'); % based on correlation
        cslope = polyfit(ciap, ctrialId', 1);
        IAPSlopeNEW(dId, sId) = cslope(1);
    end
end

for sId = 1:nsubjects
   
    for dId = 1:ndays
        cindex = unique(Rk(Dk == Days(dId)));
        
        if sum(cindex == 0) == length(cindex)
            continue
        end
        cerrors = matErrors(sId, cindex);
        ctrialId = 1:length(cerrors);
        errorSlope(dId, sId) = corr(cerrors', cindex, 'rows', 'pairwise'); % based on correlation
        cslopeErr = polyfit(cerrors, cindex', 1);
        errorSlopeNEW(dId, sId) = cslopeErr(1);
    end
end

%% performance (MOT score ou CVSA task score) and expertise (level)

%load('/mnt/data/Git/Codes/cvsa/analysis/analysis/sport/MOT/scoreBallExercise.mat');
load('D:/sauvegardePCPostdocRennes/post-doc_Camille/git/Codes/cvsa/analysis/sport/MOT/scoreBallExercise.mat');
MOT = score;
Smot = MOT([1:12 14:17],:,:);
diffMOT = diff(Smot, [], 3);
diffMOTall = diff(MOT, [], 3);

level = [13 13 14 7 10 13 10 11 9 8 14 11 9 4 6 3 5];
Slevel = level([1:12 14:17]);

%% correlation analyses 

% % correlation alpha power slope / diff MOT score post-pre, session 1
% [c_SMsession1para, p_SMsession1para] = corr(diffMOT(:, 1), IAPSlopeNEW(1, :)', 'rows', 'pairwise')
% [c_SMsession1Npara, p_SMsession1Npara] = corr(diffMOT(:, 1), IAPSlopeNEW(1, :)', 'type', 'Spearman', 'rows', 'pairwise')
% 
% % correlation alpha power slope / diff MOT score post-pre, session 2
% [c_SMsession2para, p_SMsession2para] = corr(diffMOT(:, 2), IAPSlopeNEW(2, :)', 'rows', 'pairwise')
% [c_SMsession2Npara, p_SMsession2Npara] = corr(diffMOT(:, 2), IAPSlopeNEW(2, :)', 'type', 'Spearman', 'rows', 'pairwise')

% correlation alpha power slope / diff MOT score post-pre, both sessions
rdiffMOT  = cat(1, diffMOT(:, 1), diffMOT(:, 2));
rIAPSlope = cat(2, IAPSlopeOLD(1, :), IAPSlopeOLD(2, :))';
[c_SMpara, p_SMpara] = corr(rdiffMOT, rIAPSlope, 'rows', 'pairwise')
[c_SMNpara, p_SMNpara] = corr(rdiffMOT, rIAPSlope, 'type', 'Spearman', 'rows', 'pairwise')

% correlation MOT score / evolution perfs, both sessions
rdiffMOT  = cat(1, diffMOT(:, 1), diffMOT(:, 2));
rErrorSlope = cat(2, errorSlope(1, :), errorSlope(2, :))';
[c_MPpara, p_MPpara] = corr(rdiffMOT, rErrorSlope, 'rows', 'pairwise')
[c_MPNpara, p_MPNpara] = corr(rdiffMOT, rErrorSlope, 'type', 'Spearman', 'rows', 'pairwise')

% correlation alpha power slope / evolution perfs, both sessions
rIAPSlope = cat(2, IAPSlopeOLD(1, :), IAPSlopeOLD(2, :))';
rErrorSlope = cat(2, errorSlope(1, :), errorSlope(2, :))';
[c_SPpara, p_SPpara] = corr(rIAPSlope, rErrorSlope, 'rows', 'pairwise')
[c_SPNpara, p_SPNpara] = corr(rIAPSlope, rErrorSlope, 'type', 'Spearman', 'rows', 'pairwise')


% % correlation expertise / MOT (mean MOT improvememnt on both sessions)
% 
% 
% % correlation expertise / MOT (consider each session)
% [c_EMsession1para, p_EMsession1para] = corr(diffMOT(:, 1), Slevel', 'rows', 'pairwise')
% [c_EMsession1Npara, p_EMsession1Npara] = corr(diffMOT(:, 1), Slevel', 'type', 'Spearman', 'rows', 'pairwise')
% [c_EMsession2para, p_EMsession2para] = corr(diffMOT(:, 2), Slevel', 'rows', 'pairwise')
% [c_EMsession2Npara, p_EMsession2Npara] = corr(diffMOT(:, 2), Slevel', 'type', 'Spearman', 'rows', 'pairwise')
% 
% 
% 
% % correlation expertise / alpha power slope (consider each session)
[c_ESsession1para, p_ESsession1para] = corr(Slevel', IAPSlopeOLD(1, :)', 'rows', 'pairwise')
[c_ESsession1Npara, p_ESsession1Npara] = corr(Slevel', IAPSlopeOLD(1, :)', 'type', 'Spearman', 'rows', 'pairwise')
[c_ESsession2para, p_ESsession2para] = corr(Slevel', IAPSlopeOLD(2, :)', 'rows', 'pairwise')
[c_ESsession2Npara, p_ESsession2Npara] = corr(Slevel', IAPSlopeOLD(2, :)', 'type', 'Spearman', 'rows', 'pairwise')


%% Plot

% Best and worst correlation over trials
SubjectId = 11;
cindex = Rm == false & Sk == SubjectId; 
ctrialId = 1:length(IAP(cindex)); 
fig1 = figure;
plot(ctrialId, IAP(cindex), '.'); 
lsline
title([sublist(SubjectId) ' - Best correlation over trials']);
ylabel('Alpha Power');
xlabel('Trials');

SubjectId = 5;
cindex = Rm == false & Sk == SubjectId; 
ctrialId = 1:length(IAP(cindex)); 
fig2 = figure;
plot(ctrialId, IAP(cindex), '.'); 
lsline
title([sublist(SubjectId) ' - Worst correlation over trials']);
ylabel('Alpha Power');
xlabel('Trials');

% Boxplot
fig3 = figure;
boxplot(nIAP(Rm == false), Rk(Rm == false));
title('Average iap over runs (all subjects)');
xlabel('Runs');
ylabel('IAP');

% Correlation IAP & MOT
fig4 = figure;
plot(rdiffMOT, rIAPSlope, '.');
lsline;
title('Correlation IAP Slope vs. MOT diff');
xlabel('MOT diff');
ylabel('IAP Slope');

% Correlation IAP & Error
fig5 = figure;
plot(rErrorSlope, rIAPSlope, '.');
lsline;
title('Correlation IAP Slope vs. Performance Evolution');
xlabel('Performance Slope');
ylabel('IAP Slope');

% Correlation IAP & MOT
fig6 = figure;
plot(rdiffMOT, rErrorSlope, '.');
lsline;
title('Correlation Performance Evolution vs. MOT diff');
xlabel('MOT diff');
ylabel('Performance Slope');

% Boxplot
fig7 = figure;
boxplot(matErrors, unique(Rk));
title('Average performance over runs (all subjects)');
xlabel('Runs');
ylabel('performance (% correct responses)');

%% Saving figures
cfilename = [figdir '/cvsa.sport.iap.evolution.positive.' spatialfilter '.pdf'];
fig_export(fig1, cfilename, '-pdf');

cfilename = [figdir '/cvsa.sport.iap.evolution.negative.' spatialfilter '.pdf'];
fig_export(fig2, cfilename, '-pdf');

cfilename = [figdir '/cvsa.sport.iap.evolution.boxplot.' spatialfilter '.pdf'];
fig_export(fig3, cfilename, '-pdf');

cfilename = [figdir '/cvsa.sport.iap.mot.correlation.' spatialfilter '.pdf'];
fig_export(fig4, cfilename, '-pdf');

cfilename = [figdir '/cvsa.sport.iap.perf.correlation.' spatialfilter '.pdf'];
fig_export(fig5, cfilename, '-pdf');

cfilename = [figdir '/cvsa.sport.perf.mot.correlation.' spatialfilter '.pdf'];
fig_export(fig6, cfilename, '-pdf');

cfilename = [figdir '/cvsa.sport.perf.evolution.boxplot.' spatialfilter '.pdf'];
fig_export(fig7, cfilename, '-pdf');
