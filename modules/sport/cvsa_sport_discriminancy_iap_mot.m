clc; clearvars; 


sublist = {'S1c', 'S2c', 'S3c', 'S4c', 'S5c', 'S6c', 'S7c', 'S8c', 'S9c', 'S10', 'S11', 'S12', 'S14', 'S15', 'S16', 'S17'};
nsubjects = length(sublist);

features       = 'psd';
spatialfilter  = 'laplacian';
experiment     = 'sport';
%datapath       = ['/mnt/data/Git/Codes/cvsa/analysis/' experiment '/' features '/' spatialfilter '/'];
datapath       = ['D:/sauvegardePCPostdocRennes/post-doc_Camille/git/Codes/cvsa/analysis/' experiment '/' features '/' spatialfilter '/'];
figdir = './figures/sport/';
IAP = [];
Sk  = [];
Rm  = [];
Rk  = [];

%% Loading iap
for sId = 1:nsubjects
    subject = sublist{sId};

    filename = [datapath subject '_iap_' spatialfilter '.mat'];
    util_bdisp(['[io] - Loading IAP slope for subject ' subject ': ' filename]);
    cdata = load(filename);
    
    IAP = cat(1, IAP, cdata.iap.values);
    cnvalues = length(cdata.iap.values);
    
    Sk = cat(1, Sk, sId*ones(cnvalues, 1));

    Rm = cat(1, Rm, cdata.iap.label.Rm);
    Rk = cat(1, Rk, cdata.iap.label.Rk);
    
end

iap.values = IAP;
iap.labels.Sk = Sk;
iap.labels.Rm = Rm;
iap.labels.Rk = Rk;

%% Loading Discriminancy
FS = [];
Sk = [];
for sId = 1:nsubjects
    subject = sublist{sId};

    filename = [datapath subject '_discriminancy_' spatialfilter '.mat'];
    util_bdisp(['[io] - Loading Discriminancy for subject ' subject ': ' filename]);
    cdata = load(filename);
    
    FS = cat(2, FS, cdata.discriminancy.values);
    
    Sk = cat(1, Sk, sId*ones(size(cdata.discriminancy.values, 2), 1));
    
end

Combinations = cdata.discriminancy.combinations;
ncombinations = length(Combinations);

%% Evolution of discriminancy per subject
CorrV = zeros(nsubjects, ncombinations);
CorrP = zeros(nsubjects, ncombinations);
for sId = 1:nsubjects
    for cmId = 1:ncombinations
        cdata = FS(:, Sk == sId, cmId)./repmat(max(FS(:, Sk == sId, cmId)), [size(FS, 1) 1]); 
        index = 1:8;
        [c, p] = corr(index', mean(cdata)', 'rows', 'pairwise');
        CorrV(sId, cmId) = c;
        CorrP(sId, cmId) = p;
    end
end


%% Pre-post discriminancy
DiffD = zeros(2, nsubjects, ncombinations);
for sId = 1:nsubjects
    for cmId = 1:ncombinations
        cdata = FS(:, Sk == sId, cmId)./repmat(max(FS(:, Sk == sId, cmId)), [size(FS, 1) 1]); 
        DiffD(1, sId, cmId) = mean(mean(cdata(:, 4))) - mean(mean(cdata(:, 1)));
        DiffD(2, sId, cmId) = mean(mean(cdata(:, 8))) - mean(mean(cdata(:, 5)));
    end
end