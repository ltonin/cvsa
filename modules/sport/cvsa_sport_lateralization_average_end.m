clc; clearvars; 

sublist = {'S1c', 'S2c', 'S3c', 'S4c', 'S5c', 'S6c', 'S7c', 'S8c', 'S9c', 'S10', 'S11', 'S12', 'S13', 'S14', 'S15', 'S16', 'S17'};
nsubjects = length(sublist);
features       = 'psd';
spatialfilter  = 'laplacian';
experiment     = 'sport';
%datapath    = ['/mnt/data/Git/Codes/cvsa/analysis/' experiment '/' features '/' spatialfilter '/'];
datapath    = ['D:/sauvegardePCPostdocRennes/post-doc_Camille/git/Codes/cvsa/analysis/' experiment '/' features '/' spatialfilter '/'];

BeforeTargetPeriod = 0.5; %[s]
AfterTargetPeriod  = 0.0;
wshift             = 0.0625;

LI = [];
Ck = [];
Rk = [];
Rm = [];
startp = [];
stopp  = [];
for sId = 1:nsubjects
    subject = sublist{sId};
    filename = [datapath subject '_liend_' spatialfilter '.mat'];
    util_bdisp(['[io] - Loading LI END for subject ' subject ': ' filename]);
    cdata = load(filename);

    LI = cat(1, LI, cdata.liend.overall);
    Ck = cat(1, Ck, cdata.liend.labels.trials.Ck);
    Rk = cat(1, Rk, cdata.liend.labels.trials.Rk);
    Rm = cat(1, Rm, cdata.liend.labels.trials.Rm);
    startp = cat(1, startp, cdata.liend.events.pos.start);
    stopp  = cat(1, stopp, cdata.liend.events.pos.stop);

end

idshort = ((stopp - startp)*wshift <= 2.2);
idlong = ((stopp - startp)*wshift >= 2.7);

classes = unique(Ck);
nclasses = length(classes);
cues_lb  = {'LeftUp', 'RightDown', 'LeftDown', 'RightUp'};

%% Extract epochs
MaxTrialDuration = (BeforeTargetPeriod+AfterTargetPeriod)/wshift;
ntrials = length(Ck);
LITrials = nan(MaxTrialDuration, ntrials);
for trId = 1:ntrials
    ctarget = stopp(trId);
    cstop  = ctarget + AfterTargetPeriod/wshift;
    cstart = ctarget - BeforeTargetPeriod/wshift;
    cdur   = length(cstart:cstop);
    LITrials(1:cdur, trId) = LI(cstart:cstop);
end

% BaseLine = repmat(nanmean(LITrials(1:16, :), 1), [size(LITrials, 1) 1]);

P = LITrials;

%% Combinations
combs = nchoosek(1:nclasses, 2);
h = size(P, size(combs, 1));
p = size(P, size(combs, 1));
for cmId = 1:size(combs, 1)
    for sId = 1:size(P, 1)
        [p(sId, cmId), h(sId, cmId)] = ranksum(P(sId, Ck == classes(combs(cmId, 1)) & Rm == false), P(sId, Ck == classes(combs(cmId, 2)) & Rm == false));
    end
end

%% Plotting
fig1 = figure;
t = -BeforeTargetPeriod:wshift:AfterTargetPeriod;
IdTrials = idshort;
subplot(6, 1, [1 4]);
hold on;
for cId = 1:nclasses
    plot(t, nanmean(P(:, Ck == classes(cId) & Rm == false & IdTrials), 2));
    set(gca, 'XTickLabel', []);
end
hold off;
grid on;
xlim([t(1) t(end)]);

% switch(spatialfilter)
%     case 'car'
%         ylim([0.25 0.4]);
%     case 'laplacian'
%         ylim([0.1 0.25]);
% end
plot_vline(0, '-k');

ylabel('[uV]');
legend(cues_lb, 'Location', 'Best');
title(['Grand average - LI index (' spatialfilter ')']);
co=get(gca,'ColorOrder');
cmblb = {'LU-RD', 'LU-LD', 'LU-RU', 'RD-LD', 'RD-RU', 'LD-RU'};
subplot(6, 1, 5);
hold on;
lgId = [];
hp = zeros(size(combs, 1), 1);
for cmId = 1:size(combs, 1)
    ccolor = co(cmId, :);
    cind = h(:, cmId) == 0;
    cval = cmId*h(:, cmId);
    cval(cind) = nan;
    if(isequal(length(cval), sum(isnan(cval))) == false)
        lgId = [lgId cmId];
        plot(t, cval, '*', 'Color', ccolor);
        set(gca, 'XTickLabel', []);
    end
    
    xlim([t(1) t(end)]);        
end
legend(cmblb{lgId}, 'Location', 'Best');
hold off;
plot_vline(0, '-k');
grid on;
ylabel('H');

subplot(6, 1, 6);
plot(t, 100 - 100*sum(isnan(P(:, Rm == false & IdTrials)), 2)/size(P(:, Rm == false & IdTrials), 2));
plot_vline(0, '-k');
xlim([t(1) t(end)]);
grid on;
xlabel('Time [ms]');
ylabel('% Trials');


