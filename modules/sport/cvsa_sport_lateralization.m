clc; clearvars; 
% 
subject     = 'S4c';
% subject     = 'S16';

features       = 'psd';
spatialfilter  = 'car';
experiment     = 'sport';

% RoIs
roi_l = {'P7', 'P5', 'P3', 'P1', 'PO7', 'PO3', 'O1'};
roi_r = {'P8', 'P6', 'P4', 'P2', 'PO8', 'PO4', 'O2'};
[~, labels] = proc_get_montage('eeg.inria.32.cvsa');
roi_id_l = proc_get_channel(roi_l, labels);
roi_id_r = proc_get_channel(roi_r, labels);

% Frequencies
SelFreqs = 8:14;

% Events
fixation = 768;
cues_c   = [769 770 780 774];
cues_x   = [28 29 30 31];
target   = [778 779];
beep     = 33282;
respons  = [897 898]; %CAM

cues_lb  = {'LeftUp', 'RightDown', 'RightUp', 'LeftDown'};

% Datafiles
%datapath    = ['/mnt/data/Git/Codes/cvsa/analysis/' experiment '/' features '/' spatialfilter '/'];
datapath    = ['D:/sauvegardePCPostdocRennes/post-doc_Camille/git/Codes/cvsa/analysis/' experiment '/' features '/' spatialfilter '/'];
[Files, NumFiles] = util_getfile(datapath, '.mat', ['covert-attention*' subject]);

% Concatenate data
[F, analysis] = cvsa_utilities_cat_eeg_inria(Files);

nsamples     = size(F, 1);
nfrequencies = size(F, 2);
nchannels    = size(F, 3);
events = analysis.event;

[~, SelFreqId ] = intersect(analysis.processing.f, SelFreqs);

%% Extract events
[~, evtfix] = proc_get_event2(fixation, nsamples, events.POS, events.TYP, 1);
[~, evtcue] = proc_get_event2([cues_c cues_x], nsamples, events.POS, events.TYP, 1);
[~, evttrg] = proc_get_event2(target, nsamples, events.POS, events.TYP, 1);
[~, evtbep] = proc_get_event2(beep, nsamples, events.POS, events.TYP, 1);
[~, evtcor] = proc_get_event2(respons, nsamples, events.POS, events.TYP, 1); %CAM

startpos = evtfix.POS;
stoppos  = evttrg.POS;
cuepos   = evtcue.POS;
cuetyp   = evtcue.TYP;
correct  = evtcor.TYP; %CAM

% Convert + and x cue to the same values
for cId = 1:length(cues_x)
    cuetyp(cuetyp == cues_x(cId)) = cues_c(cId);
end
classes = unique(cuetyp);
nclasses = length(classes);

if(isequal(length(startpos), length(stoppos)) == false)
    error('chk:evt', 'Different number of fixation and target events');
end

if(isequal(length(startpos), length(cuepos)) == false)
    error('chk:evt', 'Different number of fixation and cue events');
end

LengthLb = {'Short', 'Medium', 'Long'};
LengthId = [1 2 3];

ntrials = length(startpos);
Tk = zeros(nsamples, 1);
Lk = zeros(ntrials, 1);
Rk = zeros(ntrials, 1);
Xk = false(ntrials, 1); %CAM correctness of the response
Ak = false(ntrials, 1);
Bk = false(ntrials, 1);

for trId = 1:ntrials
   cstart = startpos(trId);
   cstop  = stoppos(trId);
   
   % Trial labels
   Tk(cstart:cstop) = cuetyp(trId);
   
   % Trial lenght
   cduration = cstop - cstart + 1;
   ctime = cduration/16;
   if(ctime < 2.2)
       Lk(trId) = 1;
   elseif(ctime > 2.7)
       Lk(trId) = 3;
   else
       Lk(trId) = 2;
   end
   
   % Correctness of the response
   if correct(trId) == 897
       Xk(trId) = true;
   end
       
   % Runs
   Rk(trId) = unique(analysis.label.Rk(cstart:cstop));
   
   % Rejection
   Ak(trId) = isempty(find(analysis.event.ART >= cstart & analysis.event.ART <= cstop, 1)) == false;
   cidx = diff(evtbep.POS(evtbep.POS >=  cstart & evtbep.POS <= cstop));
   Bk(trId) = sum(cidx == 0 | cidx == 1) > 0;
end

RmIdx = cvsa_sport_trial_removal(subject, Rk);

RemovedId = Ak | Bk | RmIdx;
disp(['Percentage removed trials: ' num2str(100*sum(RemovedId)./length(RemovedId)) '%']);
% RemovedId = false(ntrials, 1);    

%% Computing lateralization
Fl = log(F);
%Fl = F;
U(:, 1) = squeeze(mean(mean(Fl(:, SelFreqId, roi_id_l), 2), 3));
U(:, 2) = squeeze(mean(mean(Fl(:, SelFreqId, roi_id_r), 2), 3));

% Lateralization (right - left)
power_l = U(:, 1);
power_r = U(:, 2);

LIr = (power_r - power_l); %./mean([power_r power_l], 2);

% Computing baselines over the whole data (trial by trial)
LIb = zeros(size(LIr, 1), 1);
for trId = 1:ntrials
    bstart = startpos(trId) + floor(0.25*16);
    bstop  = bstart + floor(0.5*16);
    
    cstart = startpos(trId);
    cstop  = stoppos(trId);
    
    cbaseline = squeeze(mean(LIr(bstart:bstop), 1));
    rbaseline = repmat(cbaseline, [length(cstart:cstop) 1]);
    
    LIb(cstart:cstop) = rbaseline;
end

LI = LIr - LIb;



% % Baseline
% B = zeros(size(U));
% RB = zeros(size(U, 1), 1);
% for trId = 1:ntrials
%     bstart = startpos(trId) + floor(0.25*16);
%     bstop  = bstart + floor(0.5*16);
%    
%     cstart = startpos(trId);
%     cstop  = stoppos(trId);
%     baseline = squeeze(mean(U(bstart:bstop, :), 1));
%     rbaseline = repmat(baseline, [length(cstart:cstop) 1]);
%    
%     B(cstart:cstop, :) = rbaseline;
% 
%     RB(cstart:cstop, :) = RemovedId(trId);
% end
% keyboard
% 
% % Average baseline
% Ba = repmat(squeeze(mean(B(Tk > 0 & RB == 0, :), 1)), [length(Tk) 1]);
% 
% A = U-Ba;

% power_l = A(:, 1);
% power_r = A(:, 2);
% 
% LI = (power_r - power_l); %./mean([power_r power_l], 2);


%% Epoch extraction
MaxTrialDuration = max(stoppos - startpos)+1;
LITrials = nan(MaxTrialDuration, ntrials);

TrialDur = zeros(ntrials, 1);
for trId = 1:ntrials
   cstart = startpos(trId);
   cstop  = stoppos(trId);
   cdur   = cstop-cstart + 1;
   TrialDur(trId) = cdur;
   LITrials(1:cdur, trId) = LI(cstart:cstop);
end

T = cell(3, 1);
for lId = 1:length(LengthLb)
    cindex = Lk == lId;
    
    cli = LITrials(:, cindex);
    cdur  = TrialDur(cindex);
    
    cdata = zeros(floor(0.5*lId*16), sum(cindex));
    for trId = 1:size(cdata, 2)
        cstop  = cdur;
        cstart = cstop - floor(0.5*lId*16) +1;
        cdata(:, trId) = cli(cstart:cstop, trId); 
    end

    T{lId} = cdata;   
end


%% Saving
li.overall = LI;
li.trials  = LITrials;
li.end     = T;
li.length.label = LengthLb;
li.length.id    = LengthId;
li.duration     = TrialDur;
li.labels.trials.Ck = cuetyp;
li.labels.trials.Rm = RemovedId;
li.labels.trials.Rk = Rk;
li.labels.trials.Lk = Lk;
li.labels.trials.Xk = Xk;
li.labels.overall.Tk = Tk;
li.classes.id = classes;
li.classes.lb = cues_lb;
li.subject = subject;
li.events.pos.start = startpos;
li.events.pos.stop  = stoppos;

targetfile = [datapath '/' subject '_li_' spatialfilter '.mat'];
util_bdisp(['[out] - Saving analysis in: ' targetfile]);
save(targetfile, 'li');

%% Plotting
fig1 = figure;

t = 0:0.0625:(MaxTrialDuration-1)*0.0625;
subplot(4, 1, [1 3]);
hold on;
for cId = 1:nclasses
    plot(t, nanmean(LITrials(:, cuetyp == classes(cId) & RemovedId == false), 2));
    set(gca, 'XTickLabel', []);
end
hold off;
grid on;
%ylim([-0.5 0.2]);

plot_vline(0, '-k', 'fix');
plot_vline(1, '-k', 'cue_on');
plot_vline(1.2, '-k', 'cue_off');
plot_vline(1.7, '--k', 'min');

ylabel('[uV]');
legend(cues_lb);
title([subject ' - LI index']);

subplot(4, 1, 4);
plot(t, 100 - 100*sum(isnan(LITrials(:, RemovedId == false)), 2)/size(LITrials(:, RemovedId == false), 2));
plot_vline(0, '-k');
grid on;
xlabel('Time [ms]');
ylabel('% Trials');

fig2 = figure;
fig_set_position(fig2, 'Top');
h = zeros(3, 1);
for lId = 1:length(LengthLb)
    subplot(1, 3, lId);
    t = -(0.5*lId) + 0.0625:0.0625:0;
    cindex = Lk == lId;
    cdata = T{lId};
    hold on;
    for cId = 1:nclasses
        plot(t, nanmean(cdata(:, cuetyp(cindex) == classes(cId) & RemovedId(cindex) == false), 2));
    end
    h(lId) = gca;
    hold off;
    legend(cues_lb);
    xlabel('Time [s]');
    ylabel('[uV]');
    title(LengthLb{lId});
    grid on;
end
plot_set_limits(h, 'both', 'minmax')



