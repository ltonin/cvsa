clc; clearvars; 


sublist = {'S1c', 'S3c', 'S4c', 'S5c', 'S6c', 'S7c', 'S8c', 'S9c', 'S10', 'S11', 'S12', 'S13', 'S14', 'S15', 'S16', 'S17'};
nsubjects = length(sublist);

features       = 'psd';
spatialfilter  = 'car';
experiment     = 'sport';
%datapath       = ['/mnt/data/Git/Codes/cvsa/analysis/' experiment '/' features '/' spatialfilter '/'];
datapath       = ['D:/sauvegardePCPostdocRennes/post-doc_Camille/git/Codes/cvsa/analysis/' experiment '/' features '/' spatialfilter '/'];

SPerf = [];
SMark = [];
for sId = 1:nsubjects
    subject = sublist{sId};

    filename = [datapath subject '_li_' spatialfilter '.mat'];
    util_bdisp(['[io] - Loading LI for subject ' subject ': ' filename]);
    cdata = load(filename);

    LengthLb = cdata.li.length.label;
    tlenghts = cdata.li.length.id;
    nlengths = length(tlenghts);

    LI = cdata.li.trials;
    Ck = cdata.li.labels.trials.Ck;
    Lk = cdata.li.labels.trials.Lk;
    Rk = cdata.li.labels.trials.Rk;
    Xk = cdata.li.labels.trials.Xk;
    Rm = cdata.li.labels.trials.Rm;
    TrialDur = cdata.li.duration;
    classes = unique(Ck);
    classes_lb  = {'LeftUp', 'RightDown', 'LeftDown', 'RightUp'};
    nclasses = length(classes);
    Runs = unique(Rk);
    nruns = length(Runs);
    a = 0;
    LIMarker   = zeros(nlengths, nclasses, nruns);
    PerfMarker = zeros(nlengths, nclasses, nruns);
    for rId = 1:nruns
        cRk = Rk == Runs(rId);
        for lId = 1:nlengths
            cLk = Lk == tlenghts(lId);
            for cId = 1:nclasses
                cCk = Ck == classes(cId);
                cOk = Rm == false;

                cindex = cCk & cLk & cOk & cRk;
                cdur   = TrialDur(cindex);

                cli = LI(:, cindex);
                cli_marker = zeros(size(cli, 2), 1);
                for trId = 1:size(cli, 2)
                    cstop  = cdur(trId);
                    cstart = cstop - floor(0.5*16) + 1;
                    cli_marker(trId) = mean(cli(cstart:cstop, trId));
                end

                LIMarker(lId, cId, rId) = mean(cli_marker);
                a = a + sum(cindex);

                PerfMarker(lId, cId, rId) = 100*sum(Xk(cindex))./sum(cindex);
            end
        end

    end
    SMark = cat(3, SMark, LIMarker);
    SPerf = cat(3, SPerf, PerfMarker);
    
end


c = zeros(3, 4);
p = zeros(3, 4);

for i = 1:3, 
    subplot(1, 3, i); 
    plot(squeeze(SMark(i, :, :))', squeeze(SPerf(i, :, :))', 'o'); 
    lsline; 
    legend(classes_lb); 
    
    for cId = 1:4
        [c(i, cId), p(i, cId)] = corr(squeeze(SMark(i, cId, :)), squeeze(SPerf(i, cId, :)), 'rows', 'pairwise'); 
    end; 
end