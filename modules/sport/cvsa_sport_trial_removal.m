function RmIdx = cvsa_sport_trial_removal(subject, Rk) 

    ntrials = length(Rk);
    runs    = unique(Rk);
    nruns   = max(runs);
    RmIdx   = false(ntrials, 1);

    switch(subject)
        case 'S1c'
            RmIdx(Rk <= 4) = true;
        case 'S2c'
            RmIdx(Rk <= 4) = true;
        case 'S3c'
%             % enormous artefat at 2.30, removing everything (to be checked
%             % manually)
%             RmIdx(:) = true;
        case 'S4c'
            RmIdx(Rk == 7) = true;
        case 'S5c'
            % nothing
        case 'S6c'
        case 'S7c'
        case 'S8c'
            RmIdx(Rk >= 5) = true;
        case 'S9c'
        case 'S10'
        case 'S11'
        case 'S12'
        case 'S13'
            RmIdx = true;
        case 'S14'
        case 'S15'
            RmIdx(Rk <= 4) = true;
        case 'S16'
            RmIdx(Rk <= 4) = true;
        case 'S17'
        otherwise
            error('chk:sb', ['Unknown rules for removing trials for subject ' subject])
    end


end