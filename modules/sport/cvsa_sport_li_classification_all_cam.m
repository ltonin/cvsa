clearvars; clc;

sublist = {'S1c', 'S2c', 'S3c', 'S4c', 'S5c', 'S6c', 'S7c', 'S8c', 'S9c', 'S10', 'S11', 'S12', 'S14', 'S15', 'S16', 'S17'};

nsubjects = length(sublist);

features       = 'psd';
spatialfilter  = 'car';
experiment     = 'sport';
%datapath       = ['/mnt/data/Git/Codes/cvsa/analysis/' experiment '/' features '/' spatialfilter '/'];
datapath       = ['D:/sauvegardePCPostdocRennes/post-doc_Camille/git/Codes/cvsa/analysis/' experiment '/' features '/' spatialfilter '/'];
figdir = './figures/sport/';

%NumSelFeatures = 10;

Rk = [];
Sk = [];
Acc = [];
for sId = 1:nsubjects
    
    filename = [datapath '/' sublist{sId} '_li_classification_' spatialfilter '.mat'];
    
    cdata = load(filename);
    
    Acc = cat(1, Acc, cdata.acc);
    Sk = cat(1, Sk, sId*ones(size(cdata.acc, 1), 1));
    Rk = cat(1, Rk, (1:8)');
    
    
end

MAcc = zeros(nsubjects, 2, 3);

for lId = 1:3
    for sId = 1:nsubjects
        
        MAcc(sId, 1, lId) = nanmedian(Acc(Sk == sId & Rk <= 4, lId));
        MAcc(sId, 2, lId) = nanmedian(Acc(Sk == sId & Rk >= 5, lId));


    end
end

a = zeros(3, nsubjects); 
for i = 1:16, 
    a(:, i) = nanmedian(Acc(Sk == i & Rk <= 4, :)); 
end

b = zeros(3, nsubjects); 
for i = 1:16, 
    b(:, i) = nanmedian(Acc(Sk == i & Rk >= 5, :)); 
end

c = zeros(3, nsubjects); 
for i = 1:16, 
    c(:, i) = nanmedian(Acc(Sk == i, :)); 
end

fig1 = figure;
fig_set_position(fig1, 'All');
subplot(2, 2, 1); 
bar(a'); 
plot_hline(0.6, '-k');
grid on;
ylim([0 0.8]);
xlabel('subject');
ylabel('%');
title('Session 1');
legend('short', 'medium', 'long', 'location', 'SouthEast');


subplot(2, 2, 2); 
bar(b'); 
plot_hline(0.6, '-k');
grid on;
ylim([0 0.8]);
xlabel('subject');
ylabel('%');
title('Session 2');
legend('short', 'medium', 'long', 'location', 'SouthEast');

subplot(2, 2, [3 4]); 
bar(c');
plot_hline(0.6, '-k');
grid on;
ylim([0 0.8]);
xlabel('subject');
ylabel('%');
title('All sessions');

suptitle(['Single trial accuracy' ]);
legend('short', 'medium', 'long', 'location', 'SouthEast');

%% Saving figures
cfilename = [figdir '/cvsa.sport.classification.li.' spatialfilter '.pdf'];
fig_export(fig1, cfilename, '-pdf');

% %% Loading iap
% IAP = [];
% Sk = [];
% Rm = [];
% Rk = [];
% for sId = 1:nsubjects
%     subject = sublist{sId};
% 
%     filename = [datapath subject '_iap_' spatialfilter '.mat'];
%     util_bdisp(['[io] - Loading IAP slope for subject ' subject ': ' filename]);
%     cdata = load(filename);
%     
%     IAP = cat(1, IAP, cdata.iap.values);
%     cnvalues = length(cdata.iap.values);
%     
%     Sk = cat(1, Sk, sId*ones(cnvalues, 1));
% 
%     Rm = cat(1, Rm, cdata.iap.label.Rm);
%     Rk = cat(1, Rk, cdata.iap.label.Rk);
%     
% end
% 
% iap.values = IAP;
% iap.labels.Sk = Sk;
% iap.labels.Rm = Rm;
% iap.labels.Rk = Rk;
% 
% Dk(Rk <= 4) = 1;
% Dk(Rk >= 5) = 2;
% Dk = Dk';
% Days = unique(Dk);
% ndays = length(Days);
% 
% Runs = unique(Rk);
% nruns = length(Runs);
% %% Extracting slope (correlation) from IAP
% IAPSlope = nan(ndays, nsubjects);
% IAPSlopeOLD = nan(ndays, nsubjects);
% IAPSlopeNEW = nan(ndays, nsubjects);
% for sId = 1:nsubjects
%    
%     for dId = 1:ndays
%         cindex = Sk == sId & Dk == Days(dId) & Rm == false;
%         
%         if sum(cindex == 0) == length(cindex)
%             continue
%         end
%         ciap = iap.values(cindex);
%         ctrialId = 1:length(ciap);
%         
%         IAPSlopeOLD(dId, sId) = corr(ciap, ctrialId', 'rows', 'pairwise'); % based on correlation
%         cslope = polyfit(ciap, ctrialId', 1);
%         IAPSlopeNEW(dId, sId) = cslope(1);
%     end
% end
% 
% %% Load MOT
% load('/mnt/data/Research/cvsa/sport/MOT/scoreBallExercise.mat');
% MOT = score;
% Smot = MOT([1:12 14:17],:,:);
% diffMOT = diff(Smot, [], 3);
% diffMOTall = diff(MOT, [], 3);
% 
% level = [13 13 14 7 10 13 10 11 9 8 14 11 9 4 6 3 5];
% Slevel = level([1:12 14:17]);
% 
% rdiffMOT  = cat(1, diffMOT(:, 1), diffMOT(:, 2));
% rIAPSlope = cat(2, IAPSlopeOLD(1, :), IAPSlopeOLD(2, :))';
% rMAcc_s = cat(1, MAcc(:, 1, 1), MAcc(:, 2, 1));
% rMAcc_m = cat(1, MAcc(:, 1, 2), MAcc(:, 2, 2));
% rMAcc_l = cat(1, MAcc(:, 1, 3), MAcc(:, 2, 3));
% 
% 
% 
% [c_IAPAcc_s, p_IAPAcc_s] = corr(rIAPSlope, rMAcc_s, 'rows', 'pairwise')
% [c_IAPAcc_m, p_IAPAcc_m] = corr(rIAPSlope, rMAcc_m, 'rows', 'pairwise')
% [c_IAPAcc_l, p_IAPAcc_l] = corr(rIAPSlope, rMAcc_l, 'rows', 'pairwise')
% 
% [c_MotAcc_s, p_MotAcc_s] = corr(rdiffMOT, rMAcc_s, 'rows', 'pairwise')
% [c_MotAcc_m, p_MotAcc_m] = corr(rdiffMOT, rMAcc_m, 'rows', 'pairwise')
% [c_MotAcc_l, p_MotAcc_l] = corr(rdiffMOT, rMAcc_l, 'rows', 'pairwise')
% 
% 
