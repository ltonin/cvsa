clc; clearvars; 


sublist = {'S1c', 'S2c', 'S3c', 'S4c', 'S5c', 'S6c', 'S7c', 'S8c', 'S9c', 'S10', 'S11', 'S12', 'S14', 'S15', 'S16', 'S17'};
nsubjects = length(sublist);

features       = 'psd';
spatialfilter  = 'laplacian';
experiment     = 'sport';
% datapath       = ['D:/sauvegardePCPostdocRennes/post-doc_Camille/git/Codes/cvsa/analysis/' experiment '/' features '/' spatialfilter '/'];
datapath       = ['/mnt/data/Git/Codes/cvsa/analysis/' experiment '/' features '/' spatialfilter '/'];

FixDur   = 1.0;    % [s]
WinShift = 0.0625;
CueDur = 0.2;
ConfDur = 0.1;
EndDur = 0.5;

Sk = [];
Rm = [];
Rk = [];
IAP = [];

%% Loading iap
for sId = 1:nsubjects
    subject = sublist{sId};

    filename = [datapath subject '_iap_' spatialfilter '.mat'];
    util_bdisp(['[io] - Loading IAP slope for subject ' subject ': ' filename]);
    cdata = load(filename);
    
    IAP = cat(1, IAP, cdata.iap.values);
    cnvalues = length(cdata.iap.values);
    
    Sk = cat(1, Sk, sId*ones(cnvalues, 1));

    Rm = cat(1, Rm, cdata.iap.label.Rm);
    Rk = cat(1, Rk, cdata.iap.label.Rk);
    
end

iap.values = IAP;
iap.labels.Sk = Sk;
iap.labels.Rm = Rm;
iap.labels.Rk = Rk;

Lk = [];
Ck = [];
Rk = [];
Rm = [];
Sk = [];
Tk = [];
livalues = [];
%% Loading li
for sId = 1:nsubjects
    subject = sublist{sId};

    filename = [datapath subject '_li_alphapeak_' spatialfilter '.mat'];
    util_bdisp(['[io] - Loading LI for subject ' subject ': ' filename]);
    cdata = load(filename);
    
    % Trial based labels
    Lk = cat(1, Lk, cdata.li.labels.trials.Lk);
    Ck = cat(1, Ck, cdata.li.labels.trials.Ck);
    Rk = cat(1, Rk, cdata.li.labels.trials.Rk);
    Rm = cat(1, Rm, cdata.li.labels.trials.Rm);
    Sk = cat(1, Sk, sId*ones(length(cdata.li.labels.trials.Lk), 1));
    
    % Sample based trial label (including all trial length)
    Tk = cat(1, Tk, cdata.li.labels.overall.Tk > 0);
    
    % Sample based LI index
    livalues = cat(1, livalues, cdata.li.overall);
end

li.values = livalues;
li.labels.Ck = Ck;
li.labels.Sk = Sk;
li.labels.Tk = Tk;
li.labels.Rm = Rm;
li.labels.Lk = Lk;

%% Label fusion
Ck = li.labels.Ck;
Sk = li.labels.Sk;
Tk = li.labels.Tk;
Rm = li.labels.Rm | iap.labels.Rm;
Lk = li.labels.Lk;

%% Session labels
Dk(Rk <= 4) = 1;
Dk(Rk >= 5) = 2;
Dk = Dk';
Days = unique(Dk);
ndays = length(Days);


%% Computing standard deviation in the fixation period
ntrials = length(Ck);

fix.pos = find(diff(Tk) == 1);
fix.dur = FixDur/WinShift*ones(ntrials,1);
lidist  = cell(ntrials, 1);
for trId = 1:ntrials
    cstart = fix.pos(trId);
    cstop  = cstart + fix.dur(trId) - 1;
    cdata = li.values(cstart:cstop);
    lidist{trId} = fitdist(cdata, 'Normal');
end

%% Computing latency

cvsa.pos = fix.pos + floor(FixDur/WinShift) + floor(CueDur/WinShift);
cvsa.dur = (find(diff(Tk) == -1) -1) - cvsa.pos;
latency  = nan(ntrials, 1);
amplitude = nan(ntrials, 1);

for trId = 1:ntrials
    cstart = cvsa.pos(trId);
    cstop  = cstart + cvsa.dur(trId) -1;
    
    end_cstop  = cstop;
    end_cstart =  end_cstop - floor(EndDur/WinShift) + 1;
    
    cdata = li.values(cstart:cstop);
    cpd  = lidist{trId};
    
    if(Ck(trId) == 769 || Ck(trId) == 774)
        th = cpd.mu - 1*cpd.sigma;
        idvec = find(cdata <= th, 1);
    else
        th = cpd.mu + 1*cpd.sigma;
        idvec = find(cdata >= th, 1);
    end
    
    
    if(isempty(idvec) == false)
        
        for iid = 1:length(idvec)
            cvstart = idvec(iid);
            cvstop  = idvec(iid)+(floor(ConfDur/WinShift))-1;
            
            if(cvstop) > length(cdata)
                latency(trId) =  idvec(iid);
                break;
            end
            
            conf = cdata(cvstart:cvstop);
            
            if(Ck(trId) == 769 || Ck(trId) == 774)
               
                if(median(conf) <= th)
                    latency(trId) = idvec(iid);
                    break;
                end
            else
                if(median(conf) >= th)
                    latency(trId) = idvec(iid);
                    break;
                end
            end
        end
    end
    
    amplitude(trId) = mean(li.values(end_cstart:end_cstop));
end

%% Correlation latency over time
corrTimeV = nan(nsubjects, ndays);
corrTimeP = nan(nsubjects, ndays);
for sId = 1:nsubjects
    for dId = 1:ndays
        cindex = Sk == sId & Dk == Days(dId) & Rm == false & Lk == 2;
        
        if(sum(cindex) == 0)
            continue;
        end
        cy = latency(cindex);
        cx = (1:length(cy))';
        [cc, cp] = corr(cx, cy, 'rows', 'pairwise');
        
        corrTimeV(sId, dId) = cc;
        corrTimeP(sId, dId) = cp;
        
    end
end

%% Correlation IAP latency per subject, session
corrV = nan(nsubjects, ndays);
corrP = nan(nsubjects, ndays);
for sId = 1:nsubjects
    for dId = 1:ndays
        cindex = Sk == sId & Dk == Days(dId) & Rm == false & Lk == 2;
        
        if(sum(cindex) == 0)
            continue;
        end
        
        [cc, cp] = corr(iap.values(cindex), latency(cindex), 'rows', 'pairwise');
        
        corrV(sId, dId) = cc;
        corrP(sId, dId) = cp;
        
    end
end

%% Correlation IAP amplitude LI per subject, session and class
classes = unique(li.labels.Ck);
nclasses = length(classes);
corrAmplV = nan(nsubjects, nclasses, ndays);
corrAmplP = nan(nsubjects, nclasses, ndays);
for sId = 1:nsubjects
    for dId = 1:ndays
        for cId = 1:nclasses
            cindex = Sk == sId & Dk == Days(dId) & Rm == false & Lk == 2 & Ck == classes(cId);

            if(sum(cindex) == 0)
                continue;
            end

            [cc, cp] = corr(iap.values(cindex), amplitude(cindex), 'rows', 'pairwise');

            corrAmplV(sId, cId, dId) = cc;
            corrAmplP(sId, cId, dId) = cp;
        end
    end
end

%% STAT
anovan(latency(Rm == false), {Lk(Rm == false), Sk(Rm == false), iap.values(Rm == false)}, 'random', 2, 'continuous', 3, 'varnames', {'duration', 'subject', 'alpha'}, 'model', 'full');

%     idvec = [];
%     
%     while (isempty(idvec) == true)
%     %if(isempty(idvec) == false)
%     
%         if(Ck(trId) == 769 || Ck(trId) == 774)
%             idvec = find(cdata <= 2*cpd.mu);
%         else
%             idvec = find(cdata >= 2*cpd.mu);
%         end
%         
%         for iid = 1:length(idvec)
%             conf = cdata(idvec(iid):(idvec(iid)+(floor(ConfDur/WinShift))))
%             sig  = mean(conf)
%             if (((Ck(trId) == 769 || Ck(trId) == 774) & sig < 2*cpd.mu) | ((Ck(trId) == 770 || Ck(trId) == 780) & sig < 2*cpd.mu))
%                   cdata = cdata(idvec:);
%                   idvec = [];
%             end
%   
%         latency(trId) = id;
%         end
%     end


