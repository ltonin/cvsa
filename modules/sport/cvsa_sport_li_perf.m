% clc; clearvars; 
% 
subject     = 'S1c';

features       = 'psd';
spatialfilter  = 'car';
experiment     = 'sport';
%datapath       = ['/mnt/data/Git/Codes/cvsa/analysis/' experiment '/' features '/' spatialfilter '/'];
datapath       = ['D:/sauvegardePCPostdocRennes/post-doc_Camille/git/Codes/cvsa/analysis/' experiment '/' features '/' spatialfilter '/'];

WinShift = 0.0625;
FixDur   = 1;
CueDur   = 0.2;
EndDur   = 0.5;

%% Classifier parameters
% eegc3 style settings
eegc3.modules.smr.gau.somunits 	= [1 1]; % QDA-style
eegc3.modules.smr.gau.sharedcov = 'f'; % No difference anyway
eegc3.modules.smr.gau.epochs 	= 4;
eegc3.modules.smr.gau.mimean	= 0.01;
eegc3.modules.smr.gau.micov		= 0.001;
eegc3.modules.smr.gau.th        = 0.5;
eegc3.modules.smr.gau.terminate	= true;

%% Classes
Classes = [769 770 780 774];

%% Loading LI
filename = [datapath subject '_li_alphapeak_' spatialfilter '.mat'];
util_bdisp(['[io] - Loading LI for subject ' subject ': ' filename]);
data = load(filename);

li = data.li.overall;
nsamples = length(li);

%% Labeling
sTk = data.li.labels.overall.Tk;

Rk = data.li.labels.trials.Rk;
Rm = data.li.labels.trials.Rm;
Ck = data.li.labels.trials.Ck;
Lk = data.li.labels.trials.Lk;
Dk = zeros(length(Rk), 1);
Dk(Rk <= 4) = 1;
Dk(Rk >= 5) = 2;
ndays = length(unique(Dk));

TrialLb = sTk;
TrialLb(sTk > 0) = 1; 

trial.pos = find(diff(TrialLb) == 1);
trial.dur = find(diff(TrialLb) == -1) - trial.pos;
ntrials   = length(trial.pos);

fix.pos = trial.pos;
fix.dur = floor(FixDur/WinShift)*ones(ntrials, 1);

cvsa.pos = trial.pos + fix.dur + floor(CueDur/WinShift);
cvsa.dur = trial.dur;

cue.typ  = sTk(trial.pos + 1);

%% Extracting LI features for each trial

Fli = zeros(ntrials, 2);
% check = zeros(ntrials, 1);
for trId = 1:ntrials
    
    % Extracting the whole cvsa period
    cvsa_cstart = cvsa.pos(trId);
    cvsa_cstop  = cvsa_cstart + cvsa.dur(trId) - 1;
    cvsa_cdata  = li(cvsa_cstart:cvsa_cstop);
    
    % Extracting amplitude in the lase period
    end_cstop  = cvsa.pos(trId) + cvsa.dur(trId) - 1;
    end_cstart = end_cstop - floor(EndDur/WinShift) + 1;
    end_cdata = li(end_cstart:end_cstop);
    end_camplitude = mean(end_cdata);
    
    % Extracting latency
    fix_cstart = fix.pos(trId);
    fix_cstop  = fix_cstart + fix.dur(trId) - 1;
    fix_cdata  = li(fix_cstart:fix_cstop);
    fix_cdist  = fitdist(fix_cdata, 'Normal');
    
    cth_upper = fix_cdist.mu + 2*fix_cdist.sigma;
    cth_lower = fix_cdist.mu - 2*fix_cdist.sigma;
    
    cvsa_clatency = find((cvsa_cdata <= cth_lower) | (cvsa_cdata >= cth_upper), 1, 'first');
    
    if(isempty(cvsa_clatency) == true)
        cvsa_clatency = length(cvsa_cdata);
    end
    
%     if((cvsa_clatency == 1) || (cvsa_clatency == length(cvsa_cdata)))
%         check(trId) = true;
%     end
    
    Fli(trId, 1) = end_camplitude;
    Fli(trId, 2) = cvsa_clatency;
end