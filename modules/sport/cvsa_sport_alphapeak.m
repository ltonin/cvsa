clc; clearvars; 

subject     = 'S17';

features       = 'psd';
spatialfilter  = 'car';
experiment     = 'sport';

% RoIs
roi = {'PO7', 'PO3', 'O1', 'Oz', 'O2', 'P04', 'PO8', 'POz'};

[~, labels] = proc_get_montage('eeg.inria.32.cvsa');
roi_id = proc_get_channel(roi, labels);


% Frequency band
SelBand = 6:16;

% Events
fixation = 768;
cues_c   = [769 770 780 774];
cues_x   = [28 29 30 31];
target   = [778 779];
beep     = 33282;
respons  = [897 898]; %CAM

cues_lb  = {'LeftUp', 'RightDown', 'RightUp', 'LeftDown'};

% Datafiles
datapath    = ['D:/sauvegardePCPostdocRennes/post-doc_Camille/git/Codes/cvsa/analysis/' experiment '/' features '/' spatialfilter '/'];
[Files, NumFiles] = util_getfile(datapath, '.mat', ['covert-attention*' subject]);

% Concatenate data
[F, analysis] = cvsa_utilities_cat_eeg_inria(Files);

nsamples     = size(F, 1);
nfrequencies = size(F, 2);
nchannels    = size(F, 3);
events = analysis.event;

[~, SelFreqId ] = intersect(analysis.processing.f, SelBand);

%% Extract events
[~, evtfix] = proc_get_event2(fixation, nsamples, events.POS, events.TYP, 1);
[~, evtcue] = proc_get_event2([cues_c cues_x], nsamples, events.POS, events.TYP, 1);
[~, evttrg] = proc_get_event2(target, nsamples, events.POS, events.TYP, 1);
[~, evtbep] = proc_get_event2(beep, nsamples, events.POS, events.TYP, 1);
[~, evtcor] = proc_get_event2(respons, nsamples, events.POS, events.TYP, 1); %CAM

% Fixation period
fixstart = evtfix.POS;
fixstop  = evtcue.POS - 1;


ntrials = length(fixstart);

Fk = zeros(nsamples, 1);
Rk = zeros(ntrials, 1);
Ak = false(ntrials, 1);
Bk = false(ntrials, 1);

for trId = 1:ntrials
   cstart = fixstart(trId);
   cstop  = fixstop(trId);

   Fk(cstart:cstop) = 1;
   
   % Runs
   Rk(trId) = unique(analysis.label.Rk(cstart:cstop));
   
   % Rejection
   Ak(trId) = isempty(find(analysis.event.ART >= cstart & analysis.event.ART <= cstop, 1)) == false;
   cidx = diff(evtbep.POS(evtbep.POS >=  cstart & evtbep.POS <= cstop));
   Bk(trId) = sum(cidx == 0 | cidx == 1) > 0;
end

RmIdx = cvsa_sport_trial_removal(subject, Rk);

RemovedId = Ak | Bk | RmIdx;
disp(['Percentage removed trials: ' num2str(100*sum(RemovedId)./length(RemovedId)) '%']);
% RemovedId = false(ntrials, 1);    

%% Compute alpha peak

Fl = log(F);

U = mean(Fl(:, :, roi_id), 3);

AlphaPeaks = zeros(ntrials, 1);
AlphaValues = zeros(ntrials, 1);
for trId = 1:ntrials
    cstart = fixstart(trId);
    cstop  = fixstop(trId);
    
    cdata = mean(U(cstart:cstop, :));
    
    ndata = cdata./max(cdata);
    
    [cpeakvalue, cpeakindex] = findpeaks(ndata(SelFreqId));
    
    if isempty(cpeakindex) == false
        if length(cpeakindex) > 1
            [~, maxid] = max(cpeakvalue);
            cpeakindex = cpeakindex(maxid);
        end
        cfreqpeak = analysis.processing.f(SelFreqId(cpeakindex));
    else
        cfreqpeak = nan;
    end
    
    AlphaPeaks(trId) = cfreqpeak;
    
    
end

% GenericAlphaPeak = nanmedian(AlphaPeaks(~RemovedId));
% 
% for trId = 1:ntrials
%     cstart = fixstart(trId);
%     cstop  = fixstop(trId);
%     
%     cpeakFreqId =  AlphaPeaks(trId);
%     
%      if(isnan(cpeakFreqId))
%         cpeakFreqId = GenericAlphaPeak;
%     end
%     
%     [~, cpeakId ] = intersect(analysis.processing.f, cpeakFreqId);
%     
%     cdata = mean(U(cstart:cstop, :));
% %     ndata = cdata./sum(cdata);
% %     cdata = mean(U(cstart:cstop, :));
% %     ndata = cdata(SelFreqId)./sum(cdata(SelFreqId));
%     
%     
%     AlphaValues(trId) = mean(cdata(cpeakId-1:cpeakId+1)./max(cdata));
%     
% end
% 
% % Correlation
% [c, p] = corr(AlphaValues(~RemovedId), (1:sum(~RemovedId))', 'rows', 'pairwise');
% 
% disp(['Subject ' subject ' - Correlation over trials: ' num2str(c, 3) ' (p<' num2str(p, 3) ')']);
% 
% figure
% pAlphaValues = nan(ntrials, 1);
% pAlphaValues(RemovedId == false) = AlphaValues(RemovedId == false);
% plot(1:ntrials, pAlphaValues, '.'); 
% % plot(1:sum(~RemovedId), AlphaValues(~RemovedId), '.'); 
% title(subject);
% lsline


alphapeak.trial     = AlphaPeaks;
% alphapeak.values.trial   = AlphaValues;
alphapeak.labels.Rm = RemovedId;
alphapeak.labels.Rk = Rk;
alphapeak.band      = SelBand;

targetfile = [datapath '/' subject '_alphapeak_' spatialfilter '.mat'];
util_bdisp(['[out] - Saving analysis in: ' targetfile]);
save(targetfile, 'alphapeak')

