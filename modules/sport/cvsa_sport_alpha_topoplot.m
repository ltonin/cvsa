% clc; clearvars; 
% 
subject     = 'S1c';

features       = 'psd';
spatialfilter  = 'car';
experiment     = 'sport';

% Frequencies
SelFreqs = 8:14;

% Events
fixation = 768;
cues_c   = [769 770 780 774];
cues_x   = [28 29 30 31];
target   = [778 779];
beep     = 33282;
respons  = [897 898]; %CAM

cues_lb  = {'LeftUp', 'RightDown', 'RightUp', 'LeftDown'};

% Datafiles
%datapath    = ['/mnt/data/Git/Codes/cvsa/analysis/' experiment '/' features '/' spatialfilter '/'];
datapath    = ['D:/sauvegardePCPostdocRennes/post-doc_Camille/git/Codes/cvsa/analysis/' experiment '/' features '/' spatialfilter '/'];
[Files, NumFiles] = util_getfile(datapath, '.mat', ['covert-attention*' subject]);

% Concatenate data
[F, analysis] = cvsa_utilities_cat_eeg_inria(Files);

nsamples     = size(F, 1);
nfrequencies = size(F, 2);
nchannels    = size(F, 3);
events = analysis.event;

[~, SelFreqId ] = intersect(analysis.processing.f, SelFreqs);

%% Extract events
[~, evtfix] = proc_get_event2(fixation, nsamples, events.POS, events.TYP, 1);
[~, evtcue] = proc_get_event2([cues_c cues_x], nsamples, events.POS, events.TYP, 1);
[~, evttrg] = proc_get_event2(target, nsamples, events.POS, events.TYP, 1);
[~, evtbep] = proc_get_event2(beep, nsamples, events.POS, events.TYP, 1);
[~, evtcor] = proc_get_event2(respons, nsamples, events.POS, events.TYP, 1); %CAM

fixpos   = evtfix.POS;
trgpos   = evttrg.POS;
cuepos   = evtcue.POS;
cuetyp   = evtcue.TYP;
correct  = evtcor.TYP; %CAM


% Convert + and x cue to the same values
for cId = 1:length(cues_x)
    cuetyp(cuetyp == cues_x(cId)) = cues_c(cId);
end

classes = unique(cuetyp);
classeslb  = {'LeftUp', 'RightDown', 'LeftDown', 'RightUp'};
nclasses = length(classes);
ntrials  = length(cuepos);

Ck = zeros(ntrials, 1);
Lk = zeros(ntrials, 1);
Xk = zeros(ntrials, 1);
Rk = zeros(ntrials, 1);
Ak = zeros(ntrials, 1);
Bk = zeros(ntrials, 1);
AlphaTrial = zeros(floor(16*0.5), nchannels, ntrials);
AlphaRest  = zeros(floor(16*0.5), nchannels, ntrials);
for trId = 1:ntrials
    cstop  = trgpos(trId);
    cstart = cstop - floor(16*0.5) + 1;
   
    cstart_fix  = fixpos(trId);
    cstart_rest = cuepos(trId) - floor(16*0.75); 
    cstop_rest  = cuepos(trId) - floor(16*0.25) - 1;
    
    % Trial data
    AlphaTrial(:, :, trId) = squeeze(mean(F(cstart:cstop, SelFreqId, :), 2));
    
    % Rest data
    AlphaRest(:, :, trId)  = squeeze(mean(F(cstart_rest:cstop_rest, SelFreqId, :), 2));
    % Trial labelsalpha.events.fix = evtfix;
    Ck(trId) = cuetyp(trId);
   
    % Trial lenght: short medium long
    cduration = cstop - cstart_fix + 1;
    ctime = cduration/16;
    if(ctime < 2.2)
       Lk(trId) = 1;
    elseif(ctime > 2.7)
       Lk(trId) = 3;
    else
       Lk(trId) = 2;
    end
   
   % Correctness of the response
   if correct(trId) == 897
       Xk(trId) = true;
   end
       
   % Runs
   Rk(trId) = unique(analysis.label.Rk(cstart:cstop));
   
   % Rejection
   Ak(trId) = isempty(find(analysis.event.ART >= cstart & analysis.event.ART <= cstop, 1)) == false;
   cidx = diff(evtbep.POS(evtbep.POS >=  cstart & evtbep.POS <= cstop));
   Bk(trId) = sum(cidx == 0 | cidx == 1) > 0;
end

RmIdx = cvsa_sport_trial_removal(subject, Rk);

Rm = Ak | Bk | RmIdx;
disp(['Percentage removed trials: ' num2str(100*sum(Rm)./length(Rm)) '%']);

%% Saving
alpha.trials  = AlphaTrial;
alpha.rest    = AlphaRest;


alpha.labels.trials.Ck = Ck;
alpha.labels.trials.Rm = Rm;
alpha.labels.trials.Rk = Rk;
alpha.labels.trials.Lk = Lk;
alpha.labels.trials.Xk = Xk;
alpha.classes.id = classes;
alpha.classes.lb = cues_lb;
alpha.subject = subject;
alpha.events.fix = evtfix;
alpha.events.trg = evttrg;
alpha.events.cue = evtcue;

targetfile = [datapath '/' subject '_alpha_' spatialfilter '.mat'];
util_bdisp(['[out] - Saving analysis in: ' targetfile]);
save(targetfile, 'alpha');


%% Plotting
load('chanlocs64.mat');
LengthLb = {'Short', 'Medium', 'Long'};
nlengths = length(LengthLb);

PlotId = [1 4 3 2];
NumRows = 2;
NumCols = 2;


maplimits = [-0.5 0.5];

switch(subject)
    case 'S4c'
        maplimits = [-1.5 1.5];
    case 'S9c'
        maplimits = [-1.5 1.5];
    case 'S6c'
        maplimits = [-2.5 2.5];
    case 'S7c'
        maplimits = [-3.5 3.5];
    case 'S11'
        maplimits = [-3.5 3.5];
end
        
for lId = 1:nlengths
    figure;
    
    for cId = 1:nclasses
        subplot(NumRows, NumCols, PlotId(cId));
        cindex = Ck == classes(cId) & Rm == false & Lk == lId;
        cdata = squeeze(mean(mean(AlphaTrial(:, :, cindex), 3), 1));
        crest = squeeze(mean(mean(AlphaRest(:, :, cindex), 3), 1));
        cdata2 = cvsa_utilities_conv_channels(cdata - crest);
        topoplot(cdata2, chanlocs, 'conv', 'on', 'headrad', 'rim', 'maplimits', maplimits);
        axis image;
        title(classeslb{cId});
        colorbar;
    end    
    
    fig_export(gcf, [subject '_alpha_topoplot_' LengthLb{lId} '.pdf'], '-pdf');
end