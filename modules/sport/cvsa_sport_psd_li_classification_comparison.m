clearvars; 

sublist = {'S1c', 'S2c', 'S3c', 'S4c', 'S5c', 'S6c', 'S7c', 'S8c', 'S9c', 'S10', 'S11', 'S12', 'S14', 'S15', 'S16', 'S17'};

nsubjects = length(sublist);

features       = 'psd';
spatialfilter  = 'car';
experiment     = 'sport';
datapath       = ['/mnt/data/Git/Codes/cvsa/analysis/' experiment '/' features '/' spatialfilter '/'];
figdir = './figures/sport/';

NumSelFeatures = 2;

Rk = [];
Sk = [];
AccPSD = [];
AccLI = [];
for sId = 1:nsubjects
    
    filenamePSD = [datapath '/' sublist{sId} '_psd_classification_' num2str(NumSelFeatures) '_' spatialfilter '.mat'];
    filenameLI = [datapath '/' sublist{sId} '_li_classification_' spatialfilter '.mat'];
    cdataPSD = load(filenamePSD);
    cdataLI = load(filenameLI);
    AccPSD = cat(1, AccPSD, cdataPSD.acc);
    AccLI = cat(1, AccLI, cdataLI.acc);
    
    Sk = cat(1, Sk, sId*ones(size(cdataPSD.acc, 1), 1));
    Rk = cat(1, Rk, (1:8)');
    
end


rAccPSD = [];
rAccLI  = [];
rSk     = [];
for sId = 1:nsubjects
    caccpsd = AccPSD(Sk == sId, :);
    caccli  = AccLI(Sk == sId, :);
    rAccPSD = cat(1, rAccPSD, caccpsd(:));
    rAccLI  = cat(1, rAccLI, caccli(:));
    
    rSk = cat(1, rSk, sId*ones(length(caccpsd(:)), 1));
end

MeanAccPsd = zeros(nsubjects, 1);
MeanAccLI  = zeros(nsubjects, 1);
StdAccPsd  = zeros(nsubjects, 1);
StdAccLI   = zeros(nsubjects, 1);

for sId = 1:nsubjects
    MeanAccPsd(sId) = nanmedian(rAccPSD(rSk == sId));
    MeanAccLI(sId) = nanmedian(rAccLI(rSk == sId));
    StdAccPsd(sId) = nanstd(rAccPSD(rSk == sId))./sqrt(sum(rSk == sId));
    StdAccLI(sId) = nanstd(rAccLI(rSk == sId))./sqrt(sum(rSk == sId));
end
    
[h, p] = ttest2(rAccPSD, rAccLI);


fig1 = figure;
fig_set_position(fig1, 'Top');
a = plot_barerrors([MeanAccPsd MeanAccLI], [StdAccPsd StdAccLI]);
ylim([0 1]);
xlabel('subjects');
ylabel('Accuracy');
legend(a, ['PSD features (' num2str(NumSelFeatures) ')'], 'LI features (2)');
grid on;
plot_hline(0.5, 'k');
title(['Cross-validation single-sample accuracy - ' spatialfilter ' (p<' num2str(p) ')']);

pause(0.5)
cfilename = [figdir '/cvsa.sport.classification.psd-li.comparison.' num2str(NumSelFeatures) 'features.' spatialfilter '.pdf'];
fig_export(fig1, cfilename, '-pdf');
