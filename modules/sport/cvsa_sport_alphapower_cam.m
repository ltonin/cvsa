clc; clearvars; 
% 
subject     = 'S1c';

features       = 'psd';
spatialfilter  = 'car';
experiment     = 'sport';

% RoIs
roilb = {'PO7', 'PO3', 'O1', 'PO8', 'PO4', 'O2', 'POz', 'Oz'};
[~, labels] = proc_get_montage('eeg.inria.32.cvsa');
roiid = proc_get_channel(roilb, labels);

% Frequencies
SelFreqs = 8:14;

% Events
cues_c   = [769 770 780 774];
cues_x   = [28 29 30 31];
beep     = 33282;
respons  = [897 898]; %CAM

cues_lb  = {'LeftUp', 'RightDown', 'RightUp', 'LeftDown'};

% Datafiles
%datapath    = ['/mnt/data/Git/Codes/cvsa/analysis/' experiment '/' features '/' spatialfilter '/'];
datapath    = ['D:/sauvegardePCPostdocRennes/post-doc_Camille/git/Codes/cvsa/analysis/' experiment '/' features '/' spatialfilter '/'];
[Files, NumFiles] = util_getfile(datapath, '.mat', ['covert-attention*' subject]);

% Concatenate data
[F, analysis] = cvsa_utilities_cat_eeg_inria(Files);

nsamples     = size(F, 1);
nfrequencies = size(F, 2);
nchannels    = size(F, 3);
events = analysis.event;

[~, SelFreqId ] = intersect(analysis.processing.f, SelFreqs);

%% Extract events
[~, evtcue] = proc_get_event2([cues_c cues_x], nsamples, events.POS, events.TYP, 1);
[~, evtbep] = proc_get_event2(beep, nsamples, events.POS, events.TYP, 1);
[~, evtcor] = proc_get_event2(respons, nsamples, events.POS, events.TYP, 1); %CAM

cuepos   = evtcue.POS;
cuetyp   = evtcue.TYP;
correct  = evtcor.TYP; %CAM

% Convert + and x cue to the same values
for cId = 1:length(cues_x)
    cuetyp(cuetyp == cues_x(cId)) = cues_c(cId);
end
classes = unique(cuetyp);
nclasses = length(classes);

ntrials = length(cuepos);
Tk = zeros(nsamples, 1);
Lk = zeros(ntrials, 1);
Rk = zeros(ntrials, 1);
Xk = false(ntrials, 1); %CAM correctness of the response
Ak = false(ntrials, 1);
Bk = false(ntrials, 1);

for trId = 1:ntrials
   cstart = cuepos(trId) - floor(0.75*16);
   cstop  = cstart + floor(0.5*16) - 1;
   
   % Trial labels
   Tk(cstart:cstop) = cuetyp(trId);
   
   % Correctness of the response
   if correct(trId) == 897
       Xk(trId) = true;
   end
       
   % Runs
   Rk(trId) = unique(analysis.label.Rk(cstart:cstop));
   
   % Rejection
   Ak(trId) = isempty(find(analysis.event.ART >= cstart & analysis.event.ART <= cstop, 1)) == false;
   cidx = diff(evtbep.POS(evtbep.POS >=  cstart & evtbep.POS <= cstop));
   Bk(trId) = sum(cidx == 0 | cidx == 1) > 0;
end

RmIdx = cvsa_sport_trial_removal(subject, Rk);

RemovedId = Ak | Bk | RmIdx;
disp(['Percentage removed trials: ' num2str(100*sum(RemovedId)./length(RemovedId)) '%']);
% RemovedId = false(ntrials, 1);
%% Computing alpha power

U = squeeze(mean(mean(F(:, SelFreqId, roiid), 2), 3));


%% Epoch extraction
Duration = floor(0.5*16);
AlphaPre = nan(Duration, ntrials);

TrialDur = zeros(ntrials, 1);
for trId = 1:ntrials
   cstart = cuepos(trId) - floor(0.75*16);
   cstop  = cstart + floor(0.5*16) - 1;
  
   AlphaPre(:, trId) = U(cstart:cstop);
end


%% Saving
alphapower.overall = U;
alphapower.precue  = AlphaPre;
alphapower.labels.trials.Ck = cuetyp;
alphapower.labels.trials.Rm = RemovedId;
alphapower.labels.trials.Rk = Rk;
alphapower.labels.trials.Lk = Lk;
alphapower.labels.trials.Xk = Xk;
alphapower.labels.overall.Tk = Tk;
alphapower.classes.id = classes;
alphapower.classes.lb = cues_lb;
alphapower.subject = subject;
alphapower.events.cue   = evtcue;
alphapower.events.bep  = evtbep;
alphapower.events.cor  = evtcor;

targetfile = [datapath '/' subject '_alphapre_' spatialfilter '.mat'];
util_bdisp(['[out] - Saving analysis in: ' targetfile]);
save(targetfile, 'alphapower');





