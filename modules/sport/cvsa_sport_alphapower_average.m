clc; clearvars; 

sublist = {'S1c', 'S3c', 'S4c', 'S5c', 'S6c', 'S7c', 'S8c', 'S9c', 'S10', 'S11', 'S12'};
nsubjects = length(sublist);
features       = 'psd';
spatialfilter  = 'car';
experiment     = 'sport';
datapath    = ['/mnt/data/Git/Codes/cvsa/analysis/' experiment '/' features '/' spatialfilter '/'];

LengthLb = {'Short', 'Medium', 'Long'};
ResponLb = {'Wrong', 'Correct'};
LengthId = [1 2 3];

AlphaPre = [];

Ck = [];
Rk = [];
Lk = [];
Rm = [];
Xk = [];
Sk = [];

for sId = 1:nsubjects
    subject = sublist{sId};
    filename = [datapath subject '_alphapre_' spatialfilter '.mat'];
    util_bdisp(['[io] - Loading Alpha Pre for subject ' subject ': ' filename]);
    cdata = load(filename);
    
    AlphaPre = cat(2, AlphaPre, cdata.alphapower.precue);
    
    Sk = cat(1, Sk, sId*ones(length(cdata.alphapower.labels.trials.Ck), 1));

    Ck = cat(1, Ck, cdata.alphapower.labels.trials.Ck);
    Rm = cat(1, Rm, cdata.alphapower.labels.trials.Rm);
    Rk = cat(1, Rk, cdata.alphapower.labels.trials.Rk);
    Lk = cat(1, Lk, cdata.alphapower.labels.trials.Lk);
    Xk = cat(1, Xk, cdata.alphapower.labels.trials.Xk);
end

Asub = zeros(nsubjects, 2); 
Response = [0 1]; 

MAlphaPre = mean(AlphaPre, 1);
pval = zeros(nsubjects, 1);
for sId = 1:nsubjects
        [~, pval(sId)] = ttest2(MAlphaPre(Rm == false & Xk == 0 & Sk == sId), MAlphaPre(Rm == false & Xk == 1 & Sk == sId)); 
end