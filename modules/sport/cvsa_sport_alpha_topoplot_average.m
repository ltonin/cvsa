clc; clearvars; 


sublist = {'S1c', 'S2c', 'S3c', 'S4c', 'S5c', 'S6c', 'S7c', 'S8c', 'S9c', 'S10', 'S11', 'S12', 'S14', 'S15', 'S16', 'S17'};
nsubjects = length(sublist);

features       = 'psd';
spatialfilter  = 'car';
experiment     = 'sport';
%datapath       = ['/mnt/data/Git/Codes/cvsa/analysis/' experiment '/' features '/' spatialfilter '/'];
datapath       = ['D:/sauvegardePCPostdocRennes/post-doc_Camille/git/Codes/cvsa/analysis/' experiment '/' features '/' spatialfilter '/'];

AlphaTrial = [];
AlphaRest  = [];
Ck = [];
Rm = [];
Rk = [];
Lk = [];
Xk = [];
Sk = [];
for sId = 1:nsubjects
    subject = sublist{sId};

    filename = [datapath subject '_alpha_' spatialfilter '.mat'];
    util_bdisp(['[io] - Loading Alpha for subject ' subject ': ' filename]);
    cdata = load(filename);
    
    AlphaTrial = cat(3, AlphaTrial, cdata.alpha.trials);
    AlphaRest  = cat(3, AlphaRest, cdata.alpha.rest);

    Sk = cat(1, Sk, sId*ones(length(cdata.alpha.labels.trials.Ck), 1));

    Ck = cat(1, Ck, cdata.alpha.labels.trials.Ck);
    Rm = cat(1, Rm, cdata.alpha.labels.trials.Rm);
    Rk = cat(1, Rk, cdata.alpha.labels.trials.Rk);
    Lk = cat(1, Lk, cdata.alpha.labels.trials.Lk);
    Xk = cat(1, Xk, cdata.alpha.labels.trials.Xk);
end

classes = unique(Ck);
classeslb  = {'LeftUp', 'RightDown', 'LeftDown', 'RightUp'};
nclasses = length(classes);


%% Plotting
load('chanlocs64.mat');
LengthLb = {'Short', 'Medium', 'Long'};
nlengths = length(LengthLb);

PlotId = [1 4 3 2];
NumRows = 2;
NumCols = 2;

maplimits = [-1.5 1.5];
        
for lId = 1:nlengths
    figure;
    
    for cId = 1:nclasses
        subplot(NumRows, NumCols, PlotId(cId));
        cindex = Ck == classes(cId) & Rm == false & Lk == lId;
        cdata = squeeze(mean(mean(AlphaTrial(:, :, cindex), 3), 1));
        crest = squeeze(mean(mean(AlphaRest(:, :, cindex), 3), 1));
        cdata2 = cvsa_utilities_conv_channels(cdata - crest);
        topoplot(cdata2, chanlocs, 'conv', 'on', 'headrad', 'rim', 'maplimits', maplimits);
        axis image;
        title(classeslb{cId});
        colorbar;
    end    
    suptitle(['Trial ' LengthLb{lId}]);
    fig_export(gcf, ['grandaverage_alpha_topoplot_' LengthLb{lId} '.pdf'], '-pdf');
end