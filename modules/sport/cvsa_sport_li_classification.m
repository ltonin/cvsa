
% clc; clearvars; 
% % 
% subject     = 'S9c';

features       = 'psd';
spatialfilter  = 'car';
experiment     = 'sport';
datapath       = ['/mnt/data/Git/Codes/cvsa/analysis/' experiment '/' features '/' spatialfilter '/'];
%datapath       = ['D:/sauvegardePCPostdocRennes/post-doc_Camille/git/Codes/cvsa/analysis/' experiment '/' features '/' spatialfilter '/'];

WinShift = 0.0625;
FixDur   = 1;
CueDur   = 0.2;
EndDur   = 0.5;

%% Classifier parameters
% eegc3 style settings
eegc3.modules.smr.gau.somunits 	= [1 1]; % QDA-style
eegc3.modules.smr.gau.sharedcov = 'f'; % No difference anyway
eegc3.modules.smr.gau.epochs 	= 4;
eegc3.modules.smr.gau.mimean	= 0.01;
eegc3.modules.smr.gau.micov		= 0.001;
eegc3.modules.smr.gau.th        = 0.5;
eegc3.modules.smr.gau.terminate	= true;

%% Classes
Classes = [769 770 780 774];

%% Loading LI
filename = [datapath subject '_li_alphapeak_' spatialfilter '.mat'];
util_bdisp(['[io] - Loading LI for subject ' subject ': ' filename]);
data = load(filename);

li = data.li.overall;
nsamples = length(li);

%% Labeling
sTk = data.li.labels.overall.Tk;

Rk = data.li.labels.trials.Rk;
Rm = data.li.labels.trials.Rm;
Ck = data.li.labels.trials.Ck;
Lk = data.li.labels.trials.Lk;
Dk = zeros(length(Rk), 1);
Dk(Rk <= 4) = 1;
Dk(Rk >= 5) = 2;
ndays = length(unique(Dk));

TrialLb = sTk;
TrialLb(sTk > 0) = 1; 

trial.pos = find(diff(TrialLb) == 1);
trial.dur = find(diff(TrialLb) == -1) - trial.pos;
ntrials   = length(trial.pos);

fix.pos = trial.pos;
fix.dur = floor(FixDur/WinShift)*ones(ntrials, 1);

cvsa.pos = trial.pos + fix.dur + floor(CueDur/WinShift);
cvsa.dur = trial.dur;

cue.typ  = sTk(trial.pos + 1);

%% Extracting LI features for each trial

Fli = zeros(ntrials, 2);
% check = zeros(ntrials, 1);
for trId = 1:ntrials
    
    % Extracting the whole cvsa period
    cvsa_cstart = cvsa.pos(trId);
    cvsa_cstop  = cvsa_cstart + cvsa.dur(trId) - 1;
    cvsa_cdata  = li(cvsa_cstart:cvsa_cstop);
    
    % Extracting amplitude in the lase period
    end_cstop  = cvsa.pos(trId) + cvsa.dur(trId) - 1;
    end_cstart = end_cstop - floor(EndDur/WinShift) + 1;
    end_cdata = li(end_cstart:end_cstop);
    end_camplitude = mean(end_cdata);
    
    % Extracting latency
    fix_cstart = fix.pos(trId);
    fix_cstop  = fix_cstart + fix.dur(trId) - 1;
    fix_cdata  = li(fix_cstart:fix_cstop);
    fix_cdist  = fitdist(fix_cdata, 'Normal');
    
    cth_upper = fix_cdist.mu + 2*fix_cdist.sigma;
    cth_lower = fix_cdist.mu - 2*fix_cdist.sigma;
    
    cvsa_clatency = find((cvsa_cdata <= cth_lower) | (cvsa_cdata >= cth_upper), 1, 'first');
    
    if(isempty(cvsa_clatency) == true)
        cvsa_clatency = length(cvsa_cdata);
    end
    
%     if((cvsa_clatency == 1) || (cvsa_clatency == length(cvsa_cdata)))
%         check(trId) = true;
%     end
    
    Fli(trId, 1) = end_camplitude;
    Fli(trId, 2) = cvsa_clatency;
end

%% Create cross-validation indeces per Run in each session
CrossInd = unique(perms([1 1 1 0]), 'rows');
ncross = size(CrossInd, 1);
nrunperday = 4;

TrainIndices = false(ntrials, (ncross*ndays));
TestIndices  = false(ntrials, (ncross*ndays));
for dId = 1:ndays
    for crId = 1:ncross
        ctrainId = find(CrossInd(crId, :));
        ctestId  = find(CrossInd(crId, :) == 0);
        
        ctrainId = ctrainId + nrunperday*(dId - 1);
        ctestId  = ctestId  + nrunperday*(dId - 1);
        
        ctrain_idx = false(ntrials, 1);
        ctest_idx  = false(ntrials, 1);
        
        for i = 1:length(ctrainId)
            ctrain_idx(Rk == ctrainId(i)) = true;
        end
        
        for i = 1:length(ctestId)
            ctest_idx(Rk == ctestId(i)) = true;
        end
        
        TrainIndices(:, crId + nrunperday*(dId-1)) = ctrain_idx;
        TestIndices(:, crId + nrunperday*(dId-1))  = ctest_idx;
    end
end



%% Classification

% Convert Left-Right class labels
LRClasses = [1 2];
nlrclasses = length(LRClasses);
LeftUp      = 769;
LeftDown    = 774;
RightDown   = 770;
RightUp     = 780;
LRk = zeros(ntrials, 1);
% LRk(Ck == LeftUp  | Ck == LeftDown)  = LRClasses(1);
% LRk(Ck == RightUp | Ck == RightDown) = LRClasses(2);
% LRk(Ck == 770 | Ck == 774) = LRClasses(1);
% LRk(Ck == 769 | Ck == 780) = LRClasses(2);
LRk(Ck == LeftDown) = LRClasses(1);
LRk(Ck == RightDown) = LRClasses(2);

Gk = zeros(ntrials, (ncross*ndays));
Ak = zeros(ntrials, (ncross*ndays));
for crId = 1:(ncross*ndays)
    ctrainId = TrainIndices(:, crId);
    ctestId  = TestIndices(:, crId);
    
    if(max(ctrainId | ctestId) > 1)
        error('error');
    end
    
    % Train classifier
    cindex = ctrainId & Rm == false;
    
    if(sum(cindex) == 0)
        continue;
    end
    
    
    cP  = Fli(ctrainId & Rm == false  & LRk > 0, :);
    cPk = LRk(ctrainId & Rm == false & LRk > 0);
    cT  = Fli;
    cTk = LRk;
    
    % Train classifier
    util_bdisp('[proc] - Train classifier');
    gau = eegc3_train_gau(eegc3, cP, cPk);
    
    % Evaluate classifier
    util_bdisp('[proc] - Evaluate classifier');
    cpp = zeros(ntrials, 2);
    for trId = 1:ntrials
        [~, cpp(trId, :)] = gauClassifier(gau.M, gau.C, cT(trId, :));
    end
    
    % No need dm, one value per trial with li
    cGk = ones(ntrials, 1);
    cGk(cpp(:, 1) < 0.5) = 2;
    
    Gk(:, crId) = cGk;
    Ak(:, crId) = cTk;
end

close(60);



%% Accuracy
Durations = unique(Lk);
ndurations = length(Durations);

acc = zeros(size(Gk, 2), ndurations);
ntesting = zeros(size(Gk, 2), ndurations);
for crId = 1:size(Gk, 2)
    for lId = 1:ndurations
        cindex = TestIndices(:, crId) & Rm == false & LRk > 0 & Lk == Durations(lId);

        ntesting(crId, lId) = sum(cindex);

        acc(crId, lId) = sum(Gk(cindex, crId) == Ak(cindex, crId))./sum(cindex);
    end
end

%% Saving
targetfile = [datapath '/' subject '_li_classification_' spatialfilter '.mat'];
util_bdisp(['[out] - Saving analysis in: ' targetfile]);
save(targetfile, 'acc');

%% Plotting
figure;
subplot(1, 3, 1);
bar(acc(1:4, :));
grid on;
xlabel('Run');
ylabel('%');
title('First session - Average per run');
ylim([0 1])
plot_hline(0.6);

subplot(1, 3, 2);
bar(acc(5:8, :));
grid on;
xlabel('Run');
ylabel('%');
title('Second session - Average per run');
ylim([0 1])
plot_hline(0.6);

subplot(1, 3, 3);
bar([nanmean(acc(1:4, :)); nanmean(acc(5:8, :))]);
grid on;
xlabel('Session');
ylabel('%');
title('Average per session');
ylim([0 1])
plot_hline(0.6);

suptitle(['Subject ' subject ' - LI classification']);
