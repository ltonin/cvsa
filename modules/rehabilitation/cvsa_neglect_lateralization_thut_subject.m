clc;
clearvars; 
subject     = 'TC';
pattern     = '.va.';

features       = 'psd';
spatialfilter  = 'car';
experiment     = 'neglect';

Classes    = [1];
ClassesLb  = {'LeftCue (neglected)'}; %{'LeftCue (neglected)', 'MiddleCue'};
NumClasses = length(Classes);

%% Initialization

% Timings
timings.trial.period    = 3;            % seconds
timings.offset.period   = -1;           % seconds

% Frequencies
proc.freq.selected.range{1} = 4:6;          % Hz
proc.freq.selected.label{1} = 'theta';
proc.freq.selected.range{2} = 8:14;         % Hz
proc.freq.selected.label{2} = 'alpha';
proc.freq.selected.range{3} = 16:30;        % Hz
proc.freq.selected.label{3} = 'beta';
proc.freq.selected.range{4} = 32:48;        % Hz
proc.freq.selected.label{4} = 'gamma';
proc.freq.selected.range{4} = 6:10;        % Hz
proc.freq.selected.label{4} = 'custom';


SelectedBand   = {'custom'};


% Channels and RoIs
[~, channels.list]        = proc_get_montage('eeg.biosemi.64');
% channels.roi.left.labels  = {'P7', 'P5', 'PO7'};         % As in [Thut et al., 2006]
% channels.roi.right.labels = {'P8', 'P6', 'PO8'};         % As in [Thut et al., 2006]
channels.roi.left.labels  = {'P5', 'P3', 'P1', 'PO7', 'PO3', 'O1'};         % Custom
channels.roi.right.labels = {'P6', 'P4', 'P2', 'PO8', 'PO4', 'O2'};         % Custom
% channels.roi.left.labels  = {'F7', 'F5', 'F3', 'F1', 'FC5', 'FC3', 'FC1'};         % Custom
% channels.roi.right.labels = {'F8', 'F6', 'F4', 'F2', 'FC6', 'FC4', 'FC2'};         % Custom

channels.roi.left.id      = proc_get_channel(channels.roi.left.labels,  channels.list);
channels.roi.right.id     = proc_get_channel(channels.roi.right.labels, channels.list);
channels.roi.labels       = {'LeftRoI', 'RightRoI'};

% Datafiles
datapath    = ['/mnt/data/Git/Codes/cvsa/analysis/' experiment '/' features '/' spatialfilter '/'];
[Files, NumFiles] = util_getfile(datapath, '.mat', [subject '*' pattern]);

% Extra paths
saveroot    = '/mnt/data/Git/Codes/cvsa/';
[~, figuredir] = util_mkdir(saveroot, 'figures/lateralization/');
[~, savedir]   = util_mkdir(saveroot, 'analysis/neglect/lateralization/thut/');

analysispath = './analysis/';
%% Analysis - Import data

% Concatanate datafiles
util_bdisp(['[io] - Concatenating ' num2str(NumFiles) ' datafiles']);
[F, analysis] = cvsa_utilities_concatenate_data(Files);

% Extract information
proc.nsamples   = size(F, 1);
proc.nfreqs     = size(F, 2);
proc.nchans     = size(F, 3);
proc.samplerate = analysis.processing.fs;
proc.framesize  = analysis.processing.fsize;
proc.freq.grid  = analysis.processing.f;
for fId = 1:length(proc.freq.selected.range)
    [~, proc.freq.selected.id{fId}] = intersect(proc.freq.grid, proc.freq.selected.range{fId});
end
SelectedFreqId = proc.freq.selected.id{util_cellfind(SelectedBand, proc.freq.selected.label)};




%% Analysis - Masking data

util_bdisp('[proc] - Creating trial mask vector');
events = analysis.event;
% Convert period [in seconds] to size [in samples]
timings.trial.size  = floor(timings.trial.period*proc.samplerate/proc.framesize);
timings.offset.size = floor(timings.offset.period*proc.samplerate/proc.framesize);
timings.trial.mask  = false(proc.nsamples, 1);
timings.trial.pos   = [];
for cId = 1:NumClasses
    [cmask, cpos, cdur] = proc_get_event(events, Classes(cId), proc.nsamples, timings.trial.size, timings.offset.size);
    timings.trial.mask    = timings.trial.mask | cmask;
    timings.trial.pos     = sort([timings.trial.pos cpos']);
end

util_bdisp('[proc] - Creating trial based class labels');
index = false(length(events.TYP), 1);
for cId = 1:NumClasses
    index = events.TYP == Classes(cId) | index;
end
class.labels = events.TYP(index);

%% Extracting trials
util_bdisp('[proc] - Extracting trials');
S = F;
NumTrials = length(timings.trial.pos);
NumSamples = timings.trial.size + abs(timings.offset.size);
St = zeros(NumSamples, proc.nfreqs, proc.nchans, NumTrials);
labels.Mk = zeros(NumTrials, 1);
labels.Rk = zeros(NumTrials, 1);
labels.Dk = zeros(NumTrials, 1);
for tId = 1:NumTrials
    cstart = timings.trial.pos(tId);
    cstop  = cstart + NumSamples - 1;
    St(:, :, :, tId) = S(cstart:cstop, :, :);
    labels.Mk(tId) = unique(analysis.label.Mk(cstart:cstop));
    labels.Rk(tId) = unique(analysis.label.Rk(cstart:cstop));
    labels.Dk(tId) = unique(analysis.label.Dk(cstart:cstop));
end
Modalities    = unique(labels.Mk);
NumModalities = length(Modalities);
Runs          = unique(labels.Rk);
NumRuns       = length(Runs);


%% Analysis - Load artifacts analysis
cartifacts = load([analysispath '/neglect/artifacts/' subject '_artifacts.mat'], 'artifacts');
analysis.artifacts = cartifacts.artifacts;

ArtInd = false(length(analysis.artifacts.Ak), 1);
for cId = 1:NumClasses
    ArtInd = ArtInd | analysis.artifacts.labels.Ck == Classes(cId);
end

Ak = analysis.artifacts.Ak(ArtInd);

%% Compute Temporal spectral evolution (TSE) and Lateralization Index (LI) per RoIs ([Thut et al., 2006])
util_bdisp('[proc] - Computing ROI and lateralization index');

% Averaging across electrodes in left and right RoI
TSE = zeros(NumSamples, proc.nfreqs, 2, NumTrials);
tse_l = squeeze(mean(St(:, :, channels.roi.left.id, :), 3));
tse_r = squeeze(mean(St(:, :, channels.roi.right.id, :), 3));
TSE(:, :, 1, :) = tse_l;
TSE(:, :, 2, :) = tse_r;

% Computing LI between left and right RoI as in [Thut et al, 2006]
LI = (tse_r - tse_l)./((tse_r + tse_l)./2);

%% Plotting
util_bdisp('[plot] - Plotting results');
fig1 = figure;
fig_set_position(fig1, 'Top');



RoiNames = {'Healthy', 'Affected'};
t = timings.offset.period:proc.framesize/proc.samplerate:timings.trial.period - proc.framesize/proc.samplerate;
class.colors = {'b', 'r'};
modalities.tickness = [1 3];
modalities.styles   = {'-', '--'};

maxvalue = [];
minvalue = [];
for rId = 1:2
    for mId = 1:NumModalities
        for cId = 1:NumClasses
            values = squeeze(mean(mean(TSE(:, SelectedFreqId, rId, class.labels == Classes(cId) & labels.Mk == Modalities(mId) & ~Ak), 2), 4));
            maxvalue = max([maxvalue max(values)]);
            minvalue = min([minvalue min(values)]);
        end
    end
    
end

% TSE for Left RoI
subplot(2, 3, 1);
hold on;
for mId = 1:NumModalities
    for cId = 1:NumClasses
        values = squeeze(mean(mean(TSE(:, SelectedFreqId, 1, class.labels == Classes(cId) & labels.Mk == Modalities(mId) & ~Ak), 2), 4));
        plot(t, values, class.colors{cId}, 'LineWidth', modalities.tickness(mId), 'LineStyle', modalities.styles{mId});
    end
end
hold off;
grid on;
xlim([t(1) t(end)]);
ylim([minvalue-0.5 maxvalue + 0.5]);
xlabel('Time [s]');
ylabel('[uV]');
title('TSE - Healthy RoI (Left)');
legend(ClassesLb, 'location', 'best');
plot_vline(0, 'k', 'cue');

% Lateralization index
subplot(2, 3, 2);
hold on;
for mId = 1:NumModalities
    for cId = 1:NumClasses
        values = squeeze(mean(mean(LI(:, SelectedFreqId, class.labels == Classes(cId) & labels.Mk == Modalities(mId) & ~Ak), 2), 3));
        plot(t, values, class.colors{cId}, 'LineWidth', modalities.tickness(mId), 'LineStyle', modalities.styles{mId});
    end
end
hold off;
grid on;
xlim([t(1) t(end)]);
xlabel('Time [s]');
ylabel('[uV]');
title('Lateralization Index');
legend(ClassesLb, 'location', 'best');
plot_vline(0, 'k', 'cue');

% TSE for Right RoI
subplot(2, 3, 3);
hold on;
for mId = 1:NumModalities
    for cId = 1:NumClasses
        values = squeeze(mean(mean(TSE(:, SelectedFreqId, 2, class.labels == Classes(cId) & labels.Mk == Modalities(mId) & ~Ak), 2), 4));
        plot(t, values, class.colors{cId}, 'LineWidth', modalities.tickness(mId), 'LineStyle', modalities.styles{mId});
    end
end
hold off;
grid on;
xlim([t(1) t(end)]);
ylim([minvalue-0.5 maxvalue + 0.5]);
xlabel('Time [s]');
ylabel('[uV]')
title('TSE - Affected RoI (Right)');
legend(ClassesLb, 'location', 'best');
plot_vline(0, 'k', 'cue');

% Lateralization index over runs
subplot(2, 3, [4 5 6]);
trialstart = abs(timings.offset.size);          % discard offset
li_runs = zeros(NumRuns, NumClasses);
for rId = 1:NumRuns
    for cId = 1:NumClasses
        li_runs(rId, cId) = squeeze(mean(mean(mean(LI(trialstart:end, SelectedFreqId, class.labels == Classes(cId) & labels.Rk == Runs(rId) & ~Ak), 2), 3), 1));
    end
end
plot(Runs, li_runs, '-o');
grid on;
xlim([Runs(1) Runs(end)]);
xlabel('Run');
ylabel('[uV]')
title('Lateralization index over runs');
legend(ClassesLb, 'location', 'best');
plot_vline(1, 'k', '-> calibration');
plot_vline(labels.Rk(find(labels.Mk == 1, 1)) - 0.5, 'k', '-> online');

suptitle(['Subject ' subject]);

%% Saving figures
% figurefile = [figuredir '/' subject '_lateralization.pdf'];
% util_bdisp(['[out] - Saving figures in: ' figurefile]);
% fig_figure2pdf(fig1, figurefile, [], '-fillpage'); 

%% Saving analysis
% targetfile = [savedir '/' subject '_lateralization_' spatialfilter '_' char(SelectedBand) '.mat'];
% util_bdisp(['[out] - Saving analysis in: ' targetfile]);
% analysis.lateralization.class                = class;
% analysis.lateralization.channels             = channels;
% analysis.lateralization.timings              = timings;
% analysis.lateralization.labels               = labels;
% analysis.lateralization.proc                 = proc;
% analysis.lateralization.config.subject       = subject;
% analysis.lateralization.config.features      = features;
% analysis.lateralization.config.spatialfilter = spatialfilter;
% 
% save(targetfile, 'TSE', 'LI', 'analysis');