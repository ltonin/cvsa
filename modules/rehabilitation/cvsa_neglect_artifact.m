clc;
clearvars;

subject     = 'BC';
pattern     = '.va.';

%% Generic settings
% Classes
Classes    = [1 2 3];
NumClasses = length(Classes);

% Timings
timings.trial.period    = 3;            % seconds
timings.offset.period   = -1;           % seconds
idchannels  = 1:64;

% Get datafiles
experiment  = 'neglect';
datapath    = ['/mnt/data/Research/cvsa/' experiment '/'];
[Files, NumFiles] = cvsa_utilities_getdata(datapath, subject, pattern, '.bdf');
[~, analysisdir]  = util_mkdir('./', ['analysis/' experiment '/artifacts/']);

%% Filter settings
filter.bands = [3 20];
filter.order = 2;

if(strcmp(subject, 'SM'))
    threshold = 60;
elseif(strcmp(subject, 'TC'))
    threshold = 60;
elseif(strcmp(subject, 'BC'))  
    threshold = 110 ;
end

 %% Processing data

S  = [];
Ck = [];
for fId = 1:NumFiles
    
    % Import file
    cfilename = Files{fId};
    info = util_getfile_info(cfilename);
    util_bdisp(['[io] - Loading file ' num2str(fId) '/' num2str(NumFiles)]);
    disp(['       File: ' cfilename]);
    [s, h] = io_import_bdf(cfilename, idchannels);   
   
    proc.samplerate = h.SampleRate;
    proc.nsamples   = size(s, 1);
    proc.nchans     = size(s, 2);
    
    % Filtering data
    util_bdisp('[proc] + Filtering data:');
         disp(['       |- order: ' num2str(filter.order)]) 
         disp(['       |- bands: [' num2str(filter.bands(1)) '-' num2str(filter.bands(2)) '] Hz']);
    
    sfilter = filt_bp(s, filter.order, filter.bands, proc.samplerate);
    
    % Analysis - Masking data
    util_bdisp('[proc] - Creating trial mask vector');
    events = h.EVENT;
    timings.trial.size  = floor(timings.trial.period*proc.samplerate);
    timings.offset.size = floor(timings.offset.period*proc.samplerate);
    timings.trial.mask  = false(proc.nsamples, 1);
    timings.trial.pos   = [];
    for cId = 1:NumClasses
        [cmask, cpos, cdur] = proc_get_event(events, Classes(cId), proc.nsamples, timings.trial.size, timings.offset.size);
        timings.trial.mask    = timings.trial.mask | cmask;
        timings.trial.pos     = sort([timings.trial.pos cpos']);
    end

    util_bdisp('[proc] - Creating trial based class labels');
    index = false(length(events.TYP), 1);
    for cId = 1:NumClasses
        index = events.TYP == Classes(cId) | index;
    end
    cck = events.TYP(index);
    
    %% Extracting trials
    util_bdisp('[proc] - Extracting trials');
    NumTrials = length(timings.trial.pos);
    NumSamples = timings.trial.size + abs(timings.offset.size);
    sfilter_trials = zeros(NumSamples, proc.nchans, NumTrials);
 
    for tId = 1:NumTrials
        cstart = timings.trial.pos(tId);
        cstop  = cstart + NumSamples - 1;
        sfilter_trials(:, :, tId) = sfilter(cstart:cstop, :);
    end
    
    S = cat(3, S, sfilter_trials);
    Ck = cat(1, Ck, cck);
end

%% Mark trials with faulty electrodes
NumTrials = size(S, 3);

Ak = false(NumTrials, 1);
Am = zeros(NumTrials, 1);
for trId = 1:NumTrials
    maxvalue = max(max(abs(S(:, :, trId))));
    if (maxvalue >= threshold)
        Ak(trId) = true;
    end
    Am(trId) = maxvalue;
end

util_bdisp(['[proc] + Percentage of trials discarded: ' num2str(100*sum(Ak)./length(Ak), 3) '% (' num2str(sum(Ak)) '/' num2str(length(Ak)) ')']);
for cId = 1:NumClasses
      disp(['[proc] |- Within class : ' num2str(100*sum(Ak & Ck == Classes(cId))./sum(Ck == Classes(cId))) '% (' num2str(sum(Ak & Ck == Classes(cId))) '/' num2str(sum(Ck == Classes(cId))) ')']);
end

%% Saving 
targetfile = [analysisdir '/' subject '_artifacts.mat'];
util_bdisp(['[out] - Saving analysis in: ' targetfile]);

artifacts.Ak             = Ak;
artifacts.Am             = Am;
artifacts.classes        = Classes;
artifacts.proc           = proc;
artifacts.proc.filter    = filter;
artifacts.proc.threshold = threshold;
artifacts.timings        = timings;
artifacts.labels.Ck      = Ck;

save(targetfile, 'artifacts');


