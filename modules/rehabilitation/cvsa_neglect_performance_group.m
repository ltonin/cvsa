clc;
clearvars;

clc;
clearvars;

sublist = {'SM', 'BC', 'TC'};
NumSubjects = length(sublist);
experiment  = 'neglect';

Classes = [1 3];
NumClasses = length(Classes);

% Datafiles
baseroot      = ['/mnt/data/Git/Codes/cvsa/analysis/' experiment '/'];

Rd  = [];
Sk  = [];
Mk  = [];
Rk  = [];
Dk  = [];
Ck  = [];
Afk = [];
for sId = 1:NumSubjects
    csubject = sublist{sId};
    
    % Analysis - Load artifacts analysis
    util_bdisp(['[io] - Loading artifacts data for subject ' csubject]);
    cartifacts = load([baseroot '/artifacts/' csubject '_artifacts.mat'], 'artifacts');
    analysis.artifacts = cartifacts.artifacts;

    ArtInd = false(length(analysis.artifacts.Ak), 1);
    for ctrialId = 1:NumClasses
        ArtInd = ArtInd | analysis.artifacts.labels.Ck == Classes(ctrialId);
    end

    Ak = analysis.artifacts.Ak(ArtInd);

    if strcmpi(csubject, 'BC')
        Ak = analysis.artifacts.Am(ArtInd) >= 115;
    end

    % Analysis - Load eog analysis
    util_bdisp(['[io] - Loading eog data for subject ' csubject]);
    ceoganalysis = load([baseroot '/eog/' csubject '_eog.mat'], 'eog');
    EogInd = false(size(ceoganalysis.eog.detection, 1), 1);
    for ctrialId = 1:NumClasses
        EogInd = EogInd | ceoganalysis.eog.labels.Ck == Classes(ctrialId);
    end
    Ed = ceoganalysis.eog.detection(EogInd, 1);

    % Analysis - Artifact index
    cAfk = Ak == false & Ed == false;
    
    util_bdisp(['[io] - Loading performance data for subject ' csubject]);
    % Performances
    cdata    = load([baseroot '/performance/' csubject '_performance.mat']);
    
    Rd = cat(1, Rd, cdata.performance.Rd);
    
    Sk  = cat(1, Sk, sId*ones(length(cdata.performance.Rd), 1));
    Mk  = cat(1, Mk, cdata.performance.labels.Mk);
    Rk  = cat(1, Rk, cdata.performance.labels.Rk);
    Dk  = cat(1, Dk, cdata.performance.labels.Dk);
    Ck  = cat(1, Ck, cdata.performance.labels.Ck);
    Afk = cat(1, Afk, cAfk);
    
end

%% Performances per runs
util_bdisp('[proc] - Computing performances per run and subject');
pruns = cell(NumSubjects, 1);
for sId = 1:NumSubjects
    cindex = Sk == sId & Mk == 1;
    cruns  = unique(Rk(cindex));
    cnruns = length(cruns);
    
    cperf = zeros(cnruns, 1);
    for rId = 1:cnruns
        cperf(rId) = 100*sum(Rd(cindex & Rk == cruns(rId)) == 16)./sum(Rk(cindex) == cruns(rId));
    end
    
    pruns{sId} = cperf;
end

%% Plotting
fig1 = figure;
fig_set_position(fig1, 'Top');

NumRows = 1;
NumCols = 3;

for sId = 1:NumSubjects
    subplot(NumRows, NumCols, sId);
    bar(pruns{sId});
    ylim([0 100]);
    ylabel('[%]');
    xlabel('Run');
    grid on;
    title(['Patient ' sublist{sId}]);
end
