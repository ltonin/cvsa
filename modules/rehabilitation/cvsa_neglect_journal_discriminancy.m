clc;
clearvars;

subject     = 'TC';
pattern     = '.va.';

features       = 'psd';
spatialfilter  = 'laplacian';
experiment     = 'neglect';

Classes    = [1 3];
ClassesLb  = {'LeftCue (neglected)', 'MiddleCue'};
NumClasses = length(Classes);
ModalityLb = {'Calibration', 'Online'};

baseroot = ['/mnt/data/Git/Codes/cvsa/analysis/' experiment '/'];
datapath = [baseroot '/' features '/' spatialfilter '/'];
analysispath = './analysis/';
figuredir   = './figures/journal/';

%% Initialization

% Timings
timings.trial.period    = 3;            % seconds

% Frequencies
selectedfrequencies       = 4:48;

% Channels
[~, channels.list]        = proc_get_montage('eeg.biosemi.64');
channels.hemisphere       = proc_get_hemisphere(channels.list);

% Nodes initializiation
nodes.channels.names   = { 'P7',  'P5',  'P3', 'P1',  'Pz', 'PO7', 'PO3', 'O1', 'POz', 'Oz', 'P8',  'P6',  'P4', 'P2',  'Pz', 'PO8', 'PO4', 'O2', 'POz', 'Oz'};
nodes.channels.id      = proc_get_channel(nodes.channels.names, channels.list);
nodes.locations.id     = [1 1 1 1 1 2 2 2 2 2 3 3 3 3 3 4 4 4 4 4];
nodes.locations.names  = {'Parietal', 'Occipital', 'Parietal', 'Occipital'};
nodes.hemisphere.id    = [1 1 1 1 1 1 1 1 1 1 2 2 2 2 2 2 2 2 2 2];
nodes.hemisphere.names = {'Left', 'Right'};

NumNodeChannels = length(nodes.channels.id);
Nodes           = unique(nodes.locations.id);
NumNodes        = length(Nodes);
Hemispheres     = unique(nodes.hemisphere.id);
NumHemispheres  = length(Hemispheres);

%% Analysis - Import data

% Datafiles
[Files, NumFiles] = util_getfile(datapath, '.mat', [subject '*' pattern]);

% Concatanate datafiles
util_bdisp(['[io] - Concatenating ' num2str(NumFiles) ' datafiles']);
[F, analysis] = cvsa_utilities_concatenate_data(Files);

% Extract information
SampleRate     = analysis.processing.fs;
FrameSize      = analysis.processing.fsize;
[~, selfreqId] = intersect(analysis.processing.f, selectedfrequencies);
FreqGrid       = analysis.processing.f(selfreqId);

F = F(:, selfreqId, :);
NumTotSamples  = size(F, 1);
NumFreqs       = size(F, 2);
NumChans       = size(F, 3);

%% Analysis - Loading peaks for each trial
util_bdisp('[proc] - Loading alpha peak for each trial');
ciaf = load([baseroot '/alphapeak/' subject '_iaf.mat']);
iaf = ciaf.iaf;

%% Extract events information
util_bdisp('[proc] - Extracting events information');

timings.trial.size = floor(timings.trial.period*SampleRate/FrameSize);

[TrialLb, TrialEvt] = proc_get_event2(Classes, NumTotSamples, analysis.event.POS, analysis.event.TYP, timings.trial.size);
TrialIndex = TrialLb > 0;

Ck = TrialLb(TrialIndex);
Mk = analysis.label.Mk(TrialIndex);
Dk = analysis.label.Dk(TrialIndex);
Rk = analysis.label.Rk(TrialIndex);

%% TO DO: Artifacts and EOG
util_bdisp('[io] - Loading EOG detection');
ceog = load([baseroot '/eog/' subject '_eog_samples.mat']);
sEh = ceog.eog.labels.Eh;

Eh = sEh(1:128:end);
Eh = Eh(TrialIndex);
ArtifactFree = Eh == false;
%ArtifactFree = true;

%% Load reaction time
crtfile = [baseroot '/reactiontime/' subject '_reactiontime.mat'];
util_bdisp(['[io] - Loading RT for subject ' subject ': ' crtfile]);
crtdata = load(crtfile);

cCk = crtdata.analysis.rt.labels.Ck;
selindex = false(length(cCk), 1);
for cId = 1:NumClasses
    selindex = selindex | cCk == Classes(cId);
end


crt  = crtdata.analysis.rt.values(selindex);
cTs  = crtdata.analysis.rt.labels.Ts(selindex);
cRd  = crtdata.analysis.rt.labels.Rd(selindex);
cRk  = crtdata.analysis.rt.labels.Rk(selindex);
cAFk  = crtdata.analysis.rt.labels.AFk(selindex);
cRuns = unique(cRk);
cNumRuns = length(cRuns);
RTRun = zeros(cNumRuns, 1);
for rId = 1:cNumRuns
    cindex = cRk == cRuns(rId) & cAFk & cTs == 1 & cRd; 
    RTRun(rId) = nanmean(crt(cindex));
end

%% Extract data
P = log(F(TrialIndex, :, :) + 1);

GenericConditions = ArtifactFree;

Modalities    = unique(Mk);
NumModalities = length(Modalities);
Runs          = unique(Rk);
NumRuns       = length(Runs);
Days          = unique(Dk);
NumDays       = length(Days);

RunMod = zeros(NumRuns, 1);
for rId = 1:NumRuns
    RunMod(rId) = unique(Mk(Rk == Runs(rId)));
end

%% Compute discriminancy
util_bdisp('[proc] - Computing discriminancy');

fs = zeros(NumFreqs, NumChans, NumRuns);
for rId = 1:NumRuns
    cindex = Rk == Runs(rId) & GenericConditions;
    cP  = P(cindex, :, :);
    cCk = Ck(cindex); 
    cfs = proc_fisher2(cP, cCk);
    fs(:, :, rId) = reshape(cfs, [NumFreqs NumChans]);
end

%% Node extraction (iaf band and channels)
util_bdisp('[proc] - Extract nodes (iaf band and channels)');

fsnode = zeros(NumNodes, NumRuns);
for nId = 1:NumNodes
    for rId = 1:NumRuns
        modId = find(Modalities == unique(Mk(Rk == Runs(rId))));            % Selecting modality based on run Id
        hemId = unique(nodes.hemisphere.id(nodes.locations.id == nId));     % Selecting hemisphere based on node Id
        chnId = nodes.channels.id(nodes.locations.id == nId);               % Selecting channels based on node Id
        frqId = iaf.modalities.band(:, hemId, modId);                       % Selecting frequency band based on hemisphere and modality Ids
        
        fsnode(nId, rId) = squeeze(mean(mean(fs(frqId, chnId, rId), 1), 2));       
    end
end

%% Node groups
Groups = unique(nodes.locations.names, 'stable');
NumGroups = length(Groups);
fsgroups = zeros(NumRuns, NumGroups);
for gId = 1:NumGroups
    fsgroups(:, gId) = diff(fsnode(strcmpi(nodes.locations.names, Groups(gId)), :));
end

%% Statistical test
util_bdisp('[stat] - T-Test');
BonferroniCorrection = 2;
stat.ttest.pvalue = zeros(NumGroups, 1);
for gId = 1:NumGroups
    [~, cpval] = ttest2(fsgroups(RunMod == 0, gId), fsgroups(RunMod == 1, gId));
    stat.ttest.pvalue(gId) = cpval*BonferroniCorrection;
    fprintf('[stat] - %s nodes - Calibration vs online:\tp=%1.5f (Bonferroni)\n', Groups{gId}, stat.ttest.pvalue(gId));
end

%% Statistical test (Wilcoxon)
util_bdisp('[stat] - Wilcoxon ranksum');
BonferroniCorrection = 2;
stat.ttest.pvalue_w = zeros(NumGroups, 1);
for gId = 1:NumGroups
    cpval_w = ranksum(fsgroups(RunMod == 0, gId), fsgroups(RunMod == 1, gId));
    stat.ttest.pvalue_w(gId) = cpval_w*BonferroniCorrection;
    fprintf('[stat] - %s nodes - Wilcoxon ranksum Calibration vs online:\tp=%1.5f (Bonferroni)\n', Groups{gId}, stat.ttest.pvalue_w(gId));
end

%% Correlation
util_bdisp('[stat] - Correlations');
stat.run.corr.type   = 'Pearson';
stat.run.corr.value  = zeros(NumGroups, 1);
stat.run.corr.pvalue = zeros(NumGroups, 1);
for gId = 1:NumGroups
    [ccorr, cpval] = corr(fsgroups(:, gId), Runs, 'type', stat.run.corr.type, 'rows', 'pairwise');
    stat.run.corr.value(gId)  = ccorr;
    stat.run.corr.pvalue(gId) = cpval;
    fprintf('[stat] - %s nodes - Correlation FisherScore vs. RunIndex:\t rho=%1.5f; pval=%1.5f\n', Groups{gId}, stat.run.corr.value(gId), stat.run.corr.pvalue(gId));
end

stat.rt.corr.type   = 'Pearson';
stat.rt.corr.value  = zeros(NumGroups, 1);
stat.rt.corr.pvalue = zeros(NumGroups, 1);
for gId = 1:NumGroups
    [ccorr, cpval] = corr(fsgroups(:, gId), RTRun, 'type', stat.rt.corr.type, 'rows', 'pairwise');
    stat.rt.corr.value(gId)  = ccorr;
    stat.rt.corr.pvalue(gId) = cpval;
    fprintf('[stat] - %s nodes - Correlation FisherScore vs. ReactionTime:\t rho=%1.5f; pval=%1.5f\n', Groups{gId}, stat.rt.corr.value(gId), stat.rt.corr.pvalue(gId));
end

%% Plotting

fig1 = figure;
NumRows = 3;
NumCols = 2;

% Topoplots per modality
load('chanlocs64.mat');
fs2topo = zeros(NumChans, NumRuns);

for rId = 1:NumRuns
    modId = find(Modalities == RunMod(rId));  
    for chId = 1:NumChans
        hemId = channels.hemisphere(chId);
        frqId = iaf.modalities.band(:, :, modId);
        if isequal(hemId, 3) == false
            frqId = iaf.modalities.band(:, hemId, modId);  
        end
        fs2topo(chId, rId) = mean(fs(frqId(:), chId, rId), 1);
    end
end 

for mId = 1:NumModalities
    subplot(NumRows, NumCols, mId);
    cindex = RunMod == Modalities(mId);
    
    cdata = mean(fs2topo(:, cindex), 2);
    otherchans = setdiff(1:64, nodes.channels.id)';
    cdata(otherchans) = 0;
    topoplot(cdata, chanlocs, 'conv', 'on', 'headrad', 'rim', 'maplimits', [0 0.2]);
    axis image;
    title(ModalityLb{mId});
end

% Boxplot
for gId = 1:NumGroups
    subplot(NumRows, NumCols, gId + NumCols);
    boxplot(fsgroups(:, gId), RunMod);
    set(gca, 'XTickLabel', {'Calibration', 'Online'});
    set(gca, 'ActivePositionProperty', 'position')
    grid on;
    ylim([-0.1 0.1]);
    plot_hline(0, 'k');
    
    % Annotation
    cpos = get(gca,'Position');
    cpos(2) = cpos(2) - 0.05;
    t = annotation('textbox', cpos, 'String', ['p=' num2str(stat.ttest.pvalue(gId), '%3.3f') ' (Bonferroni)'], 'LineStyle', 'none', 'Color', 'k', 'FontWeight', 'bold');
    t.FontSize = 8;
    
    title([Groups{gId} ' nodes']);
end

% Correlation over time
for gId = 1:NumGroups
    subplot(NumRows, NumCols, gId + 2*NumCols);
    plot(Runs, fsgroups(:, gId), '.');
    hl = lsline;
    set(hl, 'Color', 'r', 'LineWidth', 2);
    xlim([Runs(1) Runs(end)]);
    ylim([-0.1 0.1]);
    grid on;
    
    plot_vline(find(diff(RunMod))+1, 'k--', 'online');
    plot_hline(0, 'k');
    
    % Annotation
    cpos = get(gca,'Position');
    cpos(2) = cpos(2) - 0.03; 
    t = annotation('textbox', cpos, 'String', ['rho=' num2str(stat.run.corr.value(gId), '%3.2f') ', p=' num2str(stat.run.corr.pvalue(gId), '%3.3f')], 'LineStyle', 'none', 'Color', 'k', 'FontWeight', 'bold');
    t.FontSize = 8;
    
    xlabel('Run Index');
    ylabel('diff(FisherScore)');
end

suptitle([subject ' - Discriminancy']);

fig_set_position(fig1, 'Left');

%% Export figure
pdffilename = [figuredir '/cvsa.neglect.journal.discriminancy.raw.' subject '.pdf'];
pngfilename = [figuredir '/cvsa.neglect.journal.discriminancy.raw.' subject '.png'];
fig_export(fig1, pdffilename, '-pdf');
fig_export(fig1, pngfilename, '-png');
