clc;
clearvars;

clc;
clearvars;

sublist = {'SM', 'BC', 'TC'};
NumSubjects = length(sublist);
experiment  = 'neglect';

Classes     = [1 3];
NumClasses  = length(Classes);

% Datafiles
baseroot      = ['/mnt/data/Git/Codes/cvsa/analysis/' experiment '/'];

RoiLabels     = {'Healthy RoI (Left)', 'Affected RoI (Right)'};
NumRois       = length(RoiLabels);
HealthyRoiId  = 1;
AffectedRoiId = 2;
FreqGrid       = 4:48;
iaf = [];
Sk  = [];
Mk  = [];
Rk  = [];
Afk = [];
RunMod = cell(NumSubjects, 1);
for sId = 1:NumSubjects
    csubject = sublist{sId};
    util_bdisp(['[io] - Loading files for subject ' csubject]);
    
    % Alpha peak analysis on task
    cdata    = load([baseroot '/alphapeak/' csubject '_iaf.mat']);
    
    iaf = cat(2, iaf, cdata.iaf.trials.raw);
    
    Sk  = cat(1, Sk, sId*ones(size(cdata.iaf.trials.raw, 2), 1));
    Mk  = cat(1, Mk, cdata.iaf.labels.Mk);
    Rk  = cat(1, Rk, cdata.iaf.labels.Rk);
    Afk = cat(1, Afk, cdata.iaf.labels.AFk);
    RunMod{sId} = cdata.iaf.labels.RunMod;
end

Modalities = unique(Mk);
NumModalities = length(Modalities);

GenericConditions = Afk;


iafmean     = cell(NumSubjects, 1);
iafstd      = cell(NumSubjects, 1);
iafmissing  = cell(NumSubjects, 1);
for sId = 1:NumSubjects
   cnumruns = length(unique(Rk(Sk == sId)));
   cmean = zeros(NumRois, cnumruns);
   cstd  = zeros(NumRois, cnumruns);
   cmissing = zeros(NumRois, cnumruns);
   for rId = 1:cnumruns
       cindex = Rk == rId & Sk == sId & GenericConditions;
       
       
       for roId = 1:NumRois
           clocations = iaf(roId, cindex);
           vlocations = clocations(isnan(clocations) == false);
           cmean(roId, rId) = mean(FreqGrid(vlocations));
           cstd(roId, rId)  = std(FreqGrid(vlocations));
           cmissing(roId, rId) = 100*sum(isnan(clocations))./length(clocations);
       end
   end
   
   iafmean{sId} = cmean;
   iafstd{sId}  = cstd;
   iafmissing{sId} = cmissing;
end



%% Plotting
fig1 = figure;
fig_set_position(fig1, 'Top');
style = {'x-', 's-', '^-'};
NumRows = 2;
NumCols = 2;

for roId = 1:NumRois
    subplot(NumRows, NumCols, roId);
    
    hold on;
    for sId = 1:NumSubjects
        
        cmeans = [squeeze(mean(iafmean{sId}(roId, RunMod{sId} == 0), 2)) squeeze(mean(iafmean{sId}(roId, RunMod{sId} == 1), 2))];
        cstds  = [squeeze(std(iafmean{sId}(roId, RunMod{sId} == 0), [], 2)) squeeze(std(iafmean{sId}(roId, RunMod{sId} == 1), [], 2))];
        errorbar(cmeans, cstds, style{sId});
    end
    hold off;
    grid on;
    ylim([4 12]);
    ylabel('[Hz]');
    set(gca, 'XTick', [1 2]);
    set(gca, 'XTickLabel', {'Calibration', 'Online'});
    xlabel('Modality');
    legend('P1', 'P2', 'P3');
    title(['IAF - ' RoiLabels{roId}]);
end

pax = zeros(NumRois, 1);
for roId = 1:NumRois
    subplot(NumRows, NumCols, roId + NumCols);
    
    hold on;
    for sId = 1:NumSubjects
        
        cmeans = [squeeze(mean(iafmissing{sId}(roId, RunMod{sId} == 0), 2)) squeeze(mean(iafmissing{sId}(roId, RunMod{sId} == 1), 2))];
        cstds  = [squeeze(std(iafmissing{sId}(roId, RunMod{sId} == 0), [], 2)) squeeze(std(iafmissing{sId}(roId, RunMod{sId} == 1), [], 2))];
        errorbar(cmeans, cstds, style{sId});
    end
    hold off;
    grid on;
    pax(roId) = gca;
    %ylim([4 12]);
    ylabel('[%]');
    set(gca, 'XTick', [1 2]);
    set(gca, 'XTickLabel', {'Calibration', 'Online'});
    xlabel('Modality');
    legend('P1', 'P2', 'P3');
    title(['Missing peaks - ' RoiLabels{roId}]);
end

plot_set_limits(pax, 'y', [-5 30]);

