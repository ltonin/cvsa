clc;
clearvars;

subject     = 'TC';
pattern     = '.va.';

features       = 'psd';
spatialfilter  = 'car';
experiment     = 'neglect';

Classes    = [1 3];
ClassesLb  = {'LeftCue (neglected)', 'MiddleCue'}; %
NumClasses = length(Classes);

%% Initialization

% Timings
timings.trial.period    = 3;            % seconds
timings.offset.period   = -1;           % seconds

% Channels and RoIs
[~, channels.list]        = proc_get_montage('eeg.biosemi.64');
% channels.roi.left.labels  = {'P7', 'P5', 'PO7'};         % As in [Thut et al., 2006]
% channels.roi.right.labels = {'P8', 'P6', 'PO8'};         % As in [Thut et al., 2006]
channels.roi.left.labels  = {'P5', 'P3', 'P1', 'PO7', 'PO3', 'O1'};         % Custom
channels.roi.right.labels = {'P6', 'P4', 'P2', 'PO8', 'PO4', 'O2'};         % Custom
channels.roi.left.id      = proc_get_channel(channels.roi.left.labels,  channels.list);
channels.roi.right.id     = proc_get_channel(channels.roi.right.labels, channels.list);
channels.roi.labels       = {'LeftRoI', 'RightRoI'};

% Frequencies
selectedfrequencies       = 4:48;

% Datafiles
baseroot      = ['/mnt/data/Git/Codes/cvsa/analysis/' experiment '/'];
datapath      = [baseroot '/' features '/' spatialfilter '/'];
[Files, NumFiles] = util_getfile(datapath, '.mat', [subject '*' pattern]);
[~, saveroot] = util_mkdir(baseroot, 'alphapeak/');


%% Analysis - Import data

% Concatanate datafiles
util_bdisp(['[io] - Concatenating ' num2str(NumFiles) ' datafiles']);
[F, analysis] = cvsa_utilities_concatenate_data(Files);

% Extract information
% Extract information
SampleRate     = analysis.processing.fs;
FrameSize      = analysis.processing.fsize;
[~, selfreqId] = intersect(analysis.processing.f, selectedfrequencies);
FreqGrid       = analysis.processing.f(selfreqId);

F = F(:, selfreqId, :);
NumTotSamples  = size(F, 1);
NumFreqs       = size(F, 2);
NumChans       = size(F, 3);

%% Analysis - Masking data

util_bdisp('[proc] - Creating trial mask vector');
events = analysis.event;
timings.trial.size  = floor(timings.trial.period*SampleRate/FrameSize);
timings.offset.size = floor(timings.offset.period*SampleRate/FrameSize);
timings.trial.mask  = false(NumTotSamples, 1);
timings.trial.pos   = [];
for cId = 1:NumClasses
    [cmask, cpos, cdur] = proc_get_event(events, Classes(cId), NumTotSamples, timings.trial.size, timings.offset.size);
    timings.trial.pos   = sort([timings.trial.pos cpos']);
end

util_bdisp('[proc] - Creating trial based class labels');
index = false(length(events.TYP), 1);
for cId = 1:NumClasses
    index = events.TYP == Classes(cId) | index;
end
Sk = events.TYP(index);

%% Extracting trials
util_bdisp('[proc] - Extracting trials');
NumTrials  = length(timings.trial.pos);
NumSamples = timings.trial.size + abs(timings.offset.size);
S = zeros(NumSamples, NumFreqs, NumChans, NumTrials);
Mk = zeros(NumTrials, 1);
Rk = zeros(NumTrials, 1);
Dk = zeros(NumTrials, 1);
for trId = 1:NumTrials
    cstart = timings.trial.pos(trId);
    cstop  = cstart + NumSamples - 1;
    S(:, :, :, trId) = F(cstart:cstop, :, :);
    Mk(trId) = unique(analysis.label.Mk(cstart:cstop));
    Rk(trId) = unique(analysis.label.Rk(cstart:cstop));
    Dk(trId) = unique(analysis.label.Dk(cstart:cstop));
end
Modalities    = unique(Mk);
NumModalities = length(Modalities);
Runs          = unique(Rk);
NumRuns       = length(Runs);
Days          = unique(Dk);
NumDays       = length(Days);

%% Analysis - Load artifacts analysis
cartifacts = load([baseroot '/artifacts/' subject '_artifacts.mat'], 'artifacts');
analysis.artifacts = cartifacts.artifacts;

ArtInd = false(length(analysis.artifacts.Ak), 1);
for cId = 1:NumClasses
    ArtInd = ArtInd | analysis.artifacts.labels.Ck == Classes(cId);
end

Ak = analysis.artifacts.Ak(ArtInd);

%% Analysis - Load eog analysis
ceoganalysis = load([baseroot '/eog/' subject '_eog.mat'], 'eog');
EogInd = false(size(ceoganalysis.eog.detection, 1), 1);
for cId = 1:NumClasses
    EogInd = EogInd | ceoganalysis.eog.labels.Ck == Classes(cId);
end
Ed = ceoganalysis.eog.detection(EogInd, 1);

%% Analysis - Artifact index
ArtifactFree = Ak == false | Ed == false;


%% Extracting spectrum
util_bdisp('[proc] - Extracting spectrum');
ToI = 1:abs(timings.offset.size);           % Time of interest 
SP  = zeros(NumFreqs, 2, NumTrials);
SPn = zeros(NumFreqs, 2, NumTrials);
for trId = 1:NumTrials
    cvalues_l = squeeze(mean(mean(S(ToI, :, channels.roi.left.id, trId), 1), 3));
    cvalues_r = squeeze(mean(mean(S(ToI, :, channels.roi.right.id, trId), 1), 3));
    
    SP(:, 1, trId) = cvalues_l;
    SP(:, 2, trId) = cvalues_r;

    SPn(:, 1, trId) = cvalues_l./max(cvalues_l);
    SPn(:, 2, trId) = cvalues_r./max(cvalues_r);
end


%% Find the alpha peak based on healthy RoI
util_bdisp('[proc] - Computing alpha peak');
if(strcmp(subject, 'SM'))
    FreqPeakRange = 8:12;
elseif(strcmp(subject, 'TC'))
    FreqPeakRange = 8:12;
elseif(strcmp(subject, 'BC'))  
    FreqPeakRange = 6:12;
end

[pks.value.healthy.calibration, pks.locs.healthy.calibration] = findpeaks(mean(SPn(:, 1, ArtifactFree == true & Mk == 0), 3), 'NPeaks',1);
[pks.value.healthy.online, pks.locs.healthy.online] = findpeaks(mean(SPn(:, 1, ArtifactFree == true & Mk == 1), 3), 'NPeaks',1);

[pks.value.affected.calibration, pks.locs.affected.calibration] = findpeaks(mean(SPn(:, 2, ArtifactFree == true & Mk == 0), 3), 'NPeaks', 1);
[pks.value.affected.online, pks.locs.affected.online] = findpeaks(mean(SPn(:, 2, ArtifactFree == true & Mk == 1), 3), 'NPeaks', 1);

% If alpha peak cannot be found in affected roi then use the peak location
% in healthy roi (calibration for calibration, online for online)
if(isempty(pks.locs.affected.calibration) || isempty(intersect(FreqPeakRange, FreqGrid(pks.locs.affected.calibration))))
    pks.locs.affected.calibration  = pks.locs.healthy.calibration;
    pks.value.affected.calibration = pks.value.healthy.calibration;
end

if(isempty(pks.locs.affected.online) || isempty(intersect(FreqPeakRange, FreqGrid(pks.locs.affected.online))))
    pks.locs.affected.online  = pks.locs.healthy.online;
    pks.value.affected.online = pks.value.healthy.online;
end

pks.band.healthy.calibration  =  pks.locs.healthy.calibration-1:pks.locs.healthy.calibration+1;
pks.band.healthy.online       =  pks.locs.healthy.online-1:pks.locs.healthy.online+1;
pks.band.affected.calibration =  pks.locs.affected.calibration-1:pks.locs.affected.calibration+1;
pks.band.affected.online      =  pks.locs.affected.online-1:pks.locs.affected.online+1;


%% Compute the alpha power around the peak for each condition
AlphaPower = [];
AlphaPower(:, 1) = [squeeze(mean(SPn(pks.band.healthy.calibration, 1, ArtifactFree == true & Mk == 0), 1)); ...               %% Alpha power for calibration trials
                    squeeze(mean(SPn(pks.band.healthy.online,      1, ArtifactFree == true & Mk == 1), 1)); ...               %% Alpha power for online trials
                    squeeze(mean(SPn(pks.band.healthy.online,      1, ArtifactFree == true & Rk > (Runs(end) - 4)), 1))];     %% Alpha power for last four runs

AlphaPower(:, 2) = [squeeze(mean(SPn(pks.band.affected.calibration, 2, ArtifactFree == true & Mk == 0), 1)); ...              %% Alpha power for calibration trials
                    squeeze(mean(SPn(pks.band.affected.online,      2, ArtifactFree == true & Mk == 1), 1)); ...              %% Alpha power for online trials
                    squeeze(mean(SPn(pks.band.affected.online,      2, ArtifactFree == true & Rk > (Runs(end) - 4)), 1))];    %% Alpha power for last four runs

Groups = [  ones(sum(ArtifactFree == true & Mk == 0), 1); ...
            2*ones(sum(ArtifactFree == true & Mk == 1), 1);...
            3*ones(sum(ArtifactFree == true & Rk > (Runs(end) - 4)), 1)];

        
%% Statitistics
% Between calibration and online trials
p.healthy.calibonline = anova1(AlphaPower(Groups < 3, 1), Groups(Groups < 3), 'off');

% Between calibration and last day trials
p.healthy.caliblastday = anova1(AlphaPower(Groups ~= 2, 1), Groups(Groups ~= 2), 'off');

% Between calibration and online trials
p.affected.calibonline = anova1(AlphaPower(Groups < 3, 2), Groups(Groups < 3), 'off');

% Between calibration and last day trials
p.affected.caliblastday = anova1(AlphaPower(Groups ~= 2, 2), Groups(Groups ~= 2), 'off');


fprintf('[stat] - Anova between calibration and online trials [Healthy]:      p=%1.5f\n', p.healthy.calibonline);
fprintf('[stat] - Anova between calibration and last days trials [Healthy]:   p=%1.5f\n', p.healthy.caliblastday);
fprintf('[stat] - Anova between calibration and online trials [Affected]:     p=%1.5f\n', p.affected.calibonline);
fprintf('[stat] - Anova between calibration and last days trials [Affected]:  p=%1.5f\n', p.affected.caliblastday);       
%% Saving analysis        

targetfile = [saveroot '/' subject '_alphapeak_modality.mat'];
util_bdisp(['[out] - Saving analysis in: ' targetfile]);

alphapeak.spectrum.raw   = SP;
alphapeak.spectrum.norm  = SPn;
alphapeak.peaks          = pks;
alphapeak.peaks.range    = FreqPeakRange;
alphapeak.channels       = channels;
alphapeak.timings        = timings;
alphapeak.labels.Mk      = Mk;
alphapeak.labels.Rk      = Rk;
alphapeak.labels.Dk      = Dk;
alphapeak.labels.Ck      = Sk;

save(targetfile, 'alphapeak');       

%% Plotting
Condition1 = Mk == 0; % & Dk == Days(1);
Condition2 = Mk == 1; % & Dk == Days(end);

SP2Plot = SPn;
fig = figure;
fig_set_position(fig, 'All');

% Top-Left subplot
subplot(2, 2, 1);
hold on;
ax1 = plot(FreqGrid, mean(SP2Plot(:, 1, ArtifactFree == true & Condition1), 3), 'LineWidth', 2);
ax2 = plot(FreqGrid, mean(SP2Plot(:, 1, ArtifactFree == true & Condition2), 3), ':', 'LineWidth', 2);
plot(FreqGrid(pks.locs.healthy.calibration), pks.value.healthy.calibration+0.01, 'v', 'MarkerEdgeColor', get(ax1, 'Color'), 'MarkerFaceColor', get(ax1, 'Color'));
plot(FreqGrid(pks.locs.healthy.online), pks.value.healthy.online+0.01, 'v', 'MarkerEdgeColor', get(ax2, 'Color'), 'MarkerFaceColor', get(ax2, 'Color'));
hold off;
grid on;
xlim([FreqGrid(1) FreqGrid(end)]);
ylim([0 1]);
xlabel('[Hz]');
ylabel('NormPsd');
title('Spectrum - Healthy RoI (Left)');
legend('Calibration', 'Online');

% Top-Right subplot
subplot(2, 2, 2);
hold on;
ax1 = plot(FreqGrid, mean(SP2Plot(:, 2, ArtifactFree == true & Condition1), 3), 'LineWidth', 2);
ax2 = plot(FreqGrid, mean(SP2Plot(:, 2, ArtifactFree == true & Condition2), 3), ':', 'LineWidth', 2);
plot(FreqGrid(pks.locs.affected.calibration), pks.value.affected.calibration+0.01, 'v', 'MarkerEdgeColor', get(ax1, 'Color'), 'MarkerFaceColor', get(ax1, 'Color'));
plot(FreqGrid(pks.locs.affected.online), pks.value.affected.online+0.01, 'v', 'MarkerEdgeColor', get(ax2, 'Color'), 'MarkerFaceColor', get(ax2, 'Color'));
hold off;
grid on;
xlim([FreqGrid(1) FreqGrid(end)]);
ylim([0 1]);
xlabel('[Hz]');
ylabel('NormPsd');
title('Spectrum - Affected RoI (Right)');
legend('Calibration', 'Online');

% Bottom-Rigth subplots
subplot(2, 2, 3);
boxplot(AlphaPower(:, 1), Groups, 'factorseparator', 1);
ylim([0 1]);
title('Healthy RoI (Left)');
set(gca, 'XTickLabel', {'Calibration', 'Online', 'LastDay'});
grid on;
ylabel('NormPsd');

subplot(2, 2, 4);
boxplot(AlphaPower(:, 2), Groups, 'factorseparator', 1);
ylim([0 1]);
title('Affected RoI (Right)');
set(gca, 'XTickLabel', {'Calibration', 'Online', 'LastDay'});
grid on;
ylabel('NormPsd');

suptitle(['Subject ' subject]);

    
    