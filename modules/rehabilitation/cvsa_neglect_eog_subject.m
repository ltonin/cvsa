clc;
clearvars;

subject     = 'TC';
pattern     = '.va.';

spatialfilter  = 'car';
experiment     = 'neglect';

Classes    = [1 2 3];
ClassesLb  = {'LeftCue (neglected)', 'Right', 'MiddleCue'}; %
NumClasses = length(Classes);

%% Initialization

% Timings
timings.trial.period    = 3;            % seconds
timings.offset.period   = 0;           % seconds

% Datafiles
datapath    = ['/mnt/data/Git/Codes/cvsa/analysis/' experiment '/eog/'];
[Files, NumFiles] = util_getfile(datapath, '.mat', [subject '*' pattern]);

% Extra paths
baseroot      = ['/mnt/data/Git/Codes/cvsa/analysis/' experiment '/'];
[~, saveroot] = util_mkdir(baseroot, 'eog/');

%% Analysis - Import data

% Concatanate datafiles
util_bdisp(['[io] - Concatenating ' num2str(NumFiles) ' datafiles']);
F   = [];
Rk  = [];
Mk  = [];
Dk  = [];
Ck  = [];
event.TYP = [];
event.POS = [];
event.DUR = [];

cdaylbl  = '';
cday     = 0;
csublbl  = '';
csub     = 0;
pproc    = '';
pspatial = '';

for fId = 1:NumFiles
        cfilename = Files{fId};
        info = util_getfile_info(cfilename);
        util_bdisp(['[io] - Loading file ' num2str(fId) '/' num2str(NumFiles)]);
        disp(['       File: ' cfilename]);

        % Get modality from filename
        switch lower(info.modality)
            case 'offline'
                cmod = 0;
            case 'online'
                cmod = 1;
            otherwise
                error('chk:mod', ['[' mfilename '] Unknown modality']);
        end

        % Get day from filename
        if strcmpi(info.date, cdaylbl) == false
            cday = cday + 1;
            cdaylbl = info.date;
        end
        
        % Get subject from filename
        if strcmpi(info.subject, csublbl) == false
            csub = csub + 1;
            csublbl = info.subject;
        end

        % Loading processed data
        cdata = load(cfilename);

        % Concatenate events structure
        cevent    = cdata.analysis.event;
        event.TYP = cat(1, event.TYP, cevent.TYP);
        event.DUR = cat(1, event.DUR, cevent.DUR);
        event.POS = cat(1, event.POS, cevent.POS + size(F, 1));

        % Concatenate data
        F = cat(1, F, cdata.eog);

        % Concatenate markers vector
        Rk = cat(1, Rk, fId*ones(size(cdata.eog, 1), 1));
        Mk = cat(1, Mk, cmod*ones(size(cdata.eog, 1), 1));
        Dk = cat(1, Dk, cday*ones(size(cdata.eog, 1), 1));
        Ck = cat(1, Ck, csub*ones(size(cdata.eog, 1), 1));
end
analysis.labels.Mk = Mk;
analysis.labels.Rk = Rk;
analysis.labels.Dk = Dk;
analysis.labels.Sk = Ck;
analysis.event     = event;
analysis.processing = cdata.analysis.processing;

% Extract information
NumTotSamples = size(F, 1);
SampleRate    = analysis.processing.fs;


%% Analysis - Masking data

util_bdisp('[proc] - Creating trial mask vector');
events = analysis.event;
timings.trial.size  = floor(timings.trial.period*SampleRate);
timings.offset.size = floor(timings.offset.period*SampleRate);
timings.trial.mask  = false(NumTotSamples, 1);
timings.trial.pos   = [];
for cId = 1:NumClasses
    [cmask, cpos, cdur] = proc_get_event(events, Classes(cId), NumTotSamples, timings.trial.size, timings.offset.size);
    timings.trial.pos   = sort([timings.trial.pos cpos']);
end

util_bdisp('[proc] - Creating trial based class labels');
index = false(length(events.TYP), 1);
for cId = 1:NumClasses
    index = events.TYP == Classes(cId) | index;
end
Ck = events.TYP(index);

%% Extracting trials
util_bdisp('[proc] - Extracting trials');
NumTrials  = length(timings.trial.pos);
NumSamples = timings.trial.size + abs(timings.offset.size);
S = zeros(NumSamples, 2, NumTrials);
Mk = zeros(NumTrials, 1);
Rk = zeros(NumTrials, 1);
Dk = zeros(NumTrials, 1);
for trId = 1:NumTrials
    cstart = timings.trial.pos(trId);
    cstop  = cstart + NumSamples - 1;
    S(:, :, trId) = F(cstart:cstop, :);
    Mk(trId) = unique(analysis.labels.Mk(cstart:cstop));
    Rk(trId) = unique(analysis.labels.Rk(cstart:cstop));
    Dk(trId) = unique(analysis.labels.Dk(cstart:cstop));
end
Modalities    = unique(Mk);
NumModalities = length(Modalities);
Runs          = unique(Rk);
NumRuns       = length(Runs);
Days          = unique(Dk);
NumDays       = length(Days);

%% Detecting EOG

threshold.horizontal = 100;
threshold.vertical   = 100;
maxvalues = zeros(NumTrials, 2);
Ed = false(NumTrials, 2);
Et = zeros(NumTrials, 1);
for trId = 1:NumTrials
    cvalues_h = abs(S(:, 1, trId));
    cvalues_v = abs(S(:, 2, trId));

    % Horizontal
    [maxvalues(trId, 1), maxid] = max(cvalues_h);
    if(max(cvalues_h) >= threshold.horizontal)
        Ed(trId, 1) = true;
        if(S(maxid, 1, trId) > 0)
            Et(trId) = 1;
        else
            Et(trId) = 2;
        end
    end

    % Vertical
    maxvalues(trId, 2) = max(cvalues_v);
    if(max(cvalues_v) >= threshold.vertical)
        Ed(trId, 2) = true;
    end
end

%% Plotting
fig1 = figure;
fig_set_position(fig1, 'Top');
t = timings.offset.period:1/analysis.processing.fs:timings.trial.period - 1/analysis.processing.fs;

subplot(1, 3, 1)
plot(t, mean(S(:, :, Ck == 1), 3));
xlim([t(1) t(end)]);
ylim([-threshold.horizontal/2 threshold.horizontal/2]);
grid on;
plot_vline(0, 'k', 'cue');
ylabel('[uV]');
xlabel('Time [s]')
legend(cdata.analysis.eog.components);
title('Class Left');


subplot(1, 3, 2)
plot(t, mean(S(:, :, Ck == 3), 3));
xlim([t(1) t(end)]);
grid on;
plot_vline(0, 'k', 'cue');
ylim([-threshold.horizontal/2 threshold.horizontal/2]);
ylabel('[uV]');
xlabel('Time [s]')
legend(cdata.analysis.eog.components);
title('Class Middle');

subplot(1, 3, 3);
bar(100*[sum(Ed(Ck == 1, 1))./sum(Ck == 1) sum(Ed(Ck == 3, 1))./sum(Ck == 3) sum(Ed(Ck == 1 | Ck == 3, 1))./sum(Ck == 1 | Ck == 3); ...
         sum(Ed(Ck == 1, 2))./sum(Ck == 1) sum(Ed(Ck == 3, 2))./sum(Ck == 3) sum(Ed(Ck == 1 | Ck == 3, 2))./sum(Ck == 1 | Ck == 3)]');
grid on;
ylim([0 100]);
set(gca, 'XTickLabel', {'Left', 'Middle', 'Total'});
legend(cdata.analysis.eog.components);
ylabel('[%]');
xlabel('Trial group');
title('EOG detection');

suptitle(['Subject ' subject ' - EOG analysis']);


%% Saving analysis
targetfile = [saveroot '/' subject '_eog.mat'];
util_bdisp(['[out] - Saving analysis in: ' targetfile]);
eog.timings = timings;
eog.detection = Ed;
eog.value     = maxvalues;
eog.threshold = threshold;
eog.labels.Mk = Mk;
eog.labels.Rk = Rk;
eog.labels.Dk = Dk;
eog.labels.Ck = Ck;
eog.proc.fs   = SampleRate;

save(targetfile, 'eog');