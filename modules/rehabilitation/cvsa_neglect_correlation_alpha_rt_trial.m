clc;
clearvars;

sublist = {'SM', 'BC', 'TC'};
NumSubjects = length(sublist);
experiment  = 'neglect';

Classes     = [1 3];
NumClasses  = length(Classes);

% Datafiles
baseroot      = ['/mnt/data/Git/Codes/cvsa/analysis/' experiment '/'];

RoiLabels     = {'Healthy RoI (Left)', 'Affected RoI (Right)'};
NumRois       = length(RoiLabels);
HealthyRoiId  = 1;
AffectedRoiId = 2;

psd_precue  = [];   % Spectrum
npsd_precue = [];   % Spectrum normalized
spo  = [];  % Spectrum eye open
spon = [];  % Spectrum normalized eye open
spc  = [];  % Spectrum eye closed
spcn = [];  % Spectrum normalized eye closed
rt  = [];   % Reaction time
Td  = [];   % Target detection
Ar  = [];   % Artifacts
Oc  = [];   % EOG
peaks = cell(NumSubjects, 1);
peaks_rest = cell(NumSubjects, 1);
timings = cell(NumSubjects, 1);

Ck = [];    % Class mask    
Mk = [];    % Modality mask
Rk = [];    % Run mas
Dk = [];    % Day mask
Sk = [];    % Subject mask
AFk = [];
Dk_rest = []; % Day mask for eye closed/open
Bk_rest = []; % Pre/post mask for eye closed/open
Sk_rest = []; % Subject mask for eye closed/open

for sId = 1:NumSubjects
    csubject = sublist{sId};
    util_bdisp(['[io] - Loading files for subject ' csubject]);
    
    %% Alpha peak analysis on task
    cdata    = load([baseroot '/alphapeak/' csubject '_alphapeak_trial.mat']);
    psd_precue  = cat(3, psd_precue,  cdata.alphapeak.spectrum.precue.raw);
    npsd_precue = cat(3, npsd_precue, cdata.alphapeak.spectrum.precue.norm);
    cCk = cdata.alphapeak.labels.Ck;
    cMk = cdata.alphapeak.labels.Mk;
    cRk = cdata.alphapeak.labels.Rk;
    cDk = cdata.alphapeak.labels.Dk;
    cAFk = cdata.alphapeak.labels.AFk;
    peaks{sId} = cdata.alphapeak.peaks;
    timings{sId} = cdata.alphapeak.timings;
    
    %% Alpha peak analyisis on rest
    cdata    = load([baseroot '/alpharest/' csubject '_alpharest.mat']);
    spo  = cat(3, spo,  cdata.alpharest.spectrum.open.raw);
    spc  = cat(3, spc,  cdata.alpharest.spectrum.closed.raw);
    spon = cat(3, spon, cdata.alpharest.spectrum.open.norm);
    spcn = cat(3, spcn, cdata.alpharest.spectrum.closed.norm);

    cDk_rest = cdata.alpharest.labels.Dk;
    cBk_rest = cdata.alpharest.labels.Bk;
    peaks_rest{sId} = cdata.alpharest.peaks;
    
    %% Reaction time analysis
    cdata       = load([baseroot '/reactiontime/' csubject '_reactiontime.mat']);
    cindex = false(size(cdata.analysis.rt.edges, 1), 1);
    for cId = 1:NumClasses
        cindex = cindex | cdata.analysis.rt.labels.Ck == Classes(cId);
    end
    
    rt = cat(1, rt, cdata.analysis.rt.edges(cindex, 1));
    Td = cat(1, Td, cdata.analysis.rt.labels.Rd(cindex));
    
    if(isequal(cCk, cdata.analysis.rt.labels.Ck(cindex)) == false)
        warning('chk:lbl', 'Ck labels are different for rt analysis');
    end
    if(isequal(cMk, cdata.analysis.rt.labels.Mk(cindex)) == false)
        warning('chk:lbl', 'Mk labels are different for rt analysis');
    end
    if(isequal(cRk, cdata.analysis.rt.labels.Rk(cindex)) == false)
        warning('chk:lbl', 'Rk labels are different for rt analysis');
    end
    if(isequal(cDk, cdata.analysis.rt.labels.Dk(cindex)) == false)
        warning('chk:lbl', 'Dk labels are different for rt analysis');
    end
    
    %% Artifacts analysis
    cdata       = load([baseroot '/artifacts/' csubject '_artifacts.mat']);
    cindex = false(length(cdata.artifacts.Ak), 1);
    for cId = 1:NumClasses
        cindex = cindex | cdata.artifacts.labels.Ck == Classes(cId);
    end
    
    Ar = cat(1, Ar, cdata.artifacts.Ak(cindex));
    if(isequal(cCk, cdata.artifacts.labels.Ck(cindex)) == false)        % To be added other labels
        warning('chk:lbl', 'Ck labels are different for rt analysis');
    end
    
    %% EOG analysis
    cdata       = load([baseroot '/eog/' csubject '_eog.mat']);
    cindex = false(size(cdata.eog.detection, 1), 1);
    for cId = 1:NumClasses
        cindex = cindex | cdata.eog.labels.Ck == Classes(cId);
    end
    Oc = cat(1, Oc, cdata.eog.detection(cindex, 1));
    
    if(isequal(cCk, cdata.eog.labels.Ck(cindex)) == false)
        warning('chk:lbl', 'Ck labels are different for eog analysis');
    end
    if(isequal(cMk, cdata.eog.labels.Mk(cindex)) == false)
        warning('chk:lbl', 'Mk labels are different for eog analysis');
    end
    if(isequal(cRk, cdata.eog.labels.Rk(cindex)) == false)
        warning('chk:lbl', 'Rk labels are different for eog analysis');
    end
    if(isequal(cDk, cdata.eog.labels.Dk(cindex)) == false)
        warning('chk:lbl', 'Dk labels are different for eog analysis');
    end
    
    %% Concatenate vectors
    Ck = cat(1, Ck, cCk);
    Mk = cat(1, Mk, cMk);
    Rk = cat(1, Rk, cRk);
    Dk = cat(1, Dk, cDk);
    AFk = cat(1, AFk, cAFk);
    Sk = cat(1, Sk, sId*ones(length(cCk), 1));
    
    Dk_rest = cat(1, Dk_rest, cDk_rest);
    Bk_rest = cat(1, Bk_rest, cBk_rest);
    Sk_rest = cat(1, Sk_rest, sId*ones(length(cDk_rest), 1));
end

%% Artifact label index
ArtifactFree = AFk;

%% Extract correct alpha peak from spectrum per trials
alphapower  = [];
for sId = 1:NumSubjects
    speaks = peaks{sId};
    stimings = timings{sId};
    
    cindex = Sk == sId;
    cnumtrials = sum(cindex);
    
    cpsd_precue = npsd_precue(:, :, cindex);

    calphapower = zeros(2, cnumtrials);
    cpeaks = [];
    for trId = 1:cnumtrials 
        for rId = 1:NumRois
            cband = speaks.trial.bands(:, rId, trId);
            calphapower(rId, trId) = nanmean(cpsd_precue(cband, rId, trId), 1);
        end
    end      
    alphapower = cat(2, alphapower, calphapower);   
end

%% Extract correct alpha peak from spectrum per runs

alphapower_run  = [];
rt_run = [];
Sk_run  = [];
for sId = 1:NumSubjects
    cindex   = Sk == sId;
    cruns    = unique(Rk(cindex));
    cnumruns = length(cruns);
    commonindex = Ck == 1 & ArtifactFree & Td == 1;
   
    calphapower_run = zeros(2, cnumruns);
    crt_run         = zeros(cnumruns, 1);
    
    calphapower   = alphapower(:, cindex & commonindex);
    crt           = rt(cindex & commonindex);
    cRk           = Rk(cindex & commonindex);
    
    for rId = 1:cnumruns
       calphapower_run(:, rId) = nanmean(calphapower(:, cRk == rId), 2); 
       crt_run(rId) = nanmean(crt(cRk == rId));
    end
    
    alphapower_run = cat(2, alphapower_run, calphapower_run);
    rt_run         = cat(1, rt_run, crt_run);
    Sk_run          = cat(1, Sk_run, sId*ones(cnumruns, 1));
end


%% Plotting
CorrAlpha = 0.05;
CorrType = 'Spearman';

NumRows = 2;
NumCols = 3;

fig1 = figure;
fig_set_position(fig1, 'All');

CommonIndex = Td == 1 & Ck == 1 & ArtifactFree;

for sId = 1:NumSubjects
    
    % Trial based correlation
    cindex = Sk == sId;
    calphapower  = alphapower(AffectedRoiId, CommonIndex & cindex);
    crt          = rt(CommonIndex & cindex);
    
    calpha2plot  = calphapower';
    subplot(NumRows, NumCols, sId);
    [crho, cpval] = corr(calpha2plot, crt, 'type', CorrType, 'rows', 'complete');
    hs = scatter(calpha2plot, crt, 'o', 'MarkerFaceColor',[0 0.4470 0.7410], 'MarkerEdgeColor', 'black');
    
    xlim([0 1]);
    ylim([0 2]);
    grid on;
    hl = lsline;
    set(hl, 'LineWidth', 1.5, 'Color', 'r');
    cpos = get(gca,'Position');
    
    textcolor = 'k';
    if(cpval <= CorrAlpha)
        textcolor = 'r';
    end
    annotation('textbox', cpos, 'String', ['rho=' num2str(crho, '%3.2f') ', p=' num2str(cpval, '%3.3f')], 'LineStyle', 'none', 'Color', textcolor, 'FontWeight', 'bold')
    
    xlabel('Alpha power [norm]');
    ylabel('Reaction time [s]');
    title(['Subject ' sublist{sId} ' - per trial']);
    
    % Run based correlation
    subplot(NumRows, NumCols, sId + NumCols);
    cindex = Sk_run == sId;
    calphapower_run  = alphapower_run(AffectedRoiId, cindex);
    crt_run          = rt_run(cindex);
    cr_alpha2plot  = calphapower_run';
    
    [cr_rho, cr_pval] = corr(cr_alpha2plot, crt_run, 'type', CorrType, 'rows', 'complete');
    r_hs = scatter(cr_alpha2plot, crt_run, 'o', 'MarkerFaceColor',[0 0.4470 0.7410], 'MarkerEdgeColor', 'black');
    
    xlim([0 1]);
    ylim([0 2]);
    grid on;
    r_hl = lsline;
    set(r_hl, 'LineWidth', 1.5, 'Color', 'r');
    r_cpos = get(gca,'Position');
    
    textcolor = 'k';
    if(cr_pval <= CorrAlpha)
        textcolor = 'r';
    end
    annotation('textbox', r_cpos, 'String', ['rho=' num2str(cr_rho, '%3.2f') ', p=' num2str(cr_pval, '%3.3f')], 'LineStyle', 'none', 'Color', textcolor, 'FontWeight', 'bold')
    
    xlabel('Alpha power [norm]');
    ylabel('Reaction time [s]');
    title(['Subject ' sublist{sId} ' - per run']);
end



