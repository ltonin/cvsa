clearvars; clc;

SubList = {'SM', 'BC', 'TC'};
NumSubjects = length(SubList);

analysisdir = './analysis/neglect/reactiontime/';
figuredir   = './figures/journal/';

BonferroniCorrection = 2;

Classes   = [1 3];
ClassesLb = {'Left', 'Middle'};
NumClasses = length(Classes);

RT = [];
Sk = [];
Mk = [];
Ts = [];
Rd = [];
Rk = [];
AFk = [];

pvalues = zeros(NumClasses, NumSubjects);
for sId = 1:NumSubjects
    csubject = SubList{sId};
    
    % Loading rt data and extract info
    crtfile = [analysisdir '/' csubject '_reactiontime.mat'];
    util_bdisp(['[io] - Loading RT for subject ' csubject ': ' crtfile]);
    
    
    cdata = load(crtfile);
    

    crt  = cdata.analysis.rt.values;
    cTs  = cdata.analysis.rt.labels.Ts;
    cAFk = cdata.analysis.rt.labels.AFk;
    cRd  = cdata.analysis.rt.labels.Rd;
    cMk  = cdata.analysis.rt.labels.Mk;
    
    cModalities = unique(cMk);
    
    
    RT  = cat(1, RT, crt);
    Sk  = cat(1, Sk, sId*ones(length(crt), 1));
    Mk  = cat(1, Mk, cMk);
    Rk  = cat(1, Rk, (1:length(crt))');
    Ts  = cat(1, Ts, cTs);
    Rd  = cat(1, Rd, cRd);
    AFk = cat(1, AFk, cAFk);
    
    % Statistics
    for cId = 1:NumClasses
        cindex = cTs == Classes(cId) & cRd == true & cAFk == true;
        [~, cpvalue] = ttest2(crt(cindex & cMk == cModalities(1)), crt(cindex & cMk == cModalities(2)));
        cpvalue = cpvalue*BonferroniCorrection;
        fprintf('[stat] - %s - %s - T-Test between modalities (Bonferroni correction): p=%1.7f\n', csubject, ClassesLb{cId}, cpvalue);
        pvalues(cId, sId) = cpvalue;
        
        fprintf('[stat] - %s - %s - Reaction Time: %1.3f+/-%1.3f (Calibration); %1.3f+/-%1.3f (Online)\n', csubject, ClassesLb{cId}, ...
                  nanmean(crt(cindex & cMk == cModalities(1))), nanstd(crt(cindex & cMk == cModalities(1))), ...
                  nanmean(crt(cindex & cMk == cModalities(2))), nanstd(crt(cindex & cMk == cModalities(2))));
    end
end

%% Plotting fig1

RTcorr = zeros(NumSubjects, 1);
RTpval = zeros(NumSubjects, 1);

NumRows = 2;
NumCols = NumSubjects;

fig1 = figure;

for sId = 1:NumSubjects

    cindex = Sk == sId & Rd == true & (Ts == 1 | Ts == 3) & AFk == true;

    subplot(NumRows, NumCols, sId);
    boxplot(RT(cindex), {Ts(cindex) Mk(cindex)}, 'FactorSeparator', 1);
    set(gca, 'ActivePositionProperty', 'position')
    grid on;
    ylim([0 1.8]);
    ytick = get(gca, 'YTick');
    set(gca, 'YTick', ytick(ytick <= 1.6));

    set(gca,'xtickmode','auto','xticklabelmode','auto')
    set(gca, 'XTick', 1:4);
    set(gca, 'XTickLabel', {'Calibration', 'Online'});
    if ( sId == 1)
        ylabel('Reaction time [s]');
    end
    title(['Patient P' num2str(sId)]);

    cpos = get(gca, 'Position');
    cpos(2) = cpos(2) - 0.05;
    t1 = annotation('textbox', cpos, 'String', ['p=' num2str(pvalues(1, sId), '%3.3f')],  'LineStyle', 'none', 'Color', 'k', 'FontWeight', 'bold');
    t1.FontSize = 8;

    cpos(1) = cpos(1) + cpos(3)/2;
    t2 = annotation('textbox', cpos, 'String', ['p=' num2str(pvalues(2, sId), '%3.3f')],  'LineStyle', 'none', 'Color', 'k', 'FontWeight', 'bold');
    t2.FontSize = 8;

    % Correlation
    CorrSelClass = 1;
    cindex = Sk == sId & Ts == CorrSelClass & Rd == true & AFk;
    TrialIndex = 1:length(RT);
    [RTcorr(sId), RTpval(sId)] = corr(RT(cindex), TrialIndex(cindex)', 'rows', 'pairwise');
    
    subplot(NumRows, NumCols, sId + NumCols);
    cvalues = RT(cindex);
    plot(TrialIndex(cindex), cvalues, '.');
    xlim([min(TrialIndex(Sk == sId)) max(TrialIndex(Sk == sId))]);
    set(gca,'xtickmode','auto','xticklabelmode','auto')
    set(gca, 'ActivePositionProperty', 'position')
    hl = lsline;
    set(hl, 'Color', 'r', 'LineWidth', 2);
    grid on;
    ylim([0 1.6]);
    xlabel('Trial index');
    if ( sId == 1)
        ylabel('Reaction time [s]');
    end
    cpos = get(gca, 'Position');
    cpos(2) = cpos(2) - 0.03;
    t1 = annotation('textbox', cpos, 'String', ['rho= ' num2str(RTcorr(sId), '%3.3f') ', p=' num2str(RTpval(sId), '%3.3f')],  'LineStyle', 'none', 'Color', 'k', 'FontWeight', 'bold');
    t1.FontSize = 8;
    plot_vline(find(diff(Mk(cindex))), 'k:', 'online'); 
    title(['Correlation - Class ' ClassesLb{CorrSelClass}]);
end

fig_set_position(fig1, 'Top');
suptitle('Reaction time');

%% Plotting fig2

NumRows = 1;
NumCols = 2;

fig2 = figure;

ax = gca;
Colors = ax.ColorOrder;
cindex = Ts == 1 & AFk & Rd == true;
[RTocorr, RTopval] = corr(RT(cindex), Rk(cindex), 'rows', 'pairwise');

for sId = 1:NumSubjects
    for mId = 1:NumModali


subplot(NumRows, NumCols, 1);
boxplot(RT(cindex), {Sk(cindex) Mk(cindex) }, 'FactorSeparator', 1);
hbox = findobj(gca, 'Tag', 'Box');
bcolors = flipud(Colors(1:3, :));
for i = 1:2:length(hbox)
    set(hbox(i:i+1), 'Color', bcolors(ceil(i/2), :), 'LineWidth', 2);
end
ylim([0 1.7]);

cpos = get(gca, 'Position');
cpos(2) = cpos(2) - 0.06;
for sId = 1:NumSubjects
    t = annotation('textbox', cpos, 'String', ['p=' num2str(pvalues(1, sId), '%3.3f') ' (Bonferroni)'],  'LineStyle', 'none', 'Color', Colors(sId, :), 'FontWeight', 'bold');
    t.FontSize = 8;
    cpos(1) = cpos(1) + cpos(3)/3;
end

set(gca,'xtickmode','auto','xticklabelmode','auto')
set(gca, 'XTick', 1:6);
set(gca, 'XTickLabel', {'Calibration', 'Online'});
xlabel('Patient/Modality');
ylabel('Time [s]');
title('Reaction time between modalities (Left class)');
set(gca, 'YGrid', 'on');


subplot(NumRows, NumCols, 2);


hsub = zeros(NumSubjects, 1);
hold on;
for sId = 1:NumSubjects
    hsub(sId) = plot(Rk(cindex & Sk == sId), RT(cindex & Sk == sId), '.');
end

hovl   = plot(Rk(cindex), RT(cindex), '.');
set(hovl, 'Visible', 'off');
hold off;

hsub_l = lsline;
set(hsub_l, 'LineStyle', '-', 'LineWidth', 1.5);
set(hsub_l(1), 'LineStyle', '-', 'LineWidth', 2.5, 'Color', 'k');
LegendLb = {'Patient P1', 'Patient P2', 'Patient P3', 'Overall'};
legend([hsub; hsub_l(1)], LegendLb);
grid on;
ylim([0 1.7]);
xlabel('Trial index');
ylabel('Time [s]');

cpos = get(gca, 'Position');
cpos(2) = cpos(2) - 0.06;
xpos = [cpos(1) cpos(1)+0.08 cpos(1)+0.16];
for sId = 1:NumSubjects
    cpos(1) = xpos(sId);
    t = annotation('textbox', cpos, 'String', ['r= ' num2str(RTcorr(sId), '%3.3f') ', p=' num2str(RTpval(sId), '%03.3f')],  'LineStyle', 'none', 'Color', Colors(sId, :), 'FontWeight', 'bold');
    t.FontSize = 8;
end
title('Reaction time (Left class)');

fig_set_position(fig2, 'Top');
suptitle('Reaction time');

%% Exporting figure
filename = [figuredir '/cvsa.neglect.journal.reactiontime.raw.pdf']; 
fig_export(fig2, filename, '-pdf');