clc;
clearvars;

subject     = 'TC';
pattern     = '.va.';

features       = 'psd';
spatialfilter  = 'laplacian';
experiment     = 'neglect';

Classes   = [1 3];
ClassesLb    = {'LeftCue (neglected)', 'MiddleCue'};
NumClasses = length(Classes);

%% Initialization

% Timings
timings.trial.period    = 3;            % seconds
timings.offset.period   = 0;           % seconds

% Frequencies
selectedfrequencies       = 4:48;

% Channels
[~, channels.list]        = proc_get_montage('eeg.biosemi.64');
channels.hemisphere       = proc_get_hemisphere(channels.list);

% Nodes list
nodes.channels.labels{1} = {'P7',  'P5',  'P3', 'P1',  'Pz'};
nodes.channels.labels{2} = {'PO7', 'PO3', 'O1', 'POz', 'Oz'};
nodes.channels.labels{3} = {'P8',  'P6',  'P4', 'P2',  'Pz'};
nodes.channels.labels{4} = {'PO8', 'PO4', 'O2', 'POz', 'Oz'};

NumNodes = length(nodes.channels.labels);
for nId = 1:NumNodes
    nodes.channels.id{nId} = proc_get_channel(nodes.channels.labels{nId}, channels.list);
end

nodes.locations.hemisphere.id = [1 1 2 2];
nodes.locations.hemisphere.label = {'Left', 'Right'};
nodes.locations.regions.id = [1 2 1 2];
nodes.locations.regions.label = {'Parietal', 'Occipital'};

NumHemispheres = length(nodes.locations.hemisphere.label);

% Datafiles
baseroot = ['/mnt/data/Git/Codes/cvsa/analysis/' experiment '/'];
datapath = [baseroot '/' features '/' spatialfilter '/'];
[Files, NumFiles] = util_getfile(datapath, '.mat', [subject '*' pattern]);

analysispath = './analysis/';

%% Analysis - Import data

% Concatanate datafiles
util_bdisp(['[io] - Concatenating ' num2str(NumFiles) ' datafiles']);
[F, analysis] = cvsa_utilities_concatenate_data(Files);

% Extract information
SampleRate     = analysis.processing.fs;
FrameSize      = analysis.processing.fsize;
[~, selfreqId] = intersect(analysis.processing.f, selectedfrequencies);
FreqGrid       = analysis.processing.f(selfreqId);

F = F(:, selfreqId, :);
NumTotSamples  = size(F, 1);
NumFreqs       = size(F, 2);
NumChans       = size(F, 3);
NumRois        = 2;

%% Analysis - Masking data

util_bdisp('[proc] - Creating trial mask vector');
events = analysis.event;
timings.trial.size  = floor(timings.trial.period*SampleRate/FrameSize);
timings.offset.size = floor(timings.offset.period*SampleRate/FrameSize);
timings.trial.mask  = false(NumTotSamples, 1);
timings.trial.pos   = [];
for ctrialId = 1:NumClasses
    [cmask, cpos, cdur] = proc_get_event(events, Classes(ctrialId), NumTotSamples, timings.trial.size, timings.offset.size);
    timings.trial.pos   = sort([timings.trial.pos cpos']);
end

util_bdisp('[proc] - Creating trial based class labels');
index = false(length(events.TYP), 1);
for ctrialId = 1:NumClasses
    index = events.TYP == Classes(ctrialId) | index;
end
Ck = events.TYP(index);

%% Extracting trials
util_bdisp('[proc] - Extracting trials');
NumTrials  = length(timings.trial.pos);
NumSamples = timings.trial.size + abs(timings.offset.size);
S = zeros(NumSamples, NumFreqs, NumChans, NumTrials);
Mk = zeros(NumTrials, 1);
Rk = zeros(NumTrials, 1);
Dk = zeros(NumTrials, 1);
for trId = 1:NumTrials
    cstart = timings.trial.pos(trId);
    cstop  = cstart + NumSamples - 1;
    S(:, :, :, trId) = F(cstart:cstop, :, :);
    Mk(trId) = unique(analysis.label.Mk(cstart:cstop));
    Rk(trId) = unique(analysis.label.Rk(cstart:cstop));
    Dk(trId) = unique(analysis.label.Dk(cstart:cstop));
end
Modalities    = unique(Mk);
NumModalities = length(Modalities);
Runs          = unique(Rk);
NumRuns       = length(Runs);
Days          = unique(Dk);
NumDays       = length(Days);

RunMod = zeros(NumRuns, 1);
for rId = 1:NumRuns, 
    RunMod(rId) = unique(Mk(Rk == rId)); 
end

%% Analysis - Load artifacts analysis
cartifacts = load([baseroot '/artifacts/' subject '_artifacts.mat'], 'artifacts');
analysis.artifacts = cartifacts.artifacts;

ArtInd = false(length(analysis.artifacts.Ak), 1);
for ctrialId = 1:NumClasses
    ArtInd = ArtInd | analysis.artifacts.labels.Ck == Classes(ctrialId);
end

Ak = analysis.artifacts.Ak(ArtInd);

if strcmpi(subject, 'BC')
    Ak = analysis.artifacts.Am(ArtInd) >= 115;
end

%% Analysis - Load eog analysis
ceoganalysis = load([baseroot '/eog/' subject '_eog.mat'], 'eog');
EogInd = false(size(ceoganalysis.eog.detection, 1), 1);
for ctrialId = 1:NumClasses
    EogInd = EogInd | ceoganalysis.eog.labels.Ck == Classes(ctrialId);
end
Ed = ceoganalysis.eog.detection(EogInd, 1);

%% Analysis - Artifact index
ArtifactFree = Ak == false & Ed == false;

%% Analysis - Loading peaks for each trial
util_bdisp('[proc] - Loading alpha peak for each trial');
ciaf = load([baseroot '/alphapeak/' subject '_iaf.mat']);
sband  = ciaf.iaf.modalities.band;

%% Generic condition
GenericCondition = true;
dolog = true;

% if strcmpi(subject, 'SM')
%     frequency.selected.label = 8:14; % 8:16; % best % SM
% elseif strcmpi(subject, 'TC')
%     frequency.selected.label = 8:14; % 8:14; % best % TC
% elseif strcmpi(subject, 'BC')
%     frequency.selected.label = 8:14; % 6:14; % best % BC
% end
% [frequency.selected.label, frequency.selected.id] = intersect(FreqGrid, frequency.selected.label);

if (dolog == true)
    P = log(S+1);
else
    P = S;
end

%% Discriminancy
util_bdisp('[proc] - Computing discriminancy along nodes');
fisher_nodes_run = cell(NumNodes, 1);
for nId = 1:NumNodes
    cchannelids  = nodes.channels.id{nId};
    cnumchannels = length(cchannelids);
    cfisherrun = zeros(cnumchannels*NumFreqs, NumRuns);
    for rId = 1:NumRuns
        cP  = P(:, :, cchannelids, GenericCondition & Rk == Runs(rId));
        cCk = Ck(GenericCondition & Rk == Runs(rId));
        cfisherrun(:, rId) = proc_fisher(cP, cCk);
    end
    fisher_nodes_run{nId} = cfisherrun;
end

nfisher_nodes_run = cell(NumNodes, 1);
for nId = 1:NumNodes
    cfishernoderun = fisher_nodes_run{nId};
    for rId = 1:NumRuns
         %nfisher_nodes_run{nId}(:, rId) = proc_zfisher(cfishernoderun(:, rId)); 
         nfisher_nodes_run{nId}(:, rId) = cfishernoderun(:, rId); 
    end
end

%% Reshaping discriminancy
util_bdisp('[proc] - Reshaping discriminancy');
rfisher = cell(NumNodes, 1);
for nId = 1:NumNodes
    cfisher = nfisher_nodes_run{nId}; 
    cnumchannels = length(nodes.channels.id{nId});
    rfisher{nId} = reshape(cfisher, [NumFreqs cnumchannels NumRuns]);
end


%% Frequency selection
util_bdisp('[proc] - Selecting frequency according to IAF');
srfisher = cell(NumNodes, 1);
% for nId = 1:NumNodes
%     cfisher = rfisher{nId}(frequency.selected.id, :, :);
%     srfisher{nId} = squeeze(sum(cfisher, 1));
% end

% parietal.frequency.dfisher  = squeeze(sum(parietal.node.dfisher(frequency.selected.id, :), 1));
% occipital.frequency.dfisher = squeeze(sum(occipital.node.dfisher(frequency.selected.id, :), 1));
% overall.frequency.dfisher   = squeeze(sum(overall.node.dfisher(frequency.selected.id, :), 1));

for nId = 1:NumNodes
    for rId = 1:NumRuns
        cfisher = rfisher{nId}(:, :, rId);
        cmodality   = find(Modalities == RunMod(rId));    
        chemisphere = nodes.locations.hemisphere.id(nId);
        cband = sband(:, chemisphere, cmodality);
        srfisher{nId}(:, rId) = squeeze(sum(cfisher(cband, :), 1));
    end
end
%% Nodes selection
util_bdisp('[proc] - Hemispheric differences along nodes');
% Differences parietal and occipital
parietal.dfisher  = squeeze(sum(srfisher{3}, 1) - sum(srfisher{1}, 1));
occipital.dfisher = squeeze(sum(srfisher{4}, 1) - sum(srfisher{2}, 1));
overall.dfisher   = squeeze((sum(srfisher{4}, 1) + sum(srfisher{3}, 1)) - (sum(srfisher{2}, 1) + sum(srfisher{1}, 1)));

%% Correlation
corrtype = 'Spearman';
util_bdisp(['[proc] - Computing correlation (' corrtype ')']);
[parietal.correlation.value,  parietal.correlation.pvalue]  = corr(Runs, parietal.dfisher',  'type', corrtype);
[occipital.correlation.value, occipital.correlation.pvalue] = corr(Runs, occipital.dfisher', 'type', corrtype);
[overall.correlation.value,   overall.correlation.pvalue]   = corr(Runs, overall.dfisher',   'type', corrtype);

%% Reconstruct channel signals
util_bdisp('[proc] - Reconstructing signal in channels space');
tfisher = nan(NumChans, NumRuns);
for rId = 1:NumRuns
    for nId = 1:NumNodes
        cfisher = srfisher{nId}(:, rId);
        cchanIds = nodes.channels.id{nId};
        rdata = nan(NumChans, 1);
        rdata(cchanIds) = cfisher;
        tfisher(:, rId) = nanmean([tfisher(:, rId) rdata], 2);
    end
end

%% Statistical test

[cpval, ctab] = anova1(parietal.dfisher, RunMod, 'off');
parietal.anova.pvalue = cpval;
parietal.anova.fvalue = ctab{2, 5};
fprintf('[stat] - Parietal nodes  - Calibration vs online:\tF=%2.1f, p=%1.5f\n', parietal.anova.fvalue, parietal.anova.pvalue);

[cpval, ctab] = anova1(occipital.dfisher, RunMod, 'off');
occipital.anova.pvalue = cpval;
occipital.anova.fvalue = ctab{2, 5};
fprintf('[stat] - Occipital nodes - Calibration vs online:\tF=%2.1f, p=%1.5f\n', occipital.anova.fvalue, occipital.anova.pvalue);

[cpval, ctab] = anova1(overall.dfisher, RunMod, 'off');
overall.anova.pvalue = cpval;
overall.anova.fvalue = ctab{2, 5};
fprintf('[stat] - Overall nodes   - Calibration vs online:\tF=%2.1f, p=%1.5f\n', overall.anova.fvalue, overall.anova.pvalue);

%% Plotting
util_bdisp('[plot] - Plotting results');
CorrAlpha = 0.05;
fig1 = figure;
fig_set_position(fig1, 'All');

NumRows = 3;
NumCols = 3;

Region2Plot = {parietal, occipital, overall};
Region2PlotLabels = {'Parietal nodes', 'Occipital nodes', 'Overall nodes'};

ModLabels = {'Calibration', 'Online'};

% Topoplots average
load('chanlocs64.mat');

for mId = 1:NumModalities
    subplot(NumRows, NumCols, mId + (mId - 1));
    cmodality = Modalities(mId);
    cdata = nansum(tfisher(:, RunMod == cmodality), 2)./max(nansum(tfisher(:, RunMod == cmodality), 2));
    topoplot(cdata, chanlocs, 'conv', 'on', 'headrad', 'rim', 'maplimits', [0 1]);
    axis image; 
    title(ModLabels{mId});
end

% Boxplots
axbox = zeros(length(Region2Plot), 1);
for pId = 1:length(Region2Plot)
    cdata = Region2Plot{pId};
    subplot(NumRows, NumCols, NumCols + pId);
    boxplot(cdata.dfisher, RunMod);
    grid on;
    axbox(pId) = gca;
    set(gca, 'XTickLabel', ModLabels);
    if pId == 1
        ylabel('dFisher');
    else
        set(gca, 'YTickLabel', '');
    end
    plot_hline(0, 'k');
    title(Region2PlotLabels{pId});
    cpos = get(gca,'Position');
    cpos(2) = cpos(2) - 0.02;

    textcolor = 'k';
    if(cdata.anova.pvalue <= CorrAlpha)
        textcolor = 'r';
    end
    t = annotation('textbox', cpos, 'String', ['F=' num2str(cdata.anova.fvalue, '%3.2f') ...
                                         ', p=' num2str(cdata.anova.pvalue, '%3.3f')], ...
                                         'LineStyle', 'none', 'Color', textcolor, 'FontWeight', 'bold');
    t.FontSize = 8;
end
plot_set_limits(axbox, 'y', [-1.5 1.5]);


% Correlation graphs
axcorr = zeros(length(Region2Plot), 1);
for pId = 1:length(Region2Plot)
    cdata = Region2Plot{pId};
    subplot(NumRows, NumCols, 2*NumCols + pId);
    scatter(Runs, cdata.dfisher, 'o');
    axcorr(pId) = gca;
    hl = lsline(gca);
    set(hl, 'LineWidth', 1.5, 'Color', 'r');
    grid on;
    
    xlim([1 NumRuns]);
    plot_hline(0, 'k');
    
    cpos = get(gca,'Position');
    cpos(2) = cpos(2) - 0.02;
    
    textcolor = 'k';
    if(cdata.correlation.pvalue <= CorrAlpha)
        textcolor = 'r';
    end
    t = annotation('textbox', cpos, 'String', ...
                  ['F=' num2str(cdata.correlation.value, '%3.2f') ...
                   ', p=' num2str(cdata.correlation.pvalue, '%3.3f')], ...
                   'LineStyle', 'none', 'Color', textcolor, 'FontWeight', 'bold');
    t.FontSize = 8;
    
    xlabel('Runs');
    if pId == 1
        ylabel('dFisher');
    else
        set(gca, 'YTickLabel', '');
    end
    title(Region2PlotLabels{pId});
end

plot_set_limits(axcorr, 'y', [-1.2 1]);
for pId = 1:length(Region2Plot)
    set(fig1, 'currentaxes', axcorr(pId));
    plot_vline(find(RunMod, 1) - 0.5, 'k--', 'online');
end
suptitle(['Patient ' subject]);
