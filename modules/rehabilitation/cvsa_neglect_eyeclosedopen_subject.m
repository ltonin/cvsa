clc;
clearvars;

subject     = 'TC';
pattern     = '.vaeyes.';

features       = 'psd';
spatialfilter  = 'car';
experiment     = 'neglect';

%% Initialization

% Channels and RoIs
[~, channels.list]        = proc_get_montage('eeg.biosemi.64');
% channels.roi.left.labels  = {'P7', 'P5', 'PO7'};         % As in [Thut et al., 2006]
% channels.roi.right.labels = {'P8', 'P6', 'PO8'};         % As in [Thut et al., 2006]
channels.roi.left.labels  = {'P5', 'P3', 'P1', 'PO7', 'PO3', 'O1'};         % Custom
channels.roi.right.labels = {'P6', 'P4', 'P2', 'PO8', 'PO4', 'O2'};         % Custom
channels.roi.left.id      = proc_get_channel(channels.roi.left.labels,  channels.list);
channels.roi.right.id     = proc_get_channel(channels.roi.right.labels, channels.list);
channels.roi.labels       = {'LeftRoI', 'RightRoI'};

% Datafiles

baseroot      = ['/mnt/data/Git/Codes/cvsa/analysis/' experiment '/'];
datapath      = [baseroot '/' features '/' spatialfilter '/'];
[Files, NumFiles] = util_getfile(datapath, '.mat', [subject '*' pattern]);
[~, saveroot] = util_mkdir(baseroot, 'alpharest/');

%% Analysis - Import data
Sc = [];
So = [];
Dk = [];
Bk = [];
cdaylbl  = '';
cday     = 0;
pproc    = '';
pspatial = '';
cbefore  = true;
% Concatanate datafiles
util_bdisp(['[io] - Concatenating ' num2str(NumFiles) ' datafiles']);
for fId = 1:NumFiles
    cfilename = Files{fId};
    info = util_getfile_info(cfilename);
    util_bdisp(['[io] - Loading file ' num2str(fId) '/' num2str(NumFiles)]);
    disp(['       File: ' cfilename]);
    
    cdata = load(cfilename);
    
    % Get day from filename
    if strcmpi(info.date, cdaylbl) == false
        cday = cday + 1;
        cdaylbl = info.date;
    end
    
    % Compare processing settings
    if isempty(pproc) == true
        pproc = cdata.analysis.processing;
    else
        if isequal(pproc, cdata.analysis.processing) == false
            error('chk:ana', ['[' mfilename '] Concatenation of processed data with different processing settings']);
        end
    end

    % Compare spatial filter settings
    if isempty(pspatial) == true
        pspatial = cdata.analysis.spatial;
    else
        if isequal(pspatial, cdata.analysis.spatial) == false
            error('chk:ana', ['[' mfilename '] Concatenation of processed data with different spatial filter settings']);
        end
    end
    
    analysis.processing = pproc;
    analysis.spatial    = pspatial;
    
    
    Sc = cat(3, Sc, cdata.psdclosed);
    So = cat(3, So, cdata.psdopen);
    Dk = cat(1, Dk, cday);
    cbefore = ~cbefore;
    Bk = cat(1, Bk, cbefore);
end

Freqs    = analysis.processing.f;
NumFreqs = size(Sc, 1);
Days     = unique(Dk);
NumDays  = length(Days);
NumRuns  = size(Sc, 3);

%% Extracting spectrum on RoI
util_bdisp('[proc] - Extracting spectrum on RoI');
SPc  = zeros(NumFreqs, 2, NumRuns);
SPo  = zeros(NumFreqs, 2, NumRuns);
SPcn = zeros(NumFreqs, 2, NumRuns);
SPon = zeros(NumFreqs, 2, NumRuns);
for rId = 1:NumRuns
    cvalues_c_l = mean(Sc(:, channels.roi.left.id, rId), 2);
    cvalues_c_r = mean(Sc(:, channels.roi.right.id, rId), 2);
    
    cvalues_o_l = mean(So(:, channels.roi.left.id, rId), 2);
    cvalues_o_r = mean(So(:, channels.roi.right.id, rId), 2);
        
    SPc(:, 1, rId) = cvalues_c_l;
    SPc(:, 2, rId) = cvalues_c_r;
    SPo(:, 1, rId) = cvalues_o_l;
    SPo(:, 2, rId) = cvalues_o_r;
    
    SPcn(:, 1, rId) = cvalues_c_l./max(cvalues_c_l);
    SPcn(:, 2, rId) = cvalues_c_r./max(cvalues_c_r);
    SPon(:, 1, rId) = cvalues_o_l./max(cvalues_o_l);
    SPon(:, 2, rId) = cvalues_o_r./max(cvalues_o_r);
end

%% Find the alpha peak based on healthy RoI
util_bdisp('[proc] - Computing alpha peak');
if(strcmp(subject, 'SM'))
    FreqPeakRange = 8:12;
elseif(strcmp(subject, 'TC'))
    FreqPeakRange = 8:12;
elseif(strcmp(subject, 'BC'))  
    FreqPeakRange = 6:12;
end


pks.closed.healthy.locs  = zeros(NumRuns, 1);
pks.closed.affected.locs = zeros(NumRuns, 1);
pks.closed.affected.band = zeros(3, NumRuns);
pks.closed.healthy.band  = zeros(3, NumRuns);
for rId = 1:NumRuns
    [~, chlocs] = findpeaks(SPcn(:, 1, rId));
    [~, calocs] = findpeaks(SPcn(:, 2, rId));
    
    hidx = find(Freqs(chlocs) >= FreqPeakRange(1) & Freqs(chlocs) <= FreqPeakRange(end), 1);    
    aidx = find(Freqs(calocs) >= FreqPeakRange(1) & Freqs(calocs) <= FreqPeakRange(end), 1);
    
    pks.closed.healthy.locs(rId) = chlocs(hidx);
    
    if(isempty(aidx)) 
        pks.closed.affected.locs(rId) = chlocs(hidx);
    else
        pks.closed.affected.locs(rId) = calocs(aidx);
    end
    
   
    pks.closed.healthy.band(:, rId)  = pks.closed.healthy.locs(rId)-1:pks.closed.healthy.locs(rId)+1;
    pks.closed.affected.band(:, rId) = pks.closed.affected.locs(rId)-1:pks.closed.affected.locs(rId)+1;
end

pks.open.healthy.locs  = zeros(NumRuns, 1);
pks.open.affected.locs = zeros(NumRuns, 1);
pks.open.affected.band = zeros(3, NumRuns);
pks.open.healthy.band  = zeros(3, NumRuns);
for rId = 1:NumRuns
    [~, chlocs] = findpeaks(SPon(:, 1, rId));
    [~, calocs] = findpeaks(SPon(:, 2, rId));
    
    hidx = find(Freqs(chlocs) >= FreqPeakRange(1) & Freqs(chlocs) <= FreqPeakRange(end), 1);    
    aidx = find(Freqs(calocs) >= FreqPeakRange(1) & Freqs(calocs) <= FreqPeakRange(end), 1);
    
    if(isempty(hidx))
        [~, hidx] =intersect(Freqs, 10);
    end
    
    pks.open.healthy.locs(rId) = chlocs(hidx);
    
    if(isempty(aidx)) 
        pks.open.affected.locs(rId) = chlocs(hidx);
    else
        pks.open.affected.locs(rId) = calocs(aidx);
    end
    
   
    pks.open.healthy.band(:, rId)  = pks.open.healthy.locs(rId)-1:pks.open.healthy.locs(rId)+1;
    pks.open.affected.band(:, rId) = pks.open.affected.locs(rId)-1:pks.open.affected.locs(rId)+1;
end

%% Extract alpha power per run
util_bdisp('[proc] - Extracting alpha power per run base on alpha peak');
AlphaPower = zeros(2, NumRuns, 2);
for rId = 1:NumRuns
    AlphaPower(1, rId, 1) = squeeze(mean(SPcn(pks.closed.healthy.band(:, rId), 1, rId), 1));
    AlphaPower(2, rId, 1) = squeeze(mean(SPcn(pks.closed.affected.band(:, rId), 2, rId), 1));
    AlphaPower(1, rId, 2) = squeeze(mean(SPon(pks.open.healthy.band(:, rId), 1, rId), 1));
    AlphaPower(2, rId, 2) = squeeze(mean(SPon(pks.open.affected.band(:, rId), 2, rId), 1));
end


%% Saving analysis        

targetfile = [saveroot '/' subject '_alpharest.mat'];
util_bdisp(['[out] - Saving analysis in: ' targetfile]);

alpharest.spectrum.closed.raw  = SPc;
alpharest.spectrum.open.raw    = SPo;
alpharest.spectrum.closed.norm = SPcn;
alpharest.spectrum.open.norm   = SPon;

alpharest.peaks          = pks;
alpharest.peaks.range    = FreqPeakRange;
alpharest.channels       = channels;
alpharest.labels.Dk      = Dk;
alpharest.labels.Bk      = Bk;

save(targetfile, 'alpharest');   


%% Plotting
fig1 = figure;
fig_set_position(fig1, 'All');

CorrAlpha = 0.05;
CorrType  = 'Spearman';

CalibrationPeriod = 1:3;
OnlinePeriod  = 4:NumRuns;

% TopLeft - EyeClosed, Healthy RoI
subplot(3, 3, 1)
hold on;
plot(Freqs, mean(SPcn(:, 1, CalibrationPeriod), 3), 'LineWidth', 2);
plot(Freqs, mean(SPcn(:, 1, OnlinePeriod), 3), ':', 'LineWidth', 2);
hold off;
xlim([Freqs(1) Freqs(end)]);
ylim([0 1]);
xlabel('[Hz]');
ylabel('NormPsd');
grid on;
title('Spectrum EyeClosed - Healthy RoI (Left)');
legend('Calibration', 'Online');

% TopMiddle - correlation EyeClosed-Runs
subplot(3, 3, 2);
hold on; 
ax1 = plot(squeeze(AlphaPower(1, :, 1)), 'bo'); 
ax2 = plot(squeeze(AlphaPower(2, :, 1)), 'ro'); 
legend('Healthy RoI', 'Affected RoI');
lsline(get(ax1, 'Parent')); 
lsline(get(ax2, 'Parent')); 
hold off
grid on;
xlim([1 NumRuns]);
ylim([min(min(AlphaPower(:, :, 1))) - 0.1 max(max(AlphaPower(:, :, 1))) + 0.1]);
xlabel('Runs')
ylabel('NormPsd');
title('EyeClosed alpha power over runs');

[crho(1), cpval(1)] = corr((1:NumRuns)', squeeze(AlphaPower(1, :, 1))', 'type', CorrType);
[crho(2), cpval(2)] = corr((1:NumRuns)', squeeze(AlphaPower(2, :, 1))', 'type', CorrType);
cpos = get(get(ax1, 'Parent'),'Position') - [0 0.03 0 0];
roilb = {'HRoI', 'ARoI'};
for i = 1:length(crho)
    textcolor = 'k';
    if(cpval(i) <= CorrAlpha)
        textcolor = 'r';
    end
    annotation('textbox', cpos, 'String', [roilb{i} ': rho=' num2str(crho(i), '%3.2f') ', p=' num2str(cpval(i), '%3.3f')], 'LineStyle', 'none', 'Color', textcolor, 'FontWeight', 'bold');
    cpos(2) = cpos(2) - 0.02;
end

% TopRight - EyeClosed, Affected RoI
subplot(3, 3, 3)
hold on;
plot(Freqs, mean(SPcn(:, 2, CalibrationPeriod), 3), 'LineWidth', 2);
plot(Freqs, mean(SPcn(:, 2, OnlinePeriod), 3), ':', 'LineWidth', 2);
hold off;
xlim([Freqs(1) Freqs(end)]);
ylim([0 1]);
xlabel('[Hz]');
ylabel('NormPsd');
grid on;
title('Spectrum EyeClosed - Affected RoI (Right)');
legend('Calibration', 'Online');

% CenterLeft - EyeOpen, Healthy RoI
subplot(3, 3, 4)
hold on;
plot(Freqs, mean(SPon(:, 1, CalibrationPeriod), 3), 'LineWidth', 2);
plot(Freqs, mean(SPon(:, 1, OnlinePeriod), 3), ':', 'LineWidth', 2);
hold off;
xlim([Freqs(1) Freqs(end)]);
ylim([0 1]);
xlabel('[Hz]');
ylabel('NormPsd');
grid on;
title('Spectrum EyeOpen - Healthy RoI (Left)');
legend('Calibration', 'Online');

% CenterMiddle - correlation EyeClosed-Runs
subplot(3, 3, 5)
hold on; 
ax1 = plot(squeeze(AlphaPower(1, :, 2)), 'bo'); 
ax2 = plot(squeeze(AlphaPower(2, :, 2)), 'ro'); 
legend('Healthy RoI', 'Affected RoI');
lsline(get(ax1, 'Parent')); 
lsline(get(ax2, 'Parent')); 
hold off
grid on;
xlim([1 NumRuns]);
ylim([min(min(AlphaPower(:, :, 2))) - 0.1 max(max(AlphaPower(:, :, 2))) + 0.1]);
xlabel('Runs')
ylabel('NormPsd');
title('EyeOpen alpha power over runs');

[crho(1), cpval(1)] = corr((1:NumRuns)', squeeze(AlphaPower(1, :, 2))', 'type', CorrType);
[crho(2), cpval(2)] = corr((1:NumRuns)', squeeze(AlphaPower(2, :, 2))', 'type', CorrType);
cpos = get(get(ax1, 'Parent'),'Position') - [0 0.01 0 0];
roilb = {'HRoI', 'ARoI'};
for i = 1:length(crho)
    textcolor = 'k';
    if(cpval(i) <= CorrAlpha)
        textcolor = 'r';
    end
    annotation('textbox', cpos, 'String', [roilb{i} ': rho=' num2str(crho(i), '%3.2f') ', p=' num2str(cpval(i), '%3.3f')], 'LineStyle', 'none', 'Color', textcolor, 'FontWeight', 'bold');
    cpos(2) = cpos(2) - 0.02;
end

% CenterRight - EyeOpen, Affected RoI
subplot(3, 3, 6)
hold on;
plot(Freqs, mean(SPon(:, 2, CalibrationPeriod), 3), 'LineWidth', 2);
plot(Freqs, mean(SPon(:, 2, OnlinePeriod), 3), ':', 'LineWidth', 2);
hold off;
xlim([Freqs(1) Freqs(end)]);
ylim([0 1]);
xlabel('[Hz]');
ylabel('NormPsd');
grid on;
title('Spectrum EyeOpen - Affected RoI (Right)');
legend('Calibration', 'Online');


% BottomLeft - Correlation EyeClosedAfter./EyeClosedPre
subplot(3, 3, 7)
hold on; 
ax1 = plot(squeeze(AlphaPower(1, Bk == 1, 1)./AlphaPower(1, Bk == 0, 1)), 'bo'); 
ax2 = plot(squeeze(AlphaPower(2, Bk == 1, 1)./AlphaPower(2, Bk == 0, 1)), 'ro'); 
legend('Healthy RoI', 'Affected RoI');
lsline(get(ax1, 'Parent')); 
lsline(get(ax2, 'Parent')); 
hold off
grid on;
xlim([1 NumRuns/2]);
ylim([0.5 1.5]);
xlabel('Runs')
ylabel('NormPsd');
title('EyeClosedPost/EyeClosedPre over runs');

[crho(1), cpval(1)] = corr((1:NumRuns/2)', squeeze(AlphaPower(1, Bk == 1, 1)./AlphaPower(1, Bk == 0, 1))', 'type', CorrType);
[crho(2), cpval(2)] = corr((1:NumRuns/2)', squeeze(AlphaPower(2, Bk == 1, 1)./AlphaPower(2, Bk == 0, 1))', 'type', CorrType);
cpos = get(get(ax1, 'Parent'),'Position') - [0 0.01 0 0];
roilb = {'HRoI', 'ARoI'};
for i = 1:length(crho)
    textcolor = 'k';
    if(cpval(i) <= CorrAlpha)
        textcolor = 'r';
    end
    annotation('textbox', cpos, 'String', [roilb{i} ': rho=' num2str(crho(i), '%3.2f') ', p=' num2str(cpval(i), '%3.3f')], 'LineStyle', 'none', 'Color', textcolor, 'FontWeight', 'bold');
    cpos(2) = cpos(2) - 0.02;
end

% BottomMiddle - Correlation EyeOpenAfter./EyeOpenPre
subplot(3, 3, 8)
hold on; 
ax1 = plot(squeeze(AlphaPower(1, Bk == 1, 2)./AlphaPower(1, Bk == 0, 2)), 'bo'); 
ax2 = plot(squeeze(AlphaPower(2, Bk == 1, 2)./AlphaPower(2, Bk == 0, 2)), 'ro'); 
legend('Healthy RoI', 'Affected RoI');
lsline(get(ax1, 'Parent')); 
lsline(get(ax2, 'Parent')); 
hold off
grid on;
xlim([1 NumRuns/2]);
ylim([min(min(AlphaPower(:, Bk == 1, 2)./AlphaPower(:, Bk == 0, 2)))-0.1 max(max(AlphaPower(:, Bk == 1, 2)./AlphaPower(:, Bk == 0, 2)))+0.1]);
xlabel('Runs')
ylabel('NormPsd');
title('EyeOpenPost/EyeOpenPre over runs');

[crho(1), cpval(1)] = corr((1:NumRuns/2)', squeeze(AlphaPower(1, Bk == 1, 2)./AlphaPower(1, Bk == 0, 2))', 'type', CorrType);
[crho(2), cpval(2)] = corr((1:NumRuns/2)', squeeze(AlphaPower(2, Bk == 1, 2)./AlphaPower(2, Bk == 0, 2))', 'type', CorrType);
cpos = get(get(ax1, 'Parent'),'Position') - [0 0.01 0 0];
roilb = {'HRoI', 'ARoI'};
for i = 1:length(crho)
    textcolor = 'k';
    if(cpval(i) <= CorrAlpha)
        textcolor = 'r';
    end
    annotation('textbox', cpos, 'String', [roilb{i} ': rho=' num2str(crho(i), '%3.2f') ', p=' num2str(cpval(i), '%3.3f')], 'LineStyle', 'none', 'Color', textcolor, 'FontWeight', 'bold');
    cpos(2) = cpos(2) - 0.02;
end

% BottomRight - Correlation EyeRatio
subplot(3, 3, 9)
hold on; 
AlphaRatio = AlphaPower(:, :, 2)./AlphaPower(:, :, 1);
ax1 = plot(squeeze(AlphaRatio(1, Bk == 1)./AlphaRatio(1, Bk == 0)), 'bo'); 
ax2 = plot(squeeze(AlphaRatio(2, Bk == 1)./AlphaRatio(2, Bk == 0)), 'ro'); 
legend('Healthy RoI', 'Affected RoI');
lsline(get(ax1, 'Parent')); 
lsline(get(ax2, 'Parent')); 
hold off
grid on;
xlim([1 NumRuns/2]);
ylim([min(min(AlphaRatio(:, Bk == 1)./AlphaRatio(:, Bk == 0)))-0.1 max(max(AlphaRatio(:, Bk == 1)./AlphaRatio(:, Bk == 0)))+0.1]);
xlabel('Runs')
ylabel('NormPsd');
title('AlphaRationPost/AlphaRationPre over runs');

[crho(1), cpval(1)] = corr((1:NumRuns/2)', squeeze(AlphaRatio(1, Bk == 1)./AlphaRatio(1, Bk == 0))', 'type', CorrType);
[crho(2), cpval(2)] = corr((1:NumRuns/2)', squeeze(AlphaRatio(2, Bk == 1)./AlphaRatio(2, Bk == 0))', 'type', CorrType);
cpos = get(get(ax1, 'Parent'),'Position') - [0 0.01 0 0];
roilb = {'HRoI', 'ARoI'};
for i = 1:length(crho)
    textcolor = 'k';
    if(cpval(i) <= CorrAlpha)
        textcolor = 'r';
    end
    annotation('textbox', cpos, 'String', [roilb{i} ': rho=' num2str(crho(i), '%3.2f') ', p=' num2str(cpval(i), '%3.3f')], 'LineStyle', 'none', 'Color', textcolor, 'FontWeight', 'bold');
    cpos(2) = cpos(2) - 0.02;
end


suptitle(['Subject ' subject]);