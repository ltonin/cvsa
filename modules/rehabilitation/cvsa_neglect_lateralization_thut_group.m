clearvars; clc;

sublist        = {'b4', 'c2', 'b2', 'f1', 'g7', 'h6', 'e7', 'a7'};
nsubjects      = length(sublist);
features       = 'psd';
spatialfilter  = 'car';
selectedband   = 'alpha';

saveroot       = '/mnt/data/Git/Codes/cvsa/';
savedir        = [saveroot 'analysis/online/lateralization/thut/'];
[~, figuredir] = util_mkdir(saveroot, 'figures/lateralization/');

LI  = [];
TSE = [];
Mk  = [];
Rk  = [];
Dk  = [];
Ck  = [];
Sk  = [];

for sId = 1:nsubjects
    csubject = sublist{sId};
    util_bdisp(['[in] - Loading lateralization analysis for subject ' csubject]);
    cfilename = [savedir '/' csubject '_lateralization_' spatialfilter '_' selectedband '.mat'];
    cdata = load(cfilename);
    
    LI = cat(3, LI, cdata.LI);
    TSE = cat(4, TSE, cdata.TSE);
    
    Mk = cat(1, Mk, cdata.analysis.lateralization.labels.Mk);
    Rk = cat(1, Rk, cdata.analysis.lateralization.labels.Rk);
    Dk = cat(1, Dk, cdata.analysis.lateralization.labels.Dk);
    Ck = cat(1, Ck, cdata.analysis.lateralization.class.labels);
    Sk = cat(1, Sk, sId*ones(length(cdata.analysis.lateralization.class.labels), 1));
    settings = cdata.analysis.lateralization;
end

Modalities    = unique(settings.labels.Mk);
NumModalities = length(Modalities);
Runs          = unique(settings.labels.Rk);
NumRuns       = length(Runs);
Classes       = unique(settings.class.values);
NumClasses    = length(Classes);
%% Plotting
util_bdisp('[plot] - Plotting results');
fig1 = figure;
fig_set_position(fig1, 'Top');

SelectedFreqId = settings.proc.freq.selected.id{util_cellfind({selectedband}, settings.proc.freq.selected.label)};
RoiNames = {'Left', 'Right'};
t = settings.timings.offset.period:settings.proc.framesize/settings.proc.samplerate:settings.timings.trial.period - settings.proc.framesize/settings.proc.samplerate;
settings.class.colors = {'b', 'r'};
modalities.tickness = [1 3];
modalities.styles   = {'-', '--'};

% TSE for Left RoI
subplot(2, 3, 1);
hold on;
for mId = 1:NumModalities
    for cId = 1:NumClasses
        values = squeeze(mean(mean(TSE(:, SelectedFreqId, 1, Ck == settings.class.values(cId) & Mk == Modalities(mId)), 2), 4));
        plot(t, values, settings.class.colors{cId}, 'LineWidth', modalities.tickness(mId), 'LineStyle', modalities.styles{mId});
    end
end
hold off;
grid on;
xlim([t(1) t(end)]);
xlabel('Time [s]');
ylabel('[uV]');
title('Left ROI');
legend(settings.class.names, 'location', 'best');
plot_vline(0, 'k', 'cue');

% Lateralization index
subplot(2, 3, 2);
hold on;
for mId = 1:NumModalities
    for cId = 1:NumClasses
        values = squeeze(mean(mean(LI(:, SelectedFreqId, Ck == settings.class.values(cId) & Mk == Modalities(mId)), 2), 3));
        plot(t, values, settings.class.colors{cId}, 'LineWidth', modalities.tickness(mId), 'LineStyle', modalities.styles{mId});
    end
end
hold off;
grid on;
xlim([t(1) t(end)]);
xlabel('Time [s]');
ylabel('[uV]');
title('Lateralization Index');
legend(settings.class.names, 'location', 'best');
plot_vline(0, 'k', 'cue');

% TSE for Right RoI
subplot(2, 3, 3);
hold on;
for mId = 1:NumModalities
    for cId = 1:NumClasses
        values = squeeze(mean(mean(TSE(:, SelectedFreqId, 2, Ck == settings.class.values(cId) & Mk == Modalities(mId)), 2), 4));
        plot(t, values, settings.class.colors{cId}, 'LineWidth', modalities.tickness(mId), 'LineStyle', modalities.styles{mId});
    end
end
hold off;
grid on;
xlim([t(1) t(end)]);
xlabel('Time [s]');
ylabel('[uV]')
title('Right RoI');
legend(settings.class.names, 'location', 'best');
plot_vline(0, 'k', 'cue');

% Lateralization index over runs
subplot(2, 3, [4 5 6]);
trialstart = abs(settings.timings.offset.size);          % discard offset
li_runs = zeros(NumRuns, NumClasses);
for rId = 1:NumRuns
    for cId = 1:NumClasses
        li_runs(rId, cId) = squeeze(mean(mean(mean(LI(trialstart:end, SelectedFreqId, Ck == settings.class.values(cId) & Rk == Runs(rId)), 2), 3), 1));
    end
end
plot(Runs, li_runs, '-o');
grid on;
xlim([Runs(1) Runs(end)]);
xlabel('Run');
ylabel('[uV]')
title('Lateralization index over runs');
legend(settings.class.names, 'location', 'best');
plot_vline(1, 'k', '-> calibration');
plot_vline(settings.labels.Rk(find(settings.labels.Mk == 1, 1)) - 0.5, 'k', '-> online');

suptitle('Healthy subjects');

%% Saving figure
figurefile = [figuredir '/group_healthy_lateralization.pdf'];
util_bdisp(['[out] - Saving figures in: ' figurefile]);
fig_figure2pdf(fig1, figurefile, [], '-fillpage'); 