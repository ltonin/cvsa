clc;
clearvars;

subject     = 'BC';
experiment  = 'neglect';

Classes    = [1 3];
ClassesLb  = {'LeftCue (neglected)', 'MiddleCue'}; %
NumClasses = length(Classes);

%% Initialization

% Analysis period
AnalysisPeriod = 'trial'; % 'trial'/'precue';

% Datafiles
baseroot      = ['/mnt/data/Git/Codes/cvsa/analysis/' experiment '/'];
datapath      = [baseroot '/connectivity/'];

ExcludedChannels = {'FPz', 'FP2', 'AF8', 'AF4', 'AFz', 'F8', 'FT8', 'T8', 'TP8', 'Iz', 'TP7', 'T7', 'FT7', 'F7', 'AF3', 'AF7', 'FP1'};

%% Analysis - Loading functional connectivity
util_bdisp(['[io] - Loading functional connectivity for subject ' subject]);

cdata = load([datapath '/' subject '_functional_connectivity_nodes_iafmodality.mat']);
connectivity = cdata.connectivity;
channels = connectivity.channels;
NumTotChannels = length(channels.list);
channels.excluded.label = ExcludedChannels;
channels.excluded.id    = proc_get_channel(ExcludedChannels, channels.list);

timings  = cdata.connectivity.timing;
nodes    = cdata.connectivity.nodes;
Hemispheres = unique(nodes.locations.hemisphere.id);
NumHemispheres = length(Hemispheres);

cnumnodes = [];
for hId = 1:NumHemispheres
    chemId = Hemispheres(hId);
    cnumnodes = cat(1, cnumnodes, sum(nodes.locations.hemisphere.id == hId));
    if(length(unique(cnumnodes)) > 1)
        error('chk:nd', '[error] Different number of nodes per hemisphere');
    end
end
NumNodesHemisphere = unique(cnumnodes);

if strcmpi(AnalysisPeriod, 'precue')
    fc  = cdata.connectivity.functional.maps.precue;
    fcp = cdata.connectivity.functional.pvalues.precue;
elseif strcmpi(AnalysisPeriod, 'trial')
    fc  = cdata.connectivity.functional.maps.trial;
    fcp = cdata.connectivity.functional.pvalues.trial;
elseif strcmpi(AnalysisPeriod, 'cuetarget1')
    fc  = cdata.connectivity.functional.maps.cuetarget1;
    fcp = cdata.connectivity.functional.pvalues.cuetarget1;
elseif strcmpi(AnalysisPeriod, 'cuetarget2')
    fc  = cdata.connectivity.functional.maps.cuetarget2;
    fcp = cdata.connectivity.functional.pvalues.cuetarget2;
end

fc(:, channels.excluded.id, :)  = nan;
fcp(:, channels.excluded.id, :) = nan;

Ck = connectivity.labels.Ck;
Dk = connectivity.labels.Dk;
Mk = connectivity.labels.Mk;
Rk = connectivity.labels.Rk;

Modalities    = unique(Mk);
NumModalities = length(Modalities);
NumNodes      = size(fc, 1);
NumChannels   = size(fc, 2);
NumTrials     = length(Ck);

%% Analysis - Load artifacts analysis
cartifacts = load([baseroot '/artifacts/' subject '_artifacts.mat'], 'artifacts');
analysis.artifacts = cartifacts.artifacts;

ArtInd = false(length(analysis.artifacts.Ak), 1);
for ctrialId = 1:NumClasses
    ArtInd = ArtInd | analysis.artifacts.labels.Ck == Classes(ctrialId);
end

Ak = analysis.artifacts.Ak(ArtInd);

if strcmpi(subject, 'BC')
    Ak = analysis.artifacts.Am(ArtInd) >= 115;
end

%% Analysis - Load eog analysis
ceoganalysis = load([baseroot '/eog/' subject '_eog.mat'], 'eog');
EogInd = false(size(ceoganalysis.eog.detection, 1), 1);
for ctrialId = 1:NumClasses
    EogInd = EogInd | ceoganalysis.eog.labels.Ck == Classes(ctrialId);
end
Ed = ceoganalysis.eog.detection(EogInd, 1);

%% Analysis - Artifact index
ArtifactFree = Ak == false & Ed == false;
ArtifactFree = Ed == false;

%% Analysis - Loading reaction time
rt     = [];
Td     = [];
cdata  = load([baseroot '/reactiontime/' subject '_reactiontime.mat']);
cindex = false(size(cdata.analysis.rt.values, 1), 1);
for cId = 1:NumClasses
    cindex = cindex | cdata.analysis.rt.labels.Ck == Classes(cId);
end

rt = cat(1, rt, cdata.analysis.rt.values(cindex, 1));
Td = cat(1, Td, cdata.analysis.rt.labels.Rd(cindex));

%% Generic conditions
GenericCondition = ArtifactFree & Td == 1 & Ck == 1;

%% Pre-processing connectivity (z-transform)
% Fisher z-transform of fc correlation values
util_bdisp('[proc] - Computing Fisher z-transform connectivity values');
fcz = zeros(NumNodes, NumChannels, NumTrials);
for nId = 1:NumNodes
    for trId = 1:NumTrials
        cfc = squeeze(fc(nId, :, trId));
        fcz(nId, :, trId) =  proc_zfisher(cfc);
    end
end

%% Selecting Nodes
SelectedRegionLb = {'Frontal', 'Parietal', 'Occipital'}; 
SelectedRegionId = util_cellfind(SelectedRegionLb, nodes.locations.regions.label);

SelectedRegionNodes = false(1, NumNodes);
for nId = 1:length(SelectedRegionId)
    SelectedRegionNodes = SelectedRegionNodes | nodes.locations.regions.id == SelectedRegionId(nId);
end
   

%% Computing average interhemispheric connectivity for each node
util_bdisp('[proc] - Computing average interhemispheric connectivity for each node');
ifcz = zeros(NumNodes, NumTrials);
for nId = 1:NumNodes
    chemisphere = nodes.locations.hemisphere.id(nId);
    othernodes = find(nodes.locations.hemisphere.id == chemisphere & SelectedRegionNodes);
    
    chIds = cell2mat(nodes.channels.id(othernodes)');
    
    for trId = 1:NumTrials
        ifcz(nId, trId) = squeeze(mean(fcz(nId, chIds, trId), 2));
    end
end

%% Computing fc:rt correlation
util_bdisp('[proc] - Computing fc:rt correlation for each node');
fcrt = zeros(NumNodes, 1);
fcrtpval = zeros(NumNodes, 1);

for nId = 1:NumNodes
    cifcz = ifcz(nId, GenericCondition)';
    crt   = rt(GenericCondition);
    [fcrt(nId), fcrtpval(nId)] = corr(cifcz, crt, 'type', 'Spearman', 'rows', 'complete');
end

%% Statistical tests
util_bdisp('[stat] - Anova on interhemispheric connectivity with respect to modality');
pvalues = zeros(NumNodes, 1);
fvalues = zeros(NumNodes, 1);
BonferroniCorrection = 6;
for nId = 1:NumNodes
%     [cpval, ctab] = anova1(ifcz(nId, GenericCondition), Mk(GenericCondition), 'off');
%     pvalues(nId) = cpval;
%     fvalues(nId) = ctab{2, 5};
    [~, cpvalues] = ttest2(ifcz(nId, GenericCondition & Mk == 0), ifcz(nId, GenericCondition & Mk == 1));
    pvalues(nId) = cpvalues*BonferroniCorrection;
    hmId  = nodes.locations.hemisphere.id(nId);
    ohmId = setdiff(Hemispheres, hmId);
    hmLb  = nodes.locations.hemisphere.label{hmId};
    ohmLb = nodes.locations.hemisphere.label{ohmId};
    rgId  = nodes.locations.regions.id(nId);
    rgLb  = nodes.locations.regions.label{rgId};
    
%     fprintf('[stat] - Anova between calibration and online trials [%s]:\tF=%2.1f, p=%1.5f\n', ['fc: ' rgLb hmLb '>' ohmLb], fvalues(nId), pvalues(nId));
    fprintf('[stat] - T-test between calibration and online trials (Bonferroni) [%s]:\tp=%1.5f\n', ['fc: ' rgLb hmLb '>' ohmLb], pvalues(nId));
end

%% Plotting

fig1 = figure;
fig_set_position(fig1, 'All');

NumRows = 3;
NumCols = 6;
CorrAlpha = 0.05;
load('chanlocs64.mat');


for hId = 1:NumHemispheres
    Nodes2Plot    = find(SelectedRegionNodes & nodes.locations.hemisphere.id == hId);
    NumNodes2Plot = length(Nodes2Plot);

    for nId = 1:NumNodes2Plot       
        subplot(NumRows, NumCols, nId + (hId - 1)*NumNodes2Plot);
        cdata = squeeze(mean(fcz(Nodes2Plot(nId), :, GenericCondition & Mk == 1), 3)) - squeeze(mean(fcz(Nodes2Plot(nId), :, GenericCondition & Mk == 0), 3));
        cdata(isnan(cdata)) = 0;
        %topoplot(cdata, chanlocs, 'headrad', 'rim', 'maplimits', [-0.8 0.8], 'emarker2', {nodes.channels.id{Nodes2Plot(nId)}, 'o', 'm', 5, 1});
        % without highlighting nodes
        topoplot(cdata, chanlocs, 'headrad', 'rim', 'maplimits', [-0.8 0.8]);
        axis image;
        title('Online - Offline');
    end
end


axfc = zeros(NumNodes2Plot, NumHemispheres);
for hId = 1:NumHemispheres
    Nodes2Plot    = find(SelectedRegionNodes & nodes.locations.hemisphere.id == hId);
    NumNodes2Plot = length(Nodes2Plot);
    
    for nId = 1:NumNodes2Plot
        subplot(NumRows, NumCols, nId + (hId - 1)*NumNodes2Plot + NumCols);
        boxplot(ifcz(Nodes2Plot(nId),GenericCondition), Mk(GenericCondition));
        axfc(nId, hId) = gca;

        hmId  = nodes.locations.hemisphere.id(Nodes2Plot(nId));
        ohmId = setdiff(Hemispheres, hmId);
        hmLb  = nodes.locations.hemisphere.label{hmId};
        ohmLb = nodes.locations.hemisphere.label{ohmId};
        rgId  = nodes.locations.regions.id(Nodes2Plot(nId));
        rgLb  = nodes.locations.regions.label{rgId};

        grid on;
        
        set(gca, 'XTickLabel', {'Calibration', 'Online'});
        if (nId == 1 && hId == 1)
            ylabel('FC z(r)');
        end
        
        if(nId > 1 || hId > 1)
            set(gca, 'YTickLabel', ' ');
        end
        title(['' rgLb hmLb '>' ohmLb]);
        cpos = get(gca,'Position');
        cpos(2) = cpos(2) - 0.02;
        
        textcolor = 'k';
        if(pvalues(Nodes2Plot(nId)) <= CorrAlpha)
            textcolor = 'r';
        end
%         t = annotation('textbox', cpos, 'String', ['F=' num2str(fvalues(Nodes2Plot(nId)), '%3.2f') ...
%                                              ', p=' num2str(pvalues(Nodes2Plot(nId)), '%3.3f')], ...
%                                              'LineStyle', 'none', 'Color', textcolor, 'FontWeight', 'bold');
        t = annotation('textbox', cpos, 'String', ['p=' num2str(pvalues(Nodes2Plot(nId)), '%3.3f') ' (Bonferroni)'], ...
                                             'LineStyle', 'none', 'Color', textcolor, 'FontWeight', 'bold');
        t.FontSize = 6;
    end
end
plot_set_limits(axfc(:), 'y', 'minmax')

axfcrt = zeros(NumNodes2Plot, NumHemispheres);
for hId = 1:NumHemispheres
    Nodes2Plot    = find(SelectedRegionNodes & nodes.locations.hemisphere.id == hId);
    NumNodes2Plot = length(Nodes2Plot);
    
    for nId = 1:NumNodes2Plot
        subplot(NumRows, NumCols, nId + (hId - 1)*NumNodes2Plot + 2*NumCols);
        hs = scatter(ifcz(Nodes2Plot(nId),GenericCondition), rt(GenericCondition), 'o', 'MarkerFaceColor',[0 0.4470 0.7410], 'MarkerEdgeColor', 'black');
        
        hmId  = nodes.locations.hemisphere.id(Nodes2Plot(nId));
        ohmId = setdiff(Hemispheres, hmId);
        hmLb  = nodes.locations.hemisphere.label{hmId};
        ohmLb = nodes.locations.hemisphere.label{ohmId};
        rgId  = nodes.locations.regions.id(Nodes2Plot(nId));
        rgLb  = nodes.locations.regions.label{rgId};
        
        if (nId == 1 && hId == 1)
            ylabel('Reaction time [s]');
        end
        if(nId > 1 || hId > 1)
            set(gca, 'YTickLabel', '');
        end
        
        xlabel('FC z(r)');
        xlim([0 1.5])
        grid on;
        hl = lsline;
        set(hl, 'LineWidth', 1.5, 'Color', 'r');
        axfcrt(nId, hId) = gca;
        cpos = get(gca,'Position');
        cpos(2) = cpos(2) - 0.02;
        textcolor = 'k';
        if(fcrtpval(Nodes2Plot(nId)) <= CorrAlpha)
            textcolor = 'r';
        end
        t = annotation('textbox', cpos, 'String', ['rho=' num2str(fcrt(Nodes2Plot(nId)), '%3.2f') ...
                                             ', p=' num2str(fcrtpval(Nodes2Plot(nId)), '%3.3f')], ...
                                             'LineStyle', 'none', 'Color', textcolor, 'FontWeight', 'bold');
        t.FontSize = 6;                                 
    end
    
end
%plot_set_limits(axfcrt(:), 'y', 'minmax')
suptitle(['Patient ' subject ' - ' AnalysisPeriod])


