clc;
clearvars;

subject     = 'BC';
pattern     = '.va.';

features       = 'psd';
spatialfilter  = 'car';
experiment     = 'neglect';

Classes    = [1 3];
ClassesLb  = {'LeftCue (neglected)', 'MiddleCue'}; %
NumClasses = length(Classes);

%% Initialization

% Timings
timings.trial.period    = 3;            % seconds
timings.offset.period   = -1;           % seconds

% Channels
[~, channels.list]        = proc_get_montage('eeg.biosemi.64');
channels.hemisphere       = proc_get_hemisphere(channels.list);

% Frequencies
selectedfrequencies       = 4:48;

% Datafiles
baseroot      = ['/mnt/data/Git/Codes/cvsa/analysis/' experiment '/'];
datapath      = [baseroot '/' features '/' spatialfilter '/'];
[Files, NumFiles] = util_getfile(datapath, '.mat', [subject '*' pattern]);
[~, saveroot] = util_mkdir(baseroot, 'connectivity/');


%% Analysis - Import data

% Concatanate datafiles
util_bdisp(['[io] - Concatenating ' num2str(NumFiles) ' datafiles']);
[F, analysis] = cvsa_utilities_concatenate_data(Files);

% Extract information
SampleRate     = analysis.processing.fs;
FrameSize      = analysis.processing.fsize;
[~, selfreqId] = intersect(analysis.processing.f, selectedfrequencies);
FreqGrid       = analysis.processing.f(selfreqId);

F = F(:, selfreqId, :);
NumTotSamples  = size(F, 1);
NumFreqs       = size(F, 2);
NumChans       = size(F, 3);

%% Analysis - Masking data

util_bdisp('[proc] - Creating trial mask vector');
events = analysis.event;
timings.trial.size  = floor(timings.trial.period*SampleRate/FrameSize);
timings.offset.size = floor(timings.offset.period*SampleRate/FrameSize);
timings.trial.mask  = false(NumTotSamples, 1);
timings.trial.pos   = [];
for ctrialId = 1:NumClasses
    [cmask, cpos, cdur] = proc_get_event(events, Classes(ctrialId), NumTotSamples, timings.trial.size, timings.offset.size);
    timings.trial.pos   = sort([timings.trial.pos cpos']);
end

util_bdisp('[proc] - Creating trial based class labels');
index = false(length(events.TYP), 1);
for ctrialId = 1:NumClasses
    index = events.TYP == Classes(ctrialId) | index;
end
Ck = events.TYP(index);

%% Extracting trials
util_bdisp('[proc] - Extracting trials');
NumTrials  = length(timings.trial.pos);
NumSamples = timings.trial.size + abs(timings.offset.size);
S = zeros(NumSamples, NumFreqs, NumChans, NumTrials);
Mk = zeros(NumTrials, 1);
Rk = zeros(NumTrials, 1);
Dk = zeros(NumTrials, 1);
for trId = 1:NumTrials
    cstart = timings.trial.pos(trId);
    cstop  = cstart + NumSamples - 1;
    S(:, :, :, trId) = F(cstart:cstop, :, :);
    Mk(trId) = unique(analysis.label.Mk(cstart:cstop));
    Rk(trId) = unique(analysis.label.Rk(cstart:cstop));
    Dk(trId) = unique(analysis.label.Dk(cstart:cstop));
end
Modalities    = unique(Mk);
NumModalities = length(Modalities);
Runs          = unique(Rk);
NumRuns       = length(Runs);
Days          = unique(Dk);
NumDays       = length(Days);

%% Analysis - Loading peaks for each trial
util_bdisp('[proc] - Loading alpha peak for each trial');
cpeak = load([baseroot '/alphapeak/' subject '_alphapeak_trial.mat']);
tpeaks = cpeak.alphapeak.peaks.trial.bands;

%% Extract alpha power for each roi and trial
util_bdisp('[proc] - Extracting alpha power (related to the peak) for each trial');
alphapower = zeros(NumSamples, NumChans, NumTrials); 
for trId = 1:NumTrials
    cdata = S(:, :, :, trId);
    for chId = 1:NumChans
        chemisphere = channels.hemisphere(chId);
        if chemisphere == 3
            chemisphere = 2;
        end
        cband = tpeaks(:, chemisphere, trId);
        alphapower(:, chId, trId) = mean(cdata(:, cband, chId), 2);
    end
end


%% Compute connectivity
util_bdisp('[proc] - Computing functional connectivity map for each trial');
ToI = abs(timings.offset.size)+1:NumSamples;
fcmap  = zeros(NumChans, NumChans, NumTrials);
fcpval = zeros(NumChans, NumChans, NumTrials);

for trId = 1:NumTrials
    util_disp_progress(trId, NumTrials, '        ');
    cdata  = alphapower(ToI, :, trId);
    for nId = 1:NumChans
        for vId = 1:NumChans
                ndata = cdata(:, nId);
                vdata = cdata(:, vId);
                [fcmap(nId, vId, trId), fcpval(nId, vId, trId)] = corr(ndata, vdata);
        end
    end
end

%% Saving results
targetfile = [saveroot '/' subject '_functional_connectivity.mat'];
util_bdisp(['[out] - Saving analysis in: ' targetfile]);

connectivity.functional.maps        = fcmap;
connectivity.functional.pvalues     = fcpval;
connectivity.timing                 = timings;
connectivity.timing.ToI             = abs(timings.offset.size)+1:NumSamples;
connectivity.channels               = channels;
connectivity.frequencies.selected   = selectedfrequencies;
connectivity.frequencies.grid       = FreqGrid;
connectivity.labels.Mk              = Mk;
connectivity.labels.Rk              = Rk;
connectivity.labels.Dk              = Dk;
connectivity.labels.Ck              = Ck;

save(targetfile, 'connectivity');       



