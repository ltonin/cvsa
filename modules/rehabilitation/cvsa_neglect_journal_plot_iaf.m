clearvars; clc;

SubList = {'SM', 'BC', 'TC'};
NumSubjects = length(SubList);

analysisdir = './analysis/neglect';
figuredir   = './figures/journal/';

Classes = [1 3];
NumClasses = length(Classes);

BonferroniCorrection = 2;

fig1 = figure;

NumRows = 2;
NumCols = NumSubjects;

rtiafcorr  = zeros(2, NumSubjects);
rtiafpval  = zeros(2, NumSubjects);
pvalues = zeros(2, NumSubjects);
for sId = 1:NumSubjects
    csubject = SubList{sId};
    
    % Loading iaf data and extract info
    ciaffile = [analysisdir '/alphapeak/' csubject '_iaf.mat'];
    util_bdisp(['[io] - Loading iaf for subject ' csubject ': ' ciaffile]);
    
    cdata = load(ciaffile);
    ciaf = cdata.iaf;
    
    cNumTrials = size(ciaf.trials.raw, 2);
    cNumRois   = length(ciaf.channels.roi.labels);
    
    cMk = ciaf.labels.Mk;
    cModalities = unique(cMk);
    cNumModalities = length(cModalities);
    
    cAFk = ciaf.labels.AFk;
    cGenericConditions = cAFk;
    
    cm_iafband = ciaf.modalities.band;
    cnspd = ciaf.spectrum.precue.nrm;
    
    % Loading rt data and extract info
    crtfile = [analysisdir '/reactiontime/' csubject '_reactiontime.mat'];
    util_bdisp(['[io] - Loading RT for subject ' csubject ': ' crtfile]);
    crtdata = load(crtfile);
    
    cCk = crtdata.analysis.rt.labels.Ck;
    selindex = false(length(cCk), 1);
    for cId = 1:NumClasses
        selindex = selindex | cCk == Classes(cId);
    end
    
    
    crt  = crtdata.analysis.rt.values(selindex);
    cTs  = crtdata.analysis.rt.labels.Ts(selindex);
    cRd  = crtdata.analysis.rt.labels.Rd(selindex);
    
    
    
    % Compute alpha power
    util_bdisp('[proc] - Compute the alpha power in the pre-cue interval');
    cpower = zeros(cNumRois, cNumTrials);

    for trId = 1:cNumTrials
        for roId = 1:cNumRois
            cmodalityId = find(cModalities == cMk(trId));
            ciafband = cm_iafband(:, roId, cmodalityId);
            cpower(roId, trId)   = squeeze(mean(cnspd(ciafband, roId, trId), 1));
        end
    end
    
    rcpower = cpower(:);
    crRoiK  = reshape((repmat((1:cNumRois)', cNumTrials, 1)), cNumTrials*cNumRois, 1);
    crMk = reshape(repmat(cMk', cNumRois, 1), cNumRois*cNumTrials, 1);
    crAFk = reshape(repmat(cGenericConditions', cNumRois, 1), cNumRois*cNumTrials, 1);
    
    % T-Test on alpha power
    for rId = 1:cNumRois
        cindex = crRoiK == rId & crAFk; 
        [~, cpvalue] = ttest2(rcpower(crMk == cModalities(1) & cindex), rcpower(crMk == cModalities(2) & cindex));
        cpvalue = cpvalue*BonferroniCorrection;
        pvalues(rId, sId) = cpvalue;
    end
    
    % Compute correlation RT-iaf for left class
    for rId = 1:cNumRois
        cindex = cAFk & cTs == 1 & cRd; 
        [rtiafcorr(rId, sId), rtiafpval(rId, sId)] = corr(crt(cindex), cpower(rId, cindex)', 'rows', 'pairwise', 'type', 'spearman');
    end
    
    % Plotting
    
    % IAP plots
    subplot(NumRows, NumCols, sId);
    boxplot(rcpower, {crRoiK crMk}, 'FactorGap', 'auto', 'FactorSeparator', 1)
    set(gca, 'ActivePositionProperty', 'position')
    ylim([0 1.2]);
    grid on;
    ytick = get(gca, 'YTick');
    set(gca, 'YTick', ytick(ytick <= 1));
    set(gca,'xtickmode','auto','xticklabelmode','auto')
    set(gca, 'XTickLabel', {'Calibration', 'Online'});
    title(['Patient P' num2str(sId)]);
    
    cpos = get(gca, 'Position');
    cpos(2) = cpos(2) - 0.03;
    t1 = annotation('textbox', cpos, 'String', ['p=' num2str(pvalues(1, sId), '%3.3f') ' (Bonferroni)'],  'LineStyle', 'none', 'Color', 'k', 'FontWeight', 'bold');
    t1.FontSize = 8;
    
    cpos = get(gca, 'Position');
    cpos(1) = cpos(1) + cpos(3)/2;
    cpos(2) = cpos(2) - 0.03;
    t2 = annotation('textbox', cpos, 'String', ['p=' num2str(pvalues(2, sId), '%3.3f') ' (Bonferroni)'],  'LineStyle', 'none', 'Color', 'k', 'FontWeight', 'bold');
    t2.FontSize = 8;
    
    if sId == 1
        ylabel('IAP');
    end
    
    % Correlation plot
    subplot(NumRows, NumCols, sId + NumCols);
    cindex = cAFk & cTs == 1 & cRd; 
    plot(cpower(2, cindex), crt(cindex), '.');
    set(gca, 'ActivePositionProperty', 'position')
    hl = lsline;
    set(hl, 'Color', 'r', 'LineWidth', 2);
    ylim([0 1.7]);
    grid on;
    xlabel('IAP');
    if sId == 1
        ylabel('Time [s]');
    end
    
    cpos = get(gca, 'Position');
    cpos(2) = cpos(2) - 0.02;
    t1 = annotation('textbox', cpos, 'String', ['rho=' num2str(rtiafcorr(rId, sId), '%3.3f') ', p=' num2str(rtiafpval(rId, sId), '%3.3f')],  'LineStyle', 'none', 'Color', 'k', 'FontWeight', 'bold');
    t1.FontSize = 8;
    
    title('Correlation - Affected - Class Left');
end
fig_set_position(fig1, 'All');
suptitle('Individual alpha power (IAP) and reaction time');

filename = [figuredir '/cvsa.neglect.journal.iaf.raw.pdf'];
fig_export(fig1, filename, '-pdf');