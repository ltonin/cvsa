clc;
clearvars;

subject     = 'TC';
pattern     = '.va.';

features       = 'psd';
spatialfilter  = 'car';
experiment     = 'neglect';

Classes    = [1 3];
ClassesLb  = {'LeftCue (neglected)', 'MiddleCue'}; %
NumClasses = length(Classes);

%% Initialization

% Timings
timings.trial.period    = 3;            % seconds
timings.offset.period   = -1;           % seconds

% Channels and RoIs
[~, channels.list]        = proc_get_montage('eeg.biosemi.64');
% channels.roi.left.labels  = {'P7', 'P5', 'PO7'};         % As in [Thut et al., 2006]
% channels.roi.right.labels = {'P8', 'P6', 'PO8'};         % As in [Thut et al., 2006]
channels.roi.left.labels  = {'P5', 'P3', 'P1', 'PO7', 'PO3', 'O1'};         % Custom
channels.roi.right.labels = {'P6', 'P4', 'P2', 'PO8', 'PO4', 'O2'};         % Custom
channels.roi.left.id      = proc_get_channel(channels.roi.left.labels,  channels.list);
channels.roi.right.id     = proc_get_channel(channels.roi.right.labels, channels.list);
channels.roi.labels       = {'Healthy RoI (Left)', 'Affected RoI (Right)'};
NumRois                   = length(channels.roi.labels);

% Frequencies
selectedfrequencies       = 4:48;

% Datafiles
baseroot      = ['/mnt/data/Git/Codes/cvsa/analysis/' experiment '/'];
datapath      = [baseroot '/' features '/' spatialfilter '/'];
[Files, NumFiles] = util_getfile(datapath, '.mat', [subject '*' pattern]);
[~, saveroot] = util_mkdir(baseroot, 'alphapeak/');


%% Analysis - Import data

% Concatanate datafiles
util_bdisp(['[io] - Concatenating ' num2str(NumFiles) ' datafiles']);
[F, analysis] = cvsa_utilities_concatenate_data(Files);

% Extract information
SampleRate     = analysis.processing.fs;
FrameSize      = analysis.processing.fsize;
[~, selfreqId] = intersect(analysis.processing.f, selectedfrequencies);
FreqGrid       = analysis.processing.f(selfreqId);

F = F(:, selfreqId, :);
NumTotSamples  = size(F, 1);
NumFreqs       = size(F, 2);
NumChans       = size(F, 3);

%% Analysis - Masking data

util_bdisp('[proc] - Creating trial mask vector');
events = analysis.event;
timings.trial.size  = floor(timings.trial.period*SampleRate/FrameSize);
timings.offset.size = floor(timings.offset.period*SampleRate/FrameSize);
timings.trial.mask  = false(NumTotSamples, 1);
timings.trial.pos   = [];
for ctrialId = 1:NumClasses
    [cmask, cpos, cdur] = proc_get_event(events, Classes(ctrialId), NumTotSamples, timings.trial.size, timings.offset.size);
    timings.trial.pos   = sort([timings.trial.pos cpos']);
end

util_bdisp('[proc] - Creating trial based class labels');
index = false(length(events.TYP), 1);
for ctrialId = 1:NumClasses
    index = events.TYP == Classes(ctrialId) | index;
end
Ck = events.TYP(index);

%% Extracting trials
util_bdisp('[proc] - Extracting trials');
NumTrials  = length(timings.trial.pos);
NumSamples = timings.trial.size + abs(timings.offset.size);
S = zeros(NumSamples, NumFreqs, NumChans, NumTrials);
Mk = zeros(NumTrials, 1);
Rk = zeros(NumTrials, 1);
Dk = zeros(NumTrials, 1);
for trId = 1:NumTrials
    cstart = timings.trial.pos(trId);
    cstop  = cstart + NumSamples - 1;
    S(:, :, :, trId) = F(cstart:cstop, :, :);
    Mk(trId) = unique(analysis.label.Mk(cstart:cstop));
    Rk(trId) = unique(analysis.label.Rk(cstart:cstop));
    Dk(trId) = unique(analysis.label.Dk(cstart:cstop));
end
Modalities    = unique(Mk);
NumModalities = length(Modalities);
Runs          = unique(Rk);
NumRuns       = length(Runs);
Days          = unique(Dk);
NumDays       = length(Days);

RunMod = zeros(NumRuns, 1);
for rId = 1:NumRuns, 
    RunMod(rId) = unique(Mk(Rk == rId)); 
end
%% Analysis - Load artifacts analysis
cartifacts = load([baseroot '/artifacts/' subject '_artifacts.mat'], 'artifacts');
analysis.artifacts = cartifacts.artifacts;

ArtInd = false(length(analysis.artifacts.Ak), 1);
for ctrialId = 1:NumClasses
    ArtInd = ArtInd | analysis.artifacts.labels.Ck == Classes(ctrialId);
end

Ak = analysis.artifacts.Ak(ArtInd);

if strcmpi(subject, 'BC')
    Ak = analysis.artifacts.Am(ArtInd) >= 115;
end

%% Analysis - Load eog analysis
ceoganalysis = load([baseroot '/eog/' subject '_eog.mat'], 'eog');
EogInd = false(size(ceoganalysis.eog.detection, 1), 1);
for ctrialId = 1:NumClasses
    EogInd = EogInd | ceoganalysis.eog.labels.Ck == Classes(ctrialId);
end
Ed = ceoganalysis.eog.detection(EogInd, 1);

%% Analysis - Artifact index
ArtifactFree = Ak == false & Ed == false;

%% Averaging data for each RoI
util_bdisp('[proc] - Averaging data for each RoI');
psd = [];
psd(:, :, 1, :) = squeeze(mean(S(:, :, channels.roi.left.id, :), 3));
psd(:, :, 2, :) = squeeze(mean(S(:, :, channels.roi.right.id, :), 3));

%% Extracting spectrum in per intervals
util_bdisp('[proc] - Extracting spectrum per intervals (by averaging the interval)');
NumTrialIntervals = 3;
SizeTrialIntervals = ((NumSamples - abs(timings.offset.size))/NumTrialIntervals);
NumIntervals = 1 + NumTrialIntervals;
Intervals = zeros(2, NumIntervals);

Intervals(:, 1) = [1 abs(timings.offset.size)];
Intervals(1, 2:end) = abs(timings.offset.size) + ...
                      (1:SizeTrialIntervals:(NumSamples-abs(timings.offset.size))); 
Intervals(2, 2:end) = Intervals(1, 2:end) + SizeTrialIntervals - 1;
Intervals = floor(Intervals);

psd_interval = zeros(NumIntervals, NumFreqs, NumRois, NumTrials);
for tId = 1:NumIntervals
    cstart  = Intervals(1, tId);
    cstop   = Intervals(2, tId);
    psd_interval(tId, :, :, :) = squeeze(mean(psd(cstart:cstop, :, :, :), 1));
end

% PreCueInterval = 1:abs(timings.offset.size);         
% psd_precue = squeeze(mean(psd(PreCueInterval, :, :, :), 1));

%% Normalizing spectrum in each interval with respect to the maximum for each RoI and trial
util_bdisp('[proc] - Normalizing spectrum in each interval');
npsd_interval =  zeros(size(psd_interval));
for tId = 1:NumIntervals
    cpsd = squeeze(psd_interval(tId, :, :, :));
    max_l = max(squeeze(cpsd(:, 1, :)), [], 1);
    max_r = max(squeeze(cpsd(:, 2, :)), [], 1);
    npsd_interval(tId, :, 1, :) = squeeze(cpsd(:, 1, :))./repmat(max_l, [NumFreqs 1]);
    npsd_interval(tId, :, 2, :) = squeeze(cpsd(:, 2, :))./repmat(max_r, [NumFreqs 1]);
end

%% Alpha peak analysis configuration
HealthyRoiId  = 1;
AffectedRoiId = 2;

if(strcmp(subject, 'SM'))
    FreqPeakRange = 8:12;
elseif(strcmp(subject, 'TC'))
    FreqPeakRange = 8:12;
elseif(strcmp(subject, 'BC'))  
    FreqPeakRange = 4:12;
end

% Trial condition
GenericCondition = ArtifactFree == true & Ck == 1;

%% Find the alpha peak based on healthy RoI for calibration and online modalities
util_bdisp('[proc] - Find alpha peak per RoIs per modality');

m_iaf = zeros(NumIntervals, NumRois, NumModalities);
for tId = 1:NumIntervals
    for mId = 1:NumModalities
        for roId = 1:NumRois
            cmodality = Modalities(mId);
            cnpsd = squeeze(npsd_interval(tId, :, :, :));
            [~, cpeaks] = findpeaks(mean(cnpsd(:, roId, GenericCondition & Mk == cmodality), 3));

            % Extracting peaks inside the selected frequency range
            [~, ~, cpeakids] = intersect(FreqPeakRange, FreqGrid(cpeaks));

            if(isempty(cpeakids))
                speak = nan;                    % If not peak have been found in the range, put NaN
            else
                speak = cpeaks(cpeakids(1));    % Otherwise, selecte the first peak in the range
            end
            m_iaf(tId, roId, mId) = speak;
        end
    end
end

%% Find the alpha peak based on healthy RoI for runs
util_bdisp('[proc] - Find alpha peak per RoIs per run');
r_iaf = zeros(NumIntervals, NumRois, NumRuns);

for tId = 1:NumIntervals
    for rId = 1:NumRuns
        for roId = 1:NumRois
            crun = Runs(rId);
            cnpsd = squeeze(npsd_interval(tId, :, :, :));
            [~, cpeaks] = findpeaks(mean(cnpsd(:, roId, GenericCondition & Rk == crun), 3));

            % Extracting peaks inside the selected frequency range
            [~, ~, cpeakids] = intersect(FreqPeakRange, FreqGrid(cpeaks));

            if(isempty(cpeakids))
                speak = nan;                    % If not peak have been found in the range, put NaN
            else
                speak = cpeaks(cpeakids(1));    % Otherwise, selecte the first peak in the range
            end
            r_iaf(tId, roId, rId) = speak;
        end
    end
end

%% Find the alpha peak based on healthy RoI for days
util_bdisp('[proc] - Find alpha peak per RoIs per day');
d_iaf = zeros(NumIntervals, NumRois, NumDays);

for tId = 1:NumIntervals
    for dId = 1:NumDays
        for roId = 1:NumRois
            cday = Days(dId);
            cnpsd = squeeze(npsd_interval(tId, :, :, :)); 
            [~, cpeaks] = findpeaks(mean(cnpsd(:, roId, GenericCondition & Dk == cday), 3));

            % Extracting peaks inside the selected frequency range
            [~, ~, cpeakids] = intersect(FreqPeakRange, FreqGrid(cpeaks));

            if(isempty(cpeakids))
                speak = nan;                    % If not peak have been found in the range, put NaN
            else
                speak = cpeaks(cpeakids(1));    % Otherwise, selecte the first peak in the range
            end
            d_iaf(tId, roId, dId) = speak;
        end
    end
end

%% Find the alpha peak based on healthy RoI for trial
util_bdisp('[proc] - Find alpha peak per RoIs per day');
t_iaf = zeros(NumIntervals, NumRois, NumTrials);

for tId = 1:NumIntervals
    for trId = 1:NumTrials
        for roId = 1:NumRois
            cnpsd = squeeze(npsd_interval(tId, :, :, :)); 
            ctrial = cnpsd(:, roId, trId);
            [~, cpeaks] = findpeaks(ctrial);

            % Extracting peaks inside the selected frequency range
            [~, ~, cpeakids] = intersect(FreqPeakRange, FreqGrid(cpeaks));

            if(isempty(cpeakids))
                speak = nan;                    % If not peak have been found in the range, put NaN
            else
                speak = cpeaks(cpeakids(1));    % Otherwise, selecte the first peak in the range
            end
            t_iaf(tId, roId, trId) = speak;
        end
    end
end

%% Plotting

fig1 = figure;
fig_set_position(fig1, 'Top');

RoiLabels = {'Healthy RoI', 'Affected RoI'};
IntervalLabels = {'precue', 'shift', 'pretarget1', 'pretarget2'};
ModalityLabels = {'Calibration', 'Online'};
NumRows = 2;
NumCols = 2;

ax = zeros(NumRois, 1);
for roId = 1:NumRois
    subplot(NumRows, NumCols, roId);
    hold on;
    for mId = 1:NumModalities
        cmean = [];
        cstd  = [];
        for tId = 1:NumIntervals
            clocs = squeeze(t_iaf(tId, roId, GenericCondition & Mk == Modalities(mId)));
            vlocs = clocs(isnan(clocs) == false);
            cmean = cat(1, cmean, nanmean(FreqGrid(vlocs)));
            cstd  = cat(1, cstd, nanstd(FreqGrid(vlocs)));
        end
        errorbar(cmean, cstd);
    end
    ax(roId) = gca;
    hold off;
    set(gca, 'XTick', 1:NumIntervals);
    set(gca, 'XTickLabel', IntervalLabels);
    grid on;

    ylabel('[Hz]');
    xlabel('Trial intervals');
    legend(ModalityLabels);
    plot_vline(1.5, 'k', 'cue');
    title(['IAF - ' RoiLabels{roId}]);
    
end

plot_set_limits(ax, 'y', 'minmax');


axbar = zeros(NumRois, 1);
for roId = 1:NumRois
    subplot(NumRows, NumCols, roId + NumCols);
    
    pnan = zeros(NumIntervals, NumModalities);
    for mId = 1:NumModalities
       for tId = 1:NumIntervals
           clocs = squeeze(t_iaf(tId, roId, GenericCondition & Mk == Modalities(mId)));
           pnan(tId, mId)  = 100*sum(isnan(clocs))./length(clocs);
       end
    end
    
    bar(pnan);
    axbar(roId) = gca;
    legend(ModalityLabels);
    plot_vline(1.5, 'k', 'cue');
    title(['Missing peaks - ' RoiLabels{roId}]);
    ylabel('[%]');
    xlabel('Trial intervals');
    grid on;
end

plot_set_limits(axbar, 'y', 'minmax');
    