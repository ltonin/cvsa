clc;
clearvars;

subject     = 'SM';
pattern     = '.va.';
experiment     = 'neglect';

Classes    = [1 3];
ClassesLb  = {'LeftCue (neglected)', 'MiddleCue'}; %
NumClasses = length(Classes);

%% Initialization
% Datafiles

baseroot      = ['/mnt/data/Git/Codes/cvsa/analysis/' experiment '/'];
datapath      = [baseroot '/reactiontime/'];
[~, saveroot] = util_mkdir(baseroot, 'reactiontime/');

[Files, NumFiles] = util_getfile(datapath, '.mat', [subject '*' pattern]);

%% Analysis - Import data

% Concatanate datafiles
util_bdisp(['[io] - Concatenating ' num2str(NumFiles) ' datafiles']);
Rk  = [];
Mk  = [];
Dk  = [];
Sk  = [];
Eg  = [];
Bp  = [];
Ts  = [];
Rd  = [];
Ck  = [];
cdaylbl  = '';
cday     = 0;
csublbl  = '';
csub     = 0;
for fId = 1:NumFiles
    cfilename = Files{fId};
    info = util_getfile_info(cfilename);
    util_bdisp(['[io] - Loading file ' num2str(fId) '/' num2str(NumFiles)]);
    disp(['       File: ' cfilename]);

    % Get modality from filename
    switch lower(info.modality)
        case 'offline'
            cmod = 0;
        case 'online'
            cmod = 1;
        otherwise
            error('chk:mod', ['[' mfilename '] Unknown modality']);
    end

    % Get day from filename
    if strcmpi(info.date, cdaylbl) == false
        cday = cday + 1;
        cdaylbl = info.date;
    end

    % Get subject from filename
    if strcmpi(info.subject, csublbl) == false
        csub = csub + 1;
        csublbl = info.subject;
    end
    
    % Loading processed data
    cdata = load(cfilename);
    
    % Concatenate data
    Eg = cat(1, Eg, cdata.button.edges);
    Bp = cat(1, Bp, cdata.button.press);
    
    % Concatenate markers vector
    Ts = cat(1, Ts, cdata.labels.Ts);
    Rd = cat(1, Rd, cdata.labels.Rd);
    Ck = cat(1, Ck, cdata.labels.Ck);
    Rk = cat(1, Rk, fId*ones(size(cdata.button.edges, 1), 1));
    Mk = cat(1, Mk, cmod*ones(size(cdata.button.edges, 1), 1));
    Dk = cat(1, Dk, cday*ones(size(cdata.button.edges, 1), 1));
    Sk = cat(1, Sk, csub*ones(size(cdata.button.edges, 1), 1));
end
Runs = unique(Rk);
NumRuns = length(Runs);
Days = unique(Dk);
NumDays = length(Days);

%% Analysis - Load artifacts analysis
cartifacts = load([baseroot '/artifacts/' subject '_artifacts.mat'], 'artifacts');
Ak = cartifacts.artifacts.Ak;

%% Analysis - Load eog analysis
ceoganalysis = load([baseroot '/eog/' subject '_eog.mat'], 'eog');
Ed = ceoganalysis.eog.detection(:, 1);

%% Reaction time
ArtifactFree = Ak == false | Ed == false;

RT = [];
RT.values.left   = [Eg(Ts == 1 & Mk==0 & Rd == true & ArtifactFree == true, 1); ...
                    Eg(Ts == 1 & Mk==1 & Rd == true & ArtifactFree == true, 1); ...
                    Eg(Ts == 1 & Dk==Days(end) & Rd == true & ArtifactFree == true, 1)];
                
RT.values.middle = [Eg(Ts == 3 & Mk==0 & Rd == true & ArtifactFree == true, 1); ...
                    Eg(Ts == 3 & Mk==1 & Rd == true & ArtifactFree == true, 1); ...
                    Eg(Ts == 3 & Dk==Days(end) & Rd == true & ArtifactFree == true, 1)];

RT.groups.left   = [  ones(sum(Ts == 1 & Mk==0 & Rd == true & ArtifactFree == true), 1); ...
                    2*ones(sum(Ts == 1 & Mk==1 & Rd == true & ArtifactFree == true), 1); ...
                    3*ones(sum(Ts == 1 & Dk==Days(end) & Rd == true & ArtifactFree == true), 1)];
                
RT.groups.middle = [  ones(sum(Ts == 3 & Mk==0 & Rd == true & ArtifactFree == true), 1); ...
                    2*ones(sum(Ts == 3 & Mk==1 & Rd == true & ArtifactFree == true), 1); ...
                    3*ones(sum(Ts == 3 & Dk==Days(end) & Rd == true & ArtifactFree == true), 1)];
   
rtruns = zeros(NumRuns, NumClasses);
for rId = 1:NumRuns
    rtruns(rId, 1) = nanmean(Eg(Rk == Runs(rId) & Rd == true & Ts == 1, 1));
    rtruns(rId, 2) = nanmean(Eg(Rk == Runs(rId) & Rd == true & Ts == 3, 1));
end
RT.runs = rtruns;

%% Statitistics
% Between calibration and online trials
[p.left.calibonline, ctab] = anova1(RT.values.left(RT.groups.left < 3), RT.groups.left(RT.groups.left < 3), 'off');
f.left.calibonline = ctab{2, 5};
% Between calibration and last day trials
[p.left.caliblastday, ctab] = anova1(RT.values.left(RT.groups.left ~= 2), RT.groups.left(RT.groups.left ~= 2), 'off');
f.left.caliblastday = ctab{2, 5};
% Between calibration and online trials
[p.middle.calibonline, ctab] = anova1(RT.values.middle(RT.groups.middle < 3), RT.groups.middle(RT.groups.middle < 3), 'off');
f.middle.calibonline = ctab{2, 5};
% Between calibration and last day trials
[p.middle.caliblastday, ctab] = anova1(RT.values.middle(RT.groups.middle ~= 2), RT.groups.middle(RT.groups.middle ~= 2), 'off');
f.middle.caliblastday = ctab{2, 5};

fprintf('[stat] - Anova between calibration and online trials [Left]:      F=%1.5f p=%1.5f\n', f.left.calibonline, p.left.calibonline);
fprintf('[stat] - Anova between calibration and last days trials [Left]:   F=%1.5f p=%1.5f\n', f.left.caliblastday, p.left.caliblastday);
fprintf('[stat] - Anova between calibration and online trials [Middle]:    F=%1.5f p=%1.5f\n', f.middle.calibonline, p.middle.calibonline);
fprintf('[stat] - Anova between calibration and last days trials [Middle]: F=%1.5f p=%1.5f\n', f.middle.caliblastday, p.middle.caliblastday);


%% Plotting
fig1 = figure;
fig_set_position(fig1, 'Top');

subplot(1, 3, 1);
boxplot(RT.values.left, RT.groups.left);
grid on;
set(gca, 'XTickLabel', {'Calibration', 'Online', 'LastDay'});
ylabel('[s]');
title('Class Left');
ylim([0 1.5]);

subplot(1, 3, 2);
boxplot(RT.values.middle, RT.groups.middle);
title('Class Middle');
ylabel('[s]');
set(gca, 'XTickLabel', {'Calibration', 'Online', 'LastDay'});
grid on;
ylim([0 1.5]);

subplot(1, 3, 3);
plot(Runs, RT.runs, 'o-');
grid on;
ylim([0 1.5]);
xlim([Runs(1) Runs(end)]);
legend('Class Left', 'Class Middle');
xlabel('Runs');
ylabel('[s]');
plot_vline(1, 'k', '-> calib');
plot_vline(Rk(find(Mk == 1, 1)) - 0.5, 'k', '-> online');
title('RT over runs');

suptitle(['Subject ' subject ' - RT analysis']);

%% Saving analysis

analysis.rt.edges = Eg;
analysis.rt.labels.Mk = Mk;
analysis.rt.labels.Rk = Rk;
analysis.rt.labels.Dk = Dk;
analysis.rt.labels.Ck = Ck;
analysis.rt.labels.Ts = Ts;
analysis.rt.labels.Rd = Rd;

targetfile = [saveroot '/' subject '_reactiontime.mat'];
util_bdisp(['[out] - Saving reaction time analysis for subject ' subject ' in: ' targetfile]);
save(targetfile, 'analysis'); 