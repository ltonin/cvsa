clc;
clearvars;

sublist = {'SM', 'BC', 'TC'};

experiment  = 'neglect';

Classes    = [1 3];
ClassesLb  = {'LeftCue (neglected)', 'MiddleCue'}; %
NumClasses = length(Classes);

%% Initialization

% Analysis period
AnalysisPeriod = 'trial'; % 'trial'/'precue';

% Datafiles
baseroot      = ['/mnt/data/Git/Codes/cvsa/analysis/' experiment '/'];
datapath      = [baseroot '/connectivity/'];

ExcludedChannels = {'FPz', 'FP2', 'AF8', 'AF4', 'AFz', 'F8', 'FT8', 'T8', 'TP8', 'Iz', 'TP7', 'T7', 'FT7', 'F7', 'AF3', 'AF7', 'FP1'};

NumSubjects = length(sublist);

fc  = [];
fcp = [];
rt  = [];

Ck = [];
Dk = [];
Mk = [];
Rk = [];
Ak = [];
Ed = [];
Td = [];
Sk = [];

for sId = 1:NumSubjects
    csubject = sublist{sId};
    
    % Analysis - Load connectivity analysis
    util_bdisp(['[io] - Loading functional connectivity for subject ' csubject]);
    cdata = load([datapath '/' csubject '_functional_connectivity_nodes_nodes.mat']);
    
    connectivity = cdata.connectivity;

    timings  = cdata.connectivity.timing;
    nodes    = cdata.connectivity.nodes;
    Hemispheres = unique(nodes.locations.hemisphere.id);
    NumHemispheres = length(Hemispheres);

    if strcmpi(AnalysisPeriod, 'precue')
        cfc  = cdata.connectivity.functional.maps.precue;
        cfcp = cdata.connectivity.functional.pvalues.precue;
    elseif strcmpi(AnalysisPeriod, 'trial')
        cfc  = cdata.connectivity.functional.maps.trial;
        cfcp = cdata.connectivity.functional.pvalues.trial;
    elseif strcmpi(AnalysisPeriod, 'cuetarget1')
        cfc  = cdata.connectivity.functional.maps.cuetarget1;
        cfcp = cdata.connectivity.functional.pvalues.cuetarget1;
    elseif strcmpi(AnalysisPeriod, 'cuetarget2')
        cfc  = cdata.connectivity.functional.maps.cuetarget2;
        cfcp = cdata.connectivity.functional.pvalues.cuetarget2;
    end
    
    
    cCk = connectivity.labels.Ck;
    cDk = connectivity.labels.Dk;
    cMk = connectivity.labels.Mk;
    cRk = connectivity.labels.Rk;
    
    % Analysis - Load artifacts analysis
    cartifacts = load([baseroot '/artifacts/' csubject '_artifacts.mat'], 'artifacts');
    analysis.artifacts = cartifacts.artifacts;

    ArtInd = false(length(analysis.artifacts.Ak), 1);
    for ctrialId = 1:NumClasses
        ArtInd = ArtInd | analysis.artifacts.labels.Ck == Classes(ctrialId);
    end

    cAk = analysis.artifacts.Ak(ArtInd);

    if strcmpi(csubject, 'BC')
        cAk = analysis.artifacts.Am(ArtInd) >= 115;
    end
    
    % Analysis - Load eog analysis
    ceoganalysis = load([baseroot '/eog/' csubject '_eog.mat'], 'eog');
    EogInd = false(size(ceoganalysis.eog.detection, 1), 1);
    for ctrialId = 1:NumClasses
        EogInd = EogInd | ceoganalysis.eog.labels.Ck == Classes(ctrialId);
    end
    cEd = ceoganalysis.eog.detection(EogInd, 1);
    
    % Analysis - Loading reaction time
    
    cdata  = load([baseroot '/reactiontime/' csubject '_reactiontime.mat']);
    cindex = false(size(cdata.analysis.rt.edges, 1), 1);
    for cId = 1:NumClasses
        cindex = cindex | cdata.analysis.rt.labels.Ck == Classes(cId);
    end
    
    crt = cdata.analysis.rt.edges(cindex, 1);
    cTd = cdata.analysis.rt.labels.Rd(cindex);
    
    fc  = cat(3, fc, cfc);
    fcp = cat(3, fcp, cfcp);
    rt  = cat(1, rt, crt);
    
    Ck  = cat(1, Ck, cCk);
    Dk  = cat(1, Dk, cDk);
    Mk  = cat(1, Mk, cMk);
    Rk  = cat(1, Rk, cRk);
    Ak  = cat(1, Ak, cAk);
    Ed  = cat(1, Ed, cEd);
    Td  = cat(1, Td, cTd);
    Sk  = cat(1, Sk, sId*ones(length(cCk), 1));
end

Modalities    = unique(Mk);
NumModalities = length(Modalities);
NumNodes      = size(fc, 1);
NumChannels   = size(fc, 2);
NumTrials     = length(Ck);


%% Analysis - Artifact index
ArtifactFree = Ak == false & Ed == false;

%% Generic conditions
GenericCondition = ArtifactFree & Td == 1 & Ck == 1;

%% Pre-processing connectivity (z-transform)
% Fisher z-transform of fc correlation values
util_bdisp('[proc] - Computing Fisher z-transform connectivity values');
fcz = zeros(NumNodes, NumNodes, NumTrials);
for nId = 1:NumNodes
    for trId = 1:NumTrials
        cfc = squeeze(fc(nId, :, trId));
        fcz(nId, :, trId) =  proc_zfisher(cfc);
    end
end

%% Statistical tests for each subject
pvalues = zeros(NumNodes, NumNodes, NumSubjects);
fvalues = zeros(NumNodes, NumNodes, NumSubjects);
for sId = 1:NumSubjects
    cindex = Sk == sId;
    for nId = 1:NumNodes
        for oId = 1:NumNodes
            [cpval, ctab] = anova1(fcz(nId, oId, cindex & GenericCondition), Mk(cindex & GenericCondition), 'off');
            pvalues(nId, oId, sId) = cpval;
            fvalues(nId, oId, sId) = ctab{2, 5};
        end
    end
end
    

%% Computing fc:rt correlation
util_bdisp('[proc] - Computing fc:rt correlation for each node');
fcrt = zeros(NumNodes, NumNodes, NumSubjects);
fcrtpval = zeros(NumNodes, NumNodes, NumSubjects);
for sId = 1:NumSubjects
    cindex = Sk == sId;
    for nId = 1:NumNodes
        for oId = 1:NumNodes
            cfcz = squeeze(fcz(nId, oId, GenericCondition & cindex));
            crt   = rt(GenericCondition & cindex);
            [fcrt(nId, oId, sId), fcrtpval(nId, oId, sId)] = corr(cfcz, crt, 'type', 'Spearman', 'rows', 'complete');
        end
    end
end

%% Plotting

fig1 = figure;
fig_set_position(fig1, 'Top');
TickLabels = {'FL', 'CL', 'PL', 'OL', 'FR', 'CR', 'PR', 'OR'};
TickIndex  = [1 5 2 6 3 7 4 8];
NumRows = 2;
NumCols = 3;

for sId = 1:NumSubjects
    subplot(NumRows, NumCols, sId);
    csubject = sublist{sId};
    cindex = Sk == sId;
    cdata = mean(fcz(:, :, cindex & GenericCondition & Mk == 1), 3) - mean(fcz(:, :, cindex & GenericCondition & Mk == 0), 3);
    cdata = tril(cdata);
    [i, j] = find(pvalues(:, :, sId) <= 0.05);
    
    hold on;
    imagesc(cdata);
    ax = plot(i(i < j), j(i < j), 'or', 'MarkerFaceColor', 'r');
    
    hold off;
    
    
    
    title([csubject ': Online - Offline']);
    plot_setCLim(gca, [-0.2 0.2]);
    
    set(gca, 'XTick', 1:NumNodes);
    set(gca, 'YTick', 1:NumNodes);
    
    set(gca, 'XTickLabel', TickLabels);
    set(gca, 'YTickLabel', TickLabels);
    axis image;
end

for sId = 1:NumSubjects
    subplot(NumRows, NumCols, sId + NumCols);
    csubject = sublist{sId};
    cdata = fcrt(:, :, sId);
    cdata = tril(cdata);
    [i, j] = find(fcrtpval(:, :, sId) <= 0.05);
    hold on;
    imagesc(cdata);
    ax = plot(i(i < j), j(i < j), 'or', 'MarkerFaceColor', 'r');
    hold off;
     plot_setCLim(gca, [-0.2 0.2]);
    title([csubject ': fc:rt']);
    set(gca, 'XTick', 1:NumNodes);
    set(gca, 'YTick', 1:NumNodes);
    
    set(gca, 'XTickLabel', TickLabels);
    set(gca, 'YTickLabel', TickLabels);
    axis image;
end

suptitle(AnalysisPeriod)


