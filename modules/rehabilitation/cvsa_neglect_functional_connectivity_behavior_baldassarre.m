clc;
clearvars;

subject     = 'SM';
experiment  = 'neglect';

Classes    = [1 3];
ClassesLb  = {'LeftCue (neglected)', 'MiddleCue'}; %
NumClasses = length(Classes);

%% Initialization

% Datafiles
baseroot      = ['/mnt/data/Git/Codes/cvsa/analysis/' experiment '/'];
datapath      = [baseroot '/connectivity/'];

ExcludedChannels = {'FPz', 'FP2', 'AF8', 'AF4', 'AFz', 'F8', 'FT8', 'T8', 'TP8', 'P10', 'Iz', 'P9', 'TP7', 'T7', 'FT7', 'F7', 'AF3', 'AF7', 'FP1'};

%% Analysis - Loading functional connectivity
util_bdisp(['[io] - Loading functional connectivity for subject ' subject]);

cdata = load([datapath '/' subject '_functional_connectivity_nodes.mat']);
connectivity = cdata.connectivity;
channels = connectivity.channels;
NumTotChannels = length(channels.list);
channels.excluded.label = ExcludedChannels;
channels.excluded.id    = proc_get_channel(ExcludedChannels, channels.list);
channels.included.id    = setdiff(1:NumTotChannels, channels.excluded.id);
channels.included.label = proc_get_channel(channels.included.id, channels.list);
channels.included.hemisphere = channels.hemisphere(channels.included.id);

timings  = cdata.connectivity.timing;
nodes = cdata.connectivity.nodes;
Hemispheres = [1 2];
NumHemispheres = length(Hemispheres);

hcluster = [];
for hId = 1:NumHemispheres
    chemId = Hemispheres(hId);
    cnodeid = find(nodes.hemisphere.id == hId | nodes.hemisphere.id == 3)';
    hcluster = cat(2, hcluster, cnodeid);
end
NumNodesHemisphere = size(hcluster, 1);



fc = cdata.connectivity.functional.precue.maps(:,channels.included.id, :);

Ck = connectivity.labels.Ck;
Dk = connectivity.labels.Dk;
Mk = connectivity.labels.Mk;
Rk = connectivity.labels.Rk;

Modalities    = unique(Mk);
NumModalities = length(Modalities);
NumNodes      = size(fc, 1);
NumChannels   = size(fc, 2);
NumTrials     = length(Ck);

%% Analysis - Load artifacts analysis
cartifacts = load([baseroot '/artifacts/' subject '_artifacts.mat'], 'artifacts');
analysis.artifacts = cartifacts.artifacts;

ArtInd = false(length(analysis.artifacts.Ak), 1);
for ctrialId = 1:NumClasses
    ArtInd = ArtInd | analysis.artifacts.labels.Ck == Classes(ctrialId);
end

Ak = analysis.artifacts.Ak(ArtInd);

%% Analysis - Load eog analysis
ceoganalysis = load([baseroot '/eog/' subject '_eog.mat'], 'eog');
EogInd = false(size(ceoganalysis.eog.detection, 1), 1);
for ctrialId = 1:NumClasses
    EogInd = EogInd | ceoganalysis.eog.labels.Ck == Classes(ctrialId);
end
Ed = ceoganalysis.eog.detection(EogInd, 1);

%% Analysis - Artifact index
ArtifactFree = Ak == false | Ed == false;

%% Analysis - Loading reaction time
rt     = [];
Td     = [];
cdata  = load([baseroot '/reactiontime/' subject '_reactiontime.mat']);
cindex = false(size(cdata.analysis.rt.edges, 1), 1);
for cId = 1:NumClasses
    cindex = cindex | cdata.analysis.rt.labels.Ck == Classes(cId);
end

rt = cat(1, rt, cdata.analysis.rt.edges(cindex, 1));
Td = cat(1, Td, cdata.analysis.rt.labels.Rd(cindex));

%% Generic conditions
GenericCondition = ArtifactFree & Td == 1 & Ck == 1;


%% Implementing Baldassarre method

% Fisher z-transform of fc correlation values
util_bdisp('[proc] - Computing Fisher z-transform connectivity values');
fcz = zeros(NumNodes, NumChannels, NumTrials);
for trId = 1:NumTrials
    for nId = 1:NumNodes
        cfc = squeeze(fc(nId, :, trId));
        fcz(nId, :, trId) =  proc_zfisher(cfc);
    end
end

% fc:rt correlation for each node
util_bdisp('[proc] - Computing fc:rt for each node');
fcrt = zeros(NumNodes, NumChannels);
for nId = 1:NumNodes
    cindex = GenericCondition;
    crt = rt(cindex);
    cfc = squeeze(fcz(nId, :, cindex))';
    fcrt(nId, :) = corr(cfc, crt, 'rows', 'pairwise');
end

% PCA for each hemisphere
util_bdisp('[proc] - Computing PCA for each hemisphere (considering nodes belonging to)');
pcacoeff = zeros(NumNodesHemisphere, NumNodesHemisphere, NumHemispheres);
pcascore = zeros(NumNodesHemisphere, NumChannels, NumHemispheres);
pcaexpl  = zeros(NumNodesHemisphere, NumHemispheres);
for hId = 1:NumHemispheres    
    cnodeid = hcluster(:, hId);
    
    cfc = fcrt(cnodeid, :)';  
    
    [ccoef, cscore, ~, ~, cexpl] = pca(cfc);
    pcacoeff(:, :, hId) = ccoef;
    pcascore(:, :, hId) = cscore';
    pcaexpl(:, hId)     = cexpl;
end

util_bdisp('[proc] - Ranking the order of PCA1 weights');
ranked = struct();
ranked.weigth  = zeros(NumNodesHemisphere, NumHemispheres);
ranked.indexes = zeros(NumNodesHemisphere, NumHemispheres);
for hId = 1:NumHemispheres
    [~, cindexes] = sort(abs(pcacoeff(:, 1, hId)), 'descend');
    cweigths = pcacoeff(cindexes, 1, hId);
    ranked.weigth(:, hId)  = cweigths;
    ranked.indexes(:, hId) = cindexes;
end


ranked.selected.number = 8;
util_bdisp(['[proc] - Selecting the ' num2str(ranked.selected.number) ' nodes']);
ranked.selected.indexes  = zeros(ranked.selected.number, NumHemispheres);

for hId = 1:NumHemispheres
    cindexes = ranked.indexes(:, hId);
    ranked.selected.indexes(:, hId)  = cindexes(1:ranked.selected.number);
end

%% Computing functional connectivity:rt based on selected nodes (average)
util_bdisp('[proc] - Computing Functional connectivity:rt based on selected nodes');
sfc = zeros(NumChannels, NumTrials, NumHemispheres);

% Averaging maps across selected nodes
for hId = 1:NumHemispheres
    cnodeid = hcluster(ranked.selected.indexes(:, hId), hId);
    for trId = 1:NumTrials
        sfc(:, trId, hId)  = squeeze(mean(fc(cnodeid, :, trId), 1));
    end
end

% Fisher z-transformation
sfcz = zeros(NumChannels, NumTrials, NumHemispheres);
for hId = 1:NumHemispheres
    for trId = 1:NumTrials
        csfc = squeeze(sfc(:, trId, hId));
        sfcz(:, trId, hId) =  proc_zfisher(csfc);
    end
end

sfcrt_cmap = zeros(NumChannels, NumModalities, NumHemispheres);
sfcrt_pval = zeros(NumChannels, NumModalities, NumHemispheres);
sfcrt_mask = zeros(NumChannels, NumModalities, NumHemispheres);
alphavalue = 0.05;

for hId = 1:NumHemispheres
    for mId = 1:NumModalities
        cmodeid = Mk == Modalities(mId) & GenericCondition;
        csfcz = sfcz(:, cmodeid, hId)';
        crt  = rt(cmodeid);

        [sfcrt_cmap(:, mId, hId), sfcrt_pval(:, mId, hId)] = corr(csfcz, crt, 'rows', 'pairwise');
        sfcrt_mask(:, mId, hId) = sfcrt_pval(:, mId, hId) <= alphavalue;
    end
end




%% Plotting
fig1 = figure;
fig_set_position(fig1, 'All');
load('chanlocs64.mat');

SelectedPCA = 1;
ModLabel = {'Calibration', 'Online'};
HemLabel = {'Left', 'Right'};
NumRows = 3;
NumCols = 3;

% Rows 1: topoplots of fc:rt PCA1 hemisphere

for hId = 1:NumHemispheres
    subplot(NumRows, NumCols, hId);
    cmap = squeeze(pcascore(SelectedPCA, :, hId)); 
    cmap2plot  = zeros(NumTotChannels, 1);
    cmap2plot(channels.included.id) = cmap;
    topoplot(cmap2plot, chanlocs, 'headrad', 'rim', 'maplimits', [-0.7 0.7]);
    axis image;
    title([HemLabel{hId} ' - PCA1']);
end

subplot(NumRows, NumCols, NumCols);
barh(flipud(pcaexpl));
grid on;
xlim([0 80]);
set(gca, 'YTickLabel', fliplr(num2cell(1:NumNodesHemisphere)));
ylabel('PCA');
xlabel('[%]');
title(['Explained variance [%]']);
legend(HemLabel, 'location', 'best');


% Rows 2-4: topoplots of selected fc:rt maps for modalities and hemisphere

for mId = 1:NumModalities
    for hId = 1:NumHemispheres
        subplot(NumRows, NumCols, hId + NumCols*(mId - 1) + NumCols);
        cmap = sfcrt_cmap(:, mId, hId);
        cmap2plot  = zeros(NumTotChannels, 1);
        cmap2plot(channels.included.id) = cmap;
        topoplot(cmap2plot, chanlocs, 'headrad', 'rim', 'maplimits', [-0.7 0.7], 'conv', 'on');
        axis image;
        title([HemLabel{hId} ' - FC:RT map ' ModLabel{mId}]);
    end
end

% % Topoplots of fcb:PCA1 for calibration and online
% for mId = 1:NumModalities
%     subplot(NumRows, NumCols, mId);
%     
%     cmaps = fcb_maps(:, :, mId)';
%     ccoef = pcacoeff(:, SelectedPCA, 1);
%     cdata = cmaps*ccoef;
%     cdata2plot = zeros(totchans, 1);
%     cdata2plot(channels.included.id) = cdata;
%     topoplot(cdata2plot, chanlocs, 'headrad', 'rim', 'maplimits', [-1 1]);
%     axis image;
%     title(['PCA1 - ' ModLabel{mId}]);
% end
% 
% % Barplot with explained variance for calibration and online
% subplot(NumRows, NumCols, 3);
% barh(flipud(pcaexpl));
% grid on;
% xlim([0 80]);
% set(gca, 'YTickLabel', fliplr(num2cell(1:NumNodes)));
% ylabel('PCA');
% xlabel('[%]');
% title('Explained variance [%]');
% legend('Calibration', 'Online', 'location', 'best');

% Topoplots of the selected nodes
% for mId = 1:NumModalities
%     subplot(NumRows, NumCols, NumCols + mId);
%     cdata = sfcb_map(:, 1, mId);
%     cdata2plot = zeros(totchans, 1);
%     cdata2plot(channels.included.id) = cdata;
%     topoplot(cdata2plot, chanlocs, 'headrad', 'rim', 'maplimits', [-0.5 0.5]);
%     axis image;
%     title(['FC:Behavior - ' ModLabel{mId}]);
% end

suptitle(['Subject ' subject]);