clc;
clearvars;

subject     = 'SM';
experiment  = 'neglect';

Classes    = [1 3];
ClassesLb  = {'LeftCue (neglected)', 'MiddleCue'}; %
NumClasses = length(Classes);

%% Initialization

% Analysis period
AnalysisPeriod = 'cuetarget2'; % 'trial'/'precue';

% Datafiles
baseroot      = ['/mnt/data/Git/Codes/cvsa/analysis/' experiment '/'];
datapath      = [baseroot '/connectivity/'];

%ExcludedChannels = {'FPz', 'FP2', 'AF8', 'AF4', 'AFz', 'F8', 'FT8', 'T8', 'TP8', 'P10', 'Iz', 'P9', 'TP7', 'T7', 'FT7', 'F7', 'AF3', 'AF7', 'FP1'};
ExcludedChannels = {'FPz', 'FP2', 'AF8', 'AF4', 'AFz', 'F8', 'FT8', 'T8', 'TP8', 'Iz', 'TP7', 'T7', 'FT7', 'F7', 'AF3', 'AF7', 'FP1'};

%% Analysis - Loading functional connectivity
util_bdisp(['[io] - Loading functional connectivity for subject ' subject]);

cdata = load([datapath '/' subject '_functional_connectivity_nodes.mat']);
connectivity = cdata.connectivity;
channels = connectivity.channels;
NumTotChannels = length(channels.list);
channels.excluded.label = ExcludedChannels;
channels.excluded.id    = proc_get_channel(ExcludedChannels, channels.list);
channels.included.id    = setdiff(1:NumTotChannels, channels.excluded.id);
channels.included.id    = 1:64;
channels.included.label = proc_get_channel(channels.included.id, channels.list);
channels.included.hemisphere = channels.hemisphere(channels.included.id);

timings  = cdata.connectivity.timing;
nodes = cdata.connectivity.nodes;
Hemispheres = unique(nodes.locations.hemisphere.id);
NumHemispheres = length(Hemispheres);

cnumnodes = [];
for hId = 1:NumHemispheres
    chemId = Hemispheres(hId);
    cnumnodes = cat(1, cnumnodes, sum(nodes.locations.hemisphere.id == hId));
    if(length(unique(cnumnodes)) > 1)
        error('chk:nd', '[error] Different number of nodes per hemisphere');
    end
end
NumNodesHemisphere = unique(cnumnodes);

if strcmpi(AnalysisPeriod, 'precue')
    fc  = cdata.connectivity.functional.maps.precue(:,channels.included.id, :);
    fcp = cdata.connectivity.functional.pvalues.precue(:,channels.included.id, :);
elseif strcmpi(AnalysisPeriod, 'trial')
    fc  = cdata.connectivity.functional.maps.trial(:,channels.included.id, :);
    fcp = cdata.connectivity.functional.pvalues.trial(:,channels.included.id, :);
elseif strcmpi(AnalysisPeriod, 'cuetarget1')
    fc  = cdata.connectivity.functional.maps.cuetarget1(:,channels.included.id, :);
    fcp = cdata.connectivity.functional.pvalues.cuetarget1(:,channels.included.id, :);
elseif strcmpi(AnalysisPeriod, 'cuetarget2')
    fc  = cdata.connectivity.functional.maps.cuetarget2(:,channels.included.id, :);
    fcp = cdata.connectivity.functional.pvalues.cuetarget2(:,channels.included.id, :);
end

Ck = connectivity.labels.Ck;
Dk = connectivity.labels.Dk;
Mk = connectivity.labels.Mk;
Rk = connectivity.labels.Rk;

Modalities    = unique(Mk);
NumModalities = length(Modalities);
NumNodes      = size(fc, 1);
NumChannels   = size(fc, 2);
NumTrials     = length(Ck);

%% Analysis - Load artifacts analysis
cartifacts = load([baseroot '/artifacts/' subject '_artifacts.mat'], 'artifacts');
analysis.artifacts = cartifacts.artifacts;

ArtInd = false(length(analysis.artifacts.Ak), 1);
for ctrialId = 1:NumClasses
    ArtInd = ArtInd | analysis.artifacts.labels.Ck == Classes(ctrialId);
end

Ak = analysis.artifacts.Ak(ArtInd);

%% Analysis - Load eog analysis
ceoganalysis = load([baseroot '/eog/' subject '_eog.mat'], 'eog');
EogInd = false(size(ceoganalysis.eog.detection, 1), 1);
for ctrialId = 1:NumClasses
    EogInd = EogInd | ceoganalysis.eog.labels.Ck == Classes(ctrialId);
end
Ed = ceoganalysis.eog.detection(EogInd, 1);

%% Analysis - Artifact index
ArtifactFree = Ak == false | Ed == false;

%% Analysis - Loading reaction time
rt     = [];
Td     = [];
cdata  = load([baseroot '/reactiontime/' subject '_reactiontime.mat']);
cindex = false(size(cdata.analysis.rt.edges, 1), 1);
for cId = 1:NumClasses
    cindex = cindex | cdata.analysis.rt.labels.Ck == Classes(cId);
end

rt = cat(1, rt, cdata.analysis.rt.edges(cindex, 1));
Td = cat(1, Td, cdata.analysis.rt.labels.Rd(cindex));

%% Generic conditions
GenericCondition = ArtifactFree & Td == 1 & Ck == 1;


%% Fc behaviour correlation

% Fisher z-transform of fc correlation values
util_bdisp('[proc] - Computing Fisher z-transform connectivity values');
fcz = zeros(NumNodes, NumChannels, NumTrials);
for trId = 1:NumTrials
    for nId = 1:NumNodes
        cfc = squeeze(fc(nId, :, trId));
        fcz(nId, :, trId) =  proc_zfisher(cfc);
    end
end

% Interhemispheric connectivity
util_bdisp('[proc] - Interhemispheric connectivity for each node');
ifcz = zeros(NumNodes, NumTrials);
for trId = 1:NumTrials
   for nId = 1:NumNodes
       chemisphere = nodes.locations.hemisphere.id(nId);
       ohemisphereid = setdiff(Hemispheres, chemisphere);
       nodes_ohem = find(nodes.locations.hemisphere.id == ohemisphereid);
       ihemchansid = cell2mat(nodes.channels.id(nodes_ohem)');
       
       ifcz(nId, trId) = squeeze(mean(fcz(nId, ihemchansid, trId), 2));
   end
end

% Statistical test on connectivity changes from calibration to online
util_bdisp('[proc] - Statistical test on connectivity changes from calibration to online for each node and for each channel');
anovap = zeros(NumNodes, NumChannels);
for nId = 1:NumNodes
    for chId = 1:NumChannels
        clabel = Mk(GenericCondition);
        cfcz = squeeze(fcz(nId, chId, GenericCondition));
        anovap(nId, chId) = anova1(cfcz, clabel, 'off');
    end
end


% Correlation with rt
util_bdisp('[proc] - Computing fc:rt correlation maps for modality and for trial');
fcrt.modality.maps = zeros(NumNodes, NumChannels, NumModalities);
fcrt.modality.pval = zeros(NumNodes, NumChannels, NumModalities);
for mId = 1:NumModalities
    for nId = 1:NumNodes
        cindex = GenericCondition & Mk == Modalities(mId);
        cfcz = squeeze(fcz(nId, :, cindex));
        crt  = rt(cindex);

        [cmaps, cpval] = corr(cfcz', crt, 'rows', 'pairwise');

        fcrt.modality.maps(nId, :, mId) = cmaps;
        fcrt.modality.pval(nId, :, mId) = cpval;
    end
end

fcrt.trial.maps = zeros(NumNodes, NumChannels);
fcrt.trial.pval = zeros(NumNodes, NumChannels);
for nId = 1:NumNodes
    cindex = GenericCondition;
    cfcz = squeeze(fcz(nId, :, cindex));
    crt  = rt(cindex);

    [cmaps, cpval] = corr(cfcz', crt, 'rows', 'pairwise');

    fcrt.trial.maps(nId, :) = cmaps;
    fcrt.trial.pval(nId, :) = cpval;
end

% %% PCA - not used
% util_bdisp('[proc] - PCA over trials (not used)');
% SelectedPCA = 1;
% weights = cell(NumModalities, 1);
% scores  = cell(NumModalities, 1);
% explained = cell(NumModalities, 1);
% for mId = 1:NumModalities
%     cindex = GenericCondition & Mk == Modalities(mId);
%     cweights = zeros(NumNodes, sum(cindex), sum(cindex));
%     cscores = zeros(NumNodes, NumChannels, sum(cindex));
%     cexplained = zeros(NumNodes, sum(cindex));
%     for nId = 1:NumNodes
%         cfcz = squeeze(fcz(nId, :, cindex));
%         try
%             [cweights(nId, :, :), cscores(nId, :, :), ~, ~, cexplained(nId, :)] = pca(cfcz, 'Economy', false);
%         catch
%             keyboard
%         end
%     end
%     weights{mId} = cweights;
%     scores{mId} = cscores;
%     explained{mId} = cexplained;
% end

%% Plotting
fig1 = figure;
fig_set_position(fig1, 'All');
load('chanlocs64.mat');

ModLabel = {'Offline', 'Online'};
HemLabel = {'Left', 'Right'};
SelectedRegions = {'Frontal', 'Parietal', 'Occipital'}; 
SelectedRegionsId = util_cellfind(SelectedRegions, nodes.locations.regions.label);
NumSelectedRegions = length(SelectedRegionsId);
NumRows = 4;
NumCols = NumSelectedRegions*NumHemispheres;


for mId = 1:NumModalities
    for rId = 1:NumSelectedRegions
        for hId = 1:NumHemispheres
            subplot(NumRows, NumCols, hId + NumHemispheres*(rId - 1) + NumCols*(mId -1)); 
            chemisphereid = Hemispheres(hId);
            cregionid     = SelectedRegionsId(rId);
            cmodality     = Modalities(mId);
            nindex = nodes.locations.hemisphere.id == chemisphereid & nodes.locations.regions.id == cregionid;
            tindex = GenericCondition & Mk == cmodality;
            cmsk  = squeeze(anovap(nindex, :) <= 0.05);
            cmap  = squeeze(nanmean(fcz(nindex, :, tindex), 3));
            cmap  = cmap.*cmsk;
            cmap2plot  = zeros(NumTotChannels, 1);
        	cmap2plot(channels.included.id) = cmap;
            topoplot(cmap2plot, chanlocs, 'headrad', 'rim', 'maplimits', [-1 1], 'emarker2', {nodes.channels.id{nindex}, 'o', 'm', 5, 1});
            axis image;
            title(['fc>' ModLabel{mId} ' - ' SelectedRegions{rId} HemLabel{hId}]);
        end
    end
end


for rId = 1:NumSelectedRegions
    for hId = 1:NumHemispheres
        subplot(NumRows, NumCols, hId + NumHemispheres*(rId - 1) + 2*NumCols); 
        chemisphereid = Hemispheres(hId);
        cregionid     = SelectedRegionsId(rId);
        nindex = nodes.locations.hemisphere.id == chemisphereid & nodes.locations.regions.id == cregionid;
        cmap  = squeeze(nanmean(fcz(nindex, :, GenericCondition & Mk == 1), 3)) - squeeze(nanmean(fcz(nindex, :, GenericCondition & Mk == 0), 3));
        cmsk  = squeeze(anovap(nindex, :) <= 0.05);
        cmap  = cmap.*cmsk;
        cmap2plot  = zeros(NumTotChannels, 1);
        cmap2plot(channels.included.id) = cmap;
        topoplot(cmap2plot, chanlocs, 'headrad', 'rim', 'maplimits', [-0.5 0.5], 'emarker2', {nodes.channels.id{nindex}, 'o', 'm', 5, 1});
        axis image;
        title(['fc>Difference - ' SelectedRegions{rId} HemLabel{hId}]);
    end
end



for rId = 1:NumSelectedRegions
    for hId = 1:NumHemispheres
        subplot(NumRows, NumCols, hId + NumHemispheres*(rId - 1) + 3*NumCols);  
        chemisphereid = Hemispheres(hId);
        cregionid     = SelectedRegionsId(rId);
        nindex = nodes.locations.hemisphere.id == chemisphereid & nodes.locations.regions.id == cregionid;
        cmap = fcrt.trial.maps(nindex, :);
        cmsk = fcrt.trial.pval(nindex, :) <= 0.05 & squeeze(anovap(nindex, :) <= 0.05);
        cmap = cmap.*cmsk;
        cmap2plot  = zeros(NumTotChannels, 1);
        cmap2plot(channels.included.id) = cmap;
        topoplot(cmap2plot, chanlocs, 'headrad', 'rim', 'maplimits', [-0.5 0.5], 'emarker2', {nodes.channels.id{nindex}, 'o', 'm', 5, 1});
        axis image;
        title(['fc:rt>' SelectedRegions{rId} HemLabel{hId}]);
    end
end
suptitle([AnalysisPeriod ' - fc and fc:rt maps - Subject ' subject]);
    
% fig2 = figure;
% fig_set_position(fig2, 'All');
% load('chanlocs64.mat');
% NumRows = 1;
% NumCols = NumSelectedRegions*NumHemispheres;
% for mId = 1:NumModalities
%     for rId = 1:NumSelectedRegions
%         for hId = 1:NumHemispheres
%             subplot(NumRows, NumCols, hId + NumHemispheres*(rId - 1) + NumCols*(mId -1));  
%             chemisphereid = Hemispheres(hId);
%             cregionid     = SelectedRegionsId(rId);
%             cmodality     = Modalities(mId);
%             nindex = nodes.locations.hemisphere.id == chemisphereid & nodes.locations.regions.id == cregionid;
%             cmap = fcrt.maps(nindex, :, mId);
%             cmap2plot  = zeros(NumTotChannels, 1);
%         	cmap2plot(channels.included.id) = cmap;
%             topoplot(cmap2plot, chanlocs, 'headrad', 'rim', 'maplimits', [-0.5 0.5], 'emarker2', {nodes.channels.id{nindex}, 'o', 'm', 5, 1});
%             axis image;
%             title([ModLabel{mId} ' - ' SelectedRegions{rId}]);
%         end
%     end
% end