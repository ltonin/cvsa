clc;
clearvars;

subject     = 'SM';
pattern     = '.va.';

features       = 'psd';
spatialfilter  = 'car';
experiment     = 'neglect';

Classes   = [1 3];
ClassesLb    = {'LeftCue (neglected)', 'MiddleCue'};
NumClasses = length(Classes);

%% Initialization

% Timings
timings.trial.period    = 3;            % seconds
timings.offset.period   = 0;           % seconds

% Frequencies
selectedfrequencies       = 4:48;

% Channels
[~, channels.list]        = proc_get_montage('eeg.biosemi.64');
channels.hemisphere       = proc_get_hemisphere(channels.list);

% Datafiles
baseroot = ['/mnt/data/Git/Codes/cvsa/analysis/' experiment '/'];
datapath = [baseroot '/' features '/' spatialfilter '/'];
[Files, NumFiles] = util_getfile(datapath, '.mat', [subject '*' pattern]);

analysispath = './analysis/';

%% Analysis - Import data

% Concatanate datafiles
util_bdisp(['[io] - Concatenating ' num2str(NumFiles) ' datafiles']);
[F, analysis] = cvsa_utilities_concatenate_data(Files);

% Extract information
SampleRate     = analysis.processing.fs;
FrameSize      = analysis.processing.fsize;
[~, selfreqId] = intersect(analysis.processing.f, selectedfrequencies);
FreqGrid       = analysis.processing.f(selfreqId);

F = F(:, selfreqId, :);
NumTotSamples  = size(F, 1);
NumFreqs       = size(F, 2);
NumChans       = size(F, 3);
NumRois        = 2;

%% Analysis - Masking data

util_bdisp('[proc] - Creating trial mask vector');
events = analysis.event;
timings.trial.size  = floor(timings.trial.period*SampleRate/FrameSize);
timings.offset.size = floor(timings.offset.period*SampleRate/FrameSize);
timings.trial.mask  = false(NumTotSamples, 1);
timings.trial.pos   = [];
for ctrialId = 1:NumClasses
    [cmask, cpos, cdur] = proc_get_event(events, Classes(ctrialId), NumTotSamples, timings.trial.size, timings.offset.size);
    timings.trial.pos   = sort([timings.trial.pos cpos']);
end

util_bdisp('[proc] - Creating trial based class labels');
index = false(length(events.TYP), 1);
for ctrialId = 1:NumClasses
    index = events.TYP == Classes(ctrialId) | index;
end
Ck = events.TYP(index);

%% Extracting trials
util_bdisp('[proc] - Extracting trials');
NumTrials  = length(timings.trial.pos);
NumSamples = timings.trial.size + abs(timings.offset.size);
S = zeros(NumSamples, NumFreqs, NumChans, NumTrials);
Mk = zeros(NumTrials, 1);
Rk = zeros(NumTrials, 1);
Dk = zeros(NumTrials, 1);
for trId = 1:NumTrials
    cstart = timings.trial.pos(trId);
    cstop  = cstart + NumSamples - 1;
    S(:, :, :, trId) = F(cstart:cstop, :, :);
    Mk(trId) = unique(analysis.label.Mk(cstart:cstop));
    Rk(trId) = unique(analysis.label.Rk(cstart:cstop));
    Dk(trId) = unique(analysis.label.Dk(cstart:cstop));
end
Modalities    = unique(Mk);
NumModalities = length(Modalities);
Runs          = unique(Rk);
NumRuns       = length(Runs);
Days          = unique(Dk);
NumDays       = length(Days);

RunMod = zeros(NumRuns, 1);
for rId = 1:NumRuns, 
    RunMod(rId) = unique(Mk(Rk == rId)); 
end

%% Analysis - Load artifacts analysis
cartifacts = load([baseroot '/artifacts/' subject '_artifacts.mat'], 'artifacts');
analysis.artifacts = cartifacts.artifacts;

ArtInd = false(length(analysis.artifacts.Ak), 1);
for ctrialId = 1:NumClasses
    ArtInd = ArtInd | analysis.artifacts.labels.Ck == Classes(ctrialId);
end

Ak = analysis.artifacts.Ak(ArtInd);

if strcmpi(subject, 'BC')
    Ak = analysis.artifacts.Am(ArtInd) >= 115;
end

%% Analysis - Load eog analysis
ceoganalysis = load([baseroot '/eog/' subject '_eog.mat'], 'eog');
EogInd = false(size(ceoganalysis.eog.detection, 1), 1);
for ctrialId = 1:NumClasses
    EogInd = EogInd | ceoganalysis.eog.labels.Ck == Classes(ctrialId);
end
Ed = ceoganalysis.eog.detection(EogInd, 1);

%% Analysis - Artifact index
ArtifactFree = Ak == false & Ed == false;

%% Analysis - Loading peaks for each trial
util_bdisp('[proc] - Loading alpha peak for each trial');
ciaf = load([baseroot '/alphapeak/' subject '_iaf.mat']);
sband  = ciaf.iaf.modalities.band;

%% Generic condition
GenericCondition = ArtifactFree & true;
HealthyRoiId  = 1;
AffectedRoiId = 2;
Hemispheres = [1 2];
NumHemispheres = length(Hemispheres);
dolog = true;

if strcmpi(subject, 'SM')
    frequency.selected.label = 8:28; % 8:16; % best % SM
elseif strcmpi(subject, 'TC')
    frequency.selected.label = 8:14; % 8:14; % best % TC
elseif strcmpi(subject, 'BC')
    frequency.selected.label = 6:14; % 6:14; % best % BC
end
[frequency.selected.label, frequency.selected.id] = intersect(FreqGrid, frequency.selected.label);

channels.selected.label = {'P7', 'P5', 'P3', 'P1', 'PO7', 'PO3', 'O1', 'P8', 'P6', 'P4', 'P2', 'PO8', 'PO4', 'O2'};
channels.selected.id    = proc_get_channel(channels.selected.label,  channels.list);
channels.selected.hemisphere = proc_get_hemisphere(channels.selected.label);


if (dolog == true)
%     P = log(mean(S(:, frequency.selected.id, channels.selected.id, :))+1);
%     P = log(mean(S(:, frequency.selected.id, channels.selected.id, :), 2)+1);
    P = log(S(:, :, channels.selected.id, :)+1);
%     P = log(S+1);
else
%     P = mean(S(:, frequency.selected.id, channels.selected.id, :));
%     P = mean(S(:, frequency.selected.id, channels.selected.id, :), 2);
    P = S(:, :, channels.selected.id, :);
%     P = S;
end

NumFrSelected = size(P, 2);
NumChSelected = size(P, 3);
%% Discriminancy
fisher_run = zeros(NumFrSelected*NumChSelected, NumRuns);
for rId = 1:NumRuns
    cP  = P(:, :, :, GenericCondition & Rk == Runs(rId));
    cCk = Ck(GenericCondition & Rk == Runs(rId));
    
    fisher_run(:, rId) = proc_fisher(cP, cCk);
end

nfisher_run = zeros(NumFrSelected*NumChSelected, NumRuns);
for rId = 1:NumRuns
%     nfisher_run(:, rId) = fisher_run(:, rId)./max(fisher_run(:, rId));
    nfisher_run(:, rId) = proc_zfisher(fisher_run(:, rId)); 
end

fisher_day = zeros(NumFrSelected*NumChSelected, NumDays);
for dId = 1:NumDays
    cP  = P(:, :, :, GenericCondition & Dk == Days(dId));
    cCk = Ck(GenericCondition & Dk == Days(dId));
    fisher_day(:, dId) = proc_fisher(cP, cCk);
end

nfisher_day = zeros(NumFrSelected*NumChSelected, NumDays);
for dId = 1:NumDays
    nfisher_day(:, dId) = proc_zfisher(fisher_day(:, dId)); 
end

%% Feature selection
selvalues = cell(NumRuns, 1);
selindex = cell(NumRuns, 1);
for rId = 1:NumRuns
   cfisher = nfisher_run(:, rId);
   [cvalues, cindex] = proc_features_selection(cfisher, 'greatests', 5);
   [cfreqid, cchanid] = proc_bin2bc([NumFrSelected NumChSelected], cindex);
   selindex{rId} = [cfreqid, cchanid];
end

rank = zeros(2, NumRuns);
for rId = 1:NumRuns
   cselection = selindex{rId};
   
   rank(:, rId) = [sum(find(selindex{rId}(:, 2) <= 7)) sum(find(selindex{rId}(:, 2) >=8))]./length(selindex{rId});
end

%% Reshaping fisher score and selecting channels/frequency
rfisher_run = reshape(nfisher_run, [NumFrSelected NumChSelected NumRuns]);

roifisher_run = zeros(size(rfisher_run, 1), NumHemispheres, NumRuns);
for hId = 1:NumHemispheres
    chemId = channels.selected.hemisphere == Hemispheres(hId);
    roifisher_run(:, hId, :) = sum(rfisher_run(:, chemId, :), 2);
end

rfisher_day = reshape(nfisher_day, [NumFrSelected NumChSelected NumDays]);
roifisher_day = zeros(size(rfisher_day, 1), NumHemispheres, NumDays);
for hId = 1:NumHemispheres
    chemId = channels.selected.hemisphere == Hemispheres(hId);
    roifisher_day(:, hId, :) = sum(rfisher_day(:, chemId, :), 2);
end

%%
aroifisher_run = zeros(3, NumHemispheres, NumRuns);
for rId = 1:NumRuns
    cmodality = find(Modalities == RunMod(rId));
    cband_l = sband(:, 1, cmodality);
    cband_r = sband(:, 2, cmodality);
    aroifisher_run(:, :, rId) = [roifisher_run(cband_l, 1, rId) roifisher_run(cband_r, 2, rId)];
end

%% Plotting
% fig1 = figure;
% fig_set_position(fig1, 'Top');

% subplot(1, 2, 1);
% bar(diff([squeeze(sum(roifisher(1, 1, :), 1)) squeeze(sum(roifisher(1, 2, :), 1))], [], 2));
bar(diff(squeeze(sum(roifisher_run(frequency.selected.id, :, :), 1))));
[cr, pr] = corr(Runs, diff(squeeze(sum(roifisher_run(frequency.selected.id, :, :), 1)))', 'type', 'pearson')
bar(diff(squeeze(sum(roifisher_day(frequency.selected.id, :, :), 1))));
[cd, pd] = corr(Days, diff(squeeze(sum(roifisher_day(frequency.selected.id, :, :), 1)))', 'type', 'pearson')
bar(diff(squeeze(sum(aroifisher_run, 1))));
[ca, pa] = corr(Runs, diff(squeeze(sum(aroifisher_run, 1)))', 'type', 'pearson')
% %% Statistical tests
% util_bdisp('[stat] - Statistical tests');
% 
% [pvalue_c, ctab] = anova1(Fc(:, AffectedRoiId), RunMod, 'off');
% fvalue_c = ctab{2, 5};
% fprintf('[stat] - Anova between calibration and online runs [AffectedRoI-Percentage]:\tF=%2.1f, p=%1.5f\n', fvalue_c, pvalue_c);
% 
% [pvalue_r, ctab] = anova1(Fr(:, AffectedRoiId), RunMod, 'off');
% fvalue_r = ctab{2, 5};
% fprintf('[stat] - Anova between calibration and online runs [AffectedRoI-Rank]:\tF=%2.1f, p=%1.5f\n', fvalue_r, pvalue_r);
% 
% 
% %% Correlation with runs
% [corrval_c, corrpval_c] = corr(Runs, Fc(:, AffectedRoiId));
% [corrval_r, corrpval_r] = corr(Runs, Fr(:, AffectedRoiId));
% 
% %% Plotting
% fig1 = figure;
% fig_set_position(fig1, 'Top');
% CorrAlpha = 0.05;
% 
% NumRows = 2;
% NumCols = 2;
% 
% % Percentage boxplots
% subplot(NumRows, NumCols, 1);
% boxplot(Fc(:, AffectedRoiId),  RunMod, 'factorseparator', 1);
% axbox(rId) = gca;
% grid on;
% title('Average recruitment (Percentage) - Affected hemisphere');
% set(gca, 'XTickLabel', {'Calibration', 'Online'});
% ylabel('[%]');
% cpos = get(gca,'Position');
% cpos(2) = cpos(2) - 0.06;
% textcolor = 'k';
% if(pvalue_c <= CorrAlpha)
%     textcolor = 'r';
% end
% t = annotation('textbox', cpos, 'String', ['F=' num2str(fvalue_c, '%3.2f') ...
%                                      ', p=' num2str(pvalue_c, '%3.3f')], ...
%                                      'LineStyle', 'none', 'Color', textcolor, 'FontWeight', 'bold');
% t.FontSize = 10;                                 
% 
% % Percentage correlation
% subplot(NumRows, NumCols, 2);
% hs = scatter(Runs, Fc(:, AffectedRoiId), 'o', 'MarkerFaceColor',[0 0.4470 0.7410], 'MarkerEdgeColor', 'black');
% hl = lsline;
% set(hl, 'LineWidth', 1.5, 'Color', 'r');
% cpos = get(gca,'Position');
% cpos(2) = cpos(2) - 0.06;
% textcolor = 'k';
% if(corrpval_c <= CorrAlpha)
%     textcolor = 'r';
% end
% t = annotation('textbox', cpos, 'String', ['rho=' num2str(corrval_c, '%3.2f') ...
%                                      ', p=' num2str(corrpval_c, '%3.3f')], ...
%                                      'LineStyle', 'none', 'Color', textcolor, 'FontWeight', 'bold');
% t.FontSize = 10;                                 
% 
% grid on;
% ylim([0 100]);
% xlim([Runs(1) Runs(end)]);
% xlabel('Runs');
% ylabel('[%]');
% title('Run correlation (Percentage) - Affected hemisphere');
% 
% % Rank boxplots
% subplot(NumRows, NumCols, 3);
% boxplot(Fr(:, AffectedRoiId),  RunMod, 'factorseparator', 1);
% axbox(rId) = gca;
% grid on;
% title('Average recruitment (Rank) - Affected hemisphere');
% set(gca, 'XTickLabel', {'Calibration', 'Online'});
% ylabel('[Rank]');
% cpos = get(gca,'Position');
% cpos(2) = cpos(2) - 0.06;
% textcolor = 'k';
% if(pvalue_r <= CorrAlpha)
%     textcolor = 'r';
% end
% t = annotation('textbox', cpos, 'String', ['F=' num2str(fvalue_r, '%3.2f') ...
%                                      ', p=' num2str(pvalue_r, '%3.3f')], ...
%                                      'LineStyle', 'none', 'Color', textcolor, 'FontWeight', 'bold');
% t.FontSize = 10;                                 
% 
% % Rank correlation
% subplot(NumRows, NumCols, 4);
% hs = scatter(Runs, Fr(:, AffectedRoiId), 'o', 'MarkerFaceColor',[0 0.4470 0.7410], 'MarkerEdgeColor', 'black');
% hl = lsline;
% set(hl, 'LineWidth', 1.5, 'Color', 'r');
% cpos = get(gca,'Position');
% cpos(2) = cpos(2) - 0.06;
% textcolor = 'k';
% if(corrpval_r <= CorrAlpha)
%     textcolor = 'r';
% end
% t = annotation('textbox', cpos, 'String', ['rho=' num2str(corrval_r, '%3.2f') ...
%                                      ', p=' num2str(corrpval_r, '%3.3f')], ...
%                                      'LineStyle', 'none', 'Color', textcolor, 'FontWeight', 'bold');
% t.FontSize = 10;                                 
% 
% grid on;
% ylim([0 100]);
% xlim([Runs(1) Runs(end)]);
% xlabel('Runs');
% ylabel('[Rank]');
% title('Run correlation (Rank) - Affected hemisphere');
% 
% suptitle(['Patient ' subject]);