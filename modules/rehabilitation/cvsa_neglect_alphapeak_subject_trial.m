clc;
clearvars;

subject     = 'SM';
pattern     = '.va.';

features       = 'psd';
spatialfilter  = 'car';
experiment     = 'neglect';

Classes    = [1 3];
ClassesLb  = {'LeftCue (neglected)', 'MiddleCue'}; %
NumClasses = length(Classes);

%% Initialization

% Timings
timings.trial.period    = 3;            % seconds
timings.offset.period   = -1;           % seconds

% Channels and RoIs
[~, channels.list]        = proc_get_montage('eeg.biosemi.64');
% channels.roi.left.labels  = {'P7', 'P5', 'PO7'};         % As in [Thut et al., 2006]
% channels.roi.right.labels = {'P8', 'P6', 'PO8'};         % As in [Thut et al., 2006]
channels.roi.left.labels  = {'P5', 'P3', 'P1', 'PO7', 'PO3', 'O1'};         % Custom
channels.roi.right.labels = {'P6', 'P4', 'P2', 'PO8', 'PO4', 'O2'};         % Custom
channels.roi.left.id      = proc_get_channel(channels.roi.left.labels,  channels.list);
channels.roi.right.id     = proc_get_channel(channels.roi.right.labels, channels.list);
channels.roi.labels       = {'Healthy RoI (Left)', 'Affected RoI (Right)'};
NumRois                   = length(channels.roi.labels);

% Frequencies
selectedfrequencies       = 4:48;

% Datafiles
baseroot      = ['/mnt/data/Git/Codes/cvsa/analysis/' experiment '/'];
datapath      = [baseroot '/' features '/' spatialfilter '/'];
[Files, NumFiles] = util_getfile(datapath, '.mat', [subject '*' pattern]);
[~, saveroot] = util_mkdir(baseroot, 'alphapeak/');


%% Analysis - Import data

% Concatanate datafiles
util_bdisp(['[io] - Concatenating ' num2str(NumFiles) ' datafiles']);
[F, analysis] = cvsa_utilities_concatenate_data(Files);

% Extract information
SampleRate     = analysis.processing.fs;
FrameSize      = analysis.processing.fsize;
[~, selfreqId] = intersect(analysis.processing.f, selectedfrequencies);
FreqGrid       = analysis.processing.f(selfreqId);

F = F(:, selfreqId, :);
NumTotSamples  = size(F, 1);
NumFreqs       = size(F, 2);
NumChans       = size(F, 3);

%% Analysis - Masking data

util_bdisp('[proc] - Creating trial mask vector');
events = analysis.event;
timings.trial.size  = floor(timings.trial.period*SampleRate/FrameSize);
timings.offset.size = floor(timings.offset.period*SampleRate/FrameSize);
timings.trial.mask  = false(NumTotSamples, 1);
timings.trial.pos   = [];
for ctrialId = 1:NumClasses
    [cmask, cpos, cdur] = proc_get_event(events, Classes(ctrialId), NumTotSamples, timings.trial.size, timings.offset.size);
    timings.trial.pos   = sort([timings.trial.pos cpos']);
end

util_bdisp('[proc] - Creating trial based class labels');
index = false(length(events.TYP), 1);
for ctrialId = 1:NumClasses
    index = events.TYP == Classes(ctrialId) | index;
end
Ck = events.TYP(index);

%% Extracting trials
util_bdisp('[proc] - Extracting trials');
NumTrials  = length(timings.trial.pos);
NumSamples = timings.trial.size + abs(timings.offset.size);
S = zeros(NumSamples, NumFreqs, NumChans, NumTrials);
Mk = zeros(NumTrials, 1);
Rk = zeros(NumTrials, 1);
Dk = zeros(NumTrials, 1);
for trId = 1:NumTrials
    cstart = timings.trial.pos(trId);
    cstop  = cstart + NumSamples - 1;
    S(:, :, :, trId) = F(cstart:cstop, :, :);
    Mk(trId) = unique(analysis.label.Mk(cstart:cstop));
    Rk(trId) = unique(analysis.label.Rk(cstart:cstop));
    Dk(trId) = unique(analysis.label.Dk(cstart:cstop));
end
Modalities    = unique(Mk);
NumModalities = length(Modalities);
Runs          = unique(Rk);
NumRuns       = length(Runs);
Days          = unique(Dk);
NumDays       = length(Days);

%% Analysis - Load artifacts analysis
cartifacts = load([baseroot '/artifacts/' subject '_artifacts.mat'], 'artifacts');
analysis.artifacts = cartifacts.artifacts;

ArtInd = false(length(analysis.artifacts.Ak), 1);
for ctrialId = 1:NumClasses
    ArtInd = ArtInd | analysis.artifacts.labels.Ck == Classes(ctrialId);
end

Ak = analysis.artifacts.Ak(ArtInd);

if strcmpi(subject, 'BC')
    Ak = analysis.artifacts.Am(ArtInd) >= 115;
end

%% Analysis - Load eog analysis
ceoganalysis = load([baseroot '/eog/' subject '_eog.mat'], 'eog');
EogInd = false(size(ceoganalysis.eog.detection, 1), 1);
for ctrialId = 1:NumClasses
    EogInd = EogInd | ceoganalysis.eog.labels.Ck == Classes(ctrialId);
end
Ed = ceoganalysis.eog.detection(EogInd, 1);

%% Analysis - Artifact index
ArtifactFree = Ak == false & Ed == false;

%% Extracting data over whole trial for each RoI
util_bdisp('[proc] - Extracting data over whole trial for each RoI');
psd = [];
psd(:, :, 1, :) = squeeze(mean(S(:, :, channels.roi.left.id, :), 3));
psd(:, :, 2, :) = squeeze(mean(S(:, :, channels.roi.right.id, :), 3));

%% Extracting spectrum in pre-cue interval
util_bdisp('[proc] - Extracting spectrum in pre-cue interval (by averaging pre-cue interval)');
PreCueInterval = 1:abs(timings.offset.size);         
psd_precue = squeeze(mean(psd(PreCueInterval, :, :, :), 1));

%% Normalizing spectrum in pre-cue interval with respect to the maximum for each RoI and trial
util_bdisp('[proc] - Normalizing spectrum in pre-cue interval');
maxpre_cue_l = max(squeeze(psd_precue(:, 1, :)), [], 1);
maxpre_cue_r = max(squeeze(psd_precue(:, 2, :)), [], 1);
% maxpre_cue_l = squeeze(psd_precue(1, 1, :))';
% maxpre_cue_r = squeeze(psd_precue(1, 2, :))';
npsd_precue(:, 1, :) = squeeze(psd_precue(:, 1, :))./repmat(maxpre_cue_l, [NumFreqs 1]);
npsd_precue(:, 2, :) = squeeze(psd_precue(:, 2, :))./repmat(maxpre_cue_r, [NumFreqs 1]);

%% Find the alpha peak based on healthy RoI for calibration and online modalities
util_bdisp('[proc] - Find alpha peak over modalities and RoIs');

HealthyRoiId  = 1;
AffectedRoiId = 2;

if(strcmp(subject, 'SM'))
    FreqPeakRange = 8:12;
elseif(strcmp(subject, 'TC'))
    FreqPeakRange = 8:12;
elseif(strcmp(subject, 'BC'))  
    FreqPeakRange = 5:10;
end

% Trial condition
GenericCondition = ArtifactFree == true;

mpeaks = zeros(NumRois, NumModalities);
for mId = 1:NumModalities
    cmodality = Modalities(mId);
    for rId = 1:NumRois
        [~, cpeaks] = findpeaks(mean(npsd_precue(:, rId, GenericCondition & Mk == cmodality), 3));
        
        % Extracting peaks inside the selected frequency range
        [~, ~, cpeakids] = intersect(FreqPeakRange, FreqGrid(cpeaks));
        
        if(isempty(cpeakids))
            speak = nan;                    % If not peak have been found in the range, put NaN
        else
            speak = cpeaks(cpeakids(1));    % Otherwise, selecte the first peak in the range
        end
        
%         if(isempty(cpeaks) == false)
%             [~, ~, cpeakids] = intersect(FreqPeakRange, FreqGrid(cpeaks));
%             if(isempty(cpeakids) == false)
%                 cpeaks = cpeaks(cpeakids(1));
%             else
%                 cpeaks = nan;
%             end
%         else
%             cpeaks = nan;
%         end
        mpeaks(rId, mId) = speak;
    end
end

% Fill empty values or values out of selected peak range, as follows:
% - if peak cannot be found in the affected roi then use the peak location
%   in the healthy roi (according to the modality)

for mId = 1:NumModalities
    if(isnan(mpeaks(AffectedRoiId, mId)) || isempty(intersect(FreqPeakRange, FreqGrid(mpeaks(AffectedRoiId, mId)))))
        mpeaks(AffectedRoiId, mId) = mpeaks(HealthyRoiId, mId);
    end
end

% Define alpha bands around alpha peak for each RoI and modality
mbands = zeros(3, NumRois, NumModalities);
for mId = 1:NumModalities
    for rId = 1:NumRois
        cpeak = mpeaks(rId, mId);
        mbands(:, rId, mId) = cpeak-1:cpeak+1; 
    end
end


%% Find the alpha peak based on healthy RoI for each trial
util_bdisp('[proc] - Find alpha peak over trials and RoIs');
tpeaksraw = zeros(NumRois, NumTrials);

for trId = 1:NumTrials
    ctrial_l = npsd_precue(:, 1, trId);
    ctrial_r = npsd_precue(:, 2, trId);

    [~, cpeak_l] =  findpeaks(ctrial_l);
    [~, cpeak_r] =  findpeaks(ctrial_r);
    

    % Left RoI
    % Extracting peaks inside the selected frequency range
    [~, ~, cpeakids] = intersect(FreqPeakRange, FreqGrid(cpeak_l));
   
    if(isempty(cpeakids))
        speak_l = nan;                   % If not peak have been found in the range, put NaN
    else
        speak_l = cpeak_l(cpeakids(1)); % Otherwise, selecte the first peak in the range
    end
    
    % Right RoI
    % Extracting peaks inside the selected frequency range
    [~, ~, cpeakids] = intersect(FreqPeakRange, FreqGrid(cpeak_r));
   
    if(isempty(cpeakids))
        speak_r = nan;                   % If not peak have been found in the range, put NaN
    else
        speak_r = cpeak_r(cpeakids(1)); % Otherwise, selecte the first peak in the range
    end
    
%     % Fill empty values (or values out of selected peak range) with nan
%     if(isempty(cpeak_l) == false)
%         [~, ~, cpeakids] = intersect(FreqPeakRange, FreqGrid(cpeak_l));
%         if(isempty(intersect(FreqPeakRange, FreqGrid(cpeak_l))))
%             if(isempty(cpeakids) == false)
%                  cpeak_l = cpeak_l(cpeakids(1));
%             else
%                 cpeak_l  = nan;
%             end
%         else
%             cpeak_l = nan;
%         end
%     end
    
%     if(isempty(cpeak_r) == false)
%         [~, ~, cpeakids] = intersect(FreqPeakRange, FreqGrid(cpeak_r));
%         if(isempty(intersect(FreqPeakRange, FreqGrid(cpeak_r))))
%             if(isempty(cpeakids) == false)
%                  cpeak_r = cpeak_r(cpeakids(1));
%             else
%                 cpeak_r  = nan;
%             end
%         else
%             cpeak_r = nan;
%         end
%     end
    
    % Save peaks for each trial
    tpeaksraw(1, trId) = speak_l;
    tpeaksraw(2, trId) = speak_r;
end

% Fill the NaN values as follows:
% - if peak was not found in THE roi, use the peak related to the modality
%   the trial belongs to

missingpeaks_l = find(isnan(tpeaksraw(1, :)));
missingpeaks_r = find(isnan(tpeaksraw(2, :)));

tpeaks = tpeaksraw;

% Left missing peaks
for mpId = 1:length(missingpeaks_l)
    ctrialId = missingpeaks_l(mpId);
    cmodality = find(Modalities == Mk(ctrialId));
    tpeaks(HealthyRoiId, ctrialId) = mpeaks(HealthyRoiId, cmodality);
end

% Right missing peaks
for mpId = 1:length(missingpeaks_r)
    ctrialId = missingpeaks_r(mpId);
    %cmodality = find(Modalities == Mk(ctrialId));
    % tpeaks(AffectedRoiId, ctrialId) = mpeaks(AffectedRoiId, cmodality);
     tpeaks(AffectedRoiId, ctrialId) = tpeaks(HealthyRoiId, ctrialId);
end

% Define alpha bands around alpha peak for each trial and RoI
tbands = zeros(3, NumRois, NumTrials);
for trId = 1:NumTrials
    for rId = 1:NumRois
        cpeak = tpeaks(rId, trId);
        tbands(:, rId, trId) = cpeak-1:cpeak+1;
    end
end   
    
%% Compute the alpha power in the pre-cue interval and during the whole trial
util_bdisp('[proc] - Compute the alpha power in the pre-cue interval and during the whole trial');
alphapower_trial  = zeros(NumSamples, NumRois, NumTrials);
alphapower_precue = zeros(NumRois, NumTrials);

for trId = 1:NumTrials
    for rId = 1:NumRois
        cband = tbands(:, rId, trId);
        alphapower_precue(rId, trId)   = squeeze(mean(npsd_precue(cband, rId, trId), 1));
        
        % Since in this case we are in the time domain, do not use the
        % normalize spectrum
        alphapower_trial(:, rId, trId) = squeeze(mean(psd(:, cband, rId, trId), 2));
    end
end

%% Compute the erd/ers for the alpha power during the whole trial
util_bdisp('[proc] - Compute the erd/ers during the whole trial (baseline normalization)');
baseline_trial = repmat(mean(alphapower_trial(PreCueInterval, :, :), 1), [NumSamples 1 1]);
% baseline_trial_calibration = repmat(mean(baseline_trial(:, :, Mk == 0 & GenericCondition), 3), [1 1 sum(Mk == 0)]);
% baseline_trial_online = repmat(mean(baseline_trial(:, :, Mk == 1 & GenericCondition), 3), [1 1 sum(Mk == 1)]);
% baseline_trial = cat(3, baseline_trial_calibration, baseline_trial_online);
activation_trial = 100*(alphapower_trial - baseline_trial)./baseline_trial;
%activation_trial = alphapower_trial;



%% Statistical tests
util_bdisp('[stat] - Statistical tests');

StatCondition = GenericCondition;
pvalues = zeros(NumRois, 1);
fvalues = zeros(NumRois, 1);
for rId = 1:NumRois
    [cpval, ctab] = anova1(alphapower_precue(rId, StatCondition), Mk(StatCondition), 'off');
    pvalues(rId) = cpval;
    fvalues(rId) = ctab{2, 5};
    fprintf('[stat] - Anova between calibration and online trials [%s]:\tF=%2.1f, p=%1.5f\n', channels.roi.labels{rId}, fvalues(rId), pvalues(rId));
end

%% Saving results
targetfile = [saveroot '/' subject '_alphapeak_trial.mat'];
util_bdisp(['[out] - Saving analysis in: ' targetfile]);

alphapeak.spectrum.precue.raw       = psd_precue;
alphapeak.spectrum.precue.norm      = npsd_precue;
alphapeak.peaks.modality.locs       = mpeaks;
alphapeak.peaks.modality.bands      = mbands;
alphapeak.peaks.modality.labels     = {'Dim1:RoIs', 'Dim2:Modalities'};
alphapeak.peaks.trial.rawlocs       = tpeaksraw;
alphapeak.peaks.trial.locs          = tpeaks;
alphapeak.peaks.trial.bands         = tbands;
alphapeak.peaks.modality.labels     = {'Dim1:RoIs', 'Dim2:Trials'};
alphapeak.peaks.range               = FreqPeakRange;
alphapeak.channels                  = channels;
alphapeak.timings                   = timings;
alphapeak.timings.precue.period     = [timings.offset.period 0];
alphapeak.timings.precue.interval   = PreCueInterval;
alphapeak.timings.precue.size       = length(PreCueInterval);
alphapeak.labels.Mk                 = Mk;
alphapeak.labels.Rk                 = Rk;
alphapeak.labels.Dk                 = Dk;
alphapeak.labels.Ck                 = Ck;
alphapeak.labels.AFk                = ArtifactFree;

save(targetfile, 'alphapeak');       

%% Plotting
util_bdisp('[out] - Plotting results');

% Plotting the spectrum of the pre-cue interval average across modalities
fig1 = figure;
fig_set_position(fig1, 'All');

NumRows = 3;
NumCols = 2;

PlotCondition = GenericCondition;
PlotSpectrumStyle = {'-', ':'};

axspectrum = zeros(NumRois, NumModalities);
for rId = 1:NumRois
    subplot(NumRows, NumCols, rId);
    
    hold on;
    for mId = 1:NumModalities
        cmodality = Modalities(mId);
        axspectrum(rId, mId) = plot(FreqGrid, mean(npsd_precue(:, rId, PlotCondition & Mk == cmodality), 3), PlotSpectrumStyle{mId}, 'LineWidth', 2);
         
        grid on;
        xlim([FreqGrid(1) FreqGrid(end)]);
    end
    for mId = 1:NumModalities
        cmodality = Modalities(mId);
        plot(get(axspectrum(rId, mId), 'Parent'), FreqGrid(mpeaks(rId, mId)), mean(npsd_precue(mpeaks(rId, mId), rId, PlotCondition & Mk == cmodality), 3)+0.03, 'v', 'MarkerEdgeColor', get(axspectrum(rId, mId), 'Color'), 'MarkerFaceColor', get(axspectrum(rId, mId), 'Color'));
    end
    hold off;
    legend('Calibration', 'Online');
    xlabel('[uV]');
    ylabel('NormPsd');
    title(['Spectrum - ' channels.roi.labels{rId}]);
end

ylimits = [0 1];
for rId = 1:NumRois
    for mId = 1:NumModalities
        climits = get(get(axspectrum(rId, mId), 'Parent'), 'YLim');
        ylimits = [min(climits(1), ylimits(1)) max(climits(2), ylimits(2))];
    end
end

for rId = 1:NumRois
    for mId = 1:NumModalities
        set(get(axspectrum(rId, mId), 'Parent'), 'YLim', ylimits);
    end
end

hold on;
for rId = 1:NumRois
    rectangle(get(axspectrum(rId, 1), 'Parent'), 'Position', [FreqPeakRange(1) - 0.2 0 (FreqPeakRange(end) - FreqPeakRange(1)) + 0.2  ylimits(2)], 'EdgeColor','g');
end
hold off;

% Plotting boxplots
axbox = zeros(NumRois, 1);
for rId = 1:NumRois
    subplot(NumRows, NumCols, rId + NumCols);
    boxplot(alphapower_precue(rId, PlotCondition),  Mk(PlotCondition), 'factorseparator', 1);
    axbox(rId) = gca;
    grid on;
    title(channels.roi.labels{rId});
    set(gca, 'XTickLabel', {'Calibration', 'Online'});
    ylabel('NormPsd');
end

ylimits = [0 1];
for rId = 1:NumRois
    climits = get(axbox(rId), 'YLim');
    ylimits = [min(climits(1), ylimits(1)) max(climits(2), ylimits(2))];
end

for rId = 1:NumRois
    set(axbox(rId), 'YLim', ylimits);
end

% Plotting ERD/ERS
axerd = zeros(NumRois, NumModalities);
t = timings.offset.period:analysis.processing.fsize/analysis.processing.fs:timings.trial.period - analysis.processing.fsize/analysis.processing.fs;
for rId = 1:NumRois
    subplot(NumRows, NumCols, rId + 2*NumCols);
    
    hold on;
    for mId = 1:NumModalities
        cmodality = Modalities(mId);
        axerd(rId, mId) = plot(t, mean(activation_trial(:, rId, true & Mk == cmodality & Ck == 1), 3), PlotSpectrumStyle{mId}, 'LineWidth', 2);
    end
    hold off;
    grid on;
    %ylim([-50 50]);
    xlabel('Time [s]');
    ylabel('[%]');
    legend('Calibration', 'Online');
    title(['ERD/ERS ' channels.roi.labels{rId}]);
end

ylimits = [0 1];
for rId = 1:NumRois
    for mId = 1:NumModalities
        climits = get(get(axerd(rId, mId), 'Parent'), 'YLim');
        ylimits = [min(climits(1), ylimits(1)) max(climits(2), ylimits(2))];
    end
end

for rId = 1:NumRois
    for mId = 1:NumModalities
        set(get(axerd(rId, mId), 'Parent'), 'YLim', ylimits);
    end
end


axes(get(axerd(HealthyRoiId, 1), 'Parent'));
plot_vline(0, 'k', 'cue');
axes(get(axerd(AffectedRoiId, 1), 'Parent'));
plot_vline(0, 'k', 'cue');

suptitle(['Patient ' subject]);

    
    