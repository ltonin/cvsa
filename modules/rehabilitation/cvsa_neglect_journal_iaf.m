clc;
clearvars;

subject     = 'TC';
pattern     = '.va.';

features       = 'psd';
spatialfilter  = 'car';
experiment     = 'neglect';

Classes    = [1 3];
ClassesLb  = {'LeftCue (neglected)', 'MiddleCue'}; %
NumClasses = length(Classes);

%% Initialization

% Timings
timings.trial.period    = 3;            % seconds
timings.offset.period   = -1;           % seconds

% Channels and RoIs
[~, channels.list]        = proc_get_montage('eeg.biosemi.64');
% channels.roi.left.labels  = {'P7', 'P5', 'PO7'};         % As in [Thut et al., 2006]
% channels.roi.right.labels = {'P8', 'P6', 'PO8'};         % As in [Thut et al., 2006]
channels.roi.left.labels  = {'P5', 'P3', 'P1', 'PO7', 'PO3', 'O1'};         % Custom
channels.roi.right.labels = {'P6', 'P4', 'P2', 'PO8', 'PO4', 'O2'};         % Custom
channels.roi.left.id      = proc_get_channel(channels.roi.left.labels,  channels.list);
channels.roi.right.id     = proc_get_channel(channels.roi.right.labels, channels.list);
channels.roi.labels       = {'Healthy RoI (Left)', 'Affected RoI (Right)'};
NumRois                   = length(channels.roi.labels);

% Frequencies
selectedfrequencies       = 4:48;

% Datafiles
baseroot      = ['/mnt/data/Git/Codes/cvsa/analysis/' experiment '/'];
datapath      = [baseroot '/' features '/' spatialfilter '/'];
[Files, NumFiles] = util_getfile(datapath, '.mat', [subject '*' pattern]);
[~, saveroot] = util_mkdir(baseroot, 'alphapeak/');


%% Analysis - Import data

% Concatanate datafiles
util_bdisp(['[io] - Concatenating ' num2str(NumFiles) ' datafiles']);
[F, analysis] = cvsa_utilities_concatenate_data(Files);

% Extract information
SampleRate     = analysis.processing.fs;
FrameSize      = analysis.processing.fsize;
[~, selfreqId] = intersect(analysis.processing.f, selectedfrequencies);
FreqGrid       = analysis.processing.f(selfreqId);

F = F(:, selfreqId, :);
NumTotSamples  = size(F, 1);
NumFreqs       = size(F, 2);
NumChans       = size(F, 3);

%% Analysis - Masking data

util_bdisp('[proc] - Creating trial mask vector');
events = analysis.event;
timings.trial.size  = floor(timings.trial.period*SampleRate/FrameSize);
timings.offset.size = floor(timings.offset.period*SampleRate/FrameSize);
timings.trial.mask  = false(NumTotSamples, 1);
timings.trial.pos   = [];
for ctrialId = 1:NumClasses
    [cmask, cpos, cdur] = proc_get_event(events, Classes(ctrialId), NumTotSamples, timings.trial.size, timings.offset.size);
    timings.trial.pos   = sort([timings.trial.pos cpos']);
end

util_bdisp('[proc] - Creating trial based class labels');
index = false(length(events.TYP), 1);
for ctrialId = 1:NumClasses
    index = events.TYP == Classes(ctrialId) | index;
end
Ck = events.TYP(index);

%% Extracting trials
util_bdisp('[proc] - Extracting trials');
NumTrials  = length(timings.trial.pos);
NumSamples = timings.trial.size + abs(timings.offset.size);
S = zeros(NumSamples, NumFreqs, NumChans, NumTrials);
Mk = zeros(NumTrials, 1);
Rk = zeros(NumTrials, 1);
Dk = zeros(NumTrials, 1);
for trId = 1:NumTrials
    cstart = timings.trial.pos(trId);
    cstop  = cstart + NumSamples - 1;
    S(:, :, :, trId) = F(cstart:cstop, :, :);
    Mk(trId) = unique(analysis.label.Mk(cstart:cstop));
    Rk(trId) = unique(analysis.label.Rk(cstart:cstop));
    Dk(trId) = unique(analysis.label.Dk(cstart:cstop));
end
Modalities    = unique(Mk);
NumModalities = length(Modalities);
Runs          = unique(Rk);
NumRuns       = length(Runs);
Days          = unique(Dk);
NumDays       = length(Days);

RunMod = zeros(NumRuns, 1);
for rId = 1:NumRuns, 
    RunMod(rId) = unique(Mk(Rk == rId)); 
end

%% Analysis - Load eog analysis
ceoganalysis = load([baseroot '/eog/' subject '_eog.mat'], 'eog');
EogInd = false(size(ceoganalysis.eog.detection, 1), 1);
for ctrialId = 1:NumClasses
    EogInd = EogInd | ceoganalysis.eog.labels.Ck == Classes(ctrialId);
end
Ed = ceoganalysis.eog.detection(EogInd, 1);

%% Analysis - Artifact index
ArtifactFree = Ed == false;

%% Averaging data for each RoI
util_bdisp('[proc] - Averaging data for each RoI');
psd = [];
psd(:, :, 1, :) = squeeze(mean(S(:, :, channels.roi.left.id, :), 3));
psd(:, :, 2, :) = squeeze(mean(S(:, :, channels.roi.right.id, :), 3));

%% Extracting spectrum in pre-cue interval
util_bdisp('[proc] - Extracting spectrum in pre-cue interval (by averaging pre-cue interval)');
PreCueInterval = 1:abs(timings.offset.size);         
psd_precue = squeeze(mean(psd(PreCueInterval, :, :, :), 1));

%% Normalizing spectrum in pre-cue interval with respect to the maximum for each RoI and trial
util_bdisp('[proc] - Normalizing spectrum in pre-cue interval');
maxpre_cue_l = max(squeeze(psd_precue(:, 1, :)), [], 1);
maxpre_cue_r = max(squeeze(psd_precue(:, 2, :)), [], 1);
npsd_precue(:, 1, :) = squeeze(psd_precue(:, 1, :))./repmat(maxpre_cue_l, [NumFreqs 1]);
npsd_precue(:, 2, :) = squeeze(psd_precue(:, 2, :))./repmat(maxpre_cue_r, [NumFreqs 1]);

%% Alpha peak analysis configuration
HealthyRoiId  = 1;
AffectedRoiId = 2;

if(strcmp(subject, 'SM'))
    FreqPeakRange = 8:12;
elseif(strcmp(subject, 'TC'))
    FreqPeakRange = 8:12;
elseif(strcmp(subject, 'BC'))  
    FreqPeakRange = 6:12;
end

% Trial condition
GenericCondition = ArtifactFree == true;

%% Find the alpha peak based on healthy RoI for calibration and online modalities
util_bdisp('[proc] - Find alpha peak per RoIs per modality');

m_iaf = zeros(NumRois, NumModalities);
for mId = 1:NumModalities
    for roId = 1:NumRois
        cmodality = Modalities(mId);
        [~, cpeaks] = findpeaks(mean(npsd_precue(:, roId, GenericCondition & Mk == cmodality), 3));
        
        % Extracting peaks inside the selected frequency range
        [~, ~, cpeakids] = intersect(FreqPeakRange, FreqGrid(cpeaks));
        
        if(isempty(cpeakids))
            speak = nan;                    % If not peak have been found in the range, put NaN
        else
            speak = cpeaks(cpeakids(1));    % Otherwise, selecte the first peak in the range
        end
        m_iaf(roId, mId) = speak;
    end
end

%% Find the alpha peak based on healthy RoI for runs
util_bdisp('[proc] - Find alpha peak per RoIs per run');
r_iaf = zeros(NumRois, NumRuns);

for rId = 1:NumRuns
    for roId = 1:NumRois
        crun = Runs(rId);
        [~, cpeaks] = findpeaks(mean(npsd_precue(:, roId, GenericCondition & Rk == crun), 3));
        
        % Extracting peaks inside the selected frequency range
        [~, ~, cpeakids] = intersect(FreqPeakRange, FreqGrid(cpeaks));
        
        if(isempty(cpeakids))
            speak = nan;                    % If not peak have been found in the range, put NaN
        else
            speak = cpeaks(cpeakids(1));    % Otherwise, selecte the first peak in the range
        end
        r_iaf(roId, rId) = speak;
    end
end

%% Find the alpha peak based on healthy RoI for days
util_bdisp('[proc] - Find alpha peak per RoIs per day');
d_iaf = zeros(NumRois, NumDays);

for dId = 1:NumDays
    for roId = 1:NumRois
        cday = Days(dId);
        [~, cpeaks] = findpeaks(mean(npsd_precue(:, roId, GenericCondition & Dk == cday), 3));
        
        % Extracting peaks inside the selected frequency range
        [~, ~, cpeakids] = intersect(FreqPeakRange, FreqGrid(cpeaks));
        
        if(isempty(cpeakids))
            speak = nan;                    % If not peak have been found in the range, put NaN
        else
            speak = cpeaks(cpeakids(1));    % Otherwise, selecte the first peak in the range
        end
        d_iaf(roId, dId) = speak;
    end
end

%% Find the alpha peak based on healthy RoI for trial
util_bdisp('[proc] - Find alpha peak per RoIs per day');
t_iaf = zeros(NumRois, NumTrials);

for trId = 1:NumTrials
    for roId = 1:NumRois
        ctrial = npsd_precue(:, roId, trId);
        [~, cpeaks] = findpeaks(ctrial);
        
        % Extracting peaks inside the selected frequency range
        [~, ~, cpeakids] = intersect(FreqPeakRange, FreqGrid(cpeaks));
        
        if(isempty(cpeakids))
            speak = nan;                    % If not peak have been found in the range, put NaN
        else
            speak = cpeaks(cpeakids(1));    % Otherwise, selecte the first peak in the range
        end
        t_iaf(roId, trId) = speak;
    end
end

%% Fill IAF-modality NaN values with the corresponding values of the modality
util_bdisp('[proc] - Fill IAF-modality NaN values');
mf_iaf = m_iaf;
for mId = 1:NumModalities
    if(isnan(m_iaf(AffectedRoiId, mId)))
        mf_iaf(AffectedRoiId, mId) = m_iaf(HealthyRoiId, mId);
    end
end

%% Fill IAF-trial NaN values with the corresponding values of the modality
util_bdisp('[proc] - Fill IAF-trial NaN values');
tf_iaf = t_iaf;
for trId = 1:NumTrials
    cmodalityId = find(Modalities == Mk(trId));
    for roId = 1:NumRois  
        if(isnan(t_iaf(roId, trId)))
            tf_iaf(roId, trId) = mf_iaf(roId, cmodalityId);
        end
    end
end

%% Compute alpha band based on IAF-modality
util_bdisp('[proc] - Computing alpha band based on IAF-modality');
m_iafband = zeros(3, NumRois, NumModalities);

for mId = 1:NumModalities
    for roId = 1:NumRois
        ciaf = mf_iaf(roId, mId);
        m_iafband(:, roId, mId) = (ciaf - 1):(ciaf + 1);
    end
end

%% Saving results
targetfile = [saveroot '/' subject '_iaf.mat'];
util_bdisp(['[out] - Saving analysis in: ' targetfile]);

iaf.spectrum.precue.raw     = psd_precue;
iaf.spectrum.precue.nrm     = npsd_precue;
iaf.modalities.raw          = m_iaf;
iaf.modalities.location     = mf_iaf;
iaf.modalities.band         = m_iafband;
iaf.trials.raw              = t_iaf;
iaf.trials.location         = tf_iaf;
iaf.runs.raw                = r_iaf;
iaf.days.raw                = d_iaf;
iaf.peaks.range             = FreqPeakRange;
iaf.channels                = channels;
iaf.timings                 = timings;
iaf.timings.precue.period   = [timings.offset.period 0];
iaf.timings.precue.interval = PreCueInterval;
iaf.timings.precue.size     = length(PreCueInterval);
iaf.labels.Mk               = Mk;
iaf.labels.Rk               = Rk;
iaf.labels.Dk               = Dk;
iaf.labels.Ck               = Ck;
iaf.labels.AFk              = ArtifactFree;
iaf.labels.RunMod           = RunMod;

save(targetfile, 'iaf');       

%% Average IAF and missing peaks per runs
iafmean     = zeros(NumRois, NumModalities);
iafstd      = zeros(NumRois, NumModalities);
iafmissing  = zeros(NumRois, NumModalities);
for mId = 1:NumModalities 
        cindex = RunMod == Modalities(mId);
        clocations = r_iaf(:, cindex);
        
        for roId = 1:NumRois
            validlocations         = clocations(roId, isnan(clocations(roId, :)) == false);
            iafmean(roId, mId)     = mean(FreqGrid(validlocations));
            iafstd(roId, mId)      = std(FreqGrid(validlocations));
            iafmissing(roId, mId)  = 100*sum(isnan(clocations(roId, :)))./sum(RunMod == Modalities(mId)); 
        end
end

%% Compute the alpha power in the pre-cue interval and during the whole trial
util_bdisp('[proc] - Compute the alpha power in the pre-cue interval and during the whole trial');
alphapower_trial  = zeros(NumSamples, NumRois, NumTrials);
alphapower_precue = zeros(NumRois, NumTrials);

for trId = 1:NumTrials
    for roId = 1:NumRois
        cmodalityId = find(Modalities == Mk(trId));
        ciafband = m_iafband(:, roId, cmodalityId);
        alphapower_precue(roId, trId)   = squeeze(mean(npsd_precue(ciafband, roId, trId), 1));
        
        % Since in this case we are in the time domain, do not use the
        % normalize spectrum
        alphapower_trial(:, roId, trId) = squeeze(mean(psd(:, ciafband, roId, trId), 2));
    end
end

%% Averaged IAF locations (Hz)
ModalityLb = {'Calibration', 'Online'};
HemisphereLb = {'Left', 'Right'};
IAFLoc_m = zeros(NumModalities, NumRois);
IAFLoc_s = zeros(NumModalities, NumRois);

for mId = 1:NumModalities
    for roId = 1:NumRois
        IAFLoc_m(mId, roId) = mean(FreqGrid(iaf.trials.location(roId, iaf.labels.Mk == Modalities(mId) & GenericCondition)));
        IAFLoc_s(mId, roId) =  std(FreqGrid(iaf.trials.location(roId, iaf.labels.Mk == Modalities(mId) & GenericCondition)));
        fprintf('[out] - IAF location [Hz] for %s modality (%s):\t%1.2f+/-%1.2f\n', ModalityLb{mId}, HemisphereLb{roId}, IAFLoc_m(mId, roId), IAFLoc_s(mId, roId));
    end
     
end

%% Statistical test (ttest)
BonferroniCorrection = 2;
StatCondition = GenericCondition;
pvalues = zeros(NumRois, 1);
for rId = 1:NumRois
    [~, cpvalue] = ttest2(alphapower_precue(rId, StatCondition & Mk == 0), alphapower_precue(rId, StatCondition & Mk == 1)); %, 'vartype', 'unequal');
    cpvalue = cpvalue*BonferroniCorrection;
    fprintf('[stat] - %s - Student t-test between modalities: p=%1.5f (Bonferroni)\n', channels.roi.labels{rId}, cpvalue);
    pvalues(rId) = cpvalue;
end

% IAF
iafpvalues = zeros(NumModalities, 1);
for mId = 1:NumModalities
    [~, cpvalue] = ttest2(FreqGrid(iaf.trials.location(1, StatCondition & Mk == Modalities(mId))), FreqGrid(iaf.trials.location(2, StatCondition & Mk == Modalities(mId))));
    cpvalue = cpvalue*BonferroniCorrection;
    fprintf('[stat] - Student t-test for IAF locations (modality: %s): p=%1.3f (Bonferroni)\n', ModalityLb{mId}, cpvalue);
    iafpvalues(rId, mId) = cpvalue;
end

%% Statistical test (wilcoxon)
BonferroniCorrection = 2;
StatCondition = GenericCondition;
pvalues_w = zeros(NumRois, 1);
for rId = 1:NumRois
    cpvalue_w = ranksum(alphapower_precue(rId, StatCondition & Mk == 0), alphapower_precue(rId, StatCondition & Mk == 1));
    cpvalue_w = cpvalue_w*BonferroniCorrection;
    fprintf('[stat] - %s - Wilcoxon ranksum test between modalities: p=%1.5f (Bonferroni)\n', channels.roi.labels{rId}, cpvalue_w);
    pvalues_w(rId) = cpvalue_w;
end

% IAF
iafpvalues_w = zeros(NumModalities, 1);
for mId = 1:NumModalities
    cpvalue_w = ranksum(FreqGrid(iaf.trials.location(1, StatCondition & Mk == Modalities(mId))), FreqGrid(iaf.trials.location(2, StatCondition & Mk == Modalities(mId))));
    cpvalue_w = cpvalue_w*BonferroniCorrection;
    fprintf('[stat] - Wilcoxon ranksum for IAF locations (modality: %s): p=%1.3f (Bonferroni)\n', ModalityLb{mId}, cpvalue_w);
    iafpvalues_w(rId, mId) = cpvalue_w;
end

%% Plotting
util_bdisp('[out] - Plotting results');

% Plotting the spectrum of the pre-cue interval average across modalities
fig1 = figure;
fig_set_position(fig1, 'All');

NumRows = 2;
NumCols = 2;

PlotCondition = GenericCondition;
PlotSpectrumStyle = {'-', ':'};

axspectrum = zeros(NumRois, NumModalities);
for rId = 1:NumRois
    subplot(NumRows, NumCols, rId);
    
    hold on;
    for mId = 1:NumModalities
        cmodality = Modalities(mId);
        axspectrum(rId, mId) = plot(FreqGrid, mean(npsd_precue(:, rId, PlotCondition & Mk == cmodality), 3), PlotSpectrumStyle{mId}, 'LineWidth', 2);
         
        grid on;
        xlim([FreqGrid(1) FreqGrid(end)]);
    end
    for mId = 1:NumModalities
        cmodality = Modalities(mId);
        plot(get(axspectrum(rId, mId), 'Parent'), FreqGrid(mf_iaf(rId, mId)), mean(npsd_precue(mf_iaf(rId, mId), rId, PlotCondition & Mk == cmodality), 3)+0.03, 'v', 'MarkerEdgeColor', get(axspectrum(rId, mId), 'Color'), 'MarkerFaceColor', get(axspectrum(rId, mId), 'Color'));
    end
    hold off;
    legend('Calibration', 'Online');
    xlabel('[uV]');
    ylabel('NormPsd');
    title(['Spectrum - ' channels.roi.labels{rId}]);
end

ylimits = [0 1];
for rId = 1:NumRois
    for mId = 1:NumModalities
        climits = get(get(axspectrum(rId, mId), 'Parent'), 'YLim');
        ylimits = [min(climits(1), ylimits(1)) max(climits(2), ylimits(2))];
    end
end

for rId = 1:NumRois
    for mId = 1:NumModalities
        set(get(axspectrum(rId, mId), 'Parent'), 'YLim', ylimits);
    end
end

hold on;
for rId = 1:NumRois
    rectangle(get(axspectrum(rId, 1), 'Parent'), 'Position', [FreqPeakRange(1) - 0.2 0 (FreqPeakRange(end) - FreqPeakRange(1)) + 0.2  ylimits(2)], 'EdgeColor','g');
end
hold off;

% Plotting boxplots
CorrAlpha = 0.05;
axbox = zeros(NumRois, 1);
for rId = 1:NumRois
    subplot(NumRows, NumCols, rId + NumCols);
    boxplot(alphapower_precue(rId, PlotCondition),  Mk(PlotCondition), 'factorseparator', 1);
    axbox(rId) = gca;
    grid on;
    title(channels.roi.labels{rId});
    set(gca, 'XTickLabel', {'Calibration', 'Online'});
    ylabel('NormPsd');
    
    cpos = get(gca,'Position');
    cpos(2) = cpos(2) - 0.02;

    textcolor = 'k';
    if(pvalues(rId) <= CorrAlpha)
        textcolor = 'r';
    end
    t = annotation('textbox', cpos, 'String', ['p=' num2str(pvalues(rId), '%3.3f') ' (Bonferroni)'],  'LineStyle', 'none', 'Color', textcolor, 'FontWeight', 'bold');
    t.FontSize = 10;
end

ylimits = [0 1];
for rId = 1:NumRois
    climits = get(axbox(rId), 'YLim');
    ylimits = [min(climits(1), ylimits(1)) max(climits(2), ylimits(2))];
end

for rId = 1:NumRois
    set(axbox(rId), 'YLim', ylimits);
end
suptitle(['Patient ' subject]);

    