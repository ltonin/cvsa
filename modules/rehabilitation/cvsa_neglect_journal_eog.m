clc;
clearvars;

subject     = 'BC';
pattern     = '.va.';
experiment  = 'neglect';

%% Initialization

% Threshold parameters
threshold.horizontal = 100;
threshold.vertical   = 100;

% Window parameters
window.horizontal = 0.4; % seconds
window.vertical   = 0.4; % seconds

% Datafiles
datapath    = ['/mnt/data/Git/Codes/cvsa/analysis/' experiment '/eog/'];
[Files, NumFiles] = util_getfile(datapath, '.mat', [subject '*' pattern]);

% Extra paths
baseroot      = ['/mnt/data/Git/Codes/cvsa/analysis/' experiment '/'];
[~, saveroot] = util_mkdir(baseroot, 'eog/');

%% Analysis - Import data

% Concatanate datafiles
util_bdisp(['[io] - Concatenating ' num2str(NumFiles) ' EOG datafiles']);
[E, analysis] = cvsa_utilities_concatenate_eog(Files, 128);                 % Removing the last 128 samples of each file to fit PSD data

NumSamples = size(E, 1);
SampleRate = analysis.fs;

%% Detecting Horizontal and Vertical EOG
hIndexes = find(E(:, 1) >= threshold.horizontal);
vIndexes = find(E(:, 2) >= threshold.vertical);

%% Applying window to the detected samples

hwin = window.horizontal*SampleRate;
vwin = window.vertical*SampleRate;

Eh = false(NumSamples, 1);
for hId = 1:length(hIndexes)
    cindex = hIndexes(hId);
    cstart = cindex - floor(hwin/2);
    cstop  = cindex + floor(hwin/2);
    Eh(cstart:cstop) = true;
end

Ev = false(NumSamples, 1);
for vId = 1:length(vIndexes)
    cindex = vIndexes(vId);
    cstart = cindex - floor(vwin/2);
    cstop  = cindex + floor(vwin/2);
    Ev(cstart:cstop) = true;
end

%% Saving results
targetfile = [saveroot '/' subject '_eog_samples.mat'];
util_bdisp(['[out] - Saving analysis in: ' targetfile]);

eog.threshold = threshold;
eog.window    = window;
eog.events    = analysis.event;
eog.labels.Eh = Eh;
eog.labels.Ev = Ev;
eog.fs        = SampleRate;

save(targetfile, 'eog');       