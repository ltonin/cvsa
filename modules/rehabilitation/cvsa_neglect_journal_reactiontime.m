clc;
clearvars;

subject     = 'TC';
pattern     = '.va.';
experiment  = 'neglect';

Classes    = [1 3];
ClassesLb  = {'Left', 'Middle'}; %
NumClasses = length(Classes);

%% Initialization
% Datafiles

baseroot      = ['/mnt/data/Git/Codes/cvsa/analysis/' experiment '/'];
datapath      = [baseroot '/reactiontime/'];
[~, saveroot] = util_mkdir(baseroot, 'reactiontime/');

[Files, NumFiles] = util_getfile(datapath, '.mat', [subject '*' pattern]);

%% Analysis - Import data

% Concatanate datafiles
util_bdisp(['[io] - Concatenating ' num2str(NumFiles) ' datafiles']);
Rk  = [];
Mk  = [];
Dk  = [];
Sk  = [];
Eg  = [];
Bp  = [];
Ts  = [];
Rd  = [];
Ck  = [];
cdaylbl  = '';
cday     = 0;
csublbl  = '';
csub     = 0;
for fId = 1:NumFiles
    cfilename = Files{fId};
    info = util_getfile_info(cfilename);
    util_bdisp(['[io] - Loading file ' num2str(fId) '/' num2str(NumFiles)]);
    disp(['       File: ' cfilename]);

    % Get modality from filename
    switch lower(info.modality)
        case 'offline'
            cmod = 0;
        case 'online'
            cmod = 1;
        otherwise
            error('chk:mod', ['[' mfilename '] Unknown modality']);
    end

    % Get day from filename
    if strcmpi(info.date, cdaylbl) == false
        cday = cday + 1;
        cdaylbl = info.date;
    end

    % Get subject from filename
    if strcmpi(info.subject, csublbl) == false
        csub = csub + 1;
        csublbl = info.subject;
    end
    
    % Loading processed data
    cdata = load(cfilename);
    
    % Concatenate data
    Eg = cat(1, Eg, cdata.button.edges);
    Bp = cat(1, Bp, cdata.button.press);
    
    % Concatenate markers vector
    Ts = cat(1, Ts, cdata.labels.Ts);
    Rd = cat(1, Rd, cdata.labels.Rd);
    Ck = cat(1, Ck, cdata.labels.Ck);
    Rk = cat(1, Rk, fId*ones(size(cdata.button.edges, 1), 1));
    Mk = cat(1, Mk, cmod*ones(size(cdata.button.edges, 1), 1));
    Dk = cat(1, Dk, cday*ones(size(cdata.button.edges, 1), 1));
    Sk = cat(1, Sk, csub*ones(size(cdata.button.edges, 1), 1));
end
Runs = unique(Rk);
NumRuns = length(Runs);
Days = unique(Dk);
NumDays = length(Days);

Modalities = unique(Mk);
NumModalities = length(Mk);

%% Analysis - Load eog analysis
ceoganalysis = load([baseroot '/eog/' subject '_eog.mat'], 'eog');
Ed = ceoganalysis.eog.detection(:, 1);

%% Reaction time
ArtifactFree = Ed == false; % Only EOG artifact since it is reaction time

%% Extracting reaction times
RT = Eg(:, 1);

%% Statistics
pvalues = zeros(NumClasses, 1);
for cId = 1:NumClasses
    cindex = Ts == Classes(cId) & Rd == true & ArtifactFree == true;
    [~, cpvalue] = ttest2(RT(cindex & Mk == Modalities(1)), RT(cindex & Mk == Modalities(2)));
    fprintf('[stat] - %s - T-Test between modalities: p=%1.7f\n', ClassesLb{cId}, cpvalue);
    pvalues(cId) = cpvalue;
end

%% Statistics (wilcoxon)
pvalues_w = zeros(NumClasses, 1);
for cId = 1:NumClasses
    cindex = Ts == Classes(cId) & Rd == true & ArtifactFree == true;
    cpvalue_w = ranksum(RT(cindex & Mk == Modalities(1)), RT(cindex & Mk == Modalities(2)));
    fprintf('[stat] - %s - Wilcoxon ranksum between modalities: p=%1.7f\n', ClassesLb{cId}, cpvalue_w);
    pvalues_w(cId) = cpvalue_w;
end


%% Plotting
fig1 = figure;
fig_set_position(fig1, 'Top');

NumRows = 1;
NumCols = 3;

for cId = 1:NumClasses
    subplot(NumRows, NumCols, cId);
    cindex = Ts == Classes(cId) & Rd == true & ArtifactFree == true;
    boxplot(RT(cindex), Mk(cindex));
    grid on;
    
    ylabel('Reaction time [s]');
    set(gca, 'XTickLabel', {'Calibration', 'Online'});
    title(ClassesLb{cId});
    ylim([0 1.6]);
    
    cpos = get(gca, 'Position');
    cpos(2) = cpos(2) - 0.06;
    t1 = annotation('textbox', cpos, 'String', ['p=' num2str(pvalues(cId), '%3.3f')],  'LineStyle', 'none', 'Color', 'k', 'FontWeight', 'bold');
    t1.FontSize = 10;
end

CorrSelClass = 1;
cindex = Ts == CorrSelClass & ArtifactFree & Rd == true;
TrialIndex = 1:length(RT);
[rtcorr, rtpval] = corr(RT(cindex), TrialIndex(cindex)', 'rows', 'pairwise');


subplot(NumRows, NumCols, NumClasses + 1);
plot(TrialIndex(cindex), RT(cindex), '.');
hl = lsline;
set(hl, 'Color', 'r', 'LineWidth', 2);
grid on;
ylim([0 1.6]);
xlabel('Trial index');
ylabel('Reaction time [s]');
cpos = get(gca, 'Position');
cpos(2) = cpos(2) - 0.06;
t1 = annotation('textbox', cpos, 'String', ['r= ' num2str(rtcorr, '%3.3f') ', p=' num2str(rtpval, '%3.3f')],  'LineStyle', 'none', 'Color', 'k', 'FontWeight', 'bold');
t1.FontSize = 10;
title(['Correlation over time - ' ClassesLb{CorrSelClass}]);

suptitle(['Subject ' subject ' - RT analysis']);

% Saving analysis

analysis.rt.values    = RT;
analysis.rt.labels.Mk = Mk;
analysis.rt.labels.Rk = Rk;
analysis.rt.labels.Dk = Dk;
analysis.rt.labels.Ck = Ck;
analysis.rt.labels.Ts = Ts;
analysis.rt.labels.Rd = Rd;
analysis.rt.labels.AFk = ArtifactFree;

targetfile = [saveroot '/' subject '_reactiontime.mat'];
util_bdisp(['[out] - Saving reaction time analysis for subject ' subject ' in: ' targetfile]);
save(targetfile, 'analysis'); 