% clc;
% clearvars;
% 
% subject     = 'SM';
pattern     = '.va.';

features       = 'psd';
spatialfilter  = 'car';
experiment     = 'neglect';

Classes    = [1 3];
ClassesLb  = {'LeftCue (neglected)', 'MiddleCue'}; %
NumClasses = length(Classes);

%% Initialization

% Timings
timings.trial.period    = 3;            % seconds
timings.offset.period   = -1;           % seconds

% Channels
[~, channels.list]        = proc_get_montage('eeg.biosemi.64');
channels.hemisphere       = proc_get_hemisphere(channels.list);

% Nodes list
nodes.channels.labels{1} = {'F5', 'F3', 'F1', 'FC5', 'FC3', 'FC1', 'Fz', 'FCz'};
nodes.channels.labels{2} = {'C5', 'C3', 'C1', 'CP5', 'CP3', 'CP1', 'Cz', 'CPz'};
nodes.channels.labels{3} = {'P7', 'P5', 'P3', 'P1', 'Pz'};
nodes.channels.labels{4} = {'PO7', 'PO3', 'O1', 'POz', 'Oz'};
nodes.channels.labels{5} = {'F6', 'F4', 'F2', 'FC6', 'FC4', 'FC2', 'Fz', 'FCz'};
nodes.channels.labels{6} = {'C6', 'C4', 'C2', 'CP6', 'CP4', 'CP2', 'Cz', 'CPz'};
nodes.channels.labels{7} = {'P8', 'P6', 'P4', 'P2', 'Pz'};
nodes.channels.labels{8} = {'PO8', 'PO4', 'O2', 'POz', 'Oz'};

NumNodes = length(nodes.channels.labels);
for nId = 1:NumNodes
    nodes.channels.id{nId} = proc_get_channel(nodes.channels.labels{nId}, channels.list);
end

nodes.locations.hemisphere.id = [1 1 1 1 2 2 2 2];
nodes.locations.hemisphere.label = {'Left', 'Right'};
nodes.locations.regions.id = [1 2 3 4 1 2 3 4];
nodes.locations.regions.label = {'Frontal', 'Central', 'Parietal', 'Occipital'};

% Frequencies
selectedfrequencies       = 4:48;

% Datafiles
baseroot      = ['/mnt/data/Git/Codes/cvsa/analysis/' experiment '/'];
datapath      = [baseroot '/' features '/' spatialfilter '/'];
[Files, NumFiles] = util_getfile(datapath, '.mat', [subject '*' pattern]);
[~, saveroot] = util_mkdir(baseroot, 'connectivity/');


%% Analysis - Import data

% Concatanate datafiles
util_bdisp(['[io] - Concatenating ' num2str(NumFiles) ' datafiles']);
[F, analysis] = cvsa_utilities_concatenate_data(Files);

% Extract information
SampleRate     = analysis.processing.fs;
FrameSize      = analysis.processing.fsize;
[~, selfreqId] = intersect(analysis.processing.f, selectedfrequencies);
FreqGrid       = analysis.processing.f(selfreqId);

F = F(:, selfreqId, :);
NumTotSamples  = size(F, 1);
NumFreqs       = size(F, 2);
NumChans       = size(F, 3);

%% Analysis - Masking data

util_bdisp('[proc] - Creating trial mask vector');
events = analysis.event;
timings.trial.size  = floor(timings.trial.period*SampleRate/FrameSize);
timings.offset.size = floor(timings.offset.period*SampleRate/FrameSize);
timings.trial.mask  = false(NumTotSamples, 1);
timings.trial.pos   = [];
for ctrialId = 1:NumClasses
    [cmask, cpos, cdur] = proc_get_event(events, Classes(ctrialId), NumTotSamples, timings.trial.size, timings.offset.size);
    timings.trial.pos   = sort([timings.trial.pos cpos']);
end

util_bdisp('[proc] - Creating trial based class labels');
index = false(length(events.TYP), 1);
for ctrialId = 1:NumClasses
    index = events.TYP == Classes(ctrialId) | index;
end
Ck = events.TYP(index);

%% Extracting trials
util_bdisp('[proc] - Extracting trials');
NumTrials  = length(timings.trial.pos);
NumSamples = timings.trial.size + abs(timings.offset.size);
S = zeros(NumSamples, NumFreqs, NumChans, NumTrials);
Mk = zeros(NumTrials, 1);
Rk = zeros(NumTrials, 1);
Dk = zeros(NumTrials, 1);
for trId = 1:NumTrials
    cstart = timings.trial.pos(trId);
    cstop  = cstart + NumSamples - 1;
    S(:, :, :, trId) = F(cstart:cstop, :, :);
    Mk(trId) = unique(analysis.label.Mk(cstart:cstop));
    Rk(trId) = unique(analysis.label.Rk(cstart:cstop));
    Dk(trId) = unique(analysis.label.Dk(cstart:cstop));
end
Modalities    = unique(Mk);
NumModalities = length(Modalities);
Runs          = unique(Rk);
NumRuns       = length(Runs);
Days          = unique(Dk);
NumDays       = length(Days);

%% Analysis - Loading peaks for each trial
util_bdisp('[proc] - Loading alpha peak for each trial');
ciaf = load([baseroot '/alphapeak/' subject '_iaf.mat']);
sband  = ciaf.iaf.modalities.band;

%% Extract alpha power for each channel and trial
util_bdisp('[proc] - Extracting alpha power (related to the peak) for each trial');
alphapower = zeros(NumSamples, NumChans, NumTrials); 
for trId = 1:NumTrials
    cdata = S(:, :, :, trId);
    for chId = 1:NumChans
        chemisphere = channels.hemisphere(chId);
        if chemisphere == 3
            chemisphere = 2;
        end
        cmodality = find(Modalities == Mk(trId));
        cband = sband(:, chemisphere, cmodality);
        alphapower(:, chId, trId) = mean(cdata(:, cband, chId), 2);
    end
end

%% Averaging alpha power for each nodeId
util_bdisp('[proc] -Averaging alpha power for each node');
alphapower_nodes = zeros(NumSamples, NumNodes, NumTrials);
for nId = 1:NumNodes
   cchId = nodes.channels.id{nId};
   cdata     = alphapower(:, cchId, :);
   alphapower_nodes(:, nId, :) = mean(cdata, 2);
end


%% Compute connectivity
util_bdisp('[proc] - Computing functional connectivity maps between each node and the rest of the channels for each trial');
ToI.precue = 1:abs(timings.offset.size);
ToI.trial  = abs(timings.offset.size)+1:NumSamples;
ToI.cuetarget1  = (1:timings.trial.size/2) + abs(timings.offset.size);
ToI.cuetarget2  = (1+timings.trial.size/2:timings.trial.size) + abs(timings.offset.size);

fcmap.trial       = zeros(NumNodes, NumChans, NumTrials);
fcpval.trial      = zeros(NumNodes, NumChans, NumTrials);
fcmap.precue      = zeros(NumNodes, NumChans, NumTrials);
fcpval.precue     = zeros(NumNodes, NumChans, NumTrials);
fcmap.cuetarget1  = zeros(NumNodes, NumChans, NumTrials);
fcpval.cuetarget1 = zeros(NumNodes, NumChans, NumTrials);
fcmap.cuetarget2  = zeros(NumNodes, NumChans, NumTrials);
fcpval.cuetarget2 = zeros(NumNodes, NumChans, NumTrials);

for trId = 1:NumTrials
    util_disp_progress(trId, NumTrials, '        ');
    for nId = 1:NumNodes
        ndata_trial      = alphapower_nodes(ToI.trial,      nId, trId);
        ndata_precue     = alphapower_nodes(ToI.precue,     nId, trId);
        ndata_cuetarget1 = alphapower_nodes(ToI.cuetarget1, nId, trId);
        ndata_cuetarget2 = alphapower_nodes(ToI.cuetarget2, nId, trId);
        for chId = 1:NumChans
                cdata_trial      = alphapower(ToI.trial,      chId, trId);
                cdata_precue     = alphapower(ToI.precue,     chId, trId);
                cdata_cuetarget1 = alphapower(ToI.cuetarget1, chId, trId);
                cdata_cuetarget2 = alphapower(ToI.cuetarget2, chId, trId);
                [fcmap.trial(nId, chId, trId),      fcpval.trial(nId, chId, trId)]      = corr(ndata_trial,      cdata_trial);
                [fcmap.precue(nId, chId, trId),     fcpval.precue(nId, chId, trId)]     = corr(ndata_precue,     cdata_precue);
                [fcmap.cuetarget1(nId, chId, trId), fcpval.cuetarget1(nId, chId, trId)] = corr(ndata_cuetarget1, cdata_cuetarget1);
                [fcmap.cuetarget2(nId, chId, trId), fcpval.cuetarget2(nId, chId, trId)] = corr(ndata_cuetarget2, cdata_cuetarget2);
                
        end
    end
end

%% Saving results
targetfile = [saveroot '/' subject '_functional_connectivity_nodes_iafmodality.mat'];
util_bdisp(['[out] - Saving analysis in: ' targetfile]);

connectivity.functional.maps           = fcmap;
connectivity.functional.pvalues        = fcpval;
connectivity.nodes                     = nodes;
connectivity.timing                    = timings;
connectivity.timing.ToI                = ToI;
connectivity.channels                  = channels;
connectivity.frequencies.selected      = selectedfrequencies;
connectivity.frequencies.grid          = FreqGrid;
connectivity.labels.Mk                 = Mk;
connectivity.labels.Rk                 = Rk;
connectivity.labels.Dk                 = Dk;
connectivity.labels.Ck                 = Ck;

save(targetfile, 'connectivity');       



