clc;
clearvars; 
subject     = 'b4';
pattern     = 'va';

features       = 'psd';
spatialfilter  = 'none';
experiment     = 'nirs-eeg';

class.values   = [1 2];
class.names    = {'LeftCue', 'RightCue'};
class.nclasses = length(class.values);

%% Initialization

% Timings
timings.trial.period    = 3;            % seconds
timings.offset.period   = -1;           % seconds

% Frequencies
proc.freq.selected.range{1} = 4:6;          % Hz
proc.freq.selected.label{1} = 'theta';
proc.freq.selected.range{2} = 8:14;         % Hz
proc.freq.selected.label{2} = 'alpha';
proc.freq.selected.range{3} = 16:30;        % Hz
proc.freq.selected.label{3} = 'beta';
proc.freq.selected.range{4} = 32:48;        % Hz
proc.freq.selected.label{4} = 'gamma';

% Channels and RoIs
[~, channels.list]        = proc_get_montage('eeg.acticamp.63.cvsa');
channels.roi.left.labels  = {'P7', 'P5', 'PO7'};         % As in [Thut et al., 2006]
channels.roi.right.labels = {'P8', 'P6', 'PO8'};         % As in [Thut et al., 2006]
% channels.roi.left.labels  = {'P5', 'P3', 'P1', 'PO7', 'PO3', 'O1'};         % Custom
% channels.roi.right.labels = {'P6', 'P4', 'P2', 'PO8', 'PO4', 'O2'};         % Custom
channels.roi.left.id      = proc_get_channel(channels.roi.left.labels,  channels.list);
channels.roi.right.id     = proc_get_channel(channels.roi.right.labels, channels.list);
channels.roi.labels       = {'LeftRoI', 'RightRoI'};

% Frequencies
selectedfrequencies       = 1:60;

% Datafiles
datapath    = ['/mnt/data/Git/Codes/cvsa/analysis/' experiment '/' features '/' spatialfilter '/'];
[Files, NumFiles] = util_getfile(datapath, '.mat', [subject '*' pattern]);

% Extra paths
saveroot    = ['/mnt/data/Git/Codes/cvsa/analysis/' experiment '/'];
[~, savedir]   = util_mkdir(saveroot, 'lateralization/thut/');

analysispath = './analysis/';
%% Analysis - Import data

% Concatanate datafiles
util_bdisp(['[io] - Concatenating ' num2str(NumFiles) ' datafiles']);

[F, analysis] = cvsa_utilities_concatenate_data(Files);

% Extract information
proc.samplerate = analysis.processing.fs;
proc.framesize  = analysis.processing.fsize;

[~, selfreqId] = intersect(analysis.processing.f, selectedfrequencies);
proc.freq.grid      = analysis.processing.f(selfreqId);
F = F(:, selfreqId, :);
proc.nsamples   = size(F, 1);
proc.nfreqs     = size(F, 2);
proc.nchans     = size(F, 3);

for fId = 1:length(proc.freq.selected.range)
    [~, proc.freq.selected.id{fId}] = intersect(proc.freq.grid, proc.freq.selected.range{fId});
end

%% Analysis - Masking data

util_bdisp('[proc] - Creating trial mask vector');
events = analysis.event;
% Convert period [in seconds] to size [in samples]
timings.trial.size  = floor(timings.trial.period*proc.samplerate/proc.framesize);
timings.offset.size = floor(timings.offset.period*proc.samplerate/proc.framesize);
timings.trial.mask  = false(proc.nsamples, 1);
timings.trial.pos   = [];
for cId = 1:class.nclasses
    [cmask, cpos, cdur] = proc_get_event(events, class.values(cId), proc.nsamples, timings.trial.size, timings.offset.size);
    timings.trial.mask    = timings.trial.mask | cmask;
    timings.trial.pos     = sort([timings.trial.pos cpos']);
end

util_bdisp('[proc] - Creating trial based class labels');
index = false(length(events.TYP), 1);
for cId = 1:class.nclasses
    index = events.TYP == class.values(cId) | index;
end
class.labels = events.TYP(index);

%% Run index cretation (just for g6)
if(strcmp(subject, 'g6'))
    RunLimits = [events.POS(events.TYP == 13) events.POS(events.TYP == 14)];

    RunLimits(1, 1) = 1;
    RunLimits(2, end) = size(F, 1);
    analysis.label.Rk = zeros(size(F, 1), 1);
    for rId = 1:size(RunLimits, 1)
        analysis.label.Rk(RunLimits(rId, 1):RunLimits(rId, 2)) = rId;
    end
end
%% Extracting trials
util_bdisp('[proc] - Extracting trials');
S = F;
NumTrials = length(timings.trial.pos);
NumSamples = timings.trial.size + abs(timings.offset.size);
St = zeros(NumSamples, proc.nfreqs, proc.nchans, NumTrials);
labels.Mk = zeros(NumTrials, 1);
labels.Rk = zeros(NumTrials, 1);
labels.Dk = zeros(NumTrials, 1);
for tId = 1:NumTrials
    cstart = timings.trial.pos(tId);
    cstop  = cstart + NumSamples - 1;
    St(:, :, :, tId) = S(cstart:cstop, :, :);
    labels.Mk(tId) = unique(analysis.label.Mk(cstart:cstop));
    labels.Rk(tId) = unique(analysis.label.Rk(cstart:cstop));
    labels.Dk(tId) = unique(analysis.label.Dk(cstart:cstop));
end
Modalities    = unique(labels.Mk);
NumModalities = length(Modalities);
Runs          = unique(labels.Rk);
NumRuns       = length(Runs);

%% Compute Temporal spectral evolution (TSE) and Lateralization Index (LI) per RoIs ([Thut et al., 2006])
util_bdisp('[proc] - Computing ROI and lateralization index');

% Averaging across electrodes in left and right RoI
TSE = zeros(NumSamples, proc.nfreqs, 2, NumTrials);
tse_l = squeeze(mean(St(:, :, channels.roi.left.id, :), 3));
tse_r = squeeze(mean(St(:, :, channels.roi.right.id, :), 3));
TSE(:, :, 1, :) = tse_l;
TSE(:, :, 2, :) = tse_r;

TSE_baseline = repmat(mean(TSE(1:20, :, :, :), 1), [size(TSE, 1) 1 1 1]);
%TSE_norm     = 100*(TSE - TSE_baseline)./TSE_baseline;
TSE_norm     = (log(TSE+1) - log(TSE_baseline+1));

% Computing LI between left and right RoI as in [Thut et al, 2006]
LI = (tse_r - tse_l)./((tse_r + tse_l)./2);

%% Plotting
util_bdisp('[plot] - Plotting results');
fig1 = figure;
fig_set_position(fig1, 'Top');

%PlotCondition = labels.Rk == 1 | labels.Rk == 3;
% PlotCondition = labels.Rk == 2 | labels.Rk == 4;
% PlotCondition = labels.Rk == 3;
PlotCondition = true;

SelectedBand   = {'alpha'};
SelectedFreqId = proc.freq.selected.id{util_cellfind(SelectedBand, proc.freq.selected.label)};
RoiNames = {'Left', 'Right'};
t = timings.offset.period:proc.framesize/proc.samplerate:timings.trial.period - proc.framesize/proc.samplerate;
class.colors = {'b', 'r'};
modalities.tickness = [1 3];
modalities.styles   = {'-', '--'};

% TSE for Left RoI
subplot(1, 3, 1);
hold on;
for mId = 1:NumModalities
    for cId = 1:class.nclasses
        values = squeeze(mean(mean(TSE_norm(:, SelectedFreqId, 1, class.labels == class.values(cId) & labels.Mk == Modalities(mId) & PlotCondition), 2), 4));
        plot(t, values, class.colors{cId}, 'LineWidth', modalities.tickness(mId), 'LineStyle', modalities.styles{mId});
    end
end
hold off;
grid on;
xlim([t(1) t(end)]);
xlabel('Time [s]');
ylabel('[%]');
title('TSE - Left ROI');
legend(class.names, 'location', 'best');
plot_vline(0, 'k', 'cue');

% Lateralization index
subplot(1, 3, 2);
hold on;
for mId = 1:NumModalities
    for cId = 1:class.nclasses
        values = squeeze(mean(mean(LI(:, SelectedFreqId, class.labels == class.values(cId) & labels.Mk == Modalities(mId) & PlotCondition), 2), 3));
        plot(t, values, class.colors{cId}, 'LineWidth', modalities.tickness(mId), 'LineStyle', modalities.styles{mId});
    end
end
hold off;
grid on;
xlim([t(1) t(end)]);
xlabel('Time [s]');
ylabel('[uV]');
title('Lateralization Index');
legend(class.names, 'location', 'best');
plot_vline(0, 'k', 'cue');

% TSE for Right RoI
subplot(1, 3, 3);
hold on;
for mId = 1:NumModalities
    for cId = 1:class.nclasses
        values = squeeze(mean(mean(TSE_norm(:, SelectedFreqId, 2, class.labels == class.values(cId) & labels.Mk == Modalities(mId)  & PlotCondition), 2), 4));
        plot(t, values, class.colors{cId}, 'LineWidth', modalities.tickness(mId), 'LineStyle', modalities.styles{mId});
    end
end
hold off;
grid on;
xlim([t(1) t(end)]);
xlabel('Time [s]');
ylabel('[%]')
title('TSE - Right RoI');
legend(class.names, 'location', 'best');
plot_vline(0, 'k', 'cue');


suptitle(['Subject ' subject]);

