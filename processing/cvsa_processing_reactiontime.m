clc; clearvars;

subject     = 'TC';
pattern     = '.va.';

ChannelLb   = 'ERGO1';
[~, ChannelList] = proc_get_montage('erg.biosemi.16');
ChannelId = proc_get_channel(ChannelLb, ChannelList);

EventTargetShow = 72;
ButtonPeriod    = 1.5;                % seconds
ButtonOffset    = 0;                % seconds
Classes         = [1 2 3];          % Left | Right | Middle
OnlineClasses   = [1 3];            % Left | Middle
NumClasses      = length(Classes);
EventDetection  = [16 32];          % Target Hit | Target Miss

experiment  = 'neglect';
datapath    = ['/mnt/data/Research/cvsa/' experiment '/'];

baseroot      = ['/mnt/data/Git/Codes/cvsa/analysis/' experiment '/'];
[~, saveroot] = util_mkdir(baseroot, 'reactiontime/');

% Get datafiles
[Files, NumFiles] = cvsa_utilities_getdata(datapath, subject, pattern, '.bdf');

for fId = 1:NumFiles
    cfilename = Files{fId};
    info = util_getfile_info(cfilename);
    util_bdisp(['[io] - Loading file ' num2str(fId) '/' num2str(NumFiles)]);
    disp(['       File: ' cfilename]);
   
    % Import file
    [s, h] = io_import_bdf(cfilename, ChannelId);   
    
    util_bdisp('[proc] - Extracting reaction time');
    % Setting data infotmations
    SampleRate    = h.SampleRate;
    events        = h.EVENT;
    NumTotSamples = size(s, 1);
    ButtonSize       = floor(ButtonPeriod*SampleRate);
    ButtonOffsetSize = floor(ButtonOffset*SampleRate);
    
    % Getting trial class index
    index = false(length(events.TYP), 1);
    for cId = 1:NumClasses
        index = events.TYP == Classes(cId) | index;
    end
    Ck = events.TYP(index);
    
    % Getting trial result index
    index = false(length(events.TYP), 1);
    
    for dId = 1:length(EventDetection)
        index = events.TYP == EventDetection(dId) | index;
    end
    Rd = events.TYP(index);
    
    % For offline runs where there are no result events
    if(isempty(Rd))                     
        Rd = EventDetection(1)*ones(length(Ck), 1);
    end
        
    % Getting trials positions
    [~, cpos, cdur] = proc_get_event(events, EventTargetShow, NumTotSamples, ButtonSize, ButtonOffsetSize);
    
    % Extract button press positions within each trial
    NumTrials = length(cpos);
    edges     = zeros(NumTrials, 2);
    press     = zeros(NumTrials, 1);
    
    % Initialize target show index as correct (equal to class index)
    Ts = Ck;
    for trId = 1:NumTrials
        cstart = cpos(trId);
        cstop  = cstart + cdur(trId) - 1;
        
        cvalues = s(cstart:cstop, :);
        craising = find(diff(cvalues > 0) == 1);
        cfalling = find(diff(cvalues > 0) == -1);
        
        % If raising edge is not found, put NaN
        if(isempty(craising))
            craising = nan;
        end
        
        % If falling edge is not found, put NaN
        if(isempty(cfalling))
            cfalling = nan;
        end
        
        % Count the number of button press. If any raising edge is found,
        % put 0.
        press(trId) = length(craising);
        if(isnan(craising))
            press(trId) = 0;
        end
        
        edges(trId, 1) = craising(1) - abs(ButtonOffsetSize);
        edges(trId, 2) = cfalling(1) - abs(ButtonOffsetSize);
        
        % Target show index
        if(Rd(trId) == EventDetection(2)) % Wrong result
            Ts(trId) = setdiff(OnlineClasses, Ck(trId));
        end
    end
    
    % Convert Rd to logical vector (0 Wrong | 1 Correct)
    Rd(Rd == EventDetection(1)) = true;
    Rd(Rd == EventDetection(2)) = false;
    
    %% Saving reaction time analysis
    timing.button.period  = ButtonPeriod;
    timing.offset.period  = ButtonOffset;
    timing.button.size    = ButtonSize;
    timing.offset.size    = ButtonOffsetSize;
    proc.fs               = SampleRate;
    proc.event.targetshow = EventTargetShow;
    proc.channel.id       = ChannelId;
    proc.channel.lb       = ChannelLb;
    button.edges          = edges/SampleRate;
    button.press          = press;
    labels.Ck             = Ck;
    labels.Ts             = Ts;
    labels.Rd             = Rd;
    
    [~, name] = fileparts(cfilename);
    sfilename = [saveroot name '.mat'];
    util_bdisp(['[out] - Saving reaction time analysis in: ' sfilename]);
    save(sfilename, 'timing', 'proc', 'button', 'labels'); 
end




