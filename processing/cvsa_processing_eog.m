clc; clearvars;

subject     = 'TC';
pattern     = '.va.';

channels.labels   = {'EXG1', 'EXG2', 'EXG3'};
[~, channels.list] = proc_get_montage('exg.biosemi.8');
channels.id = proc_get_channel(channels.labels, channels.list);

filter.order = 3;
filter.bands = [1 12];

experiment  = 'neglect';
datapath    = ['/mnt/data/Research/cvsa/' experiment '/'];
saveroot    = '/mnt/data/Git/Codes/cvsa/';
savedir     = ['analysis/' experiment '/eog/'];

% Get datafiles
[Files, NumFiles] = cvsa_utilities_getdata(datapath, subject, pattern, '.bdf');

% Create/Check for savedir
[~, savepath] = util_mkdir(saveroot, savedir);

for fId = 1:NumFiles
    cfilename = Files{fId};
    info = util_getfile_info(cfilename);
    util_bdisp(['[io] - Loading file ' num2str(fId) '/' num2str(NumFiles)]);
    disp(['       File: ' cfilename]);
   
    % Import file
    [s, h] = io_import_bdf(cfilename, channels.id);   
    
    % Filtering data
    util_bdisp(['[proc] - Filtering signals between ' num2str(filter.bands(1)) '-' num2str(filter.bands(2)) 'Hz']);
    sfilt = filt_bp(s, filter.order, filter.bands, h.SampleRate);
    
    % Compute horizontal and vertical component
    util_bdisp('[proc] - Computing horizontal and vertical eog components');
    eog = zeros(size(s, 1), 2);
    eog(:, 1) = sfilt(:, 1) - sfilt(:, 3);
    eog(:, 2) = sfilt(:, 2) - (sfilt(:, 1) + sfilt(:, 3))./2;
    
    
    % Saving analysis
    analysis.event               = h.EVENT;
    analysis.processing.filter   = filter;
    analysis.processing.channels = channels;
    analysis.processing.fs       = h.SampleRate;
    analysis.eog.components      = {'HEOG', 'VEOG'};
    
    [~, name] = fileparts(cfilename);
    sfilename = [savepath name '.mat'];
    util_bdisp(['[out] - Saving eog analysis in: ' sfilename]);
    save(sfilename, 'eog', 'analysis'); 
end