clc; clearvars;

subject     = 'BC';
pattern     = '.vaeyes.';

idchannels  = 1:64;
nchannels   = length(idchannels);
spatialtype = 'laplacian';
montage     = proc_get_montage('eeg.biosemi.64');
mask        = proc_laplacian_mask(montage, 1);
selfreqs    = 4:48;

experiment  = 'neglect';
datapath    = ['/mnt/data/Research/cvsa/' experiment '/'];
saveroot    = '/mnt/data/Git/Codes/cvsa/';
savedir     = ['analysis/' experiment '/psd/' spatialtype '/'];

EventEyeClosed = 50;
EventEyeOpen   = 55;

BorderPeriod   = 5;         % seconds

% Get datafiles
[Files, NumFiles] = cvsa_utilities_getdata(datapath, subject, pattern, '.bdf');

% Create/Check for savedir
[~, savepath] = util_mkdir(saveroot, savedir);

for fId = 1:NumFiles
    cfilename = Files{fId};
    info = util_getfile_info(cfilename);
    util_bdisp(['[io] - Loading file ' num2str(fId) '/' num2str(NumFiles)]);
    disp(['       File: ' cfilename]);
   
    % Import file
    [s, h] = io_import_bdf(cfilename, idchannels);   
   
    NumSamples = size(s, 1);
    SampleRate = h.SampleRate;
    
    % Event eyeclosed/eyeopen
    util_bdisp('[proc] - Extracting events related to eyeclosed and eyeopen');
    events = h.EVENT;
    pos = [];
    dur = [];
    [~, pos.closed] = proc_get_event(events, EventEyeClosed, NumSamples);
    [~, pos.open]   = proc_get_event(events, EventEyeOpen, NumSamples);
    
    % Define analysis period
    util_bdisp('[proc] - Defining analysis periods');
    dur.closed = floor(diff(pos.closed));
    dur.open   = floor(diff(pos.open));
    bordersize = floor(BorderPeriod*SampleRate);
    
    period.closed = [pos.closed(1) + bordersize  pos.closed(1) + dur.closed - bordersize];
    period.open   = [pos.open(1)   + bordersize  pos.open(1)   + dur.open   - bordersize];
    

    % Applying spatial filter
    util_bdisp(['[proc] - Apply spatial filter (' spatialtype '):']);
    switch(spatialtype)
        case 'laplacian'
            ss = proc_laplacian(s, mask);
        case 'car'
            ss = proc_car(s);
        case 'none'
            ss = s;
        otherwise
            error(['[proc] - ' spatialtype ': Unknown filter type']);
    end
    
     % Processing periodogram
    util_bdisp('[proc] - Processing pwelch');
    interval.closed = period.closed(1):period.closed(2);
    interval.open   = period.open(1):period.open(2);
    [psdclosed, fclosed] = pwelch(ss(interval.closed, :), hamming(SampleRate), [], selfreqs, SampleRate);
    [psdopen,   fopen]   = pwelch(ss(interval.open, :), hamming(SampleRate), [], selfreqs, SampleRate);
    
    if(isequal(fclosed, fopen) == false)
        warning('chk:f', '[warning] - Different values in frequency vectors for eyeclosed and eyeopen periods');
    end
    
    analysis.spatial.type       = spatialtype;
    analysis.event.values       = [EventEyeClosed EventEyeOpen];
    analysis.event.labels       = {'eyeclosed' 'eyeopen'};
    analysis.timings.pos        = pos;
    analysis.timings.dur        = dur;
    analysis.timings.period     = period;
    analysis.processing.type    = 'pwelch';
    analysis.processing.fs      = SampleRate;
    analysis.processing.wtype   = 'hamming';
    analysis.processing.wsize   = SampleRate;
    analysis.processing.nfft    = selfreqs;
    analysis.processing.f       = fclosed;   
    
    
    
    [~, name] = fileparts(cfilename);
    sfilename = [savepath name '.mat'];
    util_bdisp(['[out] - Saving psd (eyeclosed/open) in: ' sfilename]);
    save(sfilename, 'psdclosed', 'psdopen', 'analysis'); 
end