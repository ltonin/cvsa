% clc; clearvars;
% 
% subject     = 'TC';
pattern     = '.va.';

idchannels  = 1:64;
nchannels   = length(idchannels);
spatialtype = 'car';
montage     = proc_get_montage('eeg.biosemi.64');
mask        = proc_laplacian_mask(montage, 1);
framesize   = 0.0625;
buffersize  = 1.0;
selfreqs    = 1:60;

experiment  = 'neglect';
datapath    = ['/mnt/data/Research/cvsa/' experiment '/'];
saveroot    = '/mnt/data/Git/Codes/cvsa/';
savedir     = ['analysis/' experiment '/psd/' spatialtype '/'];

% Get datafiles
[Files, NumFiles] = cvsa_utilities_getdata(datapath, subject, pattern, '.bdf');

% Create/Check for savedir
[~, savepath] = util_mkdir(saveroot, savedir);

for fId = 1:NumFiles
    cfilename = Files{fId};
    info = util_getfile_info(cfilename);
    util_bdisp(['[io] - Loading file ' num2str(fId) '/' num2str(NumFiles)]);
    disp(['       File: ' cfilename]);
   
    % Import file
    [s, h] = io_import_bdf(cfilename, idchannels);   
   
    % Applying spatial filter
    util_bdisp(['[proc] - Apply spatial filter (' spatialtype '):']);
    switch(spatialtype)
        case 'laplacian'
            ss = proc_laplacian(s, mask);
        case 'car'
            ss = proc_car(s);
        case 'none'
            ss = s;
        otherwise
            error(['[proc] - ' spatialtype ': Unknown filter type']);
    end
    
    % Processing periodogram
    util_bdisp('[proc] - Processing periodogram');
    [psd, settings] = proc_simonline_periodogram(ss, h.SampleRate, framesize, buffersize, 'hamming');
    
    % Extract selected frequencies
    util_bdisp('[proc] - Extracting selected frequencies');
    [f, idfreqs] = intersect(settings.f, selfreqs);
    psd = psd(:, idfreqs, :);
    settings.f = f;
    
    analysis.spatial.type = spatialtype;
    analysis.processing   = settings;
    analysis.event.TYP    = h.EVENT.TYP;
    analysis.event.POS    = floor(h.EVENT.POS/settings.fsize);
    analysis.event.DUR    = floor(h.EVENT.DUR/settings.fsize);

    [~, name] = fileparts(cfilename);
    sfilename = [savepath name '.mat'];
    util_bdisp(['[out] - Saving psd in: ' sfilename]);
    save(sfilename, 'psd', 'analysis'); 
    
end