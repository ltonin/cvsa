clc; clearvars;

subject     = 'BC';
pattern     = '.va.';


Classes         = [1 3];            % Left | Middle
NumClasses      = length(Classes);
EventDetection  = [16 32];          % Target Hit | Target Miss

%% Initialization

experiment  = 'neglect';
datapath    = ['/mnt/data/Research/cvsa/' experiment '/'];
baseroot      = ['/mnt/data/Git/Codes/cvsa/analysis/' experiment '/'];
[Files, NumFiles] = cvsa_utilities_getdata(datapath, subject, pattern, '.bdf');
[~, saveroot]     = util_mkdir(baseroot, 'performance/');

cdaylbl  = '';
cday     = 0;

Rd = [];
Mk = [];
Ck = [];
Rk = [];
Dk = [];
for fId = 1:NumFiles
    cfilename = Files{fId};
    info = util_getfile_info(cfilename);
    util_bdisp(['[io] - Loading file ' num2str(fId) '/' num2str(NumFiles)]);
    disp(['       File: ' cfilename]);
   
    % Get modality from filename
    switch lower(info.modality)
        case 'offline'
            cmod = 0;
        case 'online'
            cmod = 1;
        otherwise
            error('chk:mod', ['[' mfilename '] Unknown modality']);
    end
    
    % Get day from filename
    if strcmpi(info.date, cdaylbl) == false
        cday = cday + 1;
        cdaylbl = info.date;
    end
    
    % Import file events
    evtch = io_read_bdf(cfilename, {'Status'});
    events = io_event_bdf(evtch);   
    
    % Getting trial class index
    index = false(length(events.TYP), 1);
    for cId = 1:NumClasses
        index = events.TYP == Classes(cId) | index;
    end
    cCk = events.TYP(index);
    
    % Extract event results
    index = false(length(events.TYP), 1);
    for dId = 1:length(EventDetection)
        index = events.TYP == EventDetection(dId) | index;
    end
    cRd = events.TYP(index);
    
    % For offline runs where there are no result events
    if(isempty(cRd))                     
        cRd = EventDetection(1)*ones(length(cCk), 1);
    end
        
    Rd = cat(1, Rd, cRd);
    Mk = cat(1, Mk, cmod*ones(length(cCk), 1));
    Ck = cat(1, Ck, cCk);
    Rk = cat(1, Rk, fId*ones(length(cCk), 1));
    Dk = cat(1, Dk, cday*ones(length(cCk), 1));
end

performance.Rd        = Rd;
performance.labels.Mk = Mk;
performance.labels.Ck = Ck;
performance.labels.Rk = Rk;
performance.labels.Dk = Dk;

%% Saving results
targetfile = [saveroot '/' subject '_performance.mat'];
util_bdisp(['[out] - Saving analysis in: ' targetfile]);
save(targetfile, 'performance');