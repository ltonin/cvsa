clc; clearvars;
% 
subject     = 'S12';

inchannels  = 1:34;                                 % Total number of channels
exchannels  = [17 34];                              % Excluded channels (trigger channels)
idchannels  = setdiff(inchannels, exchannels);      % Id eeg channels
nchannels   = length(idchannels);
spatialtype = 'laplacian';                          % laplacian or car

[montage, labels] = proc_get_montage('eeg.inria.32.cvsa');
mask              = proc_laplacian_mask(montage, 2);
mlength     = 1;
wlength     = 0.5;
pshift      = 0.25;                  
wshift      = 0.0625;
selfreqs    = 1:60;

artifact.filter.order = 4;
artifact.filter.band  = [2 30]; % [Hz]
artifact.nstd         = 5;
artifact.maxvalue     = 80;     % [uV]

experiment  = 'sport';
%datapath    = ['/mnt/data/Research/cvsa/' experiment '/'];
%saveroot    = '/mnt/data/Git/Codes/cvsa/';
datapath    = ['C:/post-doc_Camille/data_covertAttention/'];
saveroot    = 'C:/post-doc_Camille/git/Codes/cvsa/';
savedir     = ['analysis/' experiment '/psd/' spatialtype '/'];

% Get datafiles
[Files, NumFiles] = util_getfile([datapath subject '/EEGSignals/'], '.gdf', subject);

% Create/Check for savedir
[~, savepath] = util_mkdir(saveroot, savedir);
        

for fId = 1:NumFiles
    cfilename = Files{fId};
    util_bdisp(['[io] - Loading file ' num2str(fId) '/' num2str(NumFiles)]);
    disp(['       File: ' cfilename]);
   
    % Import file
    [s, h] = sload(cfilename, idchannels);   
   
    
    % Reversing channels for subject S7c
    if(strcmpi(subject, 'S7c'))
        if (fId == 5 || fId == 6 || fId == 7)
            util_bdisp(['[proc] - Reversing channels for subject ' subject ' run ' num2str(fId)]);
            tmp_s = s;
            tmp_s(:, [22 23 24 25]) = s(:, [26 27 28 29]);
            tmp_s(:, [26 27 28 29]) = s(:, [22 23 24 25]);
            s = tmp_s;
        end
    end
   
    % Artifacts detection
    util_bdisp('[proc] - Detecting artifacts: ');
    [Ak, P] = cvsa_sport_artifacts_detection(s, h.SampleRate, artifact.filter.order, artifact.filter.band, artifact.nstd, artifact.maxvalue);
    disp(['     Percentage of samples with artifacts: ' num2str(P) '%']);
    
    % Interpolate channels
    nbchans = [];
    ch2int  = [];
    switch(subject)
        case 'S2c'
            if fId == 2
                ch2int  = 1;
                util_bdisp(['[proc] - Interpolate channel: ' char(proc_get_channel(ch2int, labels))]);
                nbchans = proc_get_channel({'CP5', 'P3', 'P7', 'PO7'}, labels);
            end
        case 'S6c'
            if fId < 5
                ch2int = 11;
                util_bdisp(['[proc] - Interpolate channel: ' char(proc_get_channel(ch2int, labels))]);
                nbchans = proc_get_channel({'Pz', 'PO3', 'PO4', 'Oz'}, labels);
            end
        case 'S11'
            if fId == 5
                ch2int = 15;
                util_bdisp(['[proc] - Interpolate channel: ' char(proc_get_channel(ch2int, labels))]);
                nbchans = proc_get_channel({'POz', 'O1', 'O2'}, labels);
            end
    end
    si = cvsa_sport_channel_interpolation(s, ch2int, nbchans);
    artifact.interpolation.channelId = ch2int;
    artifact.interpolation.neightbourId = nbchans;
    
    % Applying spatial filter
    util_bdisp(['[proc] - Apply spatial filter (' spatialtype '):']);
    switch(spatialtype)
        case 'laplacian'
            ss = proc_laplacian(si, mask);
        case 'car'
            ss = proc_car(si);
        case 'none'
            ss = si;
        otherwise
            error(['[proc] - ' spatialtype ': Unknown filter type']);
    end
    
    % Processing periodogram
    util_bdisp('[proc] - Processing spectrogram');
    [psd, f] = proc_spectrogram(ss, wlength, wshift, pshift, h.SampleRate, mlength);
    
    % Extract selected frequencies
    util_bdisp('[proc] - Extracting selected frequencies');
    [f, idfreqs] = intersect(f, selfreqs);
    psd = psd(:, idfreqs, :);
    h.EVENT.DUR = zeros(length(h.EVENT.POS),1);
    analysis.spatial.type = spatialtype;
    analysis.psd.f        = f;
    analysis.psd.wlength  = wlength;
    analysis.psd.wshift   = wshift;
    analysis.psd.pshift   = pshift;
    analysis.psd.mlength  = mlength;
    analysis.psd.fs       = h.SampleRate;
    analysis.event.TYP    = h.EVENT.TYP;
    analysis.event.POS    = floor(h.EVENT.POS/(wshift*h.SampleRate));
    analysis.event.DUR    = floor(h.EVENT.DUR/(wshift*h.SampleRate));
    analysis.artifact     = artifact;
    analysis.event.ART    = floor(find(Ak)/(wshift*h.SampleRate));
    

    [~, name] = fileparts(cfilename);
    sfilename = [savepath name '.mat'];
    util_bdisp(['[out] - Saving psd in: ' sfilename]);
    save(sfilename, 'psd', 'analysis'); 
    
end